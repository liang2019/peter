<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
  
       // $this->call(ActividadSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(TipoPesoSeeder::class);

     //   $this->call(GeolocalizacionEstadoSeeder::class);
      //  $this->call(IncidenciaSeeder::class);


    }
    public function truncateTables(array $tables)
    {
      //DB::statement('SET FOREING_KEY_CHECKS=0;');
      foreach ($tables as $table) {
        DB::table($table)->truncate();
      }
      //DB::statement('SET FOREING_KEY_CHECKS=1;');
    }
}
