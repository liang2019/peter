<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuario')->truncate();
        DB::table('usuario')->insert([
            'empleado_id' => 1,
            'perfil_id' => 1,
            'usuario' => 'lesly',
            'password' => Hash::make('123'),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('usuario')->insert([
            'empleado_id' => 2,
            'perfil_id' => 1,
            'usuario' => 'geral',
            'password' => Hash::make('123'),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('usuario')->insert([
            'empleado_id' => 3,
            'perfil_id' => 1,
            'usuario' => 'jose',
            'password' => Hash::make('123'),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('usuario')->insert([
            'empleado_id' => 4,
            'perfil_id' => 1,
            'usuario' => 'norma',
            'password' => Hash::make('123'),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('usuario')->insert([
            'empleado_id' => 5,
            'perfil_id' => 1,
            'usuario' => 'capacitacion01',
            'password' => Hash::make('123'),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('usuario')->insert([
            'empleado_id' => 6,
            'perfil_id' => 1,
            'usuario' => 'capacitacion02',
            'password' => Hash::make('123'),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('usuario')->insert([
            'empleado_id' => 7,
            'perfil_id' => 1,
            'usuario' => 'capacitacion03',
            'password' => Hash::make('123'),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('usuario')->insert([
            'empleado_id' => 8,
            'perfil_id' => 1,
            'usuario' => 'capacitacion04',
            'password' => Hash::make('123'),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('usuario')->insert([
            'empleado_id' => 9,
            'perfil_id' => 1,
            'usuario' => 'capacitacion05',
            'password' => Hash::make('123'),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
    }
}
