<?php

use Illuminate\Database\Seeder;
use App\Modules\Dashboard\Models\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->truncate();
        DB::table('menu')->insert([
            'nombre' => 'INICIO',
            'codigo' => Str::random(4),
            'padre_id' => 0,
            'url' => '*',
            'icon' => 'home',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Pantalla Principal',
            'codigo' => Str::random(4),
            'padre_id' => 1,
            'url' => 'dashboard',
            'icon' => 'home',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'TRACKING',
            'codigo' => Str::random(4),
            'padre_id' => 0,
            'url' => 'tracking',
            'icon' => '360',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),

        ]);

        DB::table('menu')->insert([
            'nombre' => 'REQUERIMIENTO',
            'codigo' => Str::random(4),
            'padre_id' => 0,
            'url' => '*',
            'icon' => 'create_new_folder',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),

        ]);
        DB::table('menu')->insert([
            'nombre' => 'Registrar',
            'codigo' => Str::random(4),
            'padre_id' => 4,
            'url' => 'requerimiento/proyectos',
            'icon' => 'create',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Documentaria',
            'codigo' => Str::random(4),
            'padre_id' => 4,
            'url' => 'requerimiento/documentaria',
            'icon' => 'attach_file',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'PRESUPUESTO',
            'codigo' => Str::random(4),
            'padre_id' =>0,
            'url' => '*',
            'icon' => 'list_alt',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),

        ]);
        DB::table('menu')->insert([
            'nombre' => 'Registrar',
            'codigo' => Str::random(4),
            'padre_id' => 7,
            'url' => 'presupuesto/proyectos',
            'icon' => 'create',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Lista de Materiales',
            'codigo' => Str::random(4),
            'padre_id' => 7,
            'url' => 'presupuesto/materiales',
            'icon' => 'list_alt',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Precios',
            'codigo' => Str::random(4),
            'padre_id' => 7,
            'url' => 'presupuesto/solicitud',
            'icon' => 'list_alt',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Presupuesto',
            'codigo' => Str::random(4),
            'padre_id' => 7,
            'url' => 'presupuesto',
            'icon' => 'list_alt',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'GG y Utilidad',
            'codigo' => Str::random(4),
            'padre_id' => 7,
            'url' => 'presupuesto/utilidad',
            'icon' => 'list_alt',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Documentaria',
            'codigo' => Str::random(4),
            'padre_id' => 7,
            'url' => 'presupuesto/documentaria',
            'icon' => 'attach_file',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'COTIZACIÓN',
            'codigo' => Str::random(4),
            'padre_id' => 0,
            'url' => '*',
            'estado' => 0,
            'icon' => 'list_alt',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),

        ]);
        DB::table('menu')->insert([
            'nombre' => 'Emisión',
            'codigo' => Str::random(4),
            'padre_id' => 14,
            'url' => 'cotizacion/proyectos',
            'icon' => 'list_alt',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Documentaria',
            'codigo' => Str::random(4),
            'padre_id' => 14,
            'url' => 'cotizacion/documentaria',
            'icon' => 'attach_file',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'PEDIDO',
            'codigo' => Str::random(4),
            'padre_id' => 0,
            'url' => '*',
            'estado' => 0,
            'icon' => 'list_alt',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),

        ]);
        DB::table('menu')->insert([
            'nombre' => 'Documentaria',
            'codigo' => Str::random(4),
            'padre_id' => 17,
            'url' => 'pedido',
            'icon' => 'attach_file',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'DISEÑO',
            'codigo' => Str::random(4),
            'padre_id' => 0,
            'url' => '*',
            'icon' => 'list_alt',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),

        ]);
        DB::table('menu')->insert([
            'nombre' => 'Registrar',
            'codigo' => Str::random(4),
            'padre_id' => 19,
            'url' => 'diseno/proyectos',
            'icon' => 'list_alt',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Documentaria',
            'codigo' => Str::random(4),
            'padre_id' => 19,
            'url' => 'diseno/documentaria',
            'icon' => 'attach_file',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'PRODUCCIÓN',
            'codigo' => Str::random(4),
            'padre_id' => 0,
            'url' => '*',
            'estado' => 0,
            'icon' => 'business',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),

        ]);
        DB::table('menu')->insert([
            'nombre' => 'Cronograma',
            'codigo' => Str::random(4),
            'padre_id' => 22,
            'url' => 'produccion/cronograma',
            'icon' => 'calendar_today',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Pesos',
            'codigo' => Str::random(4),
            'padre_id' => 22,
            'url' => 'produccion/pesos',
            'icon' => 'nature',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
       
        DB::table('menu')->insert([
            'nombre' => 'Control Entrega Diseño',
            'codigo' => Str::random(4),
            'padre_id' => 22,
            'url' => 'produccion/entregas/solid',
            'icon' => 'toc',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Solicitudes Adicionales',
            'codigo' => Str::random(4),
            'padre_id' => 22,
            'url' => 'produccion/solicitudes',
            'icon' => 'near_me',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Control de Entrega Solicitud Adicional',
            'codigo' => Str::random(4),
            'padre_id' => 22,
            'url' => 'produccion/entregas/solicitud',
            'icon' => 'toc',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Material Restante',
            'codigo' => Str::random(4),
            'padre_id' => 22,
            'url' => 'produccion/restante/solid',
            'icon' => 'toc',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
      
        DB::table('menu')->insert([
            'nombre' => 'Tareo',
            'codigo' => Str::random(4),
            'padre_id' => 22,
            'url' => 'produccion/tareo',
            'icon' => 'library_books',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Documentaria',
            'codigo' => Str::random(4),
            'padre_id' => 22,
            'url' => 'produccion/documentaria',
            'icon' => 'attach_file',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'MONTAJE',
            'codigo' => Str::random(4),
            'padre_id' => 0,
            'estado' => 0,
            'url' => '*',
            'icon' => 'directions',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),

        ]);
        DB::table('menu')->insert([
            'nombre' => 'Cronograma',
            'codigo' => Str::random(4),
            'padre_id' => 31,
            'url' => 'montaje/cronograma',
            'icon' => 'calendar_today',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Pesos',
            'codigo' => Str::random(4),
            'padre_id' => 31,
            'url' => 'montaje/pesos',
            'icon' => 'nature',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Kardex',
            'codigo' => Str::random(4),
            'padre_id' => 31,
            'url' => 'montaje/kardex',
            'icon' => 'view_module',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Avance',
            'codigo' => Str::random(4),
            'padre_id' => 31,
            'url' => 'montaje/avance',
            'icon' => 'rotate_right',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Tareo',
            'codigo' => Str::random(4),
            'padre_id' => 31,
            'url' => 'montaje/tareo',
            'icon' => 'library_books',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Documentaria',
            'codigo' => Str::random(4),
            'padre_id' => 31,
            'url' => 'montaje/documentaria',
            'icon' => 'attach_file',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('menu')->insert([
            'nombre' => 'RRHH',
            'codigo' => Str::random(4),
            'padre_id' => 0,
            'url' => '*',
            'icon' => 'assessment',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
       
        DB::table('menu')->insert([
            'nombre' => 'Ficha de Actividades',
            'codigo' => Str::random(4),
            'padre_id' => 38,
            'url' => 'rrhh/actividades/empleado',
            'icon' => 'assessment',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Registro Actividades Grupales',
            'codigo' => Str::random(4),
            'padre_id' => 38,
            'url' => 'rrhh/actividades',
            'icon' => 'assessment',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'MANTENIMIENTO',
            'codigo' => Str::random(4),
            'padre_id' => 0,
            'url' => '*',
            'icon' => 'settings_applications',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Trabajadores',
            'codigo' => Str::random(4),
            'padre_id' => 41,
            'url' => 'mantenimiento/trabajadores',
            'icon' => 'supervised_user_circle',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Factores',
            'codigo' => Str::random(4),
            'padre_id' => 41,
            'url' => 'mantenimiento/factores',
            'icon' => 'featured_play_list',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
       
        DB::table('menu')->insert([
            'nombre' => 'Procesos',
            'codigo' => Str::random(4),
            'padre_id' => 41,
            'url' => 'mantenimiento/procesos',
            'icon' => 'build',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Tareas',
            'codigo' => Str::random(4),
            'padre_id' => 41,
            'url' => 'mantenimiento/tareas',
            'icon' => 'assignment',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Tipo Adjunto',
            'codigo' => Str::random(4),
            'padre_id' => 41,
            'url' => 'mantenimiento/adjuntos',
            'icon' => 'attachment',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Usuarios',
            'codigo' => Str::random(4),
            'padre_id' => 41,
            'url' => 'mantenimiento/usuarios',
            'icon' => 'supervisor_account',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Perfiles',
            'codigo' => Str::random(4),
            'padre_id' => 41,
            'url' => 'mantenimiento/perfiles',
            'icon' => 'verified_user',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Actividades',
            'codigo' => Str::random(4),
            'padre_id' => 41,
            'url' => 'mantenimiento/actividades',
            'icon' => 'bookmarks',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Correo',
            'codigo' => Str::random(4),
            'padre_id' => 41,
            'url' => 'mantenimiento/correo',
            'icon' => 'mail',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'IMPORTACIÓN',
            'codigo' => Str::random(4),
            'padre_id' => 0,
            'url' => '*',
            'icon' => 'cloud_upload',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Clientes',
            'codigo' => Str::random(4),
            'padre_id' => 51,
            'url' => 'importaciones/clientes',
            'icon' => 'cloud_upload',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Materiales',
            'codigo' => Str::random(4),
            'padre_id' =>  51,
            'url' => 'importaciones/materiales',
            'icon' => 'cloud_upload',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
       
        DB::table('menu')->insert([
            'nombre' => 'REPORTES',
            'codigo' => Str::random(4),
            'padre_id' => 0,
            'url' => '*',
            'icon' => 'bar_chart',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Reporte General',
            'codigo' => Str::random(4),
            'padre_id' => 54,
            'url' => 'reportes/general/proyectos',
            'icon' => 'bar_chart',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
           
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Contabilidad',
            'codigo' => Str::random(4),
            'padre_id' => 54,
            'url' => 'contabilidad/importar',
            'icon' => 'bar_chart',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
            'estado' => 1,
        ]);
        DB::table('menu')->insert([
            'nombre' => 'Materiales',
            'codigo' => Str::random(4),
            'padre_id' => 54,
            'url' => 'reportes/materiales',
            'icon' => 'bar_chart',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('menu')->insert([
            'nombre' => 'DOCUMENTOS',
            'codigo' => Str::random(4),
            'padre_id' => 0,
            'url' => 'documentos',
            'icon' => 'attachment',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),

        ]);
        DB::table('menu')->insert([
            'nombre' => 'CRONOGRAMA',
            'codigo' => Str::random(4),
            'padre_id' => 0,
            'url' => 'cronograma',
            'icon' => 'calendar_today',
            'created_at' => now()->format('Y-m-d H:i:s'),
            'updated_at' => now()->format('Y-m-d H:i:s'),

        ]);
    }
}
