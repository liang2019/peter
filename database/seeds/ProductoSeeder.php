<?php

use Illuminate\Database\Seeder;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('perfil')->truncate();
        DB::table('producto')->insert([
            'nombre' => 'Serv metamecanicos',
            'codigo' => 'metamecanicos',
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('producto')->insert([
            'nombre' => 'Estructuras',
            'codigo' => 'estructuras',
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('producto')->insert([
            'nombre' => 'Material de acero habilitado',
            'codigo' => 'acero',
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('producto')->insert([
            'nombre' => 'Proyectos',
            'codigo' => 'proyectos',
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('producto')->insert([
            'nombre' => 'Serv de ingeniería',
            'codigo' => 'ingenieria',
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
    }
}
