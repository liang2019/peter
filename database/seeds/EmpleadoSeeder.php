<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;

class EmpleadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('empleado')->truncate();
        DB::table('empleado')->insert([
       
            'codigo' => '87654321',
            'nombres' => 'Lesly',
            'apellidos' => 'Villaobos Tapia',
            'fecha_nacimiento' => now()->format('Y-m-d'),
   
            'cargo' => 'Analista',
         
            'sueldo' => Crypt::encryptString( 5005.35 ),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('empleado')->insert([
       
            'codigo' => '12345678',
            'nombres' => 'Geral Mariano',
            'apellidos' => 'Poma Santos',
            'fecha_nacimiento' => now()->format('Y-m-d'),
           
            'cargo' => 'Programador',
           
            'sueldo' => Crypt::encryptString( 5005.35 ),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('empleado')->insert([
       
            'codigo' => '07071234',
            'nombres' => 'Jose',
            'apellidos' => 'Vilalobos',
            'fecha_nacimiento' => now()->format('Y-m-d'),
           
            'cargo' => 'Analista',
           
            'sueldo' => Crypt::encryptString( 5005.35 ),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('empleado')->insert([
            'codigo' => '07071235',
            'nombres' => 'Norma',
            'apellidos' => 'Vidal',
            'fecha_nacimiento' => now()->format('Y-m-d'),
            'cargo' => 'Analista',
            'sueldo' => Crypt::encryptString( 5005.35 ),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('empleado')->insert([
            'codigo' => '07071236',
            'nombres' => 'Capacitación 01',
            'apellidos' => 'CAP. 01',
            'fecha_nacimiento' => now()->format('Y-m-d'),
            'cargo' => 'Analista',
            'sueldo' => Crypt::encryptString( 5005.35 ),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('empleado')->insert([
            'codigo' => '07071237',
            'nombres' => 'Capacitación 02',
            'apellidos' => 'CAP. 02',
            'fecha_nacimiento' => now()->format('Y-m-d'),
            'cargo' => 'Analista',
            'sueldo' => Crypt::encryptString( 5005.35 ),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('empleado')->insert([
            'codigo' => '07071238',
            'nombres' => 'Capacitación 03',
            'apellidos' => 'CAP. 03',
            'fecha_nacimiento' => now()->format('Y-m-d'),
            'cargo' => 'Analista',
            'sueldo' => Crypt::encryptString( 5005.35 ),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('empleado')->insert([
            'codigo' => '07071239',
            'nombres' => 'Capacitación 04',
            'apellidos' => 'CAP. 04',
            'fecha_nacimiento' => now()->format('Y-m-d'),
            'cargo' => 'Analista',
            'sueldo' => Crypt::encryptString( 5005.35 ),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('empleado')->insert([
            'codigo' => '07071240',
            'nombres' => 'Capacitación 05',
            'apellidos' => 'CAP. 05',
            'fecha_nacimiento' => now()->format('Y-m-d'),
            'cargo' => 'Analista',
            'sueldo' => Crypt::encryptString( 5005.35 ),
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
    }
}
