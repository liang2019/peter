<?php

use Illuminate\Database\Seeder;

class TipoPesoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //  DB::table('tipo_peso')->truncate();
        DB::table('tipo_peso')->insert([
            'nombre' => 'Pieza',
            'codigo' => 'pieza',
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('tipo_peso')->insert([
            'nombre' => 'Sub Ensamble',
            'codigo' => 'subensamble',
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('tipo_peso')->insert([
            'nombre' => 'Ensamble',
            'codigo' => 'ensamble',
            'usuario_creacion' => 1,
            'usuario_edicion' => 1,
            'fecha_creacion' => now()->format('Y-m-d H:i:s'),
            'fecha_modificacion' => now()->format('Y-m-d H:i:s'),
        ]);
    }
}
