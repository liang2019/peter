<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailTracking extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $obj;
    public function __construct($obj)
    {
        //
        $this->obj = $obj;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('sistemaemersac@gmail.com', config('app.name'))
            ->view('layouts.mails.tracking')
            ->with(
                [
                      'testVarOne' => '1',
                      'testVarTwo' => '2',
                ]);
           // ->subject($subject);
            //->greeting($greeting)
            //->salutation('Saludos cordiales.')
            //->line('Lamentamos que hayas olvidado tu contraseña')
            //->action('Probar contraseña', url('/'))
            //->line('Gracias por usar nuestro sistema!');
    }
}
