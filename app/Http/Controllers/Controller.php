<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    /*public function __construct()
    {
        $this->middleware(function ($request, $next) {
          /*if (Auth::check()) {
            DB::purge('bd_ejecucion');
            \Config::set('database.connections.bd_ejecucion.database',Auth::user()->empresa->codigo);
            DB::reconnect('bd_ejecucion');
          }
            return $next($request);
        });
    }*/
}
