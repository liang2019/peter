<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Modules\Mantenimiento\Models\Usuario;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $input = $request->all();
        $usuario = Usuario::where('usuario',$input["usuario"])->first();
        $successlogin = false;
        if (Hash::check($input["contrasena"],$usuario["password"])) {
            $successlogin =true;
        }
        return json_encode(array("success"=>$successlogin));
    }
    public function welcome()
    {
        if (Auth::check()) {
            return redirect('/dashboard');
        }
        return view('welcome');
    }
    public function loginapi(Request $request)
    {
        $input = $request->all();
        $usuario = Usuario::select('usuario.*','empleado.nombres','empleado.apellidos')
        ->leftJoin("empleado","empleado.id","=","usuario.empleado_id")->where('usuario.usuario',$input["usuario"])->first();
        $successlogin = false;
        if (Hash::check($input["contrasena"],$usuario["password"])) {
            $successlogin =true;
        }
        return response()->json($usuario,($successlogin?200:500));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
