<?php

use App\Modules\Mantenimiento\Services\MantenimientoCorreoService;
use App\Mail\EmailTracking;
use Illuminate\Support\Facades\Mail;
function getDashoboardAdmin($page = 'index')
{

    return 'dashboard::admin/home';
}
function getMenu()
{
    $theme = new App\Modules\Dashboard\Http\Controllers\HomeController();
    $current_theme = $theme->modulos();
    return $current_theme;
}
function correoTracking($arr=array()){
    $response = false;
    $obj = new \stdClass();
    $obj->codigo_proyecto = $arr["codigo_proyecto"];
    $obj->nombre_proyecto = $arr["nombre_proyecto"];
    $obj->cliente = $arr["cliente"];
    $obj->mensaje = $arr["mensaje"];
    $obj->usuario_responsable = 'usuario_responsable';
    $emails = new MantenimientoCorreoService();
    $emails = $emails->cargarAll();
    foreach ($emails as $key => $value) {
        Mail::to($value["correos"])->send(new EmailTracking($obj));
    }
    $response = true;

    return $response;
}