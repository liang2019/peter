<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFactor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('alarti_id')->unsigned();
            $table->foreign('alarti_id')->references('id')->on('alarti')->onDelete('cascade');
     
            $table->double('e', 15, 4)->nullable();
            $table->double('ld', 15, 4)->nullable();
            $table->integer('caras')->nullable();
            $table->string('unidad', 191)->nullable();
            $table->string('pieza', 191)->nullable();
            $table->double('perimetro', 15, 4)->nullable();
            $table->double('seccion', 15, 4)->nullable();
            $table->double('unidad_pieza', 15, 4)->nullable();
            $table->double('kilogramo_unidad', 15, 4)->nullable();
            $table->double('kilogramo_pieza', 15, 4)->nullable();
            $table->double('metro_unidad', 15, 4)->nullable();
            $table->double('metro_pieza', 15, 4)->nullable();
            /*
            $table->double('costo_unidad', 15, 4)->nullable();
            $table->double('costo_pieza', 15, 4)->nullable();
           */
            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion');
            $table->unsignedInteger('usuario_edicion');
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->timestamp('fecha_modificacion')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factor');
    }
}
