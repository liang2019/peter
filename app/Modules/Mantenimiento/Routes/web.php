<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['middleware' => 'auth'],function () {
    Route::group(['prefix' => 'mantenimiento'], function () {

        Route::get('/trabajadores', ['as' => 'empleados', 'uses' => 'MantenimientoEmpleadosController@index']);
        Route::get('/factores', ['as' => 'factores', 'uses' => 'MantenimientoFactoresController@index']);
        Route::get('/actividades', ['as' => 'actividades', 'uses' => 'MantenimientoActividadesController@index']);
        Route::get('/unidades', ['as' => 'unidades', 'uses' => 'MantenimientoUnidadesController@index']);
        Route::get('/procesos', ['as' => 'procesos', 'uses' => 'MantenimientoProcesosController@index']);
        Route::get('/tareas', ['as' => 'tareas', 'uses' => 'MantenimientoTareasController@index']);
        Route::get('/adjuntos', ['as' => 'adjuntos', 'uses' => 'MantenimientoAdjuntosController@index']);
        Route::get('/usuarios', ['as' => 'usuarios', 'uses' => 'MantenimientoUsuariosController@index']);
        Route::get('/perfiles', ['as' => 'niveles', 'uses' => 'MantenimientoPerfilesController@index']);
        Route::get('/perfil', ['as' => 'perfil', 'uses' => 'MantenimientoUsuariosController@index']);
        Route::get('/correo', ['as' => 'correo', 'uses' => 'MantenimientoCorreosController@index']);


        Route::get('/empleados/cargar', ['as' => 'empleados', 'uses' => 'MantenimientoEmpleadosController@cargarAll']);
        Route::post('/empleados/buscar/', ['as' => 'empleados.buscar', 'uses' => 'MantenimientoEmpleadosController@buscar']);
        Route::patch('/empleados/editar/', ['as' => 'empleados.editar', 'uses' => 'MantenimientoEmpleadosController@editar']);
        Route::post('/empleados/eliminar/', ['as' => 'empleados.eliminar', 'uses' => 'MantenimientoEmpleadosController@eliminar']);
        Route::post('/empleados/guardar/', ['as' => 'empleados.guardar', 'uses' => 'MantenimientoEmpleadosController@guardar']);
        Route::post('/empleados/dni/', ['as' => 'empleados.verificarDni', 'uses' => 'MantenimientoEmpleadosController@verificarDni']);


        Route::get('/usuarios/cargar', ['as' => 'usuarios', 'uses' => 'MantenimientoUsuariosController@cargarAll']);
        Route::post('/usuarios/buscar/', ['as' => 'usuarios.buscar', 'uses' => 'MantenimientoUsuariosController@buscar']);
        Route::post('/usuarios/verificar/', ['as' => 'usuarios.verificar', 'uses' => 'MantenimientoUsuariosController@verificar']);
        Route::patch('/usuarios/editar/', ['as' => 'usuarios.editar', 'uses' => 'MantenimientoUsuariosController@editar']);
        Route::post('/usuarios/eliminar/', ['as' => 'usuarios.eliminar', 'uses' => 'MantenimientoUsuariosController@eliminar']);
        Route::post('/usuarios/guardar/', ['as' => 'usuarios.guardar', 'uses' => 'MantenimientoUsuariosController@guardar']);
        Route::post('/usuarios/perfil/', ['as' => 'usuarios.editarPerfil', 'uses' => 'MantenimientoUsuariosController@editarPerfil']);


        Route::get('/perfiles/cargar', ['as' => 'perfiles.cargar', 'uses' => 'MantenimientoPerfilesController@cargarAll']);
        Route::post('/perfiles/buscar/', ['as' => 'perfiles.buscar', 'uses' => 'MantenimientoPerfilesController@buscar']);
        Route::patch('/perfiles/editar/', ['as' => 'perfiles.editar', 'uses' => 'MantenimientoPerfilesController@editar']);
        Route::post('/perfiles/eliminar/', ['as' => 'perfiles.eliminar', 'uses' => 'MantenimientoPerfilesController@eliminar']);
        Route::post('/perfiles/guardar/', ['as' => 'perfiles.guardar', 'uses' => 'MantenimientoPerfilesController@guardar']);

        Route::get('/procesos/cargar', ['as' => 'procesos.cargar', 'uses' => 'MantenimientoProcesosController@cargarAll']);
        Route::post('/procesos/buscar/', ['as' => 'procesos.buscar', 'uses' => 'MantenimientoProcesosController@buscar']);
        Route::patch('/procesos/editar/', ['as' => 'procesos.editar', 'uses' => 'MantenimientoProcesosController@editar']);
        Route::post('/procesos/eliminar/', ['as' => 'procesos.eliminar', 'uses' => 'MantenimientoProcesosController@eliminar']);
        Route::post('/procesos/guardar/', ['as' => 'procesos.guardar', 'uses' => 'MantenimientoProcesosController@guardar']);

        Route::get('/adjuntos/cargar', ['as' => 'adjuntos.cargar', 'uses' => 'MantenimientoAdjuntosController@cargarAll']);
        Route::post('/adjuntos/buscar/', ['as' => 'adjuntos.buscar', 'uses' => 'MantenimientoAdjuntosController@buscar']);
        Route::patch('/adjuntos/editar/', ['as' => 'adjuntos.editar', 'uses' => 'MantenimientoAdjuntosController@editar']);
        Route::post('/adjuntos/eliminar/', ['as' => 'adjuntos.eliminar', 'uses' => 'MantenimientoAdjuntosController@eliminar']);
        Route::post('/adjuntos/guardar/', ['as' => 'adjuntos.guardar', 'uses' => 'MantenimientoAdjuntosController@guardar']);

        Route::get('/unidades/cargar', ['as' => 'unidades.cargar', 'uses' => 'MantenimientoUnidadesController@cargarAll']);
        Route::post('/unidades/buscar/', ['as' => 'unidades.buscar', 'uses' => 'MantenimientoUnidadesController@buscar']);
        Route::patch('/unidades/editar/', ['as' => 'unidades.editar', 'uses' => 'MantenimientoUnidadesController@editar']);
        Route::post('/unidades/eliminar/', ['as' => 'unidades.eliminar', 'uses' => 'MantenimientoUnidadesController@eliminar']);
        Route::post('/unidades/guardar/', ['as' => 'unidades.guardar', 'uses' => 'MantenimientoUnidadesController@guardar']);

        Route::get('/tareas/cargar', ['as' => 'tareas.cargar', 'uses' => 'MantenimientoTareasController@cargarAll']);
        Route::post('/tareas/buscar/', ['as' => 'tareas.buscar', 'uses' => 'MantenimientoTareasController@buscar']);
        Route::patch('/tareas/editar/', ['as' => 'tareas.editar', 'uses' => 'MantenimientoTareasController@editar']);
        Route::post('/tareas/eliminar/', ['as' => 'tareas.eliminar', 'uses' => 'MantenimientoTareasController@eliminar']);
        Route::post('/tareas/guardar/', ['as' => 'tareas.guardar', 'uses' => 'MantenimientoTareasController@guardar']);

        Route::get('/actividades/cargar', ['as' => 'actividades.cargar', 'uses' => 'MantenimientoActividadesController@cargarAll']);
        Route::post('/actividades/buscar/', ['as' => 'actividades.buscar', 'uses' => 'MantenimientoActividadesController@buscar']);
        Route::patch('/actividades/editar/', ['as' => 'actividades.editar', 'uses' => 'MantenimientoActividadesController@editar']);
        Route::post('/actividades/eliminar/', ['as' => 'actividades.eliminar', 'uses' => 'MantenimientoActividadesController@eliminar']);
        Route::post('/actividades/guardar/', ['as' => 'actividades.guardar', 'uses' => 'MantenimientoActividadesController@guardar']);

        Route::get('/factores/cargar', ['as' => 'factores.cargar', 'uses' => 'MantenimientoFactoresController@cargarAll']);
        Route::post('/factores/buscar/', ['as' => 'factores.buscar', 'uses' => 'MantenimientoFactoresController@buscar']);
        Route::patch('/factores/editar/', ['as' => 'factores.editar', 'uses' => 'MantenimientoFactoresController@editar']);
        Route::post('/factores/eliminar/', ['as' => 'factores.eliminar', 'uses' => 'MantenimientoFactoresController@eliminar']);
        Route::post('/factores/guardar/', ['as' => 'factores.guardar', 'uses' => 'MantenimientoFactoresController@guardar']);

        Route::get('/materiales/cargar', ['as' => 'materiales.cargar', 'uses' => 'MantenimientoMaterialesController@cargarAll']);
        Route::post('/materiales/factores', ['as' => 'materiales.factores', 'uses' => 'MantenimientoMaterialesController@cargarMaterialesNoRegistradosEnFactores']);
        Route::post('/cargarMaterialesServerSide', ['as' => 'materiales.factores', 'uses' => 'MantenimientoMaterialesController@cargarMaterialesServerSide']);

        Route::get('/correo/cargar', ['as' => 'correo.cargar', 'uses' => 'MantenimientoCorreosController@cargarAll']);
        Route::post('/correo/buscar/', ['as' => 'correo.editar', 'uses' => 'MantenimientoCorreosController@buscar']);
        Route::patch('/correo/editar/', ['as' => 'correo.editar', 'uses' => 'MantenimientoCorreosController@editar']);
        Route::post('/correo/guardar/', ['as' => 'correo.guardar', 'uses' => 'MantenimientoCorreosController@guardar']);
       
    });
});
