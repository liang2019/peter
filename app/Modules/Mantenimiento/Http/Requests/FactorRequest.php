<?php

namespace App\Modules\Mantenimiento\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FactorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
           
            case 'PATCH':
                $rules = $this->rules_editar();
                break;
            default:
                $rules = $this->rules_guardar();
                break;
        }
        return $rules;
    }

    public function rules_guardar(){
        return [
      
            'alarti_id' => 'required',
            'e' => 'nullable',
            'ld' => 'required',
            'caras' => 'required',
            'unidad' => 'required',
            'pieza' => 'required',
            'perimetro' => 'required',
            'seccion' => 'required',
            'unidad_pieza' => 'required',
            'kilogramo_unidad' => 'required',
            'kilogramo_pieza' => 'required',
            'metro_unidad' => 'required',
            'metro_pieza' => 'required',
            /*
            'costo_unidad' => 'required',
            'costo_pieza' => 'required',
            */
                     
        ];
    }

    public function rules_editar(){
        return [
         
            'id' => 'required',
            'alarti_id' => 'required',
            'e' => 'nullable',
            'ld' => 'required',
            'caras' => 'required',
            'unidad' => 'required',
            'pieza' => 'required',
            'perimetro' => 'required',
            'seccion' => 'required',
            'unidad_pieza' => 'required',
            'kilogramo_unidad' => 'required',
            'kilogramo_pieza' => 'required',
            'metro_unidad' => 'required',
            'metro_pieza' => 'required',
            /*
            'costo_unidad' => 'required',
            'costo_pieza' => 'required',
            */
           
          
        ];
    }
}
