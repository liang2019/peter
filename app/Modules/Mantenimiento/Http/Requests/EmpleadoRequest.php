<?php

namespace App\Modules\Mantenimiento\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpleadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'PATCH':
                $rules = $this->rules_editar();
                break;
            default:
                $rules = $this->rules_guardar();
                break;
        }
        return $rules;

    }

    public function rules_guardar(){
        return [

                'codigo' => 'required|max:100',
                'nombres' => 'required',
                'apellidos' => 'required',
                'fecha_nacimiento' => 'required',
  
                'cargo' => 'required',
           
                'sueldo' => 'required',
                'sueldo_mes' => 'required',
                'tareo' => 'required',

        ];
    }

    public function rules_editar(){
        return [

           'id' => 'required',
           'codigo' => 'required|max:100',
           'nombres' => 'required',
           'apellidos' => 'required',
           'fecha_nacimiento' => 'required',
          
           'cargo' => 'required',
         
           'sueldo' => 'required',
           'sueldo_mes' => 'required',
           'id' => 'required',
           'tareo' => 'required',


        ];
    }
}
