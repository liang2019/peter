<?php

namespace App\Modules\Mantenimiento\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Modules\Mantenimiento\Services\MaterialService as MainService;

class MantenimientoMaterialesController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }

    public function cargarAll()
    {
        $data = $this->service->cargarAll();
        return $data;
    }

    public function cargarMaterialesNoRegistradosEnFactores(Request $request){
        $data = $this->service->cargarMaterialesNoRegistradosEnFactores($request);
        return $data;
    }
    public function cargarMaterialesServerSide(Request $request){
        $data = $this->service->cargarMaterialesServerSide($request);
        return $data;
    }

}
