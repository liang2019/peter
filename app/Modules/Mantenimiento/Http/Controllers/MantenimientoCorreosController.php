<?php

namespace App\Modules\Mantenimiento\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Mantenimiento\Http\Requests\CorreoRequest as MainRequest;
use App\Modules\Mantenimiento\Services\MantenimientoCorreoService as MainService;
 
class MantenimientoCorreosController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }
    public function cargarAll()
    {
        $data = $this->service->cargarAll();
        return $data;
    }
    public function guardar(Request $request)
    { 
        //$menu =correoTracking();
        $save = $this->service->guardar($request);
        return $save;
    }
    public function buscar(Request $request)
    {
        $empleado = $this->service->buscar($request);
        return $empleado;
    }
    public function editar(Request $request)
    {
        $data = $this->service->editar($request);
        return $data;
    }
   
}
