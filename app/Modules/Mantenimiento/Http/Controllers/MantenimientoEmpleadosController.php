<?php

namespace App\Modules\Mantenimiento\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Mantenimiento\Models\Empleado;
use App\Modules\Mantenimiento\Http\Requests\EmpleadoRequest as MainRequest;
use App\Modules\Mantenimiento\Services\EmpleadoService as MainService;

class MantenimientoEmpleadosController extends Controller
{
    public function __construct()
    {
        //$this->middleware('laraguard');
        $this->service = new MainService();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function guardar(MainRequest $request)
    {
        $form = $request->validated();
        $save = $this->service->guardar($form);
        return $save;
    }

    public function cargarAll()
    {
        $data = $this->service->cargarAll();

        return $data;
    }

    public function buscar(Request $request)
    {
        $empleado = $this->service->buscar($request);
        return $empleado;
    }
    public function editar(MainRequest $request)
    {
        $form = $request->validated();
        $empleado = $this->service->editar($form);
        if($empleado){
            return 1;
        }else{
            return 0;
        }

    }
    public function eliminar(Request $request){
        $empleado = $this->service->eliminar($request);
        if($empleado){
            return 1;
        }else{
            return 0;
        }
    }

    public function verificarDni(Request $request)
    {
        $dni = $this->service->verificarDni($request);
        if($dni){
            //dni existe 
            return 1;
        }else{
               //dni no existe 
            return 0;
        }
       
    }
}
