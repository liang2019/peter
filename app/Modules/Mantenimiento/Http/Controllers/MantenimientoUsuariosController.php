<?php

namespace App\Modules\Mantenimiento\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Mantenimiento\Models\Usuario;
use App\Modules\Mantenimiento\Http\Requests\UsuarioRequest as MainRequest;
use App\Modules\Mantenimiento\Services\UsuarioService as MainService;

class MantenimientoUsuariosController extends Controller
{
    public function __construct()
    {
        //$this->middleware('laraguard');
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));

    }

    public function guardar(MainRequest $request)
    {
        $form = $request->validated();
        $save = $this->service->guardar($form);
        return $save;
    }

    public function cargarAll()
    {
        $data = $this->service->cargarAll();
        return $data;
    }

    public function buscar(Request $request)
    {
        $data = $this->service->buscar($request);
        return $data;
    }

    public function verificar(Request $request)
    {
        $data = $this->service->verificar($request);
        if($data){
            return 1;
        }else{
            return 0;
        }
       
    }

    public function editar(MainRequest $request)
    {
        $form = $request->validated();
        $data = $this->service->editar($form);
        if($data){
            return 1;
        }else{
            return 0;
        }
       
    }

    public function editarPerfil(Request $request)
    {
        $data = $this->service->editarPerfil($request);
        if($data){
            return 1;
        }else{
            return 0;
        }
       
    }

    public function eliminar(Request $request){
        $data = $this->service->eliminar($request);
        if($data){
            return 1;
        }else{
            return 0;
        }
    }
}
