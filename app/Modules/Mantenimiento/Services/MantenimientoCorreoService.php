<?php

namespace App\Modules\Mantenimiento\Services;

use App\Modules\Mantenimiento\Models\Correo;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;


class MantenimientoCorreoService
{


    public function __construct()
    {
        $this->model = new Correo();
      
    }

    public function cargarAll(){

        $datos = $this->model->select('*')->where('estado',1)->get();
        $arr = array();
        if (count($datos)>0) {
            $datos = $datos[0];
            foreach (explode(',',$datos["correos"]) as $key => $value) {
                $arr[$key]["id"] = $key+1;
                $arr[$key]["correos"] = $value;
                $arr[$key]["fecha_creacion"] = $datos["fecha_creacion"];
                $arr[$key]["id_real"] = $datos["id"];
            }
        }
        return $arr;
    
    }

    public function guardar($data)
    {
        $data = $data->all();

        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $registros = $this->model->all();
        $save = 0;
        if (count($registros)>0) {
            $data['id'] = $registros[0]["id"];
            if (isset($data["eliminar"]) && $data["eliminar"] ) {
                $save = $this->model->destroy($data['id']);
            }else{
                $data['correos'] = isset($data["editar"]) ? $data['correos'] :$data['correos'].",".$registros[0]["correos"];
                $save = $this->editar($data);
            }
        }else{
            $save = $this->model->create($data);
        }
        return $save?1:0;

    }

    public function buscar($data){

        $datos =  $this->model->all()->where('id', $data["id"])->first();
        return $datos;
    }

    public function editar($data){
        
        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $datos = $this->model->find($data['id'])->update($data);
        return $datos;
    }


}
