<?php

namespace App\Modules\Mantenimiento\Services;

use App\Modules\Mantenimiento\Models\Empleado;
use App\Modules\Mantenimiento\Models\Usuario;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

class EmpleadoService
{

    public function __construct()
    {
        $this->model = new Empleado();
        $this->model_usuario = new Usuario();


    }

    public function cargarAll(){

        $datos = $this->model->select('*')->where('estado',1)->orderBy("apellidos","asc")->get();
        return $datos;
    }

    public function guardar($data)
    {
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $data['sueldo']=  Crypt::encryptString( $data['sueldo']);
        $data['sueldo_mes']=  Crypt::encryptString( $data['sueldo_mes']);
        $save = $this->model->create($data);
        return $save;

    }

    public function buscar($data){

        $empleado =  $this->model->all()->where('id', $data["id"])->first();
        $empleado['sueldo']= Crypt::decryptString( $empleado['sueldo']);
        $empleado['sueldo_mes']= Crypt::decryptString( $empleado['sueldo_mes']);
        return $empleado;
    }

    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $data['sueldo']=  Crypt::encryptString( $data['sueldo']);
        $data['sueldo_mes']=  Crypt::encryptString( $data['sueldo_mes']);
        $update = $this->model->find($data['id'])->update($data);
        return $update;
    }

    public function eliminar($data){

        $data["usuario_edicion"] = auth()->user()["id"];
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update(['estado' => 0]);
        $update_usuario = $this->model_usuario->where('empleado_id', $data['id'])->update(['estado' => 0]);
        return $update;
    }

    public function verificarDni($data)
    {
        $empleado =  $this->model::all()->where('codigo', $data["codigo"])->where('id','!=',$data["id"])->where('estado',1)->first();

        return $empleado;
    }

}
