<?php

namespace App\Modules\Mantenimiento\Services;

use App\Modules\Mantenimiento\Models\Tarea;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;


class TareaService
{


    public function __construct()
    {
        $this->model = new Tarea();

    }

    public function cargarAll(){

        $datos = $this->model->select('*')->where('estado',1)
        ->orderby('nombre')
        ->get();
        return $datos;
    }


    public function guardar($data)
    {
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $save = $this->model->create($data);
        return $save;

    }

    public function buscar($data){

        $datos =  $this->model->all()->where('id', $data["id"])->first();
        return $datos;
    }

    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $datos = $this->model->find($data['id'])->update($data);
        return $datos;
    }

    public function eliminar($data){

        $data["usuario_edicion"] = auth()->user()["id"];
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update(['estado' => 0]);
        return $update;
    }



}
