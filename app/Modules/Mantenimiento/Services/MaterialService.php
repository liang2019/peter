<?php

namespace App\Modules\Mantenimiento\Services;

use App\Modules\Importaciones\Models\Alarti;
use App\Modules\Importaciones\Models\Almovd;
use App\Modules\Importaciones\Models\Alstoc;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;


class MaterialService
{


    public function __construct()
    {
        $this->model = new Alarti();

    }

    public function cargarAll(){

        $datos = $this->model->select('*')->where('estado',1)->get();
        return $datos;
    }

    public function cargarMaterialesNoRegistradosEnFactores($data){
        $data["search"] = trim($data["search"]);
        $datos["items"] = $this->model->where('factor.alarti_id', null)
        ->where(function($q) use ($data){
            $q->where('alarti.codigo','like','%'.$data["search"].'%')
            ->orWhere('alarti.descripcion','like','%'.$data["search"].'%');
        })
        ->leftJoin('factor', 'alarti.id', '=', 'factor.alarti_id')
        ->select('alarti.*')
        ->orderBy($data["sortBy"],$data["sortDesc"]?'desc':'asc')
        ->skip(floatval($data["page"]))
        ->take(floatval($data["limit"]))->get();

        $datos["count"] = $this->model->where('factor.alarti_id', null)
        ->where(function($q) use ($data){
            $q->where('alarti.codigo','like','%'.$data["search"].'%')
            ->orWhere('alarti.descripcion','like','%'.$data["search"].'%');
        })
        ->leftJoin('factor', 'alarti.id', '=', 'factor.alarti_id')
        ->select('alarti.*')->count();
        return $datos;
    }

    public function cargarMaterialesServerSide($data){
        $data["search"] = trim($data["search"]);
        $datos["items"] = $this->model
        ->where(function($q) use ($data){
            $q->where('alarti.codigo','like','%'.$data["search"].'%')
            ->orWhere('alarti.descripcion','like','%'.$data["search"].'%');
        })
        ->select('alarti.*')
        ->orderBy($data["sortBy"],$data["sortDesc"]?'desc':'asc')
        //->skip( !empty($data["search"]) ? (floatval($data["page"]) >=10?(floatval($data["page"]) ==10?0:floatval($data["page"])):floatval($data["page"])) : (floatval($data["page"]) ==1 ? 0 :floatval($data["page"])) )
        ->skip( floatval($data["page"]) )
        ->take(floatval($data["limit"]))->get();

        $datos["count"] = $this->model
        ->where(function($q) use ($data){
            $q->where('alarti.codigo','like','%'.$data["search"].'%')
            ->orWhere('alarti.descripcion','like','%'.$data["search"].'%');
        })
        ->select('alarti.*')->count();
        return $datos;
    }

    public function guardar($data)
    {
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $save = $this->model->create($data);
        return $save;

    }

    public function buscar($data){

        $datos =  $this->model->all()->where('id', $data["id"])->first();
        return $datos;
    }

    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $datos = $this->model->find($data['id'])->update($data);
        return $datos;
    }

    public function eliminar($data){

        $data["usuario_edicion"] = auth()->user()["id"];
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update(['estado' => 0]);
        return $update;
    }



}
