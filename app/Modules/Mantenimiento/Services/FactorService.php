<?php

namespace App\Modules\Mantenimiento\Services;

use App\Modules\Mantenimiento\Models\Factor;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;


class FactorService
{


    public function __construct()
    {
        $this->model = new Factor();
      
    }

    public function cargarAll(){

        $datos = $this->model->where('factor.estado',1)
        ->leftJoin('alarti', 'factor.alarti_id', '=', 'alarti.id') 
        ->select('factor.*', 'alarti.codigo as codigo_material', 'alarti.descripcion as descripcion_material')->get();
    
        return $datos;
    }

    public function guardar($data)
    {
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $save = $this->model->create($data);
        return $save;

    }

    public function buscar($data){

        $datos =  $this->model->all()->where('id', $data["id"])->first();
        return $datos;
    }

    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $datos = $this->model->find($data['id'])->update($data);
        return $datos;
    }

    public function eliminar($data){

        $data["usuario_edicion"] = auth()->user()["id"];
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update(['estado' => 0]);
        return $update;
    }



}
