<?php

namespace App\Modules\Mantenimiento\Services;

use App\Modules\Mantenimiento\Models\Usuario;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;


class UsuarioService
{


    public function __construct()
    {
        $this->model = new Usuario();

    }

    public function cargarAll(){


        $datos = $this->model->where('usuario.estado',1)
                ->leftJoin('empleado', 'usuario.empleado_id', '=', 'empleado.id')
                ->leftJoin('perfil', 'usuario.perfil_id', '=', 'perfil.id')
                ->select('usuario.*', 'empleado.nombres as nombre_empleado', 'empleado.apellidos as apellido_empleado','perfil.nombre as perfil_empleado')
                ->get();

        return $datos;
    }

    public function verificar($data){

        $datos =  $this->model::all()->where('usuario', $data["usuario"])->where('id','!=',$data["id"])->first();

        return $datos;
    }

    public function guardar($data)
    {
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $data['password']=  Hash::make( $data['password']);
        $save = $this->model->create($data);
        return $save;

    }

    public function buscar($data){

        $datos =  $this->model->all()->where('id', $data["id"])->first();
        return $datos;
    }

    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $data['password']=  Hash::make( $data['password']);
        $datos = $this->model->find($data['id'])->update($data);
        return $datos;
    }

    public function editarPerfil($data){
   
        $data["password"]=  Hash::make( $data['password']);
 
        $datos = $this->model->find($data["id"])->update(array(
            "password" =>$data["password"],
            "usuario_edicion" => auth()->user()->id,
            "fecha_modificacion" => now()->format('Y-m-d H:i:s')
        ));
        return $datos;
    }

    public function eliminar($data){

        $data["usuario_edicion"] = auth()->user()["id"];
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update(['estado' => 0]);
        return $update;
    }



}
