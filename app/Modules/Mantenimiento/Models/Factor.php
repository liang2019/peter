<?php

namespace App\Modules\Mantenimiento\Models;

use Illuminate\Database\Eloquent\Model;

class Factor extends Model
{
    protected $fillable = [
                            'alarti_id',
                            'descripcion',
                            'e',
                            'ld',
                            'caras',
                            'unidad',
                            'pieza',
                            'perimetro',
                            'seccion',
                            'unidad_pieza',
                            'kilogramo_unidad',
                            'kilogramo_pieza',
                            'metro_unidad',
                            'metro_pieza',
                            /*
                            'costo_unidad',
                            'costo_pieza',
                            */
                            'estado',
                            'usuario_creacion',
                            'usuario_edicion',
                            'fecha_creacion',
                            'fecha_modificacion'];
    protected $table = 'factor';
    public $timestamps = false;
}
