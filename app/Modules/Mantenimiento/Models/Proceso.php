<?php

namespace App\Modules\Mantenimiento\Models;

use Illuminate\Database\Eloquent\Model;

class Proceso extends Model
{
    protected $fillable = ['nombre',
                            'unidad',
                            'costo',
                            'grupo',
                            'estado',
                            'usuario_creacion',
                            'usuario_edicion',
                            'fecha_creacion',
                            'fecha_modificacion'];
    protected $table = 'proceso';
    public $timestamps = false;
}
