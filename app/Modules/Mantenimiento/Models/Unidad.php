<?php

namespace App\Modules\Mantenimiento\Models;

use Illuminate\Database\Eloquent\Model;

class Unidad extends Model
{
    protected $fillable = [
        'unidad',
        'descripcion',
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'unidad';
    public $timestamps = false;
}
