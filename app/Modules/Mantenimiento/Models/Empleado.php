<?php

namespace App\Modules\Mantenimiento\Models;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $fillable = ['codigo',
                            'nombres',
                            'apellidos',
                            'fecha_nacimiento',
                            'cargo',
                            'sueldo',
                            'sueldo_mes',
                            'tareo',
                            'estado',
                            'usuario_creacion',
                            'usuario_edicion',
                            'fecha_creacion',
                            'fecha_modificacion'];
    protected $table = 'empleado';
    public $timestamps = false;

}
