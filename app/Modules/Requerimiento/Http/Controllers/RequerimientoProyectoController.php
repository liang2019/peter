<?php

namespace App\Modules\Requerimiento\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Requerimiento\Http\Requests\ProyectoRequest as MainRequest;
use App\Modules\Requerimiento\Services\ProyectoService as MainService;

class RequerimientoProyectoController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarAll()
    {
        $data = $this->service->cargarAll();
        return $data;
    }
    
    public function cargarTiposProductos()
    {
        $data = $this->service->cargarTiposProductos();
        return $data;
    }

    public function guardar(MainRequest $request)
    {
        $form = $request->validated();
        $save = $this->service->guardar($form);
        return $save;
    }

    public function buscar(Request $request)
    {
        $data = $this->service->buscar($request);
        return $data;
    }

    public function editar(MainRequest $request)
    {
        $form = $request->validated();
        $data = $this->service->editar($form);
        return $data;

    }

    public function cancelar(Request $request)
    {
        $data = $this->service->cancelar($request);
        return $data;
    }

    public function completar(Request $request)
    {
        $data = $this->service->completar($request);
        return $data;
    }

    public function cargarProyectosAdjuntos()
    {
        $data = $this->service->cargarProyectosAdjuntos();
        return $data;
    }

    public function cargarAdjuntos(Request $request)
    {
        $data = $this->service->cargarAdjuntos($request);
        return $data;
    }

    public function cargarCronogramaProyectos()
    {
        $data = $this->service->cargarCronogramaProyectos();
        return $data;
    }

}
