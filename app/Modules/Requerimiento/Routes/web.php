<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'auth'],function () {

    Route::group(['prefix' => 'documentos'], function () {
        Route::get('/', ['as' => 'proyectos', 'uses' => 'RequerimientoProyectoController@index']);

        Route::get('/proyectosadjuntos/', ['as' => 'proyectos.cargarProyectosAdjuntos', 'uses' => 'RequerimientoProyectoController@cargarProyectosAdjuntos']);
        Route::post('/proyectos/adjuntos/', ['as' => 'proyectos.cargarAdjuntos', 'uses' => 'RequerimientoProyectoController@cargarAdjuntos']);
    });
    Route::group(['prefix' => 'cronograma'], function () {
        Route::get('/', ['as' => 'cronograma', 'uses' => 'RequerimientoProyectoController@index']);

        Route::get('/cargar', ['as' => 'proyectos.cargarCronogramaProyectos', 'uses' => 'RequerimientoProyectoController@cargarCronogramaProyectos']);
      
    });
    //Route::get('/documentos', ['as' => 'proyectos', 'uses' => 'RequerimientoProyectoController@index']);
    Route::group(['prefix' => 'requerimiento'], function () {

        Route::get('/proyectos', ['as' => 'proyectos', 'uses' => 'RequerimientoProyectoController@index']);
        Route::get('/proyectos/cargar', ['as' => 'proyectos.cargar', 'uses' => 'RequerimientoProyectoController@cargarAll']);
        Route::get('/proyectos/productos', ['as' => 'proyectos.cargar', 'uses' => 'RequerimientoProyectoController@cargarTiposProductos']);
        Route::post('/proyectos/buscar/', ['as' => 'proyectos.buscar', 'uses' => 'RequerimientoProyectoController@buscar']);
        Route::patch('/proyectos/editar/', ['as' => 'proyectos.editar', 'uses' => 'RequerimientoProyectoController@editar']);
        Route::post('/proyectos/cancelar/', ['as' => 'proyectos.cancelar', 'uses' => 'RequerimientoProyectoController@cancelar']);
        Route::post('/proyectos/completar/', ['as' => 'proyectos.completar', 'uses' => 'RequerimientoProyectoController@completar']);
        Route::post('/proyectos/guardar/', ['as' => 'proyectos.guardar', 'uses' => 'RequerimientoProyectoController@guardar']);

        Route::get('/documentaria', ['as' => 'documentaria', 'uses' => 'RequerimientoController@index']);
        Route::post('/documentaria/cargar', ['as' => 'documentaria.cargar', 'uses' => 'RequerimientoController@cargarAll']);
        Route::post('/documentaria/buscar/', ['as' => 'documentaria.buscar', 'uses' => 'RequerimientoController@buscar']);
        Route::patch('/documentaria/editar/', ['as' => 'documentaria.editar', 'uses' => 'RequerimientoController@editar']);
        Route::post('/documentaria/eliminar/', ['as' => 'documentaria.eliminar', 'uses' => 'RequerimientoController@eliminar']);
        Route::post('/documentaria/guardar/', ['as' => 'documentaria.guardar', 'uses' => 'RequerimientoController@guardar']);
        Route::post('/documentaria/updatefile/', ['as' => 'documentaria.updatefile', 'uses' => 'RequerimientoController@updatefile']);
        Route::get('/documentaria/download/{id}', ['as' => 'documentaria.download', 'uses' => 'RequerimientoController@download']);
        Route::get('/documentaria/{id}', ['as' => 'documentaria.id', 'uses' => 'RequerimientoController@index']);

        

    });
});
