<?php

namespace App\Modules\Requerimiento\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('requerimiento', 'Resources/Lang', 'app'), 'requerimiento');
        $this->loadViewsFrom(module_path('requerimiento', 'Resources/Views', 'app'), 'requerimiento');
        $this->loadMigrationsFrom(module_path('requerimiento', 'Database/Migrations', 'app'), 'requerimiento');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('requerimiento', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('requerimiento', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
