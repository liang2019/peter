<?php

namespace App\Modules\Requerimiento\Services;

use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Requerimiento\Models\Producto;
use App\Modules\Tracking\Models\Tracking;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Modules\Presupuesto\Models\Presupuesto;
use App\Modules\Presupuesto\Models\Item;
use App\Modules\Presupuesto\Models\Utilidad;
use Illuminate\Support\Facades\DB;
class ProyectoService
{


    public function __construct()
    {
        $this->model = new Proyecto();
        $this->model_tarcking = new Tracking();
        $this->model_presupuesto = new Presupuesto();
        $this->model_item = new Item();
        $this->model_utilidad = new Utilidad();
        $this->model_producto= new Producto();
    }

    public function cargarAll(){

        $datos = $this->model
        ->where("tracking.requerimiento","<>", 3)
        ->leftJoin('cliente', 'proyecto.cliente_id', '=', 'cliente.id')
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*', 'cliente.descripcion as nombre_cliente',
        DB::raw("(SELECT COUNT(requerimiento.id) from requerimiento  WHERE requerimiento.proyecto_id=proyecto.id) as cantidad_adjuntos"),
            DB::raw("(SELECT COUNT(requerimiento.id) from requerimiento LEFT JOIN adjunto ON requerimiento.adjunto_id=adjunto.id  WHERE adjunto.codigo = 'requerimiento' AND requerimiento.proyecto_id=proyecto.id) as adjuntos_requerimiento"),
        'tracking.requerimiento as estado_tracking',
        DB::raw('(SELECT producto.nombre from producto WHERE producto.id = proyecto.producto_id) as producto'),
        DB::raw('(SELECT usuario from usuario WHERE usuario.id = proyecto.usuario_creacion) as user'))
        ->orderBy('proyecto.id', 'desc')
        ->get();

        return $datos;
    }
    public function cargarTiposProductos(){

        $datos = $this->model_producto
        ->where("estado",1)
       ->select("*")
        ->orderBy('nombre', 'desc')
        ->get();

        return $datos;
    }
    public function guardar($data)
    {
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $año = now()->format('y');
        $mes = now()->format('m');
        $data["codigo"] =  'E-AAMM-';
        //crear proyecto
        $save = $this->model->create($data);
        $update = $this->model->find($save->id)->update(array("codigo"=>'W-'.$año.''.$mes.'-'.($save->id<=9?('000'.$save->id):($save->id<=99?('00'.$save->id):($save->id<=999?('0'.$save->id):$save->id)))));


        if($save){
            //crear los items de utilidad y gastos

            $gastos = $this->model_item->create(array(
                        "proyecto_id"   => $save->id,
                        "nombre" => "Gastos",
                        "habilitado" => 0,
                        "cantidad" => 1,
                        "usuario_creacion" => auth()->user()["id"],
                        "usuario_edicion" => auth()->user()["id"]
                     ));

            if($gastos){
                $gastos_utl = $this->model_utilidad->create(array(
                    "proyecto_id"   => $save->id,
                    "item_id"   => $gastos->id,
                    "usuario_creacion" => auth()->user()["id"],
                    "usuario_edicion" => auth()->user()["id"]
                 ));
            }

            $utilidad = $this->model_item->create(array(
                "proyecto_id"   => $save->id,
                "nombre" => "Utilidad",
                "habilitado" => 0,
                "cantidad" => 1,
                "usuario_creacion" => auth()->user()["id"],
                "usuario_edicion" => auth()->user()["id"]
             ));

            if($utilidad){
                $utilidad_utl = $this->model_utilidad->create(array(
                    "proyecto_id"   => $save->id,
                    "item_id"   => $utilidad->id,
                    "usuario_creacion" => auth()->user()["id"],
                    "usuario_edicion" => auth()->user()["id"]
                ));
            }

        }

        //actualizar estado del tracking
        $data_tracking = array(
            "proyecto_id"   => $save->id,
            "requerimiento" => 1,
            "usuario_creacion" => auth()->user()["id"],
            "usuario_edicion" => auth()->user()["id"]
        );
        $create = $this->model_tarcking->create($data_tracking);

        return $save;

    }

    public function buscar($data){

        $datos =  $this->model
        ->leftJoin('cliente', 'proyecto.cliente_id', '=', 'cliente.id')
        ->select('proyecto.*', 
        'cliente.descripcion as nombre_cliente',
        DB::raw('(SELECT producto.nombre from producto WHERE producto.id = proyecto.producto_id) as producto')
        )
        ->where('proyecto.id', $data["id"])->first();
        return $datos;
    }

    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $datos = $this->model->find($data['id'])->update($data);
        return $datos?1:$datos;
    }

    public function cancelar($data){

        $data["usuario_edicion"] = auth()->user()["id"];
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update(['estado' => 0]);
         //actualizar estado del tracking
        $tracking = $this->model_tarcking->where('proyecto_id', $data['id'])
        ->update([
                    'requerimiento' => 0,
                    'usuario_edicion' => auth()->user()->id,
                    'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                ]);
        if(!$tracking){
            return false;
        }
        return $update?1:$update;
    }

    public function completar($data){


        $datos = array(
            "etapa"   => 1,
            "proyecto_id" => $data['id'],
            "usuario_creacion" => auth()->user()["id"],
            "usuario_edicion" => auth()->user()["id"]
        );
        $create = $this->model_presupuesto->create($datos);

         //actualizar estado del tracking
        $tracking = $this->model_tarcking->where('proyecto_id', $data['id'])
        ->update([
                    'requerimiento' => 2,
                    'requerimiento_fecha' => now()->format('Y-m-d H:i:s'),
                    'presupuesto' => 1,
                    'usuario_edicion' => auth()->user()->id,
                    'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                ]);

        return $tracking?1:$tracking;
    }

    public function cargarProyectosAdjuntos(){

        $datos = $this->model
        ->leftJoin('cliente', 'proyecto.cliente_id', '=', 'cliente.id')
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*', 'cliente.descripcion as nombre_cliente')
        ->get();

        return $datos;
    }

    public function cargarAdjuntos($data){

       

        $datos["presupuesto"] = $this->model
        ->where("proyecto.id","=", $data["id"])
        ->where("presupuesto_documentaria.estado","=", 1)
        ->leftJoin('presupuesto_documentaria', 'proyecto.id', '=','presupuesto_documentaria.proyecto_id')
        ->leftJoin('usuario', 'usuario.id', '=','presupuesto_documentaria.usuario_creacion')
        ->leftJoin('adjunto', 'presupuesto_documentaria.adjunto_id', '=', 'adjunto.id')
        ->select('presupuesto_documentaria.*','usuario.usuario as usuario','adjunto.nombre as tipo_adjunto',DB::raw('"Presupuesto" as faseAdjuntosProyectos'));

        $datos["cotizacion"] = $this->model
        ->where("proyecto.id","=", $data["id"])
        ->where("cotizacion_documentaria.estado","=",1)
        ->leftJoin('cotizacion_documentaria', 'proyecto.id', '=', 'cotizacion_documentaria.proyecto_id')
        ->leftJoin('usuario', 'usuario.id', '=','cotizacion_documentaria.usuario_creacion')
        ->leftJoin('adjunto', 'cotizacion_documentaria.adjunto_id', '=', 'adjunto.id')
        ->select('cotizacion_documentaria.*','usuario.usuario as usuario','adjunto.nombre as tipo_adjunto',DB::raw('"Cotización" as faseAdjuntosProyectos'));

        $datos["pedido"] = $this->model
        ->where("proyecto.id","=", $data["id"])
        ->where("pedido_documentaria.estado","=",1)
        ->leftJoin('pedido_documentaria', 'proyecto.id', '=', 'pedido_documentaria.proyecto_id')
        ->leftJoin('usuario', 'usuario.id', '=','pedido_documentaria.usuario_creacion')
        ->leftJoin('adjunto', 'pedido_documentaria.adjunto_id', '=', 'adjunto.id')
        ->select('pedido_documentaria.*','usuario.usuario as usuario','adjunto.nombre as tipo_adjunto',DB::raw('"Pedido" as faseAdjuntosProyectos'));

        $datos["diseno"] = $this->model
        ->where("proyecto.id","=", $data["id"])
        ->where("diseno_documentaria.estado","=",1)
        ->leftJoin('diseno_documentaria', 'proyecto.id', '=', 'diseno_documentaria.proyecto_id')
        ->leftJoin('usuario', 'usuario.id', '=','diseno_documentaria.usuario_creacion')
        ->leftJoin('adjunto', 'diseno_documentaria.adjunto_id', '=', 'adjunto.id')
        ->select('diseno_documentaria.*','usuario.usuario as usuario','adjunto.nombre as tipo_adjunto',DB::raw('"Diseño" as faseAdjuntosProyectos'));

        $datos["produccion"] = $this->model
        ->where("proyecto.id","=", $data["id"])
        ->where("produccion_documentaria.estado","=", 1)
        ->leftJoin('produccion_documentaria', 'proyecto.id', '=', 'produccion_documentaria.proyecto_id')
        ->leftJoin('usuario', 'usuario.id', '=','produccion_documentaria.usuario_creacion')
        ->leftJoin('adjunto', 'produccion_documentaria.adjunto_id', '=', 'adjunto.id')
        ->select('produccion_documentaria.*','usuario.usuario as usuario','adjunto.nombre as tipo_adjunto',DB::raw('"Producción" as faseAdjuntosProyectos'));

        $datos["montaje"] = $this->model
        ->where("proyecto.id","=", $data["id"])
        ->where("montaje_documentaria.estado","=", 1)
        ->leftJoin('montaje_documentaria', 'proyecto.id', '=', 'montaje_documentaria.proyecto_id')
        ->leftJoin('usuario', 'usuario.id', '=','montaje_documentaria.usuario_creacion')
        ->leftJoin('adjunto', 'montaje_documentaria.adjunto_id', '=', 'adjunto.id')
        ->select('montaje_documentaria.*','usuario.usuario as usuario','adjunto.nombre as tipo_adjunto',DB::raw('"Montaje" as faseAdjuntosProyectos'));

        $datos = $this->model
        ->where("proyecto.id","=", $data["id"])
        ->where("requerimiento.estado","=",1)
        ->leftJoin('requerimiento', 'proyecto.id', '=','requerimiento.proyecto_id')
        ->leftJoin('usuario', 'usuario.id', '=','requerimiento.usuario_creacion')
        ->leftJoin('adjunto', 'requerimiento.adjunto_id', '=', 'adjunto.id')

        ->select('requerimiento.*','usuario.usuario as usuario','adjunto.nombre as tipo_adjunto',DB::raw('"Requerimiento" as faseAdjuntosProyectos'))
        ->union($datos["presupuesto"])
        ->union($datos["cotizacion"])
        ->union($datos["pedido"])
        ->union($datos["diseno"])
        ->union($datos["produccion"])
        ->union($datos["montaje"])
        ->orderby('descripcion')
        ->get();

        /*
        ->where("requerimiento.estado","=", 1)
        ->where("presupuesto_documentaria.estado","=", 1)
        ->where("cotizacion_documentaria.estado","=", 1)
        ->where("pedido_documentaria.estado","=", 1)
        ->where("diseno_documentaria.estado","=", 1)
        ->where("produccion_documentaria.estado","=", 1)
        ->where("montaje_documentaria.estado","=", 1)


        ->leftJoin('requerimiento', 'proyecto.id', '=', DB::raw('requerimiento.proyecto_id AND requerimiento.estado = 1'))
        ->leftJoin('presupuesto_documentaria', 'proyecto.id', '=', DB::raw('presupuesto_documentaria.proyecto_id AND presupuesto_documentaria.estado = 1'))
        ->leftJoin('cotizacion_documentaria', 'proyecto.id', '=', DB::raw('cotizacion_documentaria.proyecto_id AND cotizacion_documentaria.estado = 1'))
        ->leftJoin('pedido_documentaria', 'proyecto.id', '=', DB::raw('pedido_documentaria.proyecto_id AND pedido_documentaria.estado = 1'))
        ->leftJoin('diseno_documentaria', 'proyecto.id', '=', DB::raw('diseno_documentaria.proyecto_id AND diseno_documentaria.estado = 1'))
        ->leftJoin('produccion_documentaria', 'proyecto.id', '=', DB::raw('produccion_documentaria.proyecto_id AND produccion_documentaria.estado = 1'))
        ->leftJoin('montaje_documentaria', 'proyecto.id', '=', DB::raw('montaje_documentaria.proyecto_id AND montaje_documentaria.estado = 1'))
   
       
        ->select(   'requerimientso.*', 
                    'presupuesto_documentaria.*', 
                    'cotizacion_documentaria.*', 
                    'pedido_documentaria.*', 
                    'diseno_documentaria.*', 
                    'produccion_documentaria.*', 
                    'montaje_documentaria.*'
                   )

  
        ->orderBy('proyecto.nombre', 'asc')
        ->get();*/

        return $datos;
    }

    public function cargarCronogramaProyectos(){

        $proyectos = $this->model
        ->where("proyecto.estado", 1)
        ->leftJoin('cliente', 'proyecto.cliente_id', '=', 'cliente.id')
        ->select('proyecto.id',
                    'proyecto.nombre as name',
                    'cliente.descripcion as details',
                    DB::raw('"blue" as color'),
                    DB::raw("DATE_FORMAT(proyecto.fecha_creacion,'%Y-%m-%d') as start"),
               /*
                DB::raw("IFNULL(CASE WHEN ((SELECT produccion_cronograma.fecha_inicio FROM produccion_cronograma WHERE produccion_cronograma.proyecto_id =proyecto.id limit 1)
                              <  (SELECT montaje_cronograma.fecha_inicio FROM montaje_cronograma WHERE montaje_cronograma.proyecto_id =proyecto.id limit 1)) THEN  
                              (SELECT produccion_cronograma.fecha_inicio FROM produccion_cronograma WHERE produccion_cronograma.proyecto_id =proyecto.id limit 1) ELSE
                              (SELECT montaje_cronograma.fecha_inicio FROM montaje_cronograma WHERE montaje_cronograma.proyecto_id =proyecto.id limit 1) END,DATE_FORMAT(proyecto.fecha_creacion,'%Y-%m-%d')) as start"),
*/
                DB::raw("IFNULL(CASE WHEN ((SELECT produccion_cronograma.fecha_final FROM produccion_cronograma WHERE produccion_cronograma.proyecto_id =proyecto.id limit 1)
                <  (SELECT montaje_cronograma.fecha_final FROM montaje_cronograma WHERE montaje_cronograma.proyecto_id =proyecto.id limit 1)) THEN 
                (SELECT montaje_cronograma.fecha_final FROM montaje_cronograma WHERE montaje_cronograma.proyecto_id =proyecto.id limit 1) ELSE
                (SELECT produccion_cronograma.fecha_final FROM produccion_cronograma WHERE produccion_cronograma.proyecto_id =proyecto.id limit 1) END,DATE_FORMAT(proyecto.fecha_creacion,'%Y-%m-%d')) as end")
        )
        ->get();
        $id=1;

        $fecha_inicio = '2050-01-01';
        $fecha_fin = '2000-01-01';


        foreach ($proyectos as $key => $value) {
           
           $datos["items"][$key]= array(
               "id" => $id,
               "name" => $value["name"],
               "details" => $value["details"],
               "color" => $value["color"],
               "start" => $value["start"],
               "end" => $value["end"]
           );
           if($value["start"]<$fecha_inicio){
            $fecha_inicio= $value["start"];
           }
           if($value["end"]>$fecha_fin){
            $fecha_fin= $value["end"];
           }

          $id++;
        }

        $datos["cabecera"]= array(
"fecha_inicio" => $fecha_inicio,
"fecha_fin" => $fecha_fin
        );

        return $datos;

    }

}
