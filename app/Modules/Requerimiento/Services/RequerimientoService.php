<?php

namespace App\Modules\Requerimiento\Services;

use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Requerimiento\Models\Requerimiento;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class RequerimientoService
{


    public function __construct()
    {
        $this->model = new Requerimiento();
        $this->model_proyecto = new Proyecto();

    }

    public function cargarAll($data){

        $datos["adjuntos"] = $this->model->where('requerimiento.proyecto_id',$data["id"])
        ->leftJoin('adjunto', 'requerimiento.adjunto_id', '=', 'adjunto.id')
        ->select('requerimiento.*', 
       
        'adjunto.nombre as tipo_adjunto')
        ->orderBy('requerimiento.id', 'desc')
        ->get();
        
        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select(
                    'proyecto.*', 
                    'tracking.requerimiento as estado_tracking'
                )
        ->first();
        return $datos;
    }

    public function guardar($data)
    {
        
        $save = 0;
        try {
            Log::info("RequerimientoService.php::guardar");
            $data["usuario_creacion"] = auth()->user()["id"];
            $data["usuario_edicion"] = auth()->user()["id"];
            $adjunto_file = $data["adjunto_file"];
            unset($data["adjunto_file"]);
            $save = $this->model->create($data);

            if(  isset($data["adjunto"]) && $data["adjunto"]!='null' ){
                $path = Storage::putFileAs(
                    config('app.upload_paths.requerimientos.adjuntos'), $adjunto_file, $save->id.'-'.$data["adjunto"]
                );
                if(!$path){
                    Log::debug('RequerimientoService.php::guardar::!$path');
                    Log::debug($path);
                    return 0;
                }
            }
        } catch (Exception $th) {
            Log::debug("RequerimientoService.php::guardar::Exception");
            Log::debug($th);
        }
        

        return $save;


    }

    public function buscar($data){

        $datos =  $this->model->all()->where('id', $data["id"])->first();
        return $datos;
    }


    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update($data);

        return $update?1:$update;

    }

    public function updatefile($data){

        
        if ($data["adjunto_file"]!='null') {
            $fileSearch = $this->model->find($data['id'])->adjunto;
            $exists = Storage::disk('local')->exists(config('app.upload_paths.requerimientos.adjuntos').'/'.$data['id'].'-'.$fileSearch);
            if ($exists) {
                $delete = Storage::disk('local')->delete(config('app.upload_paths.requerimientos.adjuntos').'/'.$data['id'].'-'.$fileSearch);
            }
            $adjunto_file = $data["adjunto_file"];
            $path = Storage::disk('local')->putFileAs(
                config('app.upload_paths.requerimientos.adjuntos'), $adjunto_file, $data['id'].'-'.$data["adjunto"]
            );
        }
        return "true";
}

    public function eliminar($data){
        $data["usuario_edicion"] = auth()->user()["id"];
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');

        // elimina el archivo
        $archivo = $this->model->find($data['id']);

        $exists = Storage::disk('local')->exists(config('app.upload_paths.requerimientos.adjuntos').'/'.$data['id'].'-'.$archivo->adjunto);

        if ($exists) {
            $delete = Storage::disk('local')->delete(config('app.upload_paths.requerimientos.adjuntos').'/'.$data['id'].'-'.$archivo->adjunto);
        }
          // elimina el adjunto
        $update = $archivo->delete();
        return $update?1:$update;
    }

    public function download($id)
    {
        $datos =  $this->model->all()->where('id', $id)->first();
        return $datos;
    }




}
