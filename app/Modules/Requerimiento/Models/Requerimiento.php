<?php

namespace App\Modules\Requerimiento\Models;

use Illuminate\Database\Eloquent\Model;

class Requerimiento extends Model
{
    protected $fillable = [
        'proyecto_id',
        'adjunto_id',
        'descripcion',
                                'monto',
                                'observacion',
                                'adjunto',
                                'estado',
                                'usuario_creacion',
                                'usuario_edicion',
                                'fecha_creacion',
                                'fecha_modificacion'
                            ];
    protected $table = 'requerimiento';
    public $timestamps = false;
}
