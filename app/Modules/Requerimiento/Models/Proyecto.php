<?php

namespace App\Modules\Requerimiento\Models;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    protected $fillable = [
                            'nombre',
                            'codigo',
                            'producto_id',
                            'montaje',
                            'cliente_id',
                            'estado',
                            'usuario_creacion',
                            'usuario_edicion',
                            'fecha_creacion',
                            'fecha_modificacion'
                        ];
    protected $table = 'proyecto';
    public $timestamps = false;
}
 