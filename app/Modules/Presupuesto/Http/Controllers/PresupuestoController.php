<?php

namespace App\Modules\Presupuesto\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Presupuesto\Services\PresupuestoService as MainService;

class PresupuestoController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }

    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }
    
    public function cargarItemsConsolidado(Request $request)
    {
        $data = $this->service->cargarItemsConsolidado($request);
        return $data;
    }

    public function actualizarMargen(Request $request)
    {
        $data = $this->service->actualizarMargen($request);
        return $data;
    }

    public function completarPresupuesto(Request $request)
    {
        $data = $this->service->completarPresupuesto($request);
        return $data;
    }
}
