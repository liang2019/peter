<?php

namespace App\Modules\Presupuesto\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Presupuesto\Services\InsumoService as MainService;
use App\Modules\Presupuesto\Http\Requests\ItemInsumoRequest as MainRequest;
use Illuminate\Support\Facades\DB;
class PresupuestoItemInsumoController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarMateriales()
    {
        $data = $this->service->cargarMateriales();
        return $data;
    }

    public function cargarAll(Request $request)
    {
        $data = $this->service->cargarAll($request);
        return $data;
    }

    public function guardar(MainRequest $request)
    {
        $form = $request->validated();
        $save = $this->service->guardar($form);
        return $save;

    }

    public function guardarInsumo($request)
    {
        $data = $this->service->guardarInsumo($request);
        return $data;
    }

    public function guardarProcesos($request)
    {
        $data = $this->service->guardarProcesos($request);
        return $data;
    }

    public function buscar(Request $request)
    {
        $data = $this->service->buscar($request);
        return $data;
    }

    public function editar(MainRequest $request)
    {
        $form = $request->validated();
        $data = $this->service->editar($form);
        return $data;
    }

    public function editarProcesos(Request $request)
    {
        $data = $this->service->editarProcesos($request);
        return $data;
    }

    public function eliminar(Request $request)
    {
        $data = $this->service->eliminar($request);
        return $data;
    }

    public function cargarProcesos(Request $request)
    {
        $data = $this->service->cargarProcesos($request);
        return $data;
    }
    
    public function importar(Request $request)
    {
        $data = $this->service->importar($request);
        return $data;
    }
}
