<?php

namespace App\Modules\Presupuesto\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Presupuesto\Services\UtilidadService as MainService;

class PresupuestoUtilidadController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarItems(Request $request)
    {
        $data = $this->service->cargarItems($request);
        return $data;
    }

    public function buscar(Request $request)
    {
        $data = $this->service->buscar($request);
        return $data;
    }

    public function actualizar(Request $request)
    {
        $data = $this->service->actualizar($request);
        return $data;
    }

    public function completarEtapa(Request $request)
    {
        $data = $this->service->completarEtapa($request);
        return $data;
    }

}
