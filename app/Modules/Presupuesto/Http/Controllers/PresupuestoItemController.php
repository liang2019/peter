<?php

namespace App\Modules\Presupuesto\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Presupuesto\Http\Requests\ItemRequest as MainRequest;
use App\Modules\Presupuesto\Services\ItemService as MainService;

class PresupuestoItemController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarAll(Request $request)
    {
        $data = $this->service->cargarAll($request);
        return $data;
    }

    public function guardar(MainRequest $request)
    {
        $form = $request->validated();
        $save = $this->service->guardar($form);
        return $save;
    }

    public function buscar(Request $request)
    {
        $data = $this->service->buscar($request);
        return $data;
    }

    public function editar(MainRequest $request)
    {
        $form = $request->validated();
        $data = $this->service->editar($form);
        return $data;
    }

    public function eliminar(Request $request)
    {
        $data = $this->service->eliminar($request);
        return $data;
    }

    public function completarRegistroItems(Request $request)
    {
        $data = $this->service->completarRegistroItems($request);
        return $data;
    }

    public function eliminarTodos(Request $request)
    {
        $data = $this->service->eliminarTodos($request);
        return $data;
    }

}
