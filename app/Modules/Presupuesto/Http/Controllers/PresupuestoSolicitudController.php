<?php

namespace App\Modules\Presupuesto\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Presupuesto\Services\SolicitudService as MainService;

class PresupuestoSolicitudController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarSolicitud(Request $request)
    {
        $data = $this->service->cargarSolicitud($request);
        return $data;
    }

    public function completarSolicitudAbastecimiento(Request $request)
    {
        $data = $this->service->completarSolicitudAbastecimiento($request);
        return $data;
    }

    public function actualizarPrecio(Request $request)
    {
        $data = $this->service->actualizarPrecio($request);
        return $data;
    }
}
