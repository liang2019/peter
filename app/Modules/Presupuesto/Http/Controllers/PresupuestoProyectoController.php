<?php

namespace App\Modules\Presupuesto\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Presupuesto\Services\ProyectoService as MainService;
class PresupuestoProyectoController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }
    
    public function cargarAll()
    {
        $data = $this->service->cargarAll();
        return $data;
    }

    public function cargarInsumos(Request $request)
    {
        $data = $this->service->cargarInsumos($request);
        return $data;
    }

    public function cancelar(Request $request)
    {
        $data = $this->service->cancelar($request);
        return $data;
    }


    
}
