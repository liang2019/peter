<?php

namespace App\Modules\Presupuesto\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Presupuesto\Services\MaterialesService as MainService;

class PresupuestoMaterialesController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }
    
    public function cargarMateriales(Request $request)
    {
        $data = $this->service->cargarMateriales($request);
        return $data;
    }

    public function guardar(Request $request)
    {
        $data = $this->service->guardar($request);
        return $data;
    }

    public function completarListaMateriales(Request $request)
    {
        $data = $this->service->completarListaMateriales($request);
        return $data;
    }

   

}
