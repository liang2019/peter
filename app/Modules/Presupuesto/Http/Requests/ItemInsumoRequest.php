<?php

namespace App\Modules\Presupuesto\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemInsumoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'PATCH':
                $rules = $this->rules_editar();
                break;
            default:
                $rules = $this->rules_guardar();
                break;
        }
        return $rules;
    }

    public function rules_guardar(){
        return [

            'proyecto_id' => 'required',
            'item_id' => 'required',
            'factor_id' => 'required',
            'elemento' => 'required',
            'cantidad'=> 'required',
            'longitud'=> 'required',
            'ancho'=> 'required',
            'caras'=> 'required',

            'area_real'=> 'required',
            'metrado_pieza'=> 'required',
            'metrado_parcial'=> 'required',
            'peso_parcial'=> 'required',
            'factor_area'=> 'required',
            'area_unitario'=> 'required',
            'area_parcial'=> 'required',

            'costo_acumulado'=> 'required',
            'costo_parcial'=> 'required',

            'precio'=> 'required',
            'procesos'=>'nullable'
        ];
    }

    public function rules_editar(){
        return [

            'id' => 'required',
            'proyecto_id' => 'required',
            'item_id' => 'required',
            'factor_id' => 'required',
            'elemento' => 'required',
            'cantidad'=> 'required',
            'longitud'=> 'required',
            'ancho'=> 'required',
            'caras'=> 'required',

            'area_real'=> 'required',
            'metrado_pieza'=> 'required',
            'metrado_parcial'=> 'required',
            'peso_parcial'=> 'required',
            'factor_area'=> 'required',
            'area_unitario'=> 'required',
            'area_parcial'=> 'required',

            'costo_acumulado'=> 'required',
            'costo_parcial'=> 'required',
            'procesos'=>'nullable'

        ];
    }
}
