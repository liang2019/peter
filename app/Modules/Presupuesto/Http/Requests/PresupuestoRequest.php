<?php

namespace App\Modules\Presupuesto\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PresupuestoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'PATCH':
                $rules = $this->rules_editar();
                break;
            default:
                $rules = $this->rules_guardar();
                break;
        }
        return $rules;
    }

    public function rules_guardar(){
        return [

            'proyecto_id' => 'required',
            'adjunto_id' => 'required',
            'monto' => 'nullable',
            'descripcion' => 'required',
            'observacion' => 'nullable',
            'adjunto' => 'required',
            'adjunto_file' => 'nullable',

        ];
    }

    public function rules_editar(){
        return [

            'id' => 'required',
            'proyecto_id' => 'required',
            'adjunto_id' => 'required',
            'monto' => 'nullable',
            'descripcion' => 'required',
            'observacion' => 'nullable',
            'adjunto' => 'required',
            'adjunto_file' => 'nullable',

        ];
    }
}
