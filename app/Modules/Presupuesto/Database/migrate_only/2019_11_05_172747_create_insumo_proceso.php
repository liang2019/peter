<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsumoProceso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insumo_proceso', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('item_insumo_id')->unsigned();
            $table->foreign('item_insumo_id')->references('id')->on('item_insumo')->onDelete('cascade');
            $table->bigInteger('proceso_id')->unsigned();
            $table->foreign('proceso_id')->references('id')->on('proceso')->onDelete('cascade');
            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion');
            $table->unsignedInteger('usuario_edicion');
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->timestamp('fecha_modificacion')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insumo_proceso');
    }
}
