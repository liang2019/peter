<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemInsumo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_insumo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('proyecto_id')->unsigned();
            $table->foreign('proyecto_id')->references('id')->on('proyecto')->onDelete('cascade');
            $table->bigInteger('item_id')->unsigned();
            $table->foreign('item_id')->references('id')->on('item')->onDelete('cascade');
            $table->bigInteger('factor_id')->unsigned();
            $table->foreign('factor_id')->references('id')->on('factor');
            $table->string('elemento', 191);
            $table->integer('cantidad');
            $table->double('longitud', 15, 4);
            $table->double('ancho', 15, 4);
            $table->integer('caras');

            $table->double('area_real', 15, 4)->nullable();
            $table->double('metrado_pieza', 15, 4)->nullable();
            $table->double('metrado_parcial', 15, 4)->nullable();
            $table->double('peso_parcial', 15, 4)->nullable();
            $table->double('factor_area', 15, 4)->nullable();
            $table->double('area_unitario', 15, 4)->nullable();
            $table->double('area_parcial', 15, 4)->nullable();
         
            $table->double('costo_acumulado', 15, 4)->nullable();
            $table->double('costo_parcial', 15, 4)->nullable();
            $table->double('metrado_material', 15, 4)->nullable();
            $table->double('metrado_material_2', 15, 4)->nullable();
            $table->double('costo_material', 15, 4)->nullable();
            $table->double('costo_material_2', 15, 4)->nullable();
       

            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion');
            $table->unsignedInteger('usuario_edicion');
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->timestamp('fecha_modificacion')->useCurrent(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_insumo');
    }
}
