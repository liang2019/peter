<?php

namespace App\Modules\Presupuesto\Models;

use Illuminate\Database\Eloquent\Model;

class Utilidad extends Model
{
    protected $fillable = [
        'proyecto_id',
        'item_id',
        'porcentaje',
        'precio_unitario',
        'precio_parcial',
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'utilidad';
    public $timestamps = false;
}
