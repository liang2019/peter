<?php

namespace App\Modules\Presupuesto\Models;

use Illuminate\Database\Eloquent\Model;

class InsumoProceso extends Model
{
    protected $fillable = [
        'item_insumo_id',
        'proceso_id',

        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'insumo_proceso';
    public $timestamps = false;
}
