<?php

namespace App\Modules\Presupuesto\Models;

use Illuminate\Database\Eloquent\Model;

class Insumo extends Model
{
    protected $fillable = [
        'proyecto_id',
        'factor_id',

        'precio',
        'precio_almovd',
        'porcentaje',
       'verificado',
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'insumo';
    public $timestamps = false;
}
