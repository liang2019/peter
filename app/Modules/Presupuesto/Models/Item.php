<?php

namespace App\Modules\Presupuesto\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
                            'proyecto_id',
                            'nombre',
                            'cantidad',
                            'margen',
                            'habilitado',
                            'estado',
                            'usuario_creacion',
                            'usuario_edicion',
                            'fecha_creacion',
                            'fecha_modificacion'
                        ];
    protected $table = 'item';
    public $timestamps = false;
}
 