<?php

namespace App\Modules\Presupuesto\Models;

use Illuminate\Database\Eloquent\Model;

class PresupuestoDocumentaria extends Model
{
    protected $fillable = [
        'proyecto_id',
        'adjunto_id',
        'descripcion',
        'monto',
        'observacion',
        'adjunto',
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
protected $table = 'presupuesto_documentaria';
public $timestamps = false;

}
