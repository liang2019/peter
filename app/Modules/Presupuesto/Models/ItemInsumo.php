<?php

namespace App\Modules\Presupuesto\Models;

use Illuminate\Database\Eloquent\Model;

class ItemInsumo extends Model
{
    protected $fillable = [
        'proyecto_id',
        'item_id',
        'factor_id',

        'elemento',
        'cantidad',
        'longitud',
        'ancho',
        'caras',

        'area_real',
        'metrado_pieza',
        'metrado_parcial',
        'peso_parcial',
        'factor_area',
        'area_unitario',
        'area_parcial',

        'costo_acumulado',
        'costo_parcial',
        'metrado_material',
        'metrado_material_2',
        'costo_material',
        'costo_material_2',

        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'item_insumo';
    public $timestamps = false;
}
