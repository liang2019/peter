<?php

namespace App\Modules\Presupuesto\Models;

use Illuminate\Database\Eloquent\Model;

class ItemServicio extends Model
{
    protected $fillable = [
        'proyecto_id',
        'descripcion',
        'precio',
        'cantidad',
        'verificado',

        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'item_servicio';
    public $timestamps = false;
}
