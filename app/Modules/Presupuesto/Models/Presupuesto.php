<?php

namespace App\Modules\Presupuesto\Models;

use Illuminate\Database\Eloquent\Model;

class Presupuesto extends Model
{
    protected $fillable = [
                            'proyecto_id',
                            'etapa',
                            'estado',
                            'usuario_creacion',
                            'usuario_edicion',
                            'fecha_creacion',
                            'fecha_modificacion'
                        ];
    protected $table = 'presupuesto';
    public $timestamps = false;
}
 
