<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'auth'],function () {
    Route::group(['prefix' => 'presupuesto'], function () {

        //lista de proyectos
        Route::get('/proyectos', ['as' => 'proyectos', 'uses' => 'PresupuestoProyectoController@index']);
        Route::get('/proyectos/cargar', ['as' => 'proyectos.cargar', 'uses' => 'PresupuestoProyectoController@cargarAll']);
        Route::post('/proyectos/cancelar/', ['as' => 'proyectos.cancelar', 'uses' => 'PresupuestoProyectoController@cancelar']);
        Route::post('/proyectos/insumos/', ['as' => 'proyectos.cargarInsumos', 'uses' => 'PresupuestoProyectoController@cargarInsumos']);

       // Route::post('/proyectos/completar/', ['as' => 'proyectos.completar', 'uses' => 'PresupuestoProyectoController@completar']);

        //items
        Route::get('/items/{id}', ['as' => 'item.id', 'uses' => 'PresupuestoItemController@index']);
        Route::post('/items/cargar', ['as' => 'item.id', 'uses' => 'PresupuestoItemController@cargarAll']);
        Route::post('/items/guardar', ['as' => 'item.guardar', 'uses' => 'PresupuestoItemController@guardar']);
        Route::post('/items/buscar', ['as' => 'item.buscar', 'uses' => 'PresupuestoItemController@buscar']);
        Route::patch('/items/editar', ['as' => 'item.editar', 'uses' => 'PresupuestoItemController@editar']);
        Route::post('/items/eliminar', ['as' => 'item.eliminar', 'uses' => 'PresupuestoItemController@eliminar']);
        Route::post('/items/eliminartodos', ['as' => 'item.eliminar', 'uses' => 'PresupuestoItemController@eliminarTodos']);
        Route::post('/items/completar', ['as' => 'item.completar', 'uses' => 'PresupuestoItemController@completarRegistroItems']);


        //insumos
        Route::get('/insumos/{id}', ['as' => 'insumo.id', 'uses' => 'PresupuestoItemInsumoController@index']);
        Route::get('/cargarmateriales', ['as' => 'insumos.cargarMateriales', 'uses' => 'PresupuestoItemInsumoController@cargarMateriales']);
        Route::post('/insumos/cargar', ['as' => 'insumo.id', 'uses' => 'PresupuestoItemInsumoController@cargarAll']);
        Route::post('/insumos/guardar', ['as' => 'insumo.guardar', 'uses' => 'PresupuestoItemInsumoController@guardar']);
        Route::post('/insumos/guardarprocesos', ['as' => 'insumo.guardarProcesos', 'uses' => 'PresupuestoItemInsumoController@guardarProcesos']);
        Route::post('/insumos/guardarinsumo', ['as' => 'insumo.guardarinsumo', 'uses' => 'PresupuestoItemInsumoController@guardarInsumo']);
        Route::post('/insumos/buscar', ['as' => 'insumo.buscar', 'uses' => 'PresupuestoItemInsumoController@buscar']);
        Route::patch('/insumos/editar', ['as' => 'insumo.editar', 'uses' => 'PresupuestoItemInsumoController@editar']);
        Route::post('/insumos/editarprocesos', ['as' => 'insumo.editarProcesos', 'uses' => 'PresupuestoItemInsumoController@editarProcesos']);
        Route::post('/insumos/eliminar', ['as' => 'insumo.eliminar', 'uses' => 'PresupuestoItemInsumoController@eliminar']);
        Route::post('/insumos/procesos', ['as' => 'insumo.procesos', 'uses' => 'PresupuestoItemInsumoController@cargarProcesos']);
        Route::post('/insumos/importar', ['as' => 'insumo.importar', 'uses' => 'PresupuestoItemInsumoController@importar']);

        //documentaria
        Route::get('/documentaria', ['as' => 'documentaria', 'uses' => 'PresupuestoAdjuntoController@index']);

        Route::post('/documentaria/cargar', ['as' => 'documentaria.cargar', 'uses' => 'PresupuestoAdjuntoController@cargarAll']);
        Route::post('/documentaria/buscar/', ['as' => 'documentaria.buscar', 'uses' => 'PresupuestoAdjuntoController@buscar']);
        Route::patch('/documentaria/editar/', ['as' => 'documentaria.editar', 'uses' => 'PresupuestoAdjuntoController@editar']);
        Route::post('/documentaria/eliminar/', ['as' => 'documentaria.eliminar', 'uses' => 'PresupuestoAdjuntoController@eliminar']);
        Route::post('/documentaria/guardar/', ['as' => 'documentaria.guardar', 'uses' => 'PresupuestoAdjuntoController@guardar']);
        Route::post('/documentaria/updatefile/', ['as' => 'documentaria.updatefile', 'uses' => 'PresupuestoAdjuntoController@updatefile']);
        Route::get('/documentaria/download/{id}', ['as' => 'documentaria.download', 'uses' => 'PresupuestoAdjuntoController@download']);
        Route::get('/documentaria/{id}', ['as' => 'documentaria.id', 'uses' => 'PresupuestoAdjuntoController@index']);

        //lista Materiales
        Route::get('/materiales', ['as' => 'proyectos', 'uses' => 'PresupuestoProyectoController@index']);
        Route::get('/materiales/proyectos', ['as' => 'materiales.proyectos', 'uses' => 'PresupuestoProyectoController@cargarAll']);
        Route::get('/materiales/{id}', ['as' => 'materiales.consolidado', 'uses' => 'PresupuestoProyectoController@index']);
        Route::post('/materiales/lista', ['as' => 'materiales.lista', 'uses' => 'PresupuestoMaterialesController@cargarMateriales']);
        Route::post('/materiales/guardar', ['as' => 'materiales.guardar', 'uses' => 'PresupuestoMaterialesController@guardar']);
        Route::post('/materiales/completarlista', ['as' => 'materiales.completar', 'uses' => 'PresupuestoMaterialesController@completarListaMateriales']);

        //solicitud de abastecimiento
        Route::get('/solicitud', ['as' => 'proyectos', 'uses' => 'PresupuestoSolicitudController@index']);
        Route::post('/solicitud/materiales', ['as' => 'solicitud.lista', 'uses' => 'PresupuestoSolicitudController@cargarSolicitud']);
        Route::post('/solicitud/actualizarPrecio', ['as' => 'solicitud.completar', 'uses' => 'PresupuestoSolicitudController@actualizarPrecio']);
        Route::post('/solicitud/completarsolicitud', ['as' => 'solicitud.completarSolicitud', 'uses' => 'PresupuestoSolicitudController@completarSolicitudAbastecimiento']);
        Route::get('/solicitud/{id}', ['as' => 'solicitud.consolidado', 'uses' => 'PresupuestoSolicitudController@index']);

         //utilidad y gastos
         Route::get('/utilidad', ['as' => 'proyectos', 'uses' => 'PresupuestoUtilidadController@index']);
         Route::post('/utilidad/items', ['as' => 'utilidad.lista', 'uses' => 'PresupuestoUtilidadController@cargarItems']);
         Route::post('/utilidad/buscar', ['as' => 'utilidad.buscar', 'uses' => 'PresupuestoUtilidadController@buscar']);
         Route::patch('/utilidad/actualizar', ['as' => 'utilidad.lista', 'uses' => 'PresupuestoUtilidadController@actualizar']);
         Route::patch('/utilidad/completar', ['as' => 'utilidad.completarEtapa', 'uses' => 'PresupuestoUtilidadController@completarEtapa']);
         Route::get('/utilidad/{id}', ['as' => 'utilidad.consolidado', 'uses' => 'PresupuestoUtilidadController@index']);


        //Presupuesto
        Route::get('/', ['as' => 'presupuesto', 'uses' => 'PresupuestoController@index']);
        Route::post('/consolidado', ['as' => 'presupuesto.items', 'uses' => 'PresupuestoController@cargarItemsConsolidado']);
        Route::post('/consolidado/margen', ['as' => 'presupuesto.actualizarMargen', 'uses' => 'PresupuestoController@actualizarMargen']);
        Route::post('/consolidado/completar', ['as' => 'presupuesto.completar', 'uses' => 'PresupuestoController@completarPresupuesto']);
        Route::get('/consolidado/{id}', ['as' => 'presupuesto.consolidado', 'uses' => 'PresupuestoController@index']);

        //servicios
        Route::get('/servicio/{id}', ['as' => 'servicio', 'uses' => 'PresupuestoServiciosController@index']);
        Route::post('/servicio/cargar', ['as' => 'servicio.cargar', 'uses' => 'PresupuestoServiciosController@cargarAll']);
        Route::post('/servicio/buscar/', ['as' => 'servicio.buscar', 'uses' => 'PresupuestoServiciosController@buscar']);
        Route::patch('/servicio/editar/', ['as' => 'servicio.editar', 'uses' => 'PresupuestoServiciosController@editar']);
        Route::post('/servicio/guardar', ['as' => 'servicio.guardar', 'uses' => 'PresupuestoServiciosController@guardar']);
        Route::post('/servicio/eliminar/', ['as' => 'servicio.eliminar', 'uses' => 'PresupuestoServiciosController@eliminar']);
        Route::post('/servicio/actualizarPrecio', ['as' => 'solicitud.actualizar', 'uses' => 'PresupuestoServiciosController@actualizarPrecio']);

    });

    Route::group(['prefix' => 'valorizacione'], function () {

        //lista de proyectos
        Route::get('/proyectos', ['as' => 'proyectos', 'uses' => 'PresupuestoProyectoController@index']);
        Route::get('/proyectos/all', ['as' => 'proyectos.cargar', 'uses' => 'PresupuestoProyectoController@cargarAll']);
    });
});
