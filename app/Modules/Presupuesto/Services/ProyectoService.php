<?php

namespace App\Modules\Presupuesto\Services;

use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Tracking\Models\Tracking;
use App\Modules\Presupuesto\Models\Presupuesto;
use App\Modules\Presupuesto\Models\ItemInsumo;
use Illuminate\Support\Facades\DB;
class ProyectoService
{


    public function __construct()
    {
        $this->model = new Proyecto();
        $this->model_presupuesto = new Presupuesto();
        $this->model_tarcking = new Tracking();
        $this->model_item_insumo = new ItemInsumo();
    }

    public function cargarAll(){

        $datos = $this->model
        ->where("tracking.presupuesto","<>", 3)
      
        ->leftJoin('cliente', 'proyecto.cliente_id', '=', 'cliente.id')
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->leftJoin('presupuesto', 'proyecto.id', '=', 'presupuesto.proyecto_id')
        ->select('proyecto.*',
        'cliente.descripcion as nombre_cliente',
        'tracking.presupuesto as estado_tracking',
        'presupuesto.etapa as estado_presupuesto',
        DB::raw('(SELECT usuario from usuario WHERE usuario.id = proyecto.usuario_creacion) as user'),
        /*DB::raw("CASE WHEN presupuesto.etapa=1 AND tracking.presupuesto=1 THEN (
            IFNULL((SELECT count(item_insumo.item_id) from item_insumo  WHERE item_insumo.estado = 1 AND
            item_insumo.proyecto_id = proyecto.id
             GROUP BY item_insumo.item_id), 0) 
        ) END as 'items_insumos'"),*/
        DB::raw("CASE WHEN presupuesto.etapa=1 AND tracking.presupuesto=1 THEN (
            IFNULL((SELECT count(item.id) from item WHERE item.estado = 1 AND
            item.habilitado = 1 AND item.proyecto_id = proyecto.id ), 0) 
        ) END as 'items_cantidad'"),
        DB::raw("CASE WHEN presupuesto.etapa=3 AND tracking.presupuesto=1 THEN (
            IFNULL((SELECT count(insumo.id) from insumo WHERE insumo.estado = 1 AND
            insumo.verificado = 0 AND insumo.proyecto_id = proyecto.id ), 0) 
        ) END as 'insumos_sin_verificar'"),
        DB::raw("CASE WHEN presupuesto.etapa=3 AND tracking.presupuesto=1 THEN (
            IFNULL((SELECT count(item_servicio.id) from item_servicio WHERE item_servicio.estado = 1 AND
            item_servicio.verificado = 0 AND item_servicio.proyecto_id = proyecto.id ), 0) 
        ) END as 'servicios_sin_verificar'")
        )
        ->orderBy('proyecto.id', 'desc')
        ->get();
        return $datos;
    }

    public function cargarInsumos($data){
        //carga todos los insumos pertenecientes al proyecto
        $datos= $this->model_item_insumo->where('item_insumo.proyecto_id', $data["id"])
        ->leftJoin('factor', 'item_insumo.factor_id', '=', 'factor.id')
        ->leftJoin('alarti', 'factor.alarti_id', '=', 'alarti.id')
        ->select('item_insumo.*', 
                'alarti.descripcion as nombre_material', 
                DB::raw("(SELECT COUNT(id) from insumo_proceso WHERE item_insumo_id = item_insumo.id ) as cantidad_procesos"),
                'alarti.codigo as codigo_material'
        )
        
        ->get();

        return $datos;
    }

    public function cancelar($data){

        $data["usuario_edicion"] = auth()->user()["id"];
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update(['estado' => 0]);
         //actualizar estado del tracking
        $tracking = $this->model_tarcking->where('proyecto_id', $data['id'])
        ->update([
                    'requerimiento' => 0,
                    'presupuesto' => 0,
                    'usuario_edicion' => auth()->user()->id,
                    'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                ]);
        if(!$tracking){
            return false;
        }
        return $update?1:$update;
    }

    

    
}
