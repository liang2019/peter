<?php

namespace App\Modules\Presupuesto\Services;

use App\Modules\Presupuesto\Models\ItemInsumo;
use App\Modules\Presupuesto\Models\InsumoProceso;
use App\Modules\Presupuesto\Models\Insumo;
use App\Modules\Mantenimiento\Models\Factor;
use App\Modules\Mantenimiento\Models\Proceso;
use App\Modules\Presupuesto\Models\Item;
use Illuminate\Support\Facades\DB;
use App\Modules\Presupuesto\Imports\InsumoImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
class InsumoService
{


    public function __construct()
    {
        $this->model_insumo_proceso = new InsumoProceso();
        $this->model_item_insumo = new ItemInsumo();
        $this->model_item = new Item();
        $this->model_factor = new Factor();
        $this->model_insumo = new Insumo();
        $this->model_proceso = new Proceso();
    }

    public function cargarMateriales()
    {

        //lista de materiales para seleccionar el insumo

        $datos = $this->model_factor
        ->leftJoin('alarti', 'factor.alarti_id', '=', 'alarti.id')
        ->leftJoin('almovd', 'alarti.codigo', '=', 'almovd.codigo')
        ->leftJoin('alstoc', 'alarti.codigo', '=', 'alstoc.codigo')
        ->select(
                    'factor.*',
                    'almovd.precio_unit as precio_material',
                    'alarti.descripcion as nombre_material',
                    'alarti.codigo as codigo_material',
                    DB::raw('IFNULL(alstoc.skdis,0) as stock_material'),
                    DB::raw('IFNULL(almovd.precio_unit,0) as precio_material')
                )

        ->get();

        return $datos;
    }

    public function cargarAll($data)
    {

        // carga todos los insumos pertenemientes al item

        $datos["items"] = $this->model_item_insumo->where('item_insumo.item_id', $data["id"])

        ->leftJoin('factor', 'item_insumo.factor_id', '=', 'factor.id')
        ->leftJoin('alarti', 'factor.alarti_id', '=', 'alarti.id')
        ->select(
                    'item_insumo.*',
                    'alarti.descripcion as nombre_material',
                    DB::raw("(SELECT COUNT(id) from insumo_proceso WHERE item_insumo_id = item_insumo.id ) as cantidad_procesos"),
                    'alarti.codigo as codigo_material'
                 )
        ->orderBy('item_insumo.id', 'asc')
        ->get();

        $datos["item"] = $this->model_item->where("item.id",$data["id"])
        ->leftJoin('presupuesto', 'item.proyecto_id', '=', 'presupuesto.proyecto_id')
        ->select('item.*','presupuesto.etapa as estado_presupuesto')->first();
        return $datos;
    }

    public function guardar($data)
    {
        //guardar el insumo seleccionado junto con sus cálculos corresondientes

        DB::beginTransaction();
        try {
            $response =false;
            $data["usuario_creacion"] = auth()->user()["id"];
            $data["usuario_edicion"] = auth()->user()["id"];
            //guardar insumo
            $save = $this->model_item_insumo->create($data);
            if($save){
                if($insumo = $this->guardarInsumo($data)){
                    if (count($data["procesos"])>0) {
                        $data_= array("procesos"=>$data["procesos"],"id"=>$save["id"]);
                        $response = $this->guardarProcesos($data_);
                    }
                }
            }
            $response=true;
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        return $response?1:$response;
    }

    public function guardarInsumo($data)
    {
        //verifica si existe el insumo en la tabla insumo y lo registra junto con el precio

        $datos =  $this->model_insumo->all()
        ->where('factor_id', $data["factor_id"])
        ->where('proyecto_id', $data["proyecto_id"])
        ->first();

        if($datos==null){
        /*
            $precio = $this->model_factor
            ->leftjoin("alarti", "alarti.id", "factor.alarti_id")
            ->leftjoin("almovd", "almovd.codigo", "alarti.codigo")
            ->where('factor.id', $data["factor_id"])
            ->select("almovd.precio_unit")
            ->first();

        */        //registra el insumo
            $datos = $this->model_insumo->create(array(
                "usuario_creacion" => auth()->user()["id"],
                "usuario_edicion" => auth()->user()["id"],
                "proyecto_id" => $data["proyecto_id"],
                //  "precio_almovd" => $precio->precio_unit,
                "precio_almovd" =>  $data["precio"],
                "factor_id" => $data["factor_id"],
                "precio" => $data["precio"]
            ));
        }

        return $datos;
    }

    public function guardarProcesos($data)
    {
        //guardar procesos

        foreach ($data["procesos"] as $key => $value) {
            $save = $this->model_insumo_proceso->create(array(
                "usuario_creacion" => auth()->user()["id"],
                "usuario_edicion" => auth()->user()["id"],
                "item_insumo_id" => $data["id"],
                "proceso_id" => $value["id"]

            ));
        }
        return $save;
    }

    public function buscar($data)
    {

        //buscar insumo
        $datos["item_insumo"] =  $this->model_item_insumo->where('item_insumo.id', $data["id"])
        ->leftJoin('factor', 'item_insumo.factor_id', '=', 'factor.id')
        ->leftJoin('alarti', 'factor.alarti_id', '=', 'alarti.id')
        ->leftJoin('insumo', 'insumo.factor_id', '=', 'factor.id')
        ->select(
                    'item_insumo.*',
                    'insumo.precio as precio_material',
                    'factor.unidad',
                    'factor.kilogramo_unidad',
                    'factor.metro_unidad',
                    'factor.caras as factor_caras',
                    'alarti.descripcion as nombre_material'
                )
        ->first();

        //encuentra los procesos relacionados a este material

        $procesos =  $this->model_insumo_proceso->where('item_insumo_id', $data["id"])
        ->leftJoin('proceso', 'proceso.id', '=', 'insumo_proceso.proceso_id')
        ->select('proceso.nombre','proceso.unidad','proceso.costo','proceso.grupo','insumo_proceso.proceso_id as id','insumo_proceso.item_insumo_id')
        ->get();
        $datos["procesos"] = $procesos;
        return $datos;
    }

    public function editar($data)
    {

        DB::beginTransaction();
        try {
            $response =false;

            $data["usuario_edicion"] = auth()->user()->id;
            $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
            $update = $this->model_item_insumo->find($data['id'])->update($data);
            if ($update) {
                //if (count($data["procesos"])>0) {
                    $this->editarProcesos($data);
                //}
            }
            $response =true;

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        return $response?1:0;
    }

    public function editarProcesos($data)
    {

        //borrar los procesos
        $save = $this->model_insumo_proceso
        ->where('item_insumo_id', $data['id'])->delete();
        //guardar los proceso

        foreach ($data["procesos"] as $key => $value) {

            $save = $this->model_insumo_proceso->create(array(
                "usuario_creacion" => auth()->user()["id"],
                "usuario_edicion" => auth()->user()["id"],
                "item_insumo_id" => $data["id"],
                "proceso_id" => $value["id"]

            ));
        }
        return $save?1:$save;
    }

    public function eliminar($data)
    {

        DB::beginTransaction();
        try {
            $response =false;
            $delete = $this->model_insumo_proceso
            ->where('item_insumo_id', $data['id'])->delete();
            //borrar el material
            $delete = $this->model_item_insumo
            ->where('id', $data['id'])->delete();

            //verificar si existen mas item_insumo con el mismo factor_id
            $datos =  $this->model_item_insumo->all()
            ->where('factor_id', $data["factor_id"])
            ->where('proyecto_id', $data["proyecto_id"])
            ->first();

            if($datos==null){
                //elimina
                $delete = $this->model_insumo
                ->where('factor_id', $data["factor_id"])
                ->where('proyecto_id', $data["proyecto_id"])
                ->delete();
            }

            $response =true;

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        //borrar los procesos
        return $response?1:$delete;
    }

    public function cargarProcesos($data)
    {

        $datos = $this->model_insumo_proceso
        ->where('insumo_proceso.item_insumo_id', $data['id'])
        ->leftjoin("proceso", "proceso.id", "insumo_proceso.proceso_id")
        ->select( "proceso.nombre",
                    DB::raw('COUNT(insumo_proceso.id) as cantidad'),
                    "proceso.id"
        )
        ->groupby("proceso.nombre", "proceso.id")
        ->orderby("proceso.nombre", "asc")
        ->get();

        return $datos;

    }

    public function importar($data)
    {

        $cantidad_item = $data["item_cantidad"];

        DB::beginTransaction();
        try {
            $response =false;
            if ($data["adjunto_file"]!='null') {
                $adjunto_file = $data["adjunto_file"];
                $exp = explode(".",$data["adjunto"]);
                $nombre_file = str_replace(".".$exp[(count($exp)-1)],"",$data["adjunto"]);
            
                $path = Storage::putFileAs(
                    config('app.upload_paths.insumos.adjuntos'), $adjunto_file, 'insumos.'.$exp[count($exp)-1]
                );
                if(!$path){
                    return 0;
                }
            }
                      
            $cotejoItems = Excel::toArray(new InsumoImport, config('app.upload_paths.insumos.adjuntos').'/insumos.'.$exp[count($exp)-1]);

            $factores = $this->model_factor
            ->leftJoin('alarti', 'factor.alarti_id', '=', 'alarti.id')
            ->select(
                        'factor.unidad',
                        'alarti.codigo as codigo_material'
                    )
            ->get();
          
            $i=0;
       
            foreach ($cotejoItems[0] as $key => $value) {

                if($value[0]!=null){
                    if($i==0){
                        $i=1;
                        continue;
                    }
    
                    /*

                    0 => "CODIGO"
                    1 => "MATERIAL" no se utiliza
                    2 => "ELEMENTO" 
                    3 => "CANTIDAD"
                    4 => "LONGITUD"
                    5 => "ANCHO"
                    6 => "CARAS"
                    7 => "CodProc1"
                    8 => "CodProc2"
                    9 => "CodProc3"
                    10 => "CodProc4"
                    11 => "CodProc5"
                    12 => "CodProc6"
                    13 => "CodProc7"

                    */
                    $codigo    = $value[0];
                    $elemento = $value[2];
                    $cantidad  = $value[3];
                    $longitud  = $value[4];
                    $ancho    = $value[5];
                    $caras     = $value[6];
                    $proceso_1 = $value[7];
                    $proceso_2 = $value[8];
                    $proceso_3 = $value[9];
                    $proceso_4 = $value[10];
                    $proceso_5 = $value[11];
                    $proceso_6 = $value[12];
                    $proceso_7 = $value[13];

                    //si uno de los procesos están vacios no entran en el array
                    $procesos = [];
                    if($proceso_1!=''){
                        array_push($procesos,  array("id" => $proceso_1));
                    }

                    if($proceso_2!=''){
                        array_push($procesos,  array("id" => $proceso_2));
                    }
                    if($proceso_3!=''){
                        array_push($procesos,  array("id" => $proceso_3));
                    }
                    if($proceso_4!=''){
                        array_push($procesos,  array("id" => $proceso_4));
                    }
                    if($proceso_5!=''){
                        array_push($procesos,  array("id" => $proceso_5));
                    }
                    if($proceso_6!=''){
                        array_push($procesos,  array("id" => $proceso_6));
                    }
                    if($proceso_7!=''){
                        array_push($procesos,  array("id" => $proceso_7));
                    }
              

                    if(($codigo=='' || $codigo==null) || ($cantidad==='' || $cantidad===null) || ($longitud==='' || $longitud===null) || ($ancho==='' || $ancho===null) || ($caras==='' || $caras===null)){
                        $i++;
                        continue;
                    }

                    //encontrar el valor del factor
                    $factor = $this->model_factor
                    ->leftJoin('alarti', 'factor.alarti_id', '=', 'alarti.id')
                    ->leftJoin('almovd', 'alarti.codigo', '=', 'almovd.codigo')
                    ->where("alarti.codigo", $codigo)
                    ->select(
                                'factor.unidad',
                                'factor.kilogramo_unidad',
                                'factor.caras',
                                'factor.metro_unidad',
                                'factor.id',
                                'almovd.precio_unit as precio_material'
                            )
                    ->first();
                    
                    $unidad = $factor["unidad"];
                    $caras_factor = $factor["caras"];
                    $metro_unidad = $factor["metro_unidad"];
                    $kilogramo_unidad = $factor["kilogramo_unidad"];
                    $area_real = ($unidad=='m2')?$ancho:1;
                    $metrado_pieza = $area_real * $longitud * $cantidad;
                    $metrado_parcial = $metrado_pieza * $cantidad_item;
                    $peso_parcial = $kilogramo_unidad * $metrado_parcial;
                    $factor_area = ($caras_factor==2)? $caras: 1;//else..asume el menos valor entre caras y 1 
                    $area_unitario = $metro_unidad * $factor_area;
                    $area_parcial = $area_unitario * $metrado_parcial;

                    //sumatoria del costo de  todos los proecesos
                    $costo_acumulado = 0;
                    foreach ($procesos as $key2 => $valor) {
                       if($valor["id"]==""){
                           continue;
                       }

                        //encontrar el valor del factor
                        $proceso = $this->model_proceso->where("id", $valor["id"])->select("*")->first();

                        $costo_proceso = ( $proceso["unidad"] == "kg" ) ? $proceso["costo"] : 
                                         (( $proceso["unidad"]=="m2" && ($peso_parcial!=0) ) ? ( ($proceso["costo"] * $area_parcial) / $peso_parcial ): 0 );

                        $costo_acumulado = $costo_acumulado + $costo_proceso;
                        
                    }

                    $costo_parcial = $costo_acumulado * $peso_parcial;

                    //mandar los valores a la función guardar
                    $valores = array(
                        "proyecto_id" => $data["proyecto_id"],
                        "item_id" => $data["item_id"],
                        "factor_id"=> $factor["id"],
                        "elemento"=> $elemento,
                        "cantidad" => $cantidad,
                        "longitud" => $longitud,
                        "ancho" => $ancho,
                        "caras" => $caras,
                        "area_real" => $area_real,
                        "metrado_pieza" => $metrado_pieza,
                        "metrado_parcial" => $metrado_parcial,
                        "peso_parcial" => $peso_parcial,
                        "factor_area" => $factor_area,
                        "area_unitario" => $area_unitario,
                        "area_parcial" => $area_parcial,
                        "costo_acumulado" => $costo_acumulado,
                        "costo_parcial" => $costo_parcial,
                        "precio" =>  $factor["precio_material"],
                        "procesos"  => $procesos
                    );

                    //mandar todo a la función guardar
                    $this->guardar($valores);

                    $i++;
    
                }
            }
          

            $numero_registros = $i;

         
            //borra el archivo
            $delete = Storage::disk('local')->delete(config('app.upload_paths.insumos.adjuntos').'/insumos.'.$exp[count($exp)-1]);
           

            $response=true;
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }


        return $delete?1:$delete;
    
    }

}
