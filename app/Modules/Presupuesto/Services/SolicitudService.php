<?php

namespace App\Modules\Presupuesto\Services;

use App\Modules\Presupuesto\Models\ItemInsumo;
use App\Modules\Presupuesto\Models\Insumo;
use App\Modules\Presupuesto\Models\Presupuesto;
use App\Modules\Requerimiento\Models\Proyecto;
use Illuminate\Support\Facades\DB;
class  SolicitudService
{


    public function __construct()
    {
        $this->model = new ItemInsumo();
        $this->model_insumo = new Insumo();
        $this->model_presupuesto = new Presupuesto();
        $this->model_proyecto = new Proyecto();
    }


    public function cargarSolicitud($data){

        //Lista de valores para Lista Materiales y Solicitud de Abastecimiento
        $datos["insumos"] = $this->model_insumo->where("insumo.proyecto_id", $data["proyecto_id"])
        ->leftJoin('factor', 'insumo.factor_id', '=', 'factor.id')
        ->leftJoin('alarti', 'factor.alarti_id', '=', 'alarti.id')
        

        ->select(
                    'insumo.id',
                    'insumo.proyecto_id',
                    'insumo.precio_almovd as precio_base',
             
                    'alarti.descripcion as nombre_material',
                    'factor.id as factor_id',
                    'insumo.id as insumo_id',
                    'insumo.verificado as verificado',
                    'alarti.codigo as codigo_material',
                    'alarti.descripcion as descripcion',
                    DB::raw('"PZA" as unidad_'),
                    DB::raw("
                    CEIL(
                            (
                                (
                                    (
                                        SELECT SUM(item_insumo.metrado_parcial) from item_insumo WHERE item_insumo.factor_id = insumo.factor_id AND
                                        item_insumo.proyecto_id=".$data["proyecto_id"]." 
                                    )
                                    *(insumo.porcentaje+1)
                                )/ factor.unidad_pieza
                            )
                    )
                     as cantidad"),
                    DB::raw("(
                        (
                            (
                                (SELECT SUM(item_insumo.metrado_parcial) from item_insumo WHERE item_insumo.factor_id = insumo.factor_id AND item_insumo.proyecto_id=".$data["proyecto_id"]." )
                                *
                                (insumo.porcentaje+1)
                            )
                            /
                                factor.unidad_pieza
                        )
                        *
                        factor.kilogramo_pieza
                    ) as peso"),


                    DB::raw("(SELECT SUM(item_insumo.area_parcial) from item_insumo WHERE item_insumo.factor_id = insumo.factor_id AND item_insumo.proyecto_id=".$data["proyecto_id"]." ) as total_area_parcial"),
                    DB::raw('(IFNULL(insumo.precio,0)) as precio_insumo')
                )

        ->get();
        // tener en cuenta para la vista lo siguiente:
        // el precio que se va mostrar es igual a ( precio_insumo == 0 ) ? precio_base : precio_insumo;
        $datos["proyecto"] = $this->model_presupuesto->where("presupuesto.proyecto_id",$data["proyecto_id"])
        ->leftJoin('proyecto', 'proyecto.id', '=', 'presupuesto.proyecto_id')
        ->select('proyecto.nombre','presupuesto.etapa as estado_presupuesto')->first();
        return $datos;
    }

    public function actualizarPrecio($data)
    {

        DB::beginTransaction();
        try {
            $response =false;
            $data["verificado"] = ($data["verificado"]==null)?0:$data["verificado"];
            //esta ruta es para solicitud de abastecimiento
            $update = $this->model_insumo->find($data['insumo_id'])->update(array(
                "precio" => $data["precio"],
                "verificado" => $data["verificado"],
                'usuario_edicion' => auth()->user()->id,
                'fecha_modificacion' => now()->format('Y-m-d H:i:s')
            ));


            //Lista de valores para Lista Materiales
            $insumo = $this->model_insumo->where("insumo.id", $data["insumo_id"])
            ->leftJoin('factor', 'insumo.factor_id', '=', 'factor.id')
            ->leftJoin('alarti', 'factor.alarti_id', '=', 'alarti.id')
            ->select(
                        'insumo.id as id',
                        'alarti.codigo as codigo_material',
                        'alarti.descripcion as nombre_material',
                        DB::raw("(SELECT SUM(item_insumo.metrado_parcial) from item_insumo WHERE item_insumo.factor_id = insumo.factor_id AND item_insumo.proyecto_id=".$data["proyecto_id"]." ) as total_metrado_parcial"),
                        'factor.unidad_pieza as unidad_pieza',
                        DB::raw("CEIL((((SELECT SUM(item_insumo.metrado_parcial) 
                        from item_insumo WHERE item_insumo.factor_id = insumo.factor_id 
                        AND item_insumo.proyecto_id=".$data["proyecto_id"]." )
                        *((insumo.porcentaje/100)+1))/ factor.unidad_pieza)) as calculo_piezas"),
                        'factor.kilogramo_pieza as kilogramo_pieza',
                        DB::raw("(
                            CEIL((
                                        (
                                            (SELECT SUM(item_insumo.metrado_parcial) from item_insumo WHERE item_insumo.factor_id = insumo.factor_id AND item_insumo.proyecto_id=".$data["proyecto_id"]." )
                                            *
                                            ((insumo.porcentaje/100)+1)
                                        )
                                        /
                                            factor.unidad_pieza
                                )    )
                                    *
                                    factor.kilogramo_pieza
                                ) as calculo_peso_parcial"),
                        DB::raw("(insumo.precio_almovd) as calculo_precio_pieza"),
                        DB::raw("   (
                                        CEIL( (((SELECT SUM(item_insumo.metrado_parcial) from item_insumo WHERE item_insumo.factor_id = insumo.factor_id AND item_insumo.proyecto_id=".$data["proyecto_id"]." )*((insumo.porcentaje/100)+1))/ factor.unidad_pieza))
                                        *(insumo.precio_almovd)
                                    )
                                    as calculo_precio_parcial"
                                ),
                        DB::raw("(SELECT SUM(item_insumo.area_parcial) from item_insumo WHERE item_insumo.factor_id = insumo.factor_id AND item_insumo.proyecto_id=".$data["proyecto_id"]." ) as total_area_parcial"),
                        'factor.id as factor_id',
                        'insumo.precio_almovd as precio',
                        'insumo.porcentaje as porcentaje'
                    )

            ->first();

            //ReCálculos XD
    
            /*
            $porcentaje = $insumo->porcentaje;
            $calculo_piezas = ($insumo->total_metrado_parcial*(($porcentaje/100)+1))/$insumo->unidad_pieza;
     
            $calculo_peso_parcial =  $calculo_piezas * $insumo->kilogramo_pieza;
           
            $calculo_precio_pieza = $data["precio"] * $insumo->kilogramo_pieza;
       
            $calculo_precio_parcial = $calculo_piezas* $calculo_precio_pieza;

            */
            
            $calculo_precio_parcial = $insumo->calculo_piezas * $data["precio"];
       
            if($update){
                $valor["proyecto_id"] = $data["proyecto_id"];
                $valor["factor_id"] = $insumo->factor_id;
                $valor["calculo_precio_parcial"] = $calculo_precio_parcial;
                $response = $this->guardarMetradoParcial($valor);
            }

            $response=true;
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

        return $update?1:$update;
    }

    public function guardarMetradoParcial($data){

        // recibe columna L de lista de materiales, lo llamaremos precio_parcial

        $materiales = $this->model
        ->where("factor_id",$data['factor_id'])
        ->where("proyecto_id",$data['proyecto_id'])
        ->select(
            'id',
            'metrado_parcial',
            DB::raw("(SELECT SUM(metrado_parcial) from item_insumo WHERE factor_id = ".$data['factor_id']." AND proyecto_id = ".$data['proyecto_id'].") as metrado_parcial_total") )
        ->get();
        $update = false;
        //recorrer cada registro en item_insumo que pertenezca a ese material
        foreach ($materiales as $key => $value) {
            if($value['metrado_parcial_total']==0){
                $metrado_material = $value['metrado_parcial'] ;
            }else{
                $metrado_material = $value['metrado_parcial'] /  $value['metrado_parcial_total'];
            }
           
            $costo_material = $metrado_material * $data['calculo_precio_parcial'];
            $update = $this->model
        ->where("id",$value['id'])
        ->update(array(
            "usuario_edicion" => auth()->user()->id,
            "fecha_modificacion" =>now()->format('Y-m-d H:i:s'),
            "metrado_material_2" => $metrado_material,
            "costo_material_2" => $costo_material
        ));
        }
        return $update?1:$update;
    }

    public function completarSolicitudAbastecimiento($data){

        //actualizar estado del presupuesto a 4

       $update = $this->model_presupuesto->where('proyecto_id', $data['id'])
       ->update([
                   'etapa' => 4,
                   'usuario_edicion' => auth()->user()->id,
                   'fecha_modificacion' => now()->format('Y-m-d H:i:s')
               ]);
        return $update?1:$update;
   }
}
