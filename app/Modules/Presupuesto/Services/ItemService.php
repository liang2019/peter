<?php

namespace App\Modules\Presupuesto\Services;

use App\Modules\Presupuesto\Models\Item;
use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Presupuesto\Models\Insumo;
use App\Modules\Presupuesto\Models\Presupuesto;
use App\Modules\Presupuesto\Models\ItemInsumo;
use App\Modules\Presupuesto\Models\InsumoProceso;
use Illuminate\Support\Facades\DB;
class ItemService
{


    public function __construct()
    {
        $this->model = new Item();
        $this->model_presupuesto = new Presupuesto();
        $this->model_proyecto = new Proyecto();
        $this->model_insumo_proceso = new InsumoProceso();
        $this->model_item_insumo = new ItemInsumo();
        $this->model_insumo = new Insumo();
    }

    public function cargarAll($data){

        $datos["items"] = $this->model->where('item.proyecto_id', $data["id"])
        ->where('item.estado', 1)
        ->where('item.habilitado', 1)
        ->select('item.*',
            DB::raw("(SELECT count(item_insumo.id) from item_insumo  WHERE item_insumo.item_id = item.id) as numero_insumos") )
        ->orderBy('item.id', 'asc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->leftJoin('presupuesto', 'proyecto.id', '=', 'presupuesto.proyecto_id')
        ->select('proyecto.*',
        'tracking.presupuesto as estado_tracking',
        'presupuesto.etapa as estado_presupuesto'
        )->first();
        return $datos;
    }

    public function guardar($data)
    {
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        //crear item
        $save = $this->model->create($data);
        return $save;
    }

    public function buscar($data){

        $datos =  $this->model->where('item.id', $data["id"])
        ->select("item.*",
        DB::raw("(SELECT count(item_insumo.id) from item_insumo  WHERE item_insumo.item_id = ".$data["id"].") as numero_insumos")
        )
        ->first();
        return $datos;
    }

    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update($data);
        return $update?1:$update;
    }

    public function eliminar($data){

        $update = $this->model->find($data['id'])->update([
            'estado' => 0,
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

    public function eliminarTodos($data)
    {

        DB::beginTransaction();
        try {
            $response =false;
          
            
            $delete = $this->model_insumo_proceso
            ->leftjoin('item_insumo', 'item_insumo.id', 'insumo_proceso.item_insumo_id')
            ->where('item_insumo.item_id', $data['id'])->delete();

            //borrar el material
            $delete = $this->model_item_insumo
            ->where('item_id', $data['id'])->delete();

            //borrar item

            $update = $this->model->find($data['id'])->update([
                'estado' => 0,
                'usuario_edicion' => auth()->user()->id,
                'fecha_modificacion' => now()->format('Y-m-d H:i:s')
            ]);

            //verificar que los insumos que quedan existan
                       
            $insumos =  $this->model_insumo
            ->where("proyecto_id", $data["proyecto_id"])
            ->select("insumo.id",
                DB::raw("(SELECT COUNT(item_insumo.id) FROM item_insumo WHERE item_insumo.factor_id=insumo.factor_id AND
                item_insumo.proyecto_id = ".$data["proyecto_id"].") as objetos")
            )
            ->get();
              
            foreach ($insumos as $key => $value) {

                if($value["objetos"]==0){
                    //eliminar en insumos
                    $delete = $this->model_insumo
                    ->where('id', $value['id'])->delete();
                }
                
            }
         
            $response =true;

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        //borrar los procesos
        return $response?1:$delete;
    }

    public function completarRegistroItems($data){

      
        $update = $this->model_presupuesto->where('proyecto_id', $data['id'])
        ->update([
                    'etapa' => 2,
                    'usuario_edicion' => auth()->user()->id,
                    'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                ]);
        return $update?1:$update;
    }
  

    

}
