<?php

namespace App\Modules\Presupuesto\Services;

use App\Modules\Presupuesto\Models\ItemInsumo;
use App\Modules\Tracking\Models\Tracking;
use App\Modules\Presupuesto\Models\Item;
use App\Modules\Presupuesto\Models\Presupuesto;
use App\Modules\Presupuesto\Models\ItemServicio;
use App\Modules\Presupuesto\Models\Insumo;
use Illuminate\Support\Facades\DB;
class PresupuestoService
{


    public function __construct()
    {
        $this->model = new Item();
        $this->model_tarcking = new Tracking();
        $this->model_presupuesto = new Presupuesto();
        $this->model_servicio = new ItemServicio();
        $this->model_insumo = new Insumo();
    }

    public function cargarItemsConsolidado($data){

  

        $servicios = $this->model_servicio->where('item_servicio.proyecto_id', $data["proyecto_id"])
        ->where('item_servicio.estado', 1)
        ->select('item_servicio.id',
            'item_servicio.descripcion as item_nombre',
            'item_servicio.cantidad as item_cantidad',
            DB::raw('"0" as porcentaje'),
            DB::raw('"PZA" as unidad_'),
            DB::raw('"0" as area'),
            DB::raw('"0" as peso'),
            DB::raw('"0" as material'),
            DB::raw('"0" as m_o'),
         
            DB::raw('item_servicio.precio as costo'),
            DB::raw('"servicio" as tipo')
    
    );
     
    
        $datos["items"] = $this->model->where("item.proyecto_id", $data["proyecto_id"])
        ->where("item.estado", 1)
        ->where("item.habilitado", 1)
        ->select(
                    'item.id',
                    'item.nombre as item_nombre',
                    'item.cantidad as item_cantidad',
                    'item.margen as porcentaje',
                    DB::raw('"PZA" as unidad_'),
                    DB::raw("(SELECT SUM(item_insumo.area_parcial) from item_insumo WHERE item_insumo.item_id = item.id) as area"),
                    DB::raw("(SELECT SUM(item_insumo.peso_parcial) from item_insumo WHERE item_insumo.item_id = item.id) as peso"),
                    DB::raw("(SELECT SUM(item_insumo.costo_material_2) from item_insumo WHERE item_insumo.item_id = item.id) as material"),
                    DB::raw("(SELECT SUM(item_insumo.costo_parcial) from item_insumo WHERE item_insumo.item_id = item.id) as m_o"),
                    DB::raw(" (
                        (SELECT SUM(item_insumo.costo_parcial) from item_insumo WHERE item_insumo.item_id = item.id)
                        +
                        (SELECT SUM(item_insumo.costo_material_2) from item_insumo WHERE item_insumo.item_id = item.id)
                    ) as costo"),
                    DB::raw('"servicio" as item')
                    
                )

                ->union($servicios)
                ->get();
/** ( (SELECT SUM(item_insumo.costo_parcial) from item_insumo WHERE item_insumo.item_id = item.id) + (SELECT SUM(item_insumo.costo_material) from item_insumo WHERE item_insumo.item_id = item.id)   ) as costo */
        foreach ($datos["items"] as $key => $value) {

            //$unidad =  ($value["item_cantidad"]>0)? ($value["costo"] + ( $value["costo"] * ($value["porcentaje"]/100)) ) * $value["item_cantidad"] : 0 ;
            $unidad =  ($value["item_cantidad"]>0)? ($value["costo"]  / $value["item_cantidad"]) : 0 ;
          
            $datos["items"][$key]["margen"] =$value["costo"]*($value["porcentaje"]/100);
            $datos["items"][$key]["unitario"] =$unidad;
            $datos["items"][$key]["parcial"] =$unidad * $value["item_cantidad"];

            
        }
        $datos["proyecto"] = $this->model_presupuesto->where("presupuesto.proyecto_id",$data["proyecto_id"])
        ->leftJoin('proyecto', 'proyecto.id', '=', 'presupuesto.proyecto_id')
        ->select('proyecto.nombre','presupuesto.etapa as estado_presupuesto')->first();
        $datos["porcentaje"] = $this->model->where("proyecto_id", $data["proyecto_id"])->select('margen as porcentaje')->first();

        return $datos;
    }

    public function actualizarMargen($data)
   {
        //esta ruta es para solicitud de abastecimiento
        $update = $this->model->where('proyecto_id',$data['proyecto_id'])->update(array(
            "margen" => $data["margen"],
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ));
        return $update?1:$update;
    }

    public function completarPresupuesto($data){

        //actualizar estado del presupuesto a 5
   

       $update = $this->model_presupuesto->where('proyecto_id', $data['id'])
       ->update([
                   'etapa' => 5,
                   'usuario_edicion' => auth()->user()->id,
                   'fecha_modificacion' => now()->format('Y-m-d H:i:s')
               ]);
    
       

        return $update?1:$update;

   }


}
