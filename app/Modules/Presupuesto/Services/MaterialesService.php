<?php

namespace App\Modules\Presupuesto\Services;

use App\Modules\Presupuesto\Models\ItemInsumo;
use App\Modules\Presupuesto\Models\Insumo;
use App\Modules\Presupuesto\Models\Presupuesto;
use Illuminate\Support\Facades\DB;

class  MaterialesService
{


    public function __construct()
    {
        $this->model = new ItemInsumo();
        $this->model_insumo = new Insumo();
        $this->model_presupuesto = new Presupuesto();
    }

    public function cargarMateriales($data){

        //Lista de valores para Lista Materiales
        $datos["insumos"] = $this->model_insumo->where("insumo.proyecto_id", $data["id"])
        ->leftJoin('factor', 'insumo.factor_id', '=', 'factor.id')
        ->leftJoin('alarti', 'factor.alarti_id', '=', 'alarti.id')
        ->select(
                    'insumo.id as id',
                    'alarti.codigo as codigo_material',
                    'alarti.descripcion as nombre_material',
                    DB::raw("(SELECT SUM(item_insumo.metrado_parcial) from item_insumo WHERE item_insumo.factor_id = insumo.factor_id AND item_insumo.proyecto_id=".$data["id"]." ) as total_metrado_parcial"),
                    'factor.unidad_pieza as unidad_pieza',
                    DB::raw("CEIL((((SELECT SUM(item_insumo.metrado_parcial) 
                    from item_insumo WHERE item_insumo.factor_id = insumo.factor_id 
                    AND item_insumo.proyecto_id=".$data["id"]." )
                    *((insumo.porcentaje/100)+1))/ factor.unidad_pieza)) as calculo_piezas"),
                    'factor.kilogramo_pieza as kilogramo_pieza',
                    DB::raw("(
                        CEIL((
                                    (
                                        (SELECT SUM(item_insumo.metrado_parcial) from item_insumo WHERE item_insumo.factor_id = insumo.factor_id AND item_insumo.proyecto_id=".$data["id"]." )
                                        *
                                        ((insumo.porcentaje/100)+1)
                                    )
                                    /
                                        factor.unidad_pieza
                            )    )
                                *
                                factor.kilogramo_pieza
                            ) as calculo_peso_parcial"),
                    DB::raw("(insumo.precio_almovd) as calculo_precio_pieza"),
                    DB::raw("   (
                                    CEIL( (((SELECT SUM(item_insumo.metrado_parcial) from item_insumo WHERE item_insumo.factor_id = insumo.factor_id AND item_insumo.proyecto_id=".$data["id"]." )*((insumo.porcentaje/100)+1))/ factor.unidad_pieza))
                                     *(insumo.precio_almovd)
                                )
                                 as calculo_precio_parcial"
                            ),
                    DB::raw("(SELECT SUM(item_insumo.area_parcial) from item_insumo WHERE item_insumo.factor_id = insumo.factor_id AND item_insumo.proyecto_id=".$data["id"]." ) as total_area_parcial"),
                    'factor.id as factor_id',
                    'insumo.precio_almovd as precio',
                    'insumo.porcentaje as porcentaje'
                )

        ->get();

        $datos["proyecto"] = $this->model_presupuesto->where("presupuesto.proyecto_id",$data["id"])
        ->leftJoin('proyecto', 'proyecto.id', '=', 'presupuesto.proyecto_id')
        ->select('proyecto.nombre','presupuesto.etapa as estado_presupuesto')->first();

        return $datos;
    }

    public function guardar($data){

        //Se actualiza el procentaje de la Lista de materiales
        DB::beginTransaction();
        try {
            $response =false;

                foreach ($data["insumos"] as $key => $value) {

                    //actualizar el porncentaje en la tabla insumo
                    $update = $this->model_insumo
                    ->where("id",$value['id'])
                    ->update(array(
                        "usuario_edicion" => auth()->user()->id,
                        "fecha_modificacion" =>now()->format('Y-m-d H:i:s'),
                        "porcentaje" => $value['porcentaje']
                    ));
                    if($update){
                        $valor["proyecto_id"] = $data["proyecto_id"];
                        $valor["factor_id"] = $value["factor_id"];
                        $valor["calculo_precio_parcial"] = $value["calculo_precio_parcial"];
                        $response = $this->guardarMetradoParcial($valor);
                    }

                }

                $response=true;
                DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        return $response?1:$response;
    }

    public function guardarMetradoParcial($data){

        // recibe columna L de lista de materiales, lo llamaremos precio_parcial

        $materiales = $this->model
        ->where("factor_id",$data['factor_id'])
        ->where("proyecto_id",$data['proyecto_id'])
        ->select(
            'id',
            'metrado_parcial',
            DB::raw("(SELECT SUM(metrado_parcial) from item_insumo WHERE factor_id = ".$data['factor_id']." AND proyecto_id = ".$data['proyecto_id'].") as metrado_parcial_total") )
        ->get();
        $update = false;
        //recorrer cada registro en item_insumo que pertenezca a ese material
        foreach ($materiales as $key => $value) {
            if($value['metrado_parcial_total']==0){
                $metrado_material = $value['metrado_parcial'] ;
            }else{
                $metrado_material = $value['metrado_parcial'] /  $value['metrado_parcial_total'];
            }
           
            $costo_material = $metrado_material * $data['calculo_precio_parcial'];
            $update = $this->model
        ->where("id",$value['id'])
        ->update(array(
            "usuario_edicion" => auth()->user()->id,
            "fecha_modificacion" =>now()->format('Y-m-d H:i:s'),
            "metrado_material" => $metrado_material,
            "costo_material" => $costo_material
        ));
        }
        return $update?1:$update;
    }

    public function completarListaMateriales($data){


        //actualizar estado del presupuesto a 3
       $update = $this->model_presupuesto->where('proyecto_id', $data['id'])
       ->update([
                   'etapa' => 3,
                   'usuario_edicion' => auth()->user()->id,
                   'fecha_modificacion' => now()->format('Y-m-d H:i:s')
               ]);
        return $update?1:$update;
    }


}
