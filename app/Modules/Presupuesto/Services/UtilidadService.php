<?php

namespace App\Modules\Presupuesto\Services;

use App\Modules\Presupuesto\Models\Item;
use App\Modules\Presupuesto\Models\Utilidad;
use App\Modules\Presupuesto\Models\Presupuesto;
use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Presupuesto\Models\ItemServicio;
use Illuminate\Support\Facades\DB;
use App\Modules\Tracking\Models\Tracking;

class  UtilidadService
{


    public function __construct()
    {
        $this->model = new Item();
        $this->model_proyecto = new Proyecto();
        $this->model_utilidad = new Utilidad();
        $this->model_presupuesto = new Presupuesto();
        $this->model_tarcking = new Tracking();
        $this->model_servicio = new ItemServicio();
    }

    public function precioTotalParcial($proyecto_id){

        $servicios = $this->model_servicio->where('item_servicio.proyecto_id', $proyecto_id)
        ->where('item_servicio.estado', 1)
        ->select('item_servicio.id',
            'item_servicio.descripcion as item_nombre',
            'item_servicio.cantidad as item_cantidad',
            DB::raw('"0" as porcentaje'),
            DB::raw('"PZA" as unidad_'),
            DB::raw('"0" as area'),
            DB::raw('"0" as peso'),
            DB::raw('"0" as material'),
            DB::raw('"0" as m_o'),
            DB::raw('item_servicio.precio as costo'),
            DB::raw('"servicio" as tipo')
    
        );
     
    
        $datos["items"] = $this->model->where("item.proyecto_id", $proyecto_id)
        ->where("item.estado", 1)
        ->where("item.habilitado", 1)
        ->select(
                    'item.id',
                    'item.nombre as item_nombre',
                    'item.cantidad as item_cantidad',
                    'item.margen as porcentaje',
                    DB::raw('"PZA" as unidad_'),
                    DB::raw("(SELECT SUM(item_insumo.area_parcial) from item_insumo WHERE item_insumo.item_id = item.id) as area"),
                    DB::raw("(SELECT SUM(item_insumo.peso_parcial) from item_insumo WHERE item_insumo.item_id = item.id) as peso"),
                    DB::raw("(SELECT SUM(item_insumo.costo_material_2) from item_insumo WHERE item_insumo.item_id = item.id) as material"),
                    DB::raw("(SELECT SUM(item_insumo.costo_parcial) from item_insumo WHERE item_insumo.item_id = item.id) as m_o"),
                    DB::raw(" (
                        (SELECT SUM(item_insumo.costo_parcial) from item_insumo WHERE item_insumo.item_id = item.id)
                        +
                        (SELECT SUM(item_insumo.costo_material_2) from item_insumo WHERE item_insumo.item_id = item.id)
                    ) as costo"),
                    DB::raw('"servicio" as item')
                    
                )

                ->union($servicios)
                ->get();
        $total = 0;
        foreach ($datos["items"] as $key => $value) {

          //  $unidad =  ($value["item_cantidad"]>0)? ($value["costo"] + ( $value["costo"] * ($value["porcentaje"]/100)) ) * $value["item_cantidad"] : 0 ;
            $unidad =  ($value["item_cantidad"]>0)? ($value["costo"]  /  $value["item_cantidad"]) : 0 ;
          
            $datos["items"][$key]["margen"] =$value["costo"]*($value["porcentaje"]/100);
            $datos["items"][$key]["unitario"] =$unidad;
            $datos["items"][$key]["parcial"] =$unidad * $value["item_cantidad"];

            $total = $total +  $datos["items"][$key]["parcial"];
            $total = round($total,2);
        }
       

        return $total;
    }


    public function cargarItems($data){

        $datos["items"] = $this->model->where('item.proyecto_id', $data["id"])
        ->where('item.estado', 1)
        ->where('item.habilitado', 0)
        ->leftJoin('utilidad', 'item.id', '=', 'utilidad.item_id')
        ->select(
                    'utilidad.id as id',
                    'item.nombre as nombre',
                    'item.id as item_id',
                    'item.cantidad as cantidad',
                    'utilidad.porcentaje as porcentaje',
                    'utilidad.precio_unitario as precio_unitario',
                    'utilidad.precio_parcial as precio_parcial'
                )
        ->get();
        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->leftJoin('presupuesto', 'proyecto.id', '=', 'presupuesto.proyecto_id')
        ->select('proyecto.*',
        'tracking.presupuesto as estado_tracking',
        'presupuesto.etapa as estado_presupuesto'
        )->first();

        $datos["total_parcial"] = $this->precioTotalParcial($data["id"]);
        return $datos;
    }

    public function buscar($data){

        $datos =  $this->model_utilidad->all()->where('id', $data["id"])->first();
        return $datos;
    }

    public function actualizar($data)
    {
        //
        $update = false;
        foreach ($data["items"] as $key => $value) {
            $update = $this->model_utilidad->find($value['id'])->update(array(
                "precio_unitario" => $value["precio_unitario"],
                "precio_parcial" => $value["precio_unitario"],
                "porcentaje" => $value["porcentaje"],
                'usuario_edicion' => auth()->user()->id,
                'fecha_modificacion' => now()->format('Y-m-d H:i:s')
            ));
        }

        return $update?1:$update;
    }

    public function completarEtapa($data){

        //actualizar estado del presupuesto a 6
        //completar la etapa de presupuesto

       $update = $this->model_presupuesto->where('proyecto_id', $data['id'])
       ->update([
                   'etapa' => 6,
                   'usuario_edicion' => auth()->user()->id,
                   'fecha_modificacion' => now()->format('Y-m-d H:i:s')
               ]);
        if($update){
            $update = $this->model_tarcking->where('proyecto_id', $data['id'])
            ->update([
                        'presupuesto' => 2,
                        'presupuesto_fecha' => now()->format('Y-m-d H:i:s'),
                        'cotizacion' => 1,
                        'usuario_edicion' => auth()->user()->id,
                        'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                    ]);
        }

       

        return $update?1:$update;

   }


}
