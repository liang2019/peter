<?php

namespace App\Modules\Presupuesto\Services;

use App\Modules\Presupuesto\Models\ItemServicio;
use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Presupuesto\Models\Presupuesto;
use Illuminate\Support\Facades\DB;

class ItemServicioService
{


    public function __construct()
    {
        $this->model = new ItemServicio();
        $this->model_presupuesto = new Presupuesto();
        $this->model_proyecto = new Proyecto();
    }

    public function cargarAll($data){

        $datos["items"] = $this->model->where('item_servicio.proyecto_id', $data["proyecto_id"])
        ->where('item_servicio.estado', 1)
        ->select('item_servicio.*' )
        ->orderBy('item_servicio.id', 'asc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["proyecto_id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->leftJoin('presupuesto', 'proyecto.id', '=', 'presupuesto.proyecto_id')
        ->select('proyecto.*',
        'tracking.presupuesto as estado_tracking',
        'presupuesto.etapa as estado_presupuesto'
        )->first();
        return $datos;
    }

    public function guardar($data)
    {
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        //crear servicio
        $save = $this->model->create($data);
        return $save;
    }

    public function buscar($data){

        $datos =  $this->model->all()->where('id', $data["id"])->first();
        return $datos;
    }

    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update($data);
        return $update?1:$update;
    }

    public function eliminar($data){

        $update = $this->model->find($data['id'])->update([
            'estado' => 0,
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

    public function actualizarPrecio($data)
   {
        $update = $this->model->find($data['id'])->update(array(
            "precio" => $data["precio"],
            "verificado" => $data["verificado"],
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ));
        return $update?1:$update;
    }    

}
