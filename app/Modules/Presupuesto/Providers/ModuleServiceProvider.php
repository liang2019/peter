<?php

namespace App\Modules\Presupuesto\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('presupuesto', 'Resources/Lang', 'app'), 'presupuesto');
        $this->loadViewsFrom(module_path('presupuesto', 'Resources/Views', 'app'), 'presupuesto');
        $this->loadMigrationsFrom(module_path('presupuesto', 'Database/Migrations', 'app'), 'presupuesto');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('presupuesto', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('presupuesto', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
