<?php

namespace App\Modules\Tracking\Models;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    protected $fillable = [
                            'proyecto_id',
                            'requerimiento',
                            'requerimiento_fecha',
                            'presupuesto',
                            'presupuesto_fecha',
                            'cotizacion',
                            'cotizacion_fecha',
                            'pedido',
                            'pedido_fecha',
                            'diseno',
                            'diseno_fecha',
                            'produccion',
                            'produccion_fecha',
                            'montaje',
                            'montaje_fecha',
                            'estado',
                            'usuario_creacion',
                            'usuario_edicion',
                            'fecha_creacion',
                            'fecha_modificacion'
                        ];
    protected $table = 'tracking';
    public $timestamps = false;
}
 