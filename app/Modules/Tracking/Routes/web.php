<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'auth'],function () {
    Route::group(['prefix' => 'tracking'], function () {
        Route::get('/', ['as' => 'tracking', 'uses' => 'TrackingController@index']);
        Route::get('/cargar', ['as' => 'tracking.cargar', 'uses' => 'TrackingController@cargarAll']);
        Route::get('/exportar', ['as' => 'tracking.exportar', 'uses' => 'TrackingController@exportar']);
    });
});