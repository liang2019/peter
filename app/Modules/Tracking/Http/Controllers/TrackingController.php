<?php

namespace App\Modules\Tracking\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Tracking\Services\TrackingService as MainService;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\Tracking\Exports\ReporteTrackingExport;

class TrackingController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarAll()
    {
        $data = $this->service->cargarAll();
        return $data;
    }

    public function exportar() 
    {
    
        return Excel::download(new ReporteTrackingExport, 'Tracking.xlsx');
    }
}
