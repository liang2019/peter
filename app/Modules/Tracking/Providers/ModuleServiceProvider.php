<?php

namespace App\Modules\Tracking\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('tracking', 'Resources/Lang', 'app'), 'tracking');
        $this->loadViewsFrom(module_path('tracking', 'Resources/Views', 'app'), 'tracking');
        $this->loadMigrationsFrom(module_path('tracking', 'Database/Migrations', 'app'), 'tracking');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('tracking', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('tracking', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
