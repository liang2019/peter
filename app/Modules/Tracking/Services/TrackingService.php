<?php

namespace App\Modules\Tracking\Services;


use App\Modules\Tracking\Models\Tracking;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;


class TrackingService
{


    public function __construct()
    {
        $this->model = new Tracking();
    }

    public function cargarAll(){

        $datos = $this->model
        ->selectRaw('proyecto.codigo as codigo')
        ->selectRaw('proyecto.nombre as nombre')
        ->selectRaw('proyecto.montaje as indicador_montaje')
        ->selectRaw('proyecto.fecha_creacion as fecha_inicio')
        ->leftJoin('proyecto', 'tracking.proyecto_id', '=', 'proyecto.id')
        ->leftJoin('cliente', 'proyecto.cliente_id', '=', 'cliente.id')
        ->leftJoin('producto', 'producto.id', '=', 'proyecto.producto_id')
        ->selectRaw('tracking.id as id')
        ->selectRaw('(CASE WHEN tracking.requerimiento = 1 THEN "En Curso" WHEN tracking.requerimiento = 2 THEN "Completado"  WHEN tracking.requerimiento=0 THEN "Cancelado" ELSE "" END) as requerimiento')
        ->selectRaw('tracking.requerimiento_fecha')
        ->selectRaw('(CASE WHEN tracking.presupuesto = 1 THEN "En Curso" WHEN tracking.presupuesto = 2 THEN "Completado"  WHEN tracking.presupuesto=0 THEN "Cancelado" ELSE "" END) as presupuesto')
        ->selectRaw('tracking.presupuesto_fecha')
        ->selectRaw('(CASE WHEN tracking.cotizacion = 1 THEN "En Curso" WHEN tracking.cotizacion = 2 THEN "Completado"  WHEN tracking.cotizacion=0 THEN "Cancelado" ELSE "" END) as cotizacion')
        ->selectRaw('tracking.cotizacion_fecha')
        ->selectRaw('(CASE WHEN tracking.pedido = 1 THEN "En Curso" WHEN tracking.pedido = 2 THEN "Completado"  WHEN tracking.pedido=0 THEN "Cancelado" ELSE "" END) as pedido')
        ->selectRaw('tracking.pedido_fecha')
        ->selectRaw('(CASE WHEN tracking.diseno = 1 THEN "En Curso" WHEN tracking.diseno = 2 THEN "Completado"  WHEN tracking.diseno=0 THEN "Cancelado" ELSE "" END) as diseno')
        ->selectRaw('tracking.diseno_fecha')
        ->selectRaw('(CASE WHEN tracking.produccion = 1 THEN "En Curso" WHEN tracking.produccion = 2 THEN "Completado"  WHEN tracking.produccion=0 THEN "Cancelado" ELSE "" END) as produccion')
        ->selectRaw('tracking.produccion_fecha')
        ->selectRaw('(CASE WHEN tracking.montaje = 1 THEN "En Curso" WHEN tracking.montaje = 2 THEN "Completado"  WHEN tracking.montaje=0 THEN "Cancelado" ELSE "" END) as montaje')
        ->selectRaw('tracking.montaje_fecha')
        ->selectRaw('cliente.descripcion as nombre_cliente')
        ->selectRaw('producto.nombre as producto')
        ->orderBy('proyecto.id', 'desc')
        ->get();

        return $datos;
    }


}
