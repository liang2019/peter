<?php

namespace App\Modules\Tracking\Exports;


use App\Modules\Tracking\Services\TrackingService as MainService;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Facades\DB;

use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class ReporteTrackingExport implements ShouldAutoSize,WithEvents,FromView//FromCollection, WithHeadings, ShouldAutoSize
{
    private $countReg;

    public function __construct()
    {
        $this->service = new MainService();
    }
    /*public function collection()
    {
        ini_set('memory_limit','350M');
        ini_set('max_execution_time', 680);
        $data = $this->service->cargarAll();
        return $data;
    }
    public function headings(): array
    {
        return ["CÓDIGO", "PROYECTO", "REQUERIMIENTO", "PRESUPUESTO", "COTIZACIÓN", "PEDIDO", "DISEÑO", "PRODUCCIÓN", "MONTAJE"];
    }*/
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $border =
                    array(
                        'outline' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => 'FFFF0000'],
                        ),
                    );
                    $cellRange = 'B3:T3'; // All headers
                    $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(13);

                $event->sheet->getStyle('B3:T3')->applyFromArray(
                    array(
                        'borders'=>$border
                    )
                );
                $event->sheet->getStyle('B3:T'.(3+$this->countReg))->applyFromArray(
                    array(
                        'borders'=>$border
                    )
                );

            }
        ];
    }
    public function view(): View
    {
        ini_set('memory_limit','350M');
        ini_set('max_execution_time', 680);
        $data = $this->service->cargarAll();
        $this->countReg = count($data);

      /*  
        $sheet->getStyle('A1')->applyFromArray(array(
            'fill' => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FF0000')
            )
        ));*/

        return view('layouts.reportes.tracking', [
            'tracking' => $data,
        ]);
    }
}
