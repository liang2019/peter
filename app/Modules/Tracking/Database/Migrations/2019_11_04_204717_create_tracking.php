<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTracking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('proyecto_id')->unsigned();
            $table->foreign('proyecto_id')->references('id')->on('proyecto')->onDelete('cascade');
            $table->unsignedInteger('requerimiento')->default(3);
            $table->timestamp('requerimiento_fecha')->nullable();
            $table->unsignedInteger('presupuesto')->default(3);
            $table->timestamp('presupuesto_fecha')->nullable();
            $table->unsignedInteger('cotizacion')->default(3);
            $table->timestamp('cotizacion_fecha')->nullable();
            $table->unsignedInteger('pedido')->default(3);
            $table->timestamp('pedido_fecha')->nullable();
            $table->unsignedInteger('diseno')->default(3);
            $table->timestamp('diseno_fecha')->nullable();
            $table->unsignedInteger('produccion')->default(3);
            $table->timestamp('produccion_fecha')->nullable();
            $table->unsignedInteger('montaje')->default(3);
            $table->timestamp('montaje_fecha')->nullable();
            
            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion');
            $table->unsignedInteger('usuario_edicion');
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->timestamp('fecha_modificacion')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking');
    }
}
