<?php

namespace App\Modules\Cotizacion\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Cotizacion\Services\CotizacionService as MainService;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\Cotizacion\Exports\CotizacionExport;

class CotizacionController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarItems(Request $request)
    {
        $data = $this->service->cargarItems($request);
        return $data;
    }

    public function montaje(Request $request)
    {
        $data = $this->service->montaje($request);
        return $data;
    }

    public function completar(Request $request)
    {
        $data = $this->service->completar($request);
        return $data;
    }
    
    public function aprobarPresupuesto(Request $request)
    {
        $data = $this->service->aprobarPresupuesto($request);
        return $data;
    }

    public function exportar($id) 
    {

      return Excel::download(new CotizacionExport($id), 'Cotizacion.xlsx');
    }
}
