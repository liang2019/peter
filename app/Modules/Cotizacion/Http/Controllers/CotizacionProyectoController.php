<?php

namespace App\Modules\Cotizacion\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Cotizacion\Services\CotizacionProyectoService as MainService;

class CotizacionProyectoController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }
    
    public function cargarAll()
    {
        $data = $this->service->cargarAll();
        return $data;
    }

    public function cancelar(Request $request)
    {
        $data = $this->service->cancelar($request);
        return $data;
    }

    

}
