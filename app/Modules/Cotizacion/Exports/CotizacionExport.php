<?php

namespace App\Modules\Cotizacion\Exports;


use App\Modules\Cotizacion\Services\CotizacionService as MainService;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CotizacionExport implements  ShouldAutoSize,WithEvents,FromView
{
    //FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithDrawings,
    private $id;
    private $countReg;
    public function __construct(int $id)
    {
        $this->id = $id;
        $this->service = new MainService();
     
    }
    /*public function collection( )
    {

        ini_set('memory_limit','350M');
        ini_set('max_execution_time', 680);

        //mandar el proyecto_id
       $datos = array("proyecto_id" =>$this->id);
        $data = $this->service->exportarCotizacion($datos);
        return $data;
    }*/
  /*  public function headings(): array
    {
        return ["NOMBRE", "UNIDAD", "CANTIDAD", "PRECIO UNINTARIO", "PRECIO PARCIAL"];
    }*/

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $border =
                    array(
                        'outline' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => 'FFFF0000'],
                        ),
                    );
                    $cellRange = 'B7:F7'; // All headers
                    $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(13);

                $event->sheet->getStyle('B7:F7')->applyFromArray(
                    array(
                        'borders'=>$border
                    )
                );
                $event->sheet->getStyle('B7:F'.(7+$this->countReg))->applyFromArray(
                    array(
                        'borders'=>$border
                    )
                );

            }
        ];

       /* return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A7:E7'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
              
                $styleArray = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                            'color' => ['argb' => '2196f3'],

                        ],
                        'top' => array(
                            'style' => 'solid',
                            'color' => ['argb' => 'ffffff'],
                            'bold'      =>  true,
                        ),
                    ],
                    'font' => [
                        'name'      =>  'Calibri',
                        'size'      =>  13,
                        'bold'      =>  true,
                        'color' => ['argb' => '2196f3'],
                    ],
                    'fill' => [
                        'type'  => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'color' => array('rgb' => '2196f3'),
                    ]
                ];
               
                $event->sheet->getStyle('A1:E1')->applyFromArray(array(
                    'fill' => array(
                        'type'  => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'color' => array('rgb' => '2196f3')
                    )
                ));
              
            $event->sheet->getStyle('A7:E7')->applyFromArray($styleArray);
            },
        ];*/
    }
    public function view(): View
    {
        ini_set('memory_limit','350M');
        ini_set('max_execution_time', 680);

        //mandar el proyecto_id
       $datos = array("proyecto_id" =>$this->id);
        $data = $this->service->exportarCotizacion($datos);
        $this->countReg = count($data["cotiza"]);

      /*  
        $sheet->getStyle('A1')->applyFromArray(array(
            'fill' => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FF0000')
            )
        ));*/

        return view('layouts.reportes.cotizacion', [
            'proyecto' => $data["proyecto"],
            'cotizacion' => $data["cotiza"],
        ]);
    }
}
