<?php

namespace App\Modules\Cotizacion\Models;

use Illuminate\Database\Eloquent\Model;

class CotizacionDocumentaria extends Model
{
    protected $fillable = [
        'proyecto_id',
        'adjunto_id',
        'descripcion',
        'monto',
        'observacion',
        'adjunto',
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'cotizacion_documentaria';
    public $timestamps = false;
}
