<?php

namespace App\Modules\Cotizacion\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('cotizacion', 'Resources/Lang', 'app'), 'cotizacion');
        $this->loadViewsFrom(module_path('cotizacion', 'Resources/Views', 'app'), 'cotizacion');
        $this->loadMigrationsFrom(module_path('cotizacion', 'Database/Migrations', 'app'), 'cotizacion');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('cotizacion', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('cotizacion', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
