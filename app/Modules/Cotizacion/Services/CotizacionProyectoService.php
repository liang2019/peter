<?php

namespace App\Modules\Cotizacion\Services;

use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Tracking\Models\Tracking;
use Illuminate\Support\Facades\DB;

class CotizacionProyectoService
{

    public function __construct()
    {
        $this->model = new Proyecto();
        $this->model_tarcking = new Tracking();

    }

    public function cargarAll(){

        $datos = $this->model
        ->where("tracking.cotizacion","<>", 3)
        ->leftJoin('cliente', 'proyecto.cliente_id', '=', 'cliente.id')
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*', 'cliente.descripcion as nombre_cliente',
        DB::raw("(SELECT COUNT(cotizacion_documentaria.id) from cotizacion_documentaria  WHERE cotizacion_documentaria.proyecto_id=proyecto.id) as cantidad_adjuntos"),
        'tracking.cotizacion as estado_tracking')
        ->orderBy('proyecto.id', 'desc')
        ->get();

        return $datos;
    }


    public function cancelar($data){

        $data["usuario_edicion"] = auth()->user()["id"];
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update(['estado' => 0]);
         //actualizar estado del tracking
        $tracking = $this->model_tarcking->where('proyecto_id', $data['id'])
        ->update([
                    'cotizacion' => 0,
                    'usuario_edicion' => auth()->user()->id,
                    'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                ]);
        if(!$tracking){
            return false;
        }
        return $update?1:$update;
    }

}
