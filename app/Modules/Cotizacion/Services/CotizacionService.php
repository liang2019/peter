<?php

namespace App\Modules\Cotizacion\Services;

use App\Modules\Presupuesto\Models\Item;
use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Tracking\Models\Tracking;
use App\Modules\Presupuesto\Models\ItemServicio;
use App\Modules\Presupuesto\Models\Presupuesto;
use App\Modules\Presupuesto\Models\Insumo;
use App\Modules\Presupuesto\Models\Utilidad;
use App\Modules\Cotizacion\Models\CotizacionDocumentaria;

use Illuminate\Support\Facades\DB;
class CotizacionService
{

    public function __construct()
    {
        $this->model = new Item();
        $this->model_tarcking = new Tracking();
        $this->model_proyecto = new Proyecto();
        $this->model_utilidad = new Utilidad();
        $this->model_servicio = new ItemServicio();
        $this->model_presupuesto = new Presupuesto();
        $this->model_insumo = new Insumo();
        $this->model_cotizacion_documentaria = new CotizacionDocumentaria();

    }

    public function cargarItems($data){

        $servicios = $this->model_servicio->where('item_servicio.proyecto_id', $data["proyecto_id"])
        ->where('item_servicio.estado', 1)
        ->select('item_servicio.id',
            'item_servicio.descripcion as item_nombre',
            DB::raw('"1" as habilitado'),
            'item_servicio.cantidad as item_cantidad',
            DB::raw('"0" as porcentaje'),
            DB::raw('"PZA" as unidad_'),
            DB::raw('"0" as area'),
            DB::raw('"0" as peso'),
            DB::raw('"0" as material'),
            DB::raw('"0" as m_o'),
            DB::raw('item_servicio.precio as costo'),
            DB::raw('item_servicio.precio   as precio_parcial_utilidad'),
            DB::raw('item_servicio.precio  as precio_unitario_utilidad'),
            DB::raw('"servicio" as tipo'),
            'item_servicio.fecha_creacion as fecha_creacion'
        );

        $datos["items"] = $this->model->where("item.proyecto_id", $data["proyecto_id"])
        ->where("item.estado", 1)

        ->select(
                    'item.id',
                    'item.nombre as item_nombre',
                    'item.habilitado as habilitado',
                    'item.cantidad as item_cantidad',
                    'item.margen as porcentaje',
                    DB::raw('"PZA" as unidad_'),
                    DB::raw("(SELECT SUM(item_insumo.area_parcial) from item_insumo WHERE item_insumo.item_id = item.id) as area"),
                    DB::raw("(SELECT SUM(item_insumo.peso_parcial) from item_insumo WHERE item_insumo.item_id = item.id) as peso"),
                    DB::raw("(SELECT SUM(item_insumo.costo_material_2) from item_insumo WHERE item_insumo.item_id = item.id) as material"),
            
                    DB::raw("(SELECT SUM(item_insumo.costo_parcial) from item_insumo WHERE item_insumo.item_id = item.id) as m_o"),
                   
                    DB::raw(" (
                                (SELECT SUM(item_insumo.costo_parcial) from item_insumo WHERE item_insumo.item_id = item.id)
                                +
                                (SELECT SUM(item_insumo.costo_material_2) from item_insumo WHERE item_insumo.item_id = item.id)
                            ) as costo"),
                    
                    DB::raw("IFNULL((SELECT utilidad.precio_parcial from utilidad WHERE utilidad.item_id = item.id),0) as precio_parcial_utilidad"),
                    DB::raw("IFNULL((SELECT utilidad.precio_unitario from utilidad WHERE utilidad.item_id = item.id),0) as precio_unitario_utilidad"),
                    DB::raw('"material" as tipo'),
                    'item.fecha_creacion as fecha_creacion'
                    )

                ->union($servicios)
                ->orderBy('fecha_creacion', 'desc')
                ->get();

        foreach ($datos["items"] as $key => $value) {

          //  $unidad =  ($value["item_cantidad"]>0)? ($value["costo"] + ( $value["costo"] * ($value["porcentaje"]/100)) ) * $value["item_cantidad"] : 0 ;
            $unidad =  ($value["item_cantidad"]>0)? ( $value["costo"]  / $value["item_cantidad"] ) : 0 ;

            if($value["habilitado"]==0){
                $datos["items"][$key]["unitario"] =$value["precio_unitario_utilidad"];
                $datos["items"][$key]["parcial"] =$value["precio_parcial_utilidad"];
            }else{
                if($value["tipo"]=="servicio"){
                    $datos["items"][$key]["unitario"] =$value["costo"];
                    $datos["items"][$key]["parcial"] =$value["costo"] * $value["item_cantidad"];
                }else{
                    $datos["items"][$key]["unitario"] =$unidad;
                    $datos["items"][$key]["parcial"] =$unidad * $value["item_cantidad"];
                }
            }


        }

        $datos["proyecto"] = $this->model_tarcking->where("tracking.proyecto_id",$data["proyecto_id"])
        ->leftJoin('proyecto', 'tracking.proyecto_id', '=', 'proyecto.id')

        ->select('proyecto.nombre','proyecto.montaje as montaje',
        'tracking.cotizacion as estado_tracking')->first();

        return $datos;
    }

    public function montaje($data){

        //indicar si tiene montaje
        //1 tiene montaje
        //0 no tiene montaje

        $update = $this->model_proyecto->where('id', $data['id'])
        ->update([
                    'montaje' => $data['montaje'],
                    'usuario_edicion' => auth()->user()->id,
                    'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                ]);

        return $update?1:$update;

   }

    public function completar($data){

        //completar la etapa de cotizacion

        $adjuntos = $this->model_cotizacion_documentaria
        ->leftJoin('adjunto', 'cotizacion_documentaria.adjunto_id', '=', 'adjunto.id')
        ->where('cotizacion_documentaria.proyecto_id',$data["id"])
        ->where('adjunto.codigo','cotizacion')
        ->select('cotizacion_documentaria.*')
        ->get();
        $update=0;
        if(count($adjuntos)>0){
            $update = $this->model_tarcking->where('proyecto_id', $data['id'])
            ->update([
                        'cotizacion' => 2,
                        'cotizacion_fecha'=>now()->format('Y-m-d H:i:s'),
                        'pedido' => 1,
                        'usuario_edicion' => auth()->user()->id,
                        'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                    ]);
        }

        return $update?1:$update;

   }
   public function aprobarPresupuesto($data){
    

        //actualizar estado del presupuesto a 1
        //habilitar la etapa de presupuesto
        //deshabilitar etapa de cotiza
        //isumo.verificado = 0

        DB::beginTransaction();
        try {
            $response =false;

                $update = $this->model_presupuesto->where('proyecto_id', $data['id'])
                    ->update([
                        'etapa' => 1,
                        'usuario_edicion' => auth()->user()->id,
                        'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                    ]);
                if($update){
                    $update = $this->model_tarcking->where('proyecto_id', $data['id'])
                    ->update([
                                'presupuesto' => 1,
                                'presupuesto_fecha' => now()->format('Y-m-d H:i:s'),
                                'cotizacion' => 3,
                                'usuario_edicion' => auth()->user()->id,
                                'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                            ]);

                    if($update){
                        $update = $this->model_insumo->where('proyecto_id', $data['id'])
                        ->update([
                                'verificado' => 0,
                                'usuario_edicion' => auth()->user()->id,
                                'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                            ]);


                            if($update){

                                $update = $this->model_utilidad->where('proyecto_id', $data['id'])
                                ->update([
                                        'precio_unitario' => 0,
                                        'precio_parcial' => 0,
                                        'porcentaje' => 0,
                                        'usuario_edicion' => auth()->user()->id,
                                        'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                                    ]);
                                if($update){
                                 
                                    $update = $this->model_servicio->where('proyecto_id', $data['id'])
                                ->update([
                                       
                                        'verificado' => 0,
                                        'usuario_edicion' => auth()->user()->id,
                                        'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                                    ]);
                                }
                            }
                    }
                }
                $response=true;
                DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        return $update?1:$update;

   }

   public function exportarCotizacion($data){
    $servicios = $this->model_servicio->where('item_servicio.proyecto_id', $data["proyecto_id"])
    ->where('item_servicio.estado', 1)
    ->select(
        'item_servicio.id',
        'item_servicio.descripcion as item_nombre',
        DB::raw('"PZA" as unidad_'),
        'item_servicio.cantidad as item_cantidad',
        DB::raw('"1" as habilitado'),
        DB::raw('"0" as porcentaje'),
        DB::raw('"0" as area'),
        DB::raw('"0" as peso'),
        DB::raw('"0" as material'),
        DB::raw('"0" as m_o'),
        DB::raw('item_servicio.precio as costo'),
        DB::raw('item_servicio.precio   as precio_parcial_utilidad'),
        DB::raw('item_servicio.precio  as precio_unitario_utilidad'),
        DB::raw('"servicio" as tipo'),
        'item_servicio.fecha_creacion as fecha_creacion'
    );

        $datos = $this->model->where("item.proyecto_id", $data["proyecto_id"])
        ->where("item.estado", 1) 

        ->select(
                    'item.id',
                    'item.nombre as item_nombre',
                    DB::raw('"PZA" as unidad_'),
                    'item.cantidad as item_cantidad',
                    'item.habilitado as habilitado',
                    'item.margen as porcentaje',
                    DB::raw("(SELECT SUM(item_insumo.area_parcial) from item_insumo WHERE item_insumo.item_id = item.id) as area"),
                    DB::raw("(SELECT SUM(item_insumo.peso_parcial) from item_insumo WHERE item_insumo.item_id = item.id) as peso"),
                   DB::raw("(SELECT SUM(item_insumo.costo_material_2) from item_insumo WHERE item_insumo.item_id = item.id) as material"),
               
                    DB::raw("(SELECT SUM(item_insumo.costo_parcial) from item_insumo WHERE item_insumo.item_id = item.id) as m_o"),
                    
                    DB::raw(" (
                            (SELECT SUM(item_insumo.costo_parcial) from item_insumo WHERE item_insumo.item_id = item.id)
                            +
                            (SELECT SUM(item_insumo.costo_material_2) from item_insumo WHERE item_insumo.item_id = item.id)
                        ) as costo"),

                    DB::raw("IFNULL((SELECT utilidad.precio_parcial from utilidad WHERE utilidad.item_id = item.id),0) as precio_parcial_utilidad"),
                    DB::raw("IFNULL((SELECT utilidad.precio_unitario from utilidad WHERE utilidad.item_id = item.id),0) as precio_unitario_utilidad"),
                    DB::raw('"material" as tipo'),
                    'item.fecha_creacion as fecha_creacion'
                )

                ->union($servicios)
                ->orderBy('fecha_creacion', 'desc')
                ->get();


        foreach ($datos as $key => $value) {

          //  $unidad =  ($value["item_cantidad"]>0)? ($value["costo"] + ( $value["costo"] * ($value["porcentaje"]/100)) ) * $value["item_cantidad"] : 0 ;
            $unidad =  ($value["item_cantidad"]>0)? ( $value["costo"]  / $value["item_cantidad"] ) : 0 ;


            $cotiza[$key]["nombre"] =$value["item_nombre"];
            $cotiza[$key]["unidad"] =$value["unidad_"];
            $cotiza[$key]["cantidad"] =$value["item_cantidad"];
            if($value["habilitado"]==0){
                $cotiza[$key]["unitario"] =$value["precio_unitario_utilidad"];
                $cotiza[$key]["parcial"] =$value["precio_parcial_utilidad"];
            }else{
                if($value["tipo"]=="servicio"){
                    $cotiza[$key]["unitario"] =$value["costo"];
                    $cotiza[$key]["parcial"] =$value["costo"]* $value["item_cantidad"];
                }else{
                    $cotiza[$key]["unitario"] =$unidad;
                    $cotiza[$key]["parcial"] =$unidad * $value["item_cantidad"];
                }
               
            }

        }

        //$cotiza=collect($cotiza);
        $datos["cotiza"]=$cotiza;
        $datos["proyecto"] = $this->model_proyecto
        ->leftJoin('cliente','cliente.id','=','proyecto.cliente_id')
        ->where("proyecto.id",$data["proyecto_id"])
        ->select('proyecto.nombre','proyecto.codigo','cliente.descripcion','proyecto.fecha_creacion')->first();

        return $datos;
    }

}
