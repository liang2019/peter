<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'auth'],function () {
    Route::group(['prefix' => 'cotizacion'], function () {

        Route::get('/proyectos', ['as' => 'cotizacion', 'uses' => 'CotizacionProyectoController@index']);
        Route::get('/proyectos/cargar', ['as' => 'cotizacion.cargar', 'uses' => 'CotizacionProyectoController@cargarAll']);
        Route::post('/proyectos/cancelar', ['as' => 'cotizacion.cancelar', 'uses' => 'CotizacionProyectoController@cancelar']);

        Route::post('/proyectos/cargar', ['as' => 'cotizacion.cargar', 'uses' => 'CotizacionController@cargarItems']);
        Route::post('/proyectos/completar', ['as' => 'cotizacion.completar', 'uses' => 'CotizacionController@completar']);
        Route::post('/proyectos/aprobar', ['as' => 'cotizacion.aprobarPresupuesto', 'uses' => 'CotizacionController@aprobarPresupuesto']);
        Route::post('/proyectos/montaje', ['as' => 'cotizacion.montaje', 'uses' => 'CotizacionController@montaje']);
        Route::get('/proyectos/{id}', ['as' => 'cotizacion.id', 'uses' => 'CotizacionController@index']);
        Route::get('/exportar/{id}', ['as' => 'cotizacion.exportar', 'uses' => 'CotizacionController@exportar']);

        Route::get('/documentaria', ['as' => 'documentaria', 'uses' => 'CotizacionAdjuntoController@index']);
        Route::get('/documentaria/{id}', ['as' => 'documentaria.id', 'uses' => 'CotizacionAdjuntoController@index']);
        Route::post('/documentaria/cargar', ['as' => 'documentaria.cargar', 'uses' => 'CotizacionAdjuntoController@cargarAll']);
        Route::post('/documentaria/buscar/', ['as' => 'documentaria.buscar', 'uses' => 'CotizacionAdjuntoController@buscar']);
        Route::patch('/documentaria/editar/', ['as' => 'documentaria.editar', 'uses' => 'CotizacionAdjuntoController@editar']);
        Route::post('/documentaria/eliminar/', ['as' => 'documentaria.eliminar', 'uses' => 'CotizacionAdjuntoController@eliminar']);
        Route::post('/documentaria/guardar/', ['as' => 'documentaria.guardar', 'uses' => 'CotizacionAdjuntoController@guardar']);
        Route::post('/documentaria/updatefile/', ['as' => 'documentaria.updatefile', 'uses' => 'CotizacionAdjuntoController@updatefile']);
        Route::get('/documentaria/download/{id}', ['as' => 'documentaria.download', 'uses' => 'CotizacionAdjuntoController@download']);

    });
});
