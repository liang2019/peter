<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'auth'],function () {
    Route::group(['prefix' => 'contabilidad'], function () {

        Route::get('/importar', ['as' => 'reports', 'uses' => 'ContabilidadImportacionesController@index']);

        
        Route::post('/importar/cuentas', ['as' => 'reports', 'uses' => 'ContabilidadImportacionesController@importarCuentas']);
        Route::get('/reporte/download/{id}', ['as' => 'documentaria.download', 'uses' => 'ContabilidadReporteController@download']);
        Route::get('/reporte1/cargar', ['as' => 'reportes', 'uses' => 'ContabilidadReporteController@cargarReporte1All']);
        Route::post('/reporte1/buscar/', ['as' => 'reportes.buscar', 'uses' => 'ContabilidadReporteController@cargarReporte1']);
        Route::patch('/reporte1/editar/', ['as' => 'reportes.editar', 'uses' => 'ContabilidadReporteController@editarReporte1']);
        Route::post('/reporte1/eliminar/', ['as' => 'reportes.eliminar', 'uses' => 'ContabilidadReporteController@eliminar']);
      


    });
});
