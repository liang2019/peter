<?php

namespace App\Modules\Contabilidad\Services;


use App\Modules\Contabilidad\Models\ContabilidadCuentas;
use App\Modules\Contabilidad\Models\ContabilidadAgrupacion;
use App\Modules\Contabilidad\Models\ContabilidadReporte;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
class ContabilidadReporteService
{

    public function __construct()
    {
        $this->model = new ContabilidadReporte();
        $this->model_cuentas = new ContabilidadCuentas();
        $this->model_agrupacion = new ContabilidadAgrupacion();

    }
    

    public function cargarReporte1All(){
        $datos = $this->model->where('estado',1)
        ->select('*','id as identificador_auto')
        ->orderBy('id', 'desc')
        ->get();
        
       
        return $datos;
    }
    public function cargarReporte1($data){
        $datos = $this->model->where('estado',1)
        ->where('id','=',$data["id"])
        ->select('*')
        ->first();
        
       
        return $datos;
    }
    
    public function editarReporte1($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update(array("comentario"=>$data["comentario"],"usuario_edicion"=>auth()->user()->id,"fecha_modificacion"=>now()->format('Y-m-d H:i:s')));
        return $update?1:$update;
    }

    public function eliminar($data){
        $data["usuario_edicion"] = auth()->user()["id"];
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');

        // elimina el archivo
        $archivo = $this->model->find($data['id']);

        $exists = Storage::disk('local')->exists(config('app.upload_paths.contabilidad.reportes').'/'.$data['id'].'-'.$archivo->adjunto);

        if ($exists) {
            $delete = Storage::disk('local')->delete(config('app.upload_paths.contabilidad.reportes').'/'.$data['id'].'-'.$archivo->adjunto);
        }
          // elimina el adjunto
        $update = $archivo->delete();
        return $update?1:$update;
    }

    public function download($id)
    {
        $datos =  $this->model->all()->where('id', $id)->first();
        return $datos;
    }

}
