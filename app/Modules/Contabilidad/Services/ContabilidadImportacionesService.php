<?php

namespace App\Modules\Contabilidad\Services;


use App\Modules\Contabilidad\Models\ContabilidadCuentas;
use App\Modules\Contabilidad\Models\ContabilidadAgrupacion;
use App\Modules\Contabilidad\Models\ContabilidadReporte;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use org\majkel\dbase\Table;
use XBase\Table as Table2;

class ContabilidadImportacionesService
{

    public function __construct()
    {
        $this->model_cuentas = new ContabilidadCuentas();
        $this->model_agrupacion = new ContabilidadAgrupacion();
        $this->model_reporte = new ContabilidadReporte();

    }
    public function cuentasArray($record){
        $array = array(
         
            'dsubdia'=>utf8_encode($record->dsubdia),
            
            'dcompro'=>utf8_encode($record->dcompro),
            'dsecue'=>utf8_encode($record->dsecue),
            'dfeccom'=>utf8_encode($record->dfeccom),
            'dcuenta'=>utf8_encode($record->dcuenta),
            'dcodane'=>utf8_encode($record->dcodane),
            'dcencos'=>utf8_encode($record->dcencos),
            'dcodmon'=>utf8_encode($record->dcodmon),
            'ddh'=>utf8_encode($record->ddh),
            'dimport'=>$record->dimport,
            'dtipdoc'=>utf8_encode($record->dtipdoc),
            'dnumdoc'=>utf8_encode($record->dnumdoc),
            'dfecdoc'=>utf8_encode($record->dfecdoc),
            'dfecven'=>utf8_encode($record->dfecven),
            'darea'=>utf8_encode($record->darea),
            'dflag'=>utf8_encode($record->dflag),
            'ddate'=>date('Y-m-d',strtotime($record->ddate)),
            'dxglosa'=>utf8_encode($record->dxglosa),
            'dusimpor'=>$record->dusimpor,
            'dmnimpor'=>$record->dmnimpor,
            'dcodarc'=>utf8_encode($record->dcodarc),
            'dfeccom2'=>date('Y-m-d',strtotime($record->dfeccom2)),
            'dfecdoc2'=>date('Y-m-d',strtotime($record->dfecdoc2)),
            'dfecven2'=>date('Y-m-d',strtotime($record->dfecven2)),
            'dcodane2'=>utf8_encode($record->dcodane2),
            'dvanexo'=>utf8_encode($record->dvanexo),
            'dvanexo2'=>utf8_encode($record->dvanexo2),
            'dtipcam'=>utf8_encode($record->dtipcam),
            'dcantid'=>utf8_encode($record->dcantid),
            'drete'=>utf8_encode($record->drete),
            'dporre'=>utf8_encode($record->dporre),
            'dtipdor'=>utf8_encode($record->dtipdor),
            'dnumdor'=>utf8_encode($record->dnumdor),
            'dfecdo2'=>date('Y-m-d',strtotime($record->dfecdo2)),
            'dtiptas'=>utf8_encode($record->dtiptas),
            'dimptas'=>utf8_encode($record->dimptas),
            'dimpbmn'=>utf8_encode($record->dimpbmn),
            'dimpbus'=>utf8_encode($record->dimpbus),
            'dinacom'=>utf8_encode($record->dinacom),
            'digvcom'=>utf8_encode($record->digvcom),
            'dmedpag'=>utf8_encode($record->dmedpag),
            'dmoncom'=>utf8_encode($record->dmoncom),
            'dcolcom'=>utf8_encode($record->dcolcom),
            'dbascom'=>utf8_encode($record->dbascom),
            'dtpconv'=>utf8_encode($record->dtpconv),
            'dflgcom'=>utf8_encode($record->dflgcom),
            'dtipaco'=>utf8_encode($record->dtipaco),
            'danecom'=>utf8_encode($record->danecom)
        );
        return $array;
    }
  
    public function guardarReporte($request){
        $input = $request->all();
        $input["usuario_creacion"] = auth()->user()["id"];
        $input["usuario_edicion"] = auth()->user()["id"];
        $input["comentario"] = $input["comentario"];
        unset($input["adjunto_file"]);
        $input["adjunto"] = pathinfo($input["adjunto"]);;
        $input["adjunto"] = $input["adjunto"]['filename'];
        $save = $this->model_reporte->create($input);

        return $save;
    }
    public function importarCuentas($request){
        $input = $request->all();
  
       
            $adjunto_file = $input["adjunto_file"];
            unset($input["adjunto_file"]);
            $nombre = "cuentas.dbf";
            // borramos el archivo 
            $exists = Storage::disk('local')->exists(config('app.upload_paths.importaciones.adjuntos').'/'.$nombre);
            if ($exists) {
                $delete = Storage::disk('local')->delete(config('app.upload_paths.importaciones.adjuntos').'/'.$nombre);
            }
            // borramos el archivo 

            if(  isset($input["adjunto"]) && $input["adjunto"]!='null' ){
                $path = Storage::putFileAs(
                    config('app.upload_paths.importaciones.adjuntos'), $adjunto_file, $nombre
                );
                if(!$path){
                    return 0;
                }
            }
            $filename=base_path().'/storage/app/'.config('app.upload_paths.importaciones.adjuntos').'/'.$nombre;

            try {

                $totalSum = 0;
                //documentación https://packagist.org/packages/org.majkel/dbase
                //$dbf = Table::fromFile($filename);
                $dbf = new Table2($filename);
              
                $i=0;
                ini_set('memory_limit','460M');
                ini_set('max_execution_time', 2280);
                $i=0; 
                DB::beginTransaction();
                $this->model_cuentas->truncate();
                
                while ($record = $dbf->nextRecord()) {
                  
                    if (! empty($record->dcuenta) || $record->dcuenta != null || $record->dcuenta != "") {
                        $valuesDBF = $this->cuentasArray($record);
                        $this->model_cuentas->create( $valuesDBF);
                    }
                }
                
                DB::commit();

                //code...
            } catch (Exception $th) {
             
                Storage::disk('local')->delete(config('app.upload_paths.importaciones.adjuntos').'/'.$nombre);
                DB::rollBack();
                //throw $th;
            }
            
            
        return json_encode(array("response"=>true));
        
    }
    
    public function exportarContabilidad(){
        //saldo_inicial siempre comienza con "10"
       /* ini_set('memory_limit','360M');
        ini_set('max_execution_time', 1280);*/
        $cuentas = $this->model_cuentas::select('dusimpor','dcuenta','ddh')->get();
        //dd(json_decode($cuentas));die();
        $saldo_inicial_D = 0;
        $saldo_inicial_H = 0;
        $ingresos_D = 0;
        $ingresos_H = 0;
        $ctas_cobrar_comerciales_D = 0;
        $ctas_cobrar_comerciales_H = 0;
        $ctas_cobrar_relacionadas_D = 0;
        $ctas_cobrar_relacionadas_H = 0;
        $ctas_cobrar_empleados_D = 0;
        $ctas_cobrar_empleados_H = 0;
        $ctas_cobrar_accionistas_D = 0;
        $ctas_cobrar_accionistas_H = 0;
        $ctas_cobrar_gerentes_D = 0;
        $ctas_cobrar_gerentes_H = 0;
        $ctas_cobrar_terceros_D = 0;
        $ctas_cobrar_terceros_H = 0;
        $ctas_cobrar_divrelacionadas_D = 0;
        $ctas_cobrar_divrelacionadas_H = 0;
        $total_ingresos = array("D"=>0,"H"=>0);
        //egresos
        // - TRIBUTOS POR PAGAR 
        $igv_cta_propia = array("D"=>0,"H"=>0);
        $igv_pend_pago = array("D"=>0,"H"=>0);
        $igv_cta_propia_me = array("D"=>0,"H"=>0);
        $igv_serv_nodomiciliario = array("D"=>0,"H"=>0);
        $igv_reg_percep_ventas = array("D"=>0,"H"=>0);
        $igv_reg_percep_compras = array("D"=>0,"H"=>0);
        $igv_reg_percep_aplicar_me= array("D"=>0,"H"=>0);
        $igv_reg_percep_aplicar_mn = array("D"=>0,"H"=>0);
        $igv_reg_percep_vtas_cobrar_mn = array("D"=>0,"H"=>0);
        $igv_reg_percep_vtas_cobrar_me = array("D"=>0,"H"=>0);
        $igv_reg_retenciones = array("D"=>0,"H"=>0);
        $derechos_arancelarios = array("D"=>0,"H"=>0);
        $renta_3era_categ = array("D"=>0,"H"=>0);
        $renta_4ta_categ_mn = array("D"=>0,"H"=>0);
        $renta_4ta_categ_me = array("D"=>0,"H"=>0);
        $renta_5ta_categ = array("D"=>0,"H"=>0);
        $renta_no_domiciliados = array("D"=>0,"H"=>0);
        $otras_retenciones = array("D"=>0,"H"=>0);
        $itf_impto_financieras = array("D"=>0,"H"=>0);
        $itan_impto_activos_netos = array("D"=>0,"H"=>0);
        $otros_impuestos = array("D"=>0,"H"=>0);
        $osce_bienes_servicios = array("D"=>0,"H"=>0);
        $essalud = array("D"=>0,"H"=>0);
        $complement_trab_riesgo = array("D"=>0,"H"=>0);
        $essalud_vida = array("D"=>0,"H"=>0);
        $essalud_vacaciones_devengadas_no_pagadas = array("D"=>0,"H"=>0);
        $onp = array("D"=>0,"H"=>0);
        $contrib_senati=array("D"=>0,"H"=>0);
        $conasev = array("D"=>0,"H"=>0);
        $impto_patrimonio_vehicular = array("D"=>0,"H"=>0);
        $impto_alcabala = array("D"=>0,"H"=>0);
        $impto_predial = array("D"=>0,"H"=>0);
        $licencia_apertura_establec = array("D"=>0,"H"=>0);
        $servicios_publicos_arbitrios =array("D"=>0,"H"=>0);
        $fracc_sunat = array("D"=>0,"H"=>0);
        $fracc_essalud = array("D"=>0,"H"=>0);
        $fracc_onp = array("D"=>0,"H"=>0);
        $tributos_pagar = array("D"=>0,"H"=>0);//es el total del grupo
        //egresos
        // - AFP
        $afp_profuturo = array("D"=>0,"H"=>0);
        $afp_horizonte = array("D"=>0,"H"=>0);
        $afp_integra = array("D"=>0,"H"=>0);
        $afp_prima = array("D"=>0,"H"=>0);
        $afp_habitat = array("D"=>0,"H"=>0);
        $afp = array("D"=>0,"H"=>0);//es el total del grupo
        
        //egresos
        // - REMUNERACIONES Y PARTICIPACIONES POR PAGAR
        $sueldos = array("D"=>0,"H"=>0);
        $salarios = array("D"=>0,"H"=>0);
        $gratif_empleados = array("D"=>0,"H"=>0);
        $gratif_obreros = array("D"=>0,"H"=>0);
        $vacaciones_empleados = array("D"=>0,"H"=>0);
        $vacaciones_obreros = array("D"=>0,"H"=>0);
        $particip_empleados_pagar = array("D"=>0,"H"=>0);
        $particip_obreros_pagar = array("D"=>0,"H"=>0);
        $cts_empleados_mn = array("D"=>0,"H"=>0);
        $cts_empleados_me = array("D"=>0,"H"=>0);
        $adelanto_cts_empleados = array("D"=>0,"H"=>0);
        $adelanto_cts_obreros = array("D"=>0,"H"=>0);
        $otras_remuneraciones_particip_pagar = array("D"=>0,"H"=>0);
        $remune_partici_pagar = array("D"=>0,"H"=>0);//es el total del grupo

        //egresos
        // CUENTAS POR PAGAR COMERCIALES 
        $facturas_mn_noemitidas_pagar = array("D"=>0,"H"=>0);
        $facturas_me_noemitidas_pagar = array("D"=>0,"H"=>0);
        $fact_emit_pagar_mn_terceros = array("D"=>0,"H"=>0);
        $fact_emit_pagar_me_terceros = array("D"=>0,"H"=>0);
        $detracc_pagar_mn = array("D"=>0,"H"=>0);
        $fact_detracc_pagar_mn = array("D"=>0,"H"=>0);
        $fact_detracc_pagar_me = array("D"=>0,"H"=>0);
        $anticip_provee_mn = array("D"=>0,"H"=>0);
        $anticip_provee_me = array("D"=>0,"H"=>0);
        $letras_pagar_mn_terceros = array("D"=>0,"H"=>0);
        $letras_pagar_me_terceros = array("D"=>0,"H"=>0);
        $honorarios_pagar_mn = array("D"=>0,"H"=>0);
        $honorarios_pagar_me = array("D"=>0,"H"=>0);
        $ctas_pagar_comerciales = array("D"=>0,"H"=>0);//es el total del grupo

        //egresos
        // CUENTAS POR PAGAR RELACIONADAS  
        $fact_emit_pagar_mn_matriz = array("D"=>0,"H"=>0);
        $fact_emit_pagar_me_matriz = array("D"=>0,"H"=>0);
        $fact_emit_pagar_me_subsid = array("D"=>0,"H"=>0);
        $fact_emit_pagar_mn_subsid = array("D"=>0,"H"=>0);
        $fact_emit_pagar_mn_asoc = array("D"=>0,"H"=>0);
        $fact_emit_pagar_me_asoc = array("D"=>0,"H"=>0);
        $fact_emit_pagar_mn_sucur = array("D"=>0,"H"=>0);
        $fact_emit_pagar_me_sucur = array("D"=>0,"H"=>0);
        $antic_otorgados_mn_matriz = array("D"=>0,"H"=>0);
        $antic_otorgados_me_matriz = array("D"=>0,"H"=>0);
        $antic_otorgados_mn_subsid = array("D"=>0,"H"=>0);
        $antic_otorgados_me_subsid = array("D"=>0,"H"=>0);
        $antic_otorgados_mn_asoc = array("D"=>0,"H"=>0);
        $antic_otorgados_me_asoc = array("D"=>0,"H"=>0);
        $antic_otorgados_mn_sucur = array("D"=>0,"H"=>0);
        $antic_otorgados_me_sucur = array("D"=>0,"H"=>0);
        $antic_otorgados_mn_otros = array("D"=>0,"H"=>0);
        $antic_otorgados_me_otros = array("D"=>0,"H"=>0);
        $letras_pagar_mn_relac_matriz = array("D"=>0,"H"=>0);
        $letras_pagar_me_relac_matriz = array("D"=>0,"H"=>0);
        $ctas_pagar_relacionadas = array("D"=>0,"H"=>0);//es el total del grupo

        //egresos
        // PRESTAMOS POR PAGAR ACCIONISTAS DIRECTORES GTES  
        $ptmos_accion_corto_plazo_mn = array("D"=>0,"H"=>0);
        $ptmos_accion_corto_plazo_me = array("D"=>0,"H"=>0);
        $ptmos_accion_largo_plazo_mn = array("D"=>0,"H"=>0);
        $ptmos_accion_largo_plazo_me = array("D"=>0,"H"=>0);
        $dietas_directorio = array("D"=>0,"H"=>0);
        $ptmos_pagar_accion_direct_gtes = array("D"=>0,"H"=>0);//es el total del grupo

        //egresos
        // PRESTAMOS Y OBLIGACIONES FINANCIERAS 
        $ptmos_inst_financ_mn = array("D"=>0,"H"=>0);
        $ptmos_inst_financ_me = array("D"=>0,"H"=>0);
        $leasing_mn = array("D"=>0,"H"=>0);
        $leasing_me = array("D"=>0,"H"=>0);
        $leaseback_mn = array("D"=>0,"H"=>0);
        $leaseback_me = array("D"=>0,"H"=>0);
        $pagares_mn = array("D"=>0,"H"=>0);
        $pagares_me = array("D"=>0,"H"=>0);
        $confirming_mn = array("D"=>0,"H"=>0);
        $confirming_me = array("D"=>0,"H"=>0);
        $factoring_mn = array("D"=>0,"H"=>0);
        $factoring_me = array("D"=>0,"H"=>0);
        $letras_dsc_mn = array("D"=>0,"H"=>0);
        $letras_dsc_me = array("D"=>0,"H"=>0);
        $tarjeta_credito_me = array("D"=>0,"H"=>0);
        $ptmos_oblig_financ = array("D"=>0,"H"=>0);//es el total del grupo
        
        //egresos
        // CUENTAS POR PAGAR DIVERSAS 
        $reclam_terceros_mn = array("D"=>0,"H"=>0);
        $reclam_terceros_me = array("D"=>0,"H"=>0);
        $depos_recib_garantia = array("D"=>0,"H"=>0);
        $ctas_pagar_div_mn = array("D"=>0,"H"=>0);
        $ctas_pagar_div_me = array("D"=>0,"H"=>0);
        $ctas_pagar_diversas = array("D"=>0,"H"=>0);//es el total del grupo

        //egresos
        // CUENTAS POR PAGAR DIVERSAS REALCIONADAS
        $ptmos_recib_mn = array("D"=>0,"H"=>0);
        $ptmos_recib_me = array("D"=>0,"H"=>0);
        $ptmos_accion_mn = array("D"=>0,"H"=>0);
        $ptmos_accion_me = array("D"=>0,"H"=>0);
        $ctas_pagar_diversas_rela = array("D"=>0,"H"=>0);//es el total del grupo

        //egresos
        // PROVICIONES POR PAGAR
        $prov_pagar = array("D"=>0,"H"=>0);
        
        $total_egresos = array("D"=>0,"H"=>0);
        foreach ($cuentas as $key => $value) {
            if (substr($value["dcuenta"],0,2) == "10") {
                $saldo_inicial_D += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                $saldo_inicial_H += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
            }
            //INGRESOS
            if(floatval($value["dcuenta"]) >= 121201 && floatval($value["dcuenta"]) <= 171212){
                /*if (floatval($value["dcuenta"]) > 120000 && floatval($value["dcuenta"]) < 180000) {
                    $ingresos_D += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $ingresos_H += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }*/
                if (floatval($value["dcuenta"]) > 120000 && floatval($value["dcuenta"]) < 130000) {
                    $ctas_cobrar_comerciales_D += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $ctas_cobrar_comerciales_H += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                if (floatval($value["dcuenta"]) > 130000 && floatval($value["dcuenta"]) < 140000) {
                    $ctas_cobrar_relacionadas_D += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $ctas_cobrar_relacionadas_H += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                if (floatval($value["dcuenta"]) > 140000 && floatval($value["dcuenta"]) < 142200) {
                    $ctas_cobrar_empleados_D += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $ctas_cobrar_empleados_H += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                if (floatval($value["dcuenta"]) == 142201 && floatval($value["dcuenta"]) == 142202) {
                    $ctas_cobrar_accionistas_D += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $ctas_cobrar_accionistas_H += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                if (floatval($value["dcuenta"]) > 143000 && floatval($value["dcuenta"]) < 144303) {
                    $ctas_cobrar_gerentes_D += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $ctas_cobrar_gerentes_H += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                if (floatval($value["dcuenta"]) > 161000 && floatval($value["dcuenta"]) < 171000) {
                    $ctas_cobrar_terceros_D += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $ctas_cobrar_terceros_H += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                if (floatval($value["dcuenta"]) > 161000 && floatval($value["dcuenta"]) < 171000) {
                    $ctas_cobrar_divrelacionadas_D += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $ctas_cobrar_divrelacionadas_H += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                $total_ingresos["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                $total_ingresos["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                
            }

            //EGRESOS 
            //TRIBUTOS POR PAGAR
            if(floatval($value["dcuenta"]) >= 401111 && floatval($value["dcuenta"]) <= 486101){
                if ((floatval($value["dcuenta"]) >= 401111 && floatval($value["dcuenta"]) <= 406341) || (floatval($value["dcuenta"]) >= 409991 && floatval($value["dcuenta"]) <= 409993) ) {
                    if (floatval($value["dcuenta"]) == 401111) {
                        $igv_cta_propia["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $igv_cta_propia["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401112) {
                        $igv_pend_pago["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $igv_pend_pago["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401113) {
                        $igv_cta_propia_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $igv_cta_propia_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401121) {
                        $igv_serv_nodomiciliario["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $igv_serv_nodomiciliario["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401130) {
                        $igv_reg_percep_ventas["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $igv_reg_percep_ventas["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401131) {
                        $igv_reg_percep_compras["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $igv_reg_percep_compras["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401132) {
                        $igv_reg_percep_aplicar_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $igv_reg_percep_aplicar_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401133) {
                        $igv_reg_percep_aplicar_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $igv_reg_percep_aplicar_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401134) {
                        $igv_reg_percep_vtas_cobrar_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $igv_reg_percep_vtas_cobrar_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401135) {
                        $igv_reg_percep_vtas_cobrar_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $igv_reg_percep_vtas_cobrar_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401141) {
                        $igv_reg_retenciones["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $igv_reg_retenciones["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401511) {
                        $derechos_arancelarios["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $derechos_arancelarios["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401711) {
                        $renta_3era_categ["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $renta_3era_categ["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401721) {
                        $renta_4ta_categ_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $renta_4ta_categ_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401722) {
                        $renta_4ta_categ_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $renta_4ta_categ_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401731) {
                        $renta_5ta_categ["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $renta_5ta_categ["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401741) {
                        $renta_no_domiciliados["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $renta_no_domiciliados["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401751) {
                        $otras_retenciones["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $otras_retenciones["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401811) {
                        $itf_impto_financieras["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $itf_impto_financieras["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401861) {
                        $itan_impto_activos_netos["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $itan_impto_activos_netos["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401891) {
                        $otros_impuestos["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $otros_impuestos["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401891) {
                        $otros_impuestos["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $otros_impuestos["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 401892) {
                        $osce_bienes_servicios["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $osce_bienes_servicios["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 403101) {
                        $essalud["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $essalud["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 403102) {
                        $complement_trab_riesgo["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $complement_trab_riesgo["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 403103) {
                        $essalud_vida["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $essalud_vida["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 403104) {
                        $essalud_vacaciones_devengadas_no_pagadas["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $essalud_vacaciones_devengadas_no_pagadas["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 403201) {
                        $onp["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $onp["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 403301) {
                        $contrib_senati["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $contrib_senati["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 403901) {
                        $conasev["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $conasev["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 406111) {
                        $impto_patrimonio_vehicular["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $impto_patrimonio_vehicular["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 406141) {
                        $impto_alcabala["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $impto_alcabala["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 406151) {
                        $impto_predial["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $impto_predial["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 406311) {
                        $licencia_apertura_establec["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $licencia_apertura_establec["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 406341) {
                        $servicios_publicos_arbitrios["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $servicios_publicos_arbitrios["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 409991) {
                        $fracc_sunat["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fracc_sunat["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 409992) {
                        $fracc_essalud["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fracc_essalud["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 409993) {
                        $fracc_onp["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fracc_onp["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    $tributos_pagar["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $tributos_pagar["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                //EGRESOS - AFP
    
                if (floatval($value["dcuenta"]) >= 407101 && floatval($value["dcuenta"]) <= 407105) {
                    if (floatval($value["dcuenta"]) == 407101) {
                        $afp_profuturo["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $afp_profuturo["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 407102) {
                        $afp_horizonte["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $afp_horizonte["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 407103) {
                        $afp_integra["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $afp_integra["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 407104) {
                        $afp_prima["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $afp_prima["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 407105) {
                        $afp_habitat["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $afp_habitat["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    $afp["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $afp["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                //EGRESOS -  REMUNERACIONES Y PARTICIPACIONES POR PAGAR 
    
                if (floatval($value["dcuenta"]) >= 411101 && floatval($value["dcuenta"]) <= 419101) {
                    if (floatval($value["dcuenta"]) == 411101) {
                        $sueldos["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $sueldos["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 411102) {
                        $salarios["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $salarios["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 411401) {
                        $gratif_empleados["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $gratif_empleados["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 411402) {
                        $gratif_obreros["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $gratif_obreros["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 411501) {
                        $vacaciones_empleados["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $vacaciones_empleados["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 411502) {
                        $vacaciones_obreros["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $vacaciones_obreros["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 413101) {
                        $particip_empleados_pagar["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $particip_empleados_pagar["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 413102) {
                        $particip_obreros_pagar["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $particip_obreros_pagar["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 415101) {
                        $cts_empleados_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $cts_empleados_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 415102) {
                        $cts_empleados_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $cts_empleados_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 415201) {
                        $adelanto_cts_empleados["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $adelanto_cts_empleados["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 415202) {
                        $adelanto_cts_obreros["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $adelanto_cts_obreros["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    
                    if (floatval($value["dcuenta"]) == 419101) {
                        $otras_remuneraciones_particip_pagar["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $otras_remuneraciones_particip_pagar["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    $remune_partici_pagar["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $remune_partici_pagar["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                //EGRESOS -  CUENTAS POR PAGAR COMERCIALES 
    
                if (floatval($value["dcuenta"]) >= 421101 && floatval($value["dcuenta"]) <= 424102) {
                    if (floatval($value["dcuenta"]) == 421101) {
                        $facturas_mn_noemitidas_pagar["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $facturas_mn_noemitidas_pagar["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 421102) {
                        $facturas_me_noemitidas_pagar["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $facturas_me_noemitidas_pagar["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 421201) {
                        $fact_emit_pagar_mn_terceros["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fact_emit_pagar_mn_terceros["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 421202) {
                        $fact_emit_pagar_me_terceros["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fact_emit_pagar_me_terceros["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 421203) {
                        $detracc_pagar_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $detracc_pagar_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 421211) {
                        $fact_detracc_pagar_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fact_detracc_pagar_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 421212) {
                        $fact_detracc_pagar_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fact_detracc_pagar_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 422101) {
                        $anticip_provee_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $anticip_provee_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 422102) {
                        $anticip_provee_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $anticip_provee_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 423101) {
                        $letras_pagar_mn_terceros["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $letras_pagar_mn_terceros["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 423102) {
                        $letras_pagar_me_terceros["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $letras_pagar_me_terceros["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 424101) {
                        $honorarios_pagar_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $honorarios_pagar_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 424102) {
                        $honorarios_pagar_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $honorarios_pagar_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    $ctas_pagar_comerciales["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $ctas_pagar_comerciales["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                //EGRESOS -  CUENTAS POR PAGAR RELACIONADAS  
    
                if (floatval($value["dcuenta"]) >= 431211 && floatval($value["dcuenta"]) <= 433112) {
                    if (floatval($value["dcuenta"]) == 431211) {
                        $fact_emit_pagar_mn_matriz["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fact_emit_pagar_mn_matriz["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 431212) {
                        $fact_emit_pagar_me_matriz["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fact_emit_pagar_me_matriz["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 431221) {
                        $fact_emit_pagar_me_subsid["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fact_emit_pagar_me_subsid["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 431222) {
                        $fact_emit_pagar_mn_subsid["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fact_emit_pagar_mn_subsid["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 431231) {
                        $fact_emit_pagar_mn_asoc["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fact_emit_pagar_mn_asoc["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 431232) {
                        $fact_emit_pagar_me_asoc["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fact_emit_pagar_me_asoc["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 431241) {
                        $fact_emit_pagar_mn_sucur["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fact_emit_pagar_mn_sucur["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 431242) {
                        $fact_emit_pagar_me_sucur["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $fact_emit_pagar_me_sucur["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 432101) {
                        $antic_otorgados_mn_matriz["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $antic_otorgados_mn_matriz["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 432102) {
                        $antic_otorgados_me_matriz["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $antic_otorgados_me_matriz["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 432121) {
                        $antic_otorgados_mn_subsid["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $antic_otorgados_mn_subsid["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 432122) {
                        $antic_otorgados_me_subsid["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $antic_otorgados_me_subsid["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 432131) {
                        $antic_otorgados_mn_asoc["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $antic_otorgados_mn_asoc["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 432132) {
                        $antic_otorgados_me_asoc["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $antic_otorgados_me_asoc["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 432141) {
                        $antic_otorgados_mn_sucur["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $antic_otorgados_mn_sucur["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 432142) {
                        $antic_otorgados_me_sucur["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $antic_otorgados_me_sucur["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 432151) {
                        $antic_otorgados_mn_otros["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $antic_otorgados_mn_otros["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 432152) {
                        $antic_otorgados_me_otros["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $antic_otorgados_me_otros["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 433111) {
                        $letras_pagar_mn_relac_matriz["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $letras_pagar_mn_relac_matriz["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 433112) {
                        $letras_pagar_me_relac_matriz["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $letras_pagar_me_relac_matriz["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    $ctas_pagar_relacionadas["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $ctas_pagar_relacionadas["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                //EGRESOS -  PRESTAMOS POR PAGAR ACCIONISTAS DIRECTORES GTES  
    
                if (floatval($value["dcuenta"]) >= 441111 && floatval($value["dcuenta"]) <= 442101) {
                    if (floatval($value["dcuenta"]) == 441111) {
                        $ptmos_accion_corto_plazo_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $ptmos_accion_corto_plazo_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 441112) {
                        $ptmos_accion_corto_plazo_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $ptmos_accion_corto_plazo_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 441121) {
                        $ptmos_accion_largo_plazo_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $ptmos_accion_largo_plazo_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 441122) {
                        $ptmos_accion_largo_plazo_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $ptmos_accion_largo_plazo_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 442101) {
                        $dietas_directorio["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $dietas_directorio["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    $ptmos_pagar_accion_direct_gtes["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $ptmos_pagar_accion_direct_gtes["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                //EGRESOS - PRESTAMOS Y OBLIGACIONES FINANCIERAS
    
                if (floatval($value["dcuenta"]) >= 451101 && floatval($value["dcuenta"]) <= 454916) {
                    if (floatval($value["dcuenta"]) == 451101) {
                        $ptmos_inst_financ_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $ptmos_inst_financ_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 451102) {
                        $ptmos_inst_financ_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $ptmos_inst_financ_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 452101) {
                        $leasing_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $leasing_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 452102) {
                        $leasing_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $leasing_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 452111) {
                        $leaseback_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $leaseback_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 452112) {
                        $leaseback_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $leaseback_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 454401) {
                        $pagares_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $pagares_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 454402) {
                        $pagares_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $pagares_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 454901) {
                        $confirming_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $confirming_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 454902) {
                        $confirming_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $confirming_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 454911) {
                        $factoring_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $factoring_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 454912) {
                        $factoring_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $factoring_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 454913) {
                        $letras_dsc_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $letras_dsc_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 454914) {
                        $letras_dsc_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $letras_dsc_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 454916) {
                        $tarjeta_credito_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $tarjeta_credito_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    $ptmos_oblig_financ["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $ptmos_oblig_financ["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                //EGRESOS - CUENTAS POR PAGAR DIVERSAS
                if (floatval($value["dcuenta"]) >= 461101 && floatval($value["dcuenta"]) <= 469902) {
                    if (floatval($value["dcuenta"]) == 461101) {
                        $reclam_terceros_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $reclam_terceros_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 461102) {
                        $reclam_terceros_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $reclam_terceros_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 467101) {
                        $depos_recib_garantia["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $depos_recib_garantia["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 469901) {
                        $ctas_pagar_div_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $ctas_pagar_div_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 469902) {
                        $ctas_pagar_div_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $ctas_pagar_div_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    $ctas_pagar_diversas["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $ctas_pagar_diversas["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                //EGRESOS - CUENTAS POR PAGAR DIVERSAS REALCIONADAS
    
                if (floatval($value["dcuenta"]) >= 471201 && floatval($value["dcuenta"]) <= 479152) {
                    if (floatval($value["dcuenta"]) == 471201) {
                        $ptmos_recib_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $ptmos_recib_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 471202) {
                        $ptmos_recib_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $ptmos_recib_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 479151) {
                        $ptmos_accion_mn["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $ptmos_accion_mn["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    if (floatval($value["dcuenta"]) == 479152) {
                        $ptmos_accion_me["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                        $ptmos_accion_me["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                    }
                    
                    $ctas_pagar_diversas_rela["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $ctas_pagar_diversas_rela["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
                //EGRESOS - PROVICIONES POR PAGAR
    
                if (floatval($value["dcuenta"]) == 486101) {
                    $prov_pagar["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                    $prov_pagar["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
                }
            
                $total_egresos["D"] += $value["ddh"] == "D" ? $value["dusimpor"] : 0;
                $total_egresos["H"] += $value["ddh"] == "H" ? $value["dusimpor"] : 0;
            
            }
            
            
        }

        //código que optimizar todo el proceso pero se ve afectado el performance (MEJORAR CÓDIGO :C)
        /*$cuentas_agrup = $this->model_agrupacion::select('pcuenta')->get();

        $arr = [];
        foreach ($cuentas_agrup as $key => $ctas_agrup) {
            //foreach ($cuentas as $key => $ctas) {
            $found_cta = array_filter(json_decode($cuentas,true), function($k) use($ctas_agrup){
                return $k["dcuenta"] ==  $ctas_agrup["pcuenta"];
            });
            foreach ($found_cta as $key => $ctas) {
                //if ($ctas["dcuenta"]==$ctas_agrup["pcuenta"]) {
                    if (!isset($arr[$ctas_agrup["subgrupo"]][$ctas_agrup["pcuenta"]]["D"])) {
                        $arr[$ctas_agrup["subgrupo"]][$ctas_agrup["pcuenta"]]["D"] = 0;
                    }
                    if (!isset($arr[$ctas_agrup["subgrupo"]][$ctas_agrup["pcuenta"]]["H"])) {
                        $arr[$ctas_agrup["subgrupo"]][$ctas_agrup["pcuenta"]]["H"] = 0;
                    }
                    $arr[$ctas_agrup["subgrupo"]][$ctas_agrup["pcuenta"]]["D"] += $ctas["ddh"] == "D" ? $ctas["dusimpor"] : 0;
                    $arr[$ctas_agrup["subgrupo"]][$ctas_agrup["pcuenta"]]["H"] += $ctas["ddh"] == "H" ? $ctas["dusimpor"] : 0;
                break;
                //}
            }
            //}
        }
        print_r($arr);die();*/
        $datos["saldo_inicial"] = number_format(number_format($saldo_inicial_D,2,'.','')-number_format($saldo_inicial_H,2,'.',''),2);  
        //$datos["ingresos"] = $ingresos_D-$ingresos_H;
        $datos["ctas_cobrar_comerciales"] = number_format(number_format($ctas_cobrar_comerciales_D ,2,'.','')-number_format($ctas_cobrar_comerciales_H,2,'.',''),2);  
        $datos["ctas_cobrar_relacionadas"] = number_format(number_format($ctas_cobrar_relacionadas_D,2,'.','')-number_format($ctas_cobrar_relacionadas_H,2,'.',''),2);
        $datos["ctas_cobrar_empleados"] = number_format(number_format($ctas_cobrar_empleados_D,2,'.','')-number_format($ctas_cobrar_empleados_H,2,'.',''),2); 
        $datos["ctas_cobrar_accionistas"] = number_format(number_format($ctas_cobrar_accionistas_D,2,'.','')-number_format($ctas_cobrar_accionistas_H,2,'.',''),2); 
        $datos["ctas_cobrar_gerentes"] = number_format(number_format($ctas_cobrar_gerentes_D,2,'.','')-number_format($igv_cta_propia["H"],2,'.',''),2);
        $datos["ctas_cobrar_terceros"] = number_format(number_format($igv_cta_propia["D"],2,'.','')-number_format($ctas_cobrar_gerentes_H,2,'.',''),2);
        $datos["ctas_cobrar_divrelacionadas"] = number_format(number_format($ctas_cobrar_divrelacionadas_D,2,'.','')-number_format($ctas_cobrar_divrelacionadas_H,2,'.',''),2); 
        $datos["total_ingresos"] =  number_format(number_format($total_ingresos["D"],2,'.','')-number_format($total_ingresos["H"],2,'.',''),2);
        $datos["fecha_creacion"] = date("Y-m-d h:i:s");
        
        //EGRESOS - TRIBUTOS PAGAR
        //$datos["igv_cta_propiaxd"] = $arr["TRIBUTOS POR PAGAR"]["401111"]["D"]-$arr["TRIBUTOS POR PAGAR"]["401111"]["H"];
        //print_r($datos["igv_cta_propiaxd"]);die();

        $datos["igv_cta_propia"] = number_format(number_format($igv_cta_propia["D"],2,'.','')-number_format($igv_cta_propia["H"],2,'.',''),2);
        $datos["igv_pend_pago"] = number_format(number_format($igv_pend_pago["D"],2,'.','')-number_format($igv_pend_pago["H"],2,'.',''),2); 
        $datos["igv_cta_propia_me"] = number_format(number_format($igv_cta_propia_me["D"],2,'.','')-number_format($igv_cta_propia_me["H"],2,'.',''),2); 
        $datos["igv_serv_nodomiciliario"] = number_format(number_format($igv_serv_nodomiciliario["D"],2,'.','')-number_format($igv_serv_nodomiciliario["H"],2,'.',''),2); 
        $datos["igv_reg_percep_ventas"] = number_format(number_format($igv_reg_percep_ventas["D"],2,'.','')-number_format($igv_reg_percep_ventas["H"],2,'.',''),2);  
        $datos["igv_reg_percep_compras"] = number_format(number_format($igv_reg_percep_compras["D"],2,'.','')-number_format($igv_reg_percep_compras["H"],2,'.',''),2);
       
        $datos["igv_reg_percep_aplicar_me"] = number_format(number_format($igv_reg_percep_aplicar_me["D"],2,'.','')-number_format($igv_reg_percep_aplicar_me["H"],2,'.',''),2); 
        $datos["igv_reg_percep_aplicar_mn"] = number_format(number_format($igv_reg_percep_aplicar_mn["D"],2,'.','')-number_format($igv_reg_percep_aplicar_mn["H"],2,'.',''),2); 
        $datos["igv_reg_percep_vtas_cobrar_mn"] = number_format(number_format($igv_reg_percep_vtas_cobrar_mn["D"],2,'.','')-number_format($igv_reg_percep_vtas_cobrar_mn["H"],2,'.',''),2);  
        $datos["igv_reg_percep_vtas_cobrar_me"] = number_format(number_format($igv_reg_percep_vtas_cobrar_me["D"],2,'.','')-number_format($igv_reg_percep_vtas_cobrar_me["H"],2,'.',''),2);  
        $datos["igv_reg_retenciones"] = number_format(number_format($igv_reg_retenciones["D"],2,'.','')-number_format($igv_reg_retenciones["H"],2,'.',''),2); 
        $datos["derechos_arancelarios"] =number_format(number_format($derechos_arancelarios["D"],2,'.','')-number_format($derechos_arancelarios["H"],2,'.',''),2);   
        $datos["renta_3era_categ"] =number_format(number_format($renta_3era_categ["D"],2,'.','')-number_format($renta_3era_categ["H"],2,'.',''),2);  
        $datos["renta_4ta_categ_mn"] =number_format(number_format($renta_4ta_categ_mn["D"],2,'.','')-number_format($renta_4ta_categ_mn["H"],2,'.',''),2);   
        $datos["renta_4ta_categ_me"] =number_format(number_format($renta_4ta_categ_me["D"],2,'.','')-number_format($renta_4ta_categ_me["H"],2,'.',''),2); 
        $datos["renta_5ta_categ"] =number_format(number_format($renta_5ta_categ["D"],2,'.','')-number_format($renta_5ta_categ["H"],2,'.',''),2);   
        $datos["renta_no_domiciliados"] =number_format(number_format($renta_no_domiciliados["D"],2,'.','')-number_format($renta_no_domiciliados["H"],2,'.',''),2);  
        $datos["otras_retenciones"] = number_format(number_format($otras_retenciones["D"],2,'.','')-number_format($otras_retenciones["H"],2,'.',''),2);    
        $datos["itf_impto_financieras"] = number_format(number_format($itf_impto_financieras["D"],2,'.','')-number_format($itf_impto_financieras["H"],2,'.',''),2);   
        $datos["itan_impto_activos_netos"] = number_format(number_format($itan_impto_activos_netos["D"],2,'.','')-number_format($itan_impto_activos_netos["H"],2,'.',''),2);   
        $datos["otros_impuestos"] = number_format(number_format($otros_impuestos["D"],2,'.','')-number_format($otros_impuestos["H"],2,'.',''),2);   
        $datos["osce_bienes_servicios"] = number_format(number_format($osce_bienes_servicios["D"],2,'.','')-number_format($osce_bienes_servicios["H"],2,'.',''),2);  
        $datos["essalud"] = number_format(number_format($essalud["D"],2,'.','')-number_format($essalud["H"],2,'.',''),2);   
        $datos["complement_trab_riesgo"] = number_format(number_format($complement_trab_riesgo["D"],2,'.','')-number_format($complement_trab_riesgo["H"],2,'.',''),2);  
        $datos["essalud_vida"] = number_format(number_format($essalud_vida["D"],2,'.','')-number_format($essalud_vida["H"],2,'.',''),2);  
        $datos["essalud_vacaciones_devengadas_no_pagadas"] = number_format(number_format($essalud_vacaciones_devengadas_no_pagadas["D"],2,'.','')-number_format($essalud_vacaciones_devengadas_no_pagadas["H"],2,'.',''),2);   
        $datos["onp"] = number_format(number_format($onp["D"],2,'.','')-number_format($onp["H"],2,'.',''),2);   
        $datos["contrib_senati"] = number_format(number_format($contrib_senati["D"],2,'.','')-number_format($contrib_senati["H"],2,'.',''),2);   
        $datos["conasev"] = number_format(number_format($conasev["D"],2,'.','')-number_format($conasev["H"],2,'.',''),2);   
        $datos["impto_patrimonio_vehicular"] = number_format(number_format($impto_patrimonio_vehicular["D"],2,'.','')-number_format($impto_patrimonio_vehicular["H"],2,'.',''),2);   
        $datos["impto_alcabala"] = number_format(number_format($impto_alcabala["D"],2,'.','')-number_format($impto_alcabala["H"],2,'.',''),2);    
        $datos["impto_predial"] = number_format(number_format($impto_predial["D"],2,'.','')-number_format($impto_predial["H"],2,'.',''),2);   
        $datos["licencia_apertura_establec"] = number_format(number_format($licencia_apertura_establec["D"],2,'.','')-number_format($licencia_apertura_establec["H"],2,'.',''),2);  
        $datos["servicios_publicos_arbitrios"] = number_format(number_format($servicios_publicos_arbitrios["D"],2,'.','')-number_format($servicios_publicos_arbitrios["H"],2,'.',''),2);   
        $datos["fracc_sunat"] = number_format(number_format($fracc_sunat["D"],2,'.','')-number_format($fracc_sunat["H"],2,'.',''),2);  
        $datos["fracc_essalud"] = number_format(number_format($fracc_essalud["D"],2,'.','')-number_format($fracc_essalud["H"],2,'.',''),2);  
        $datos["fracc_onp"] = number_format(number_format($fracc_onp["D"],2,'.','')-number_format($fracc_onp["H"],2,'.',''),2); 

        $datos["tributos_pagar"] = number_format(number_format($tributos_pagar["D"],2,'.','')-number_format($tributos_pagar["H"],2,'.',''),2);  

        //EGRESOS - AFP

        $datos["afp_profuturo"] = number_format(number_format($afp_profuturo["D"],2,'.','')-number_format($afp_profuturo["H"],2,'.',''),2); 
        $datos["afp_horizonte"] = number_format(number_format($afp_horizonte["D"],2,'.','')-number_format($afp_horizonte["H"],2,'.',''),2);  
        $datos["afp_integra"] = number_format(number_format($afp_integra["D"],2,'.','')-number_format($afp_integra["H"],2,'.',''),2);  
        $datos["afp_prima"] = number_format(number_format($afp_prima["D"],2,'.','')-number_format($afp_prima["H"],2,'.',''),2); 
        $datos["afp_habitat"] = number_format(number_format($afp_habitat["D"],2,'.','')-number_format($afp_habitat["H"],2,'.',''),2);  

        $datos["afp"] = number_format(number_format($afp["D"],2,'.','')-number_format($afp["H"],2,'.',''),2);   

        //EGRESOS -  REMUNERACIONES Y PARTICIPACIONES POR PAGAR 

        $datos["sueldos"] = number_format(number_format($sueldos["D"],2,'.','')-number_format($sueldos["H"],2,'.',''),2); 
        $datos["salarios"] = number_format(number_format($salarios["D"],2,'.','')-number_format($salarios["H"],2,'.',''),2); 
        $datos["gratif_empleados"] = number_format(number_format($gratif_empleados["D"],2,'.','')-number_format($gratif_empleados["H"],2,'.',''),2);  
        $datos["gratif_obreros"] = number_format(number_format($gratif_obreros["D"],2,'.','')-number_format($gratif_obreros["H"],2,'.',''),2);  
        $datos["vacaciones_empleados"] = number_format(number_format($vacaciones_empleados["D"],2,'.','')-number_format($vacaciones_empleados["H"],2,'.',''),2);  
        $datos["vacaciones_obreros"] = number_format(number_format($vacaciones_obreros["D"],2,'.','')-number_format($vacaciones_obreros["H"],2,'.',''),2);   
        $datos["particip_empleados_pagar"] = number_format(number_format($particip_empleados_pagar["D"],2,'.','')-number_format($particip_empleados_pagar["H"],2,'.',''),2);  
        $datos["particip_obreros_pagar"] = number_format(number_format($particip_obreros_pagar["D"],2,'.','')-number_format($particip_obreros_pagar["H"],2,'.',''),2);   
        $datos["cts_empleados_mn"] = number_format(number_format($cts_empleados_mn["D"],2,'.','')-number_format($cts_empleados_mn["H"],2,'.',''),2);   
        $datos["cts_empleados_me"] = number_format(number_format($cts_empleados_me["D"],2,'.','')-number_format($cts_empleados_me["H"],2,'.',''),2);  
        
        $datos["adelanto_cts_empleados"] = number_format(number_format($adelanto_cts_empleados["D"],2,'.','')-number_format($adelanto_cts_empleados["H"],2,'.',''),2);   
        $datos["adelanto_cts_obreros"] = number_format(number_format($adelanto_cts_obreros["D"],2,'.','')-number_format($adelanto_cts_obreros["H"],2,'.',''),2);   
        $datos["otras_remuneraciones_particip_pagar"] = number_format(number_format($otras_remuneraciones_particip_pagar["D"],2,'.','')-number_format($otras_remuneraciones_particip_pagar["H"],2,'.',''),2);  

        $datos["remune_partici_pagar"] = number_format(number_format($remune_partici_pagar["D"],2,'.','')-number_format($remune_partici_pagar["H"],2,'.',''),2);  

        //EGRESOS -  CUENTAS POR PAGAR COMERCIALES 
        $datos["facturas_mn_noemitidas_pagar"] = number_format(number_format($facturas_mn_noemitidas_pagar["D"],2,'.','')-number_format($facturas_mn_noemitidas_pagar["H"],2,'.',''),2);  
        $datos["facturas_me_noemitidas_pagar"] = number_format(number_format($facturas_me_noemitidas_pagar["D"],2,'.','')-number_format($facturas_me_noemitidas_pagar["H"],2,'.',''),2);   
        $datos["fact_emit_pagar_mn_terceros"] = number_format(number_format($fact_emit_pagar_mn_terceros["D"],2,'.','')-number_format($fact_emit_pagar_mn_terceros["H"],2,'.',''),2);   
        $datos["fact_emit_pagar_me_terceros"] = number_format(number_format($fact_emit_pagar_me_terceros["D"],2,'.','')-number_format($fact_emit_pagar_me_terceros["H"],2,'.',''),2);   
        $datos["detracc_pagar_mn"] = number_format(number_format($detracc_pagar_mn["D"],2,'.','')-number_format($detracc_pagar_mn["H"],2,'.',''),2);   
        $datos["fact_detracc_pagar_mn"] = number_format(number_format($fact_detracc_pagar_mn["D"],2,'.','')-number_format($fact_detracc_pagar_mn["H"],2,'.',''),2);  
        $datos["fact_detracc_pagar_me"] = number_format(number_format($fact_detracc_pagar_me["D"],2,'.','')-number_format($fact_detracc_pagar_me["H"],2,'.',''),2);   
        $datos["anticip_provee_mn"] = number_format(number_format($anticip_provee_mn["D"],2,'.','')-number_format($anticip_provee_mn["H"],2,'.',''),2);   
        $datos["anticip_provee_me"] = number_format(number_format($anticip_provee_me["D"],2,'.','')-number_format($anticip_provee_me["H"],2,'.',''),2);  
        $datos["letras_pagar_mn_terceros"] = number_format(number_format($letras_pagar_mn_terceros["D"],2,'.','')-number_format($letras_pagar_mn_terceros["H"],2,'.',''),2);  
        $datos["letras_pagar_me_terceros"] = number_format(number_format($letras_pagar_me_terceros["D"],2,'.','')-number_format($letras_pagar_me_terceros["H"],2,'.',''),2);   
        $datos["honorarios_pagar_mn"] = number_format(number_format($honorarios_pagar_mn["D"],2,'.','')-number_format($honorarios_pagar_mn["H"],2,'.',''),2); 
        $datos["honorarios_pagar_me"] = number_format(number_format($honorarios_pagar_me["D"],2,'.','')-number_format($honorarios_pagar_me["H"],2,'.',''),2);  
        
        $datos["ctas_pagar_comerciales"] = number_format(number_format($ctas_pagar_comerciales["D"],2,'.','')-number_format($ctas_pagar_comerciales["H"],2,'.',''),2);   

        //EGRESOS -  CUENTAS POR PAGAR RELACIONADAS  
        $datos["fact_emit_pagar_mn_matriz"] = number_format(number_format($fact_emit_pagar_mn_matriz["D"],2,'.','')-number_format($fact_emit_pagar_mn_matriz["H"],2,'.',''),2);   
        $datos["fact_emit_pagar_me_matriz"] = number_format(number_format($fact_emit_pagar_me_matriz["D"],2,'.','')-number_format($fact_emit_pagar_me_matriz["H"],2,'.',''),2);  
        $datos["fact_emit_pagar_mn_subsid"] = number_format(number_format($fact_emit_pagar_mn_subsid["D"],2,'.','')-number_format($fact_emit_pagar_mn_subsid["H"],2,'.',''),2);   
        $datos["fact_emit_pagar_me_subsid"] = number_format(number_format($fact_emit_pagar_me_subsid["D"],2,'.','')-number_format($fact_emit_pagar_me_subsid["H"],2,'.',''),2);  
        $datos["fact_emit_pagar_mn_asoc"] = number_format(number_format($fact_emit_pagar_mn_asoc["D"],2,'.','')-number_format($fact_emit_pagar_mn_asoc["H"],2,'.',''),2);   
        $datos["fact_emit_pagar_me_asoc"] = number_format(number_format($fact_emit_pagar_me_asoc["D"],2,'.','')-number_format($fact_emit_pagar_me_asoc["H"],2,'.',''),2);  
        $datos["fact_emit_pagar_mn_sucur"] = number_format(number_format($fact_emit_pagar_mn_sucur["D"],2,'.','')-number_format($fact_emit_pagar_mn_sucur["H"],2,'.',''),2);   
        $datos["fact_emit_pagar_me_sucur"] = number_format(number_format($fact_emit_pagar_me_sucur["D"],2,'.','')-number_format($fact_emit_pagar_me_sucur["H"],2,'.',''),2);   
        $datos["antic_otorgados_mn_matriz"] = number_format(number_format($antic_otorgados_mn_matriz["D"],2,'.','')-number_format($antic_otorgados_mn_matriz["H"],2,'.',''),2);   
        $datos["antic_otorgados_me_matriz"] = number_format(number_format($antic_otorgados_me_matriz["D"],2,'.','')-number_format($antic_otorgados_me_matriz["H"],2,'.',''),2);  
        $datos["antic_otorgados_mn_subsid"] = number_format(number_format($antic_otorgados_mn_subsid["D"],2,'.','')-number_format($antic_otorgados_mn_subsid["H"],2,'.',''),2);   
        $datos["antic_otorgados_me_subsid"] = number_format(number_format($antic_otorgados_me_subsid["D"],2,'.','')-number_format($antic_otorgados_me_subsid["H"],2,'.',''),2);   
        $datos["antic_otorgados_mn_asoc"] = number_format(number_format($antic_otorgados_mn_asoc["D"],2,'.','')-number_format($antic_otorgados_mn_asoc["H"],2,'.',''),2);   
        $datos["antic_otorgados_me_asoc"] = number_format(number_format($antic_otorgados_me_asoc["D"],2,'.','')-number_format($antic_otorgados_me_asoc["H"],2,'.',''),2);   
        $datos["antic_otorgados_mn_sucur"] = number_format(number_format($antic_otorgados_mn_sucur["D"],2,'.','')-number_format($antic_otorgados_mn_sucur["H"],2,'.',''),2); 
        $datos["antic_otorgados_me_sucur"] = number_format(number_format($antic_otorgados_me_sucur["D"],2,'.','')-number_format($antic_otorgados_me_sucur["H"],2,'.',''),2);  
        $datos["antic_otorgados_mn_otros"] = number_format(number_format($antic_otorgados_mn_otros["D"],2,'.','')-number_format($antic_otorgados_mn_otros["H"],2,'.',''),2);   
        $datos["antic_otorgados_me_otros"] = number_format(number_format($antic_otorgados_me_otros["D"],2,'.','')-number_format($antic_otorgados_me_otros["H"],2,'.',''),2);  
        $datos["letras_pagar_mn_relac_matriz"] = number_format(number_format($letras_pagar_mn_relac_matriz["D"],2,'.','')-number_format($letras_pagar_mn_relac_matriz["H"],2,'.',''),2);   
        $datos["letras_pagar_me_relac_matriz"] = number_format(number_format($letras_pagar_me_relac_matriz["D"],2,'.','')-number_format($letras_pagar_me_relac_matriz["H"],2,'.',''),2);  
        
        $datos["ctas_pagar_relacionadas"] = number_format(number_format($ctas_pagar_relacionadas["D"],2,'.','')-number_format($ctas_pagar_relacionadas["H"],2,'.',''),2);   

        //EGRESOS -  PRESTAMOS POR PAGAR ACCIONISTAS DIRECTORES GTES  
        $datos["ptmos_accion_corto_plazo_mn"] = number_format(number_format($ptmos_accion_corto_plazo_mn["D"],2,'.','')-number_format($ptmos_accion_corto_plazo_mn["H"],2,'.',''),2);  
        $datos["ptmos_accion_corto_plazo_me"] = number_format(number_format($ptmos_accion_corto_plazo_me["D"],2,'.','')-number_format($ptmos_accion_corto_plazo_me["H"],2,'.',''),2);  
        $datos["ptmos_accion_largo_plazo_mn"] = number_format(number_format($ptmos_accion_largo_plazo_mn["D"],2,'.','')-number_format($ptmos_accion_largo_plazo_mn["H"],2,'.',''),2);  
        $datos["ptmos_accion_largo_plazo_me"] = number_format(number_format($ptmos_accion_largo_plazo_me["D"],2,'.','')-number_format($ptmos_accion_largo_plazo_me["H"],2,'.',''),2);  
        $datos["dietas_directorio"] = number_format(number_format($dietas_directorio["D"],2,'.','')-number_format($dietas_directorio["H"],2,'.',''),2);  
        
        $datos["ptmos_pagar_accion_direct_gtes"] = number_format(number_format($ptmos_pagar_accion_direct_gtes["D"],2,'.','')-number_format($ptmos_pagar_accion_direct_gtes["H"],2,'.',''),2);  
  
        //EGRESOS - PRESTAMOS Y OBLIGACIONES FINANCIERAS
        $datos["ptmos_inst_financ_mn"] = number_format(number_format($ptmos_inst_financ_mn["D"],2,'.','')-number_format($igv_reg_retenciones["H"],2,'.',''),2);   
        $datos["ptmos_inst_financ_me"] = number_format(number_format($ptmos_inst_financ_me["D"],2,'.','')-number_format($ptmos_inst_financ_me["H"],2,'.',''),2);  
        $datos["leasing_mn"] = number_format(number_format($leasing_mn["D"],2,'.','')-number_format($leasing_mn["H"],2,'.',''),2);  
        $datos["leasing_me"] = number_format(number_format($leasing_me["D"],2,'.','')-number_format($leasing_me["H"],2,'.',''),2);  
        $datos["leaseback_mn"] = number_format(number_format($leaseback_mn["D"],2,'.','')-number_format($leaseback_mn["H"],2,'.',''),2);  
        $datos["leaseback_me"] = number_format(number_format($leaseback_me["D"],2,'.','')-number_format($leaseback_me["H"],2,'.',''),2);   
        $datos["pagares_mn"] = number_format(number_format($pagares_mn["D"],2,'.','')-number_format($pagares_mn["H"],2,'.',''),2);  
        $datos["pagares_me"] = number_format(number_format($pagares_me["D"],2,'.','')-number_format($pagares_me["H"],2,'.',''),2);  
        $datos["confirming_mn"] = number_format(number_format($confirming_mn["D"],2,'.','')-number_format($confirming_mn["H"],2,'.',''),2);   
        $datos["confirming_me"] = number_format(number_format($confirming_me["D"],2,'.','')-number_format($confirming_me["H"],2,'.',''),2);  
        $datos["factoring_mn"] = number_format(number_format($factoring_mn["D"],2,'.','')-number_format($factoring_mn["H"],2,'.',''),2);   
        $datos["factoring_me"] = number_format(number_format($factoring_me["D"],2,'.','')-number_format($factoring_me["H"],2,'.',''),2); 
        $datos["letras_dsc_mn"] = number_format(number_format($letras_dsc_mn["D"],2,'.','')-number_format($letras_dsc_mn["H"],2,'.',''),2);  
        $datos["letras_dsc_me"] = number_format(number_format($letras_dsc_me["D"],2,'.','')-number_format($letras_dsc_me["H"],2,'.',''),2);  
        $datos["tarjeta_credito_me"] = number_format(number_format($tarjeta_credito_me["D"],2,'.','')-number_format($tarjeta_credito_me["H"],2,'.',''),2);  
        
        $datos["ptmos_oblig_financ"] = number_format(number_format($ptmos_oblig_financ["D"],2,'.','')-number_format($ptmos_oblig_financ["H"],2,'.',''),2);   

        //EGRESOS - CUENTAS POR PAGAR DIVERSAS
        $datos["reclam_terceros_mn"] = number_format(number_format($reclam_terceros_mn["D"],2,'.','')-number_format($reclam_terceros_mn["H"],2,'.',''),2);  
        $datos["reclam_terceros_me"] = number_format(number_format($reclam_terceros_me["D"],2,'.','')-number_format($reclam_terceros_me["H"],2,'.',''),2);   
        $datos["depos_recib_garantia"] = number_format(number_format($depos_recib_garantia["D"],2,'.','')-number_format($depos_recib_garantia["H"],2,'.',''),2);   
        $datos["ctas_pagar_div_mn"] = number_format(number_format($ctas_pagar_div_mn["D"],2,'.','')-number_format($ctas_pagar_div_mn["H"],2,'.',''),2);  
        $datos["ctas_pagar_div_me"] = number_format(number_format($ctas_pagar_div_me["D"],2,'.','')-number_format($ctas_pagar_div_me["H"],2,'.',''),2); 

        $datos["ctas_pagar_diversas"] = number_format(number_format($ctas_pagar_diversas["D"],2,'.','')-number_format($ctas_pagar_diversas["H"],2,'.',''),2);  

        //EGRESOS - CUENTAS POR PAGAR DIVERSAS REALCIONADAS
        $datos["ptmos_recib_mn"] = number_format(number_format($ptmos_recib_mn["D"],2,'.','')-number_format($ptmos_recib_mn["H"],2,'.',''),2);   
        $datos["ptmos_recib_me"] = number_format(number_format($ptmos_recib_me["D"],2,'.','')-number_format($ptmos_recib_me["H"],2,'.',''),2);  
        $datos["ptmos_accion_mn"] = number_format(number_format($ptmos_accion_mn["D"],2,'.','')-number_format($ptmos_accion_mn["H"],2,'.',''),2);   
        $datos["ptmos_accion_me"] = number_format(number_format($ptmos_accion_me["D"],2,'.','')-number_format($ptmos_accion_me["H"],2,'.',''),2); 
        
        $datos["ctas_pagar_diversas_rela"] = number_format(number_format($ctas_pagar_diversas_rela["D"],2,'.','')-number_format($ctas_pagar_diversas_rela["H"],2,'.',''),2);  

        //EGRESOS - PROVICIONES POR PAGAR
        $datos["prov_pagar"] = number_format(number_format($prov_pagar["D"],2,'.','')-number_format($prov_pagar["H"],2,'.',''),2); 

        $datos["total_egresos"] = number_format(number_format($total_egresos["D"],2,'.','')-number_format($total_egresos["H"],2,'.',''),2);
        $total_egresos_positivo = str_replace(",","",$datos["total_egresos"])<0?(str_replace(",","",$datos["total_egresos"])*-1):str_replace(",","",$datos["total_egresos"]);
        $datos["saldo_final"] = number_format(str_replace(",","",$datos["total_ingresos"])-($total_egresos_positivo),2);
        return $datos;
         
    }


}
