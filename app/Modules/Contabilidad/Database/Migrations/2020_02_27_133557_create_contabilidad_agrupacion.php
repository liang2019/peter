<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContabilidadAgrupacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contabilidad_agrupacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pcuenta');
            $table->integer('xx');
            $table->string('pdescripcion', 250);
            $table->string('descripcion', 250);
            $table->string('tipo', 250);
            $table->string('indice_3', 250);
            $table->string('subgrupo', 250);
            $table->string('indice_4', 250);
            $table->string('grupo_flujo', 250);
            $table->string('grupo_flujo_proyectado', 250);
            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion')->nullable();;
            $table->unsignedInteger('usuario_edicion')->nullable();;
            $table->timestamp('fecha_creacion')->useCurrent()->nullable();;
            $table->timestamp('fecha_modificacion')->useCurrent()->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contabilidad_agrupacion');
    }
}
