<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContabilidadCuentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contabilidad_cuentas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('dsubdia', 50)->nullable();
            $table->text('dcompro', 50)->nullable();
            $table->text('dsecue', 50)->nullable();
            $table->text('dfeccom', 50)->nullable();
            $table->text('dcuenta', 50)->nullable();
            $table->text('dcodane', 50)->nullable();
            $table->text('dcencos', 50)->nullable();
            $table->text('dcodmon', 50)->nullable();
            $table->text('ddh', 50)->nullable();
            $table->decimal('dimport', 16,2)->nullable();
            $table->text('dtipdoc', 50)->nullable();
            $table->text('dnumdoc', 50)->nullable();
            $table->text('dfecdoc', 50)->nullable();
            $table->text('dfecven', 50)->nullable();
            $table->text('darea', 50)->nullable();
            $table->text('dflag', 50)->nullable();
            $table->date('ddate')->nullable();
            $table->text('dxglosa', 50)->nullable();
            $table->decimal('dusimpor', 16,2)->nullable();
            $table->decimal('dmnimpor', 16,2)->nullable();
            $table->text('dcodarc', 50)->nullable();
            $table->date('dfeccom2')->nullable();
            $table->date('dfecdoc2')->nullable();
            $table->date('dfecven2')->nullable();
            $table->text('dcodane2', 50)->nullable();
            $table->text('dvanexo', 50)->nullable();
            $table->text('dvanexo2', 50)->nullable();
            $table->text('dtipcam', 50)->nullable();
            $table->text('dcantid', 50)->nullable();
            $table->text('drete', 50)->nullable();
            $table->text('dporre', 50)->nullable();
            $table->text('dtipdor', 50)->nullable();
            $table->text('dnumdor', 50)->nullable();
            $table->text('dfecdo2', 50)->nullable();
            $table->text('dtiptas', 50)->nullable();
            $table->text('dimptas', 50)->nullable();
            $table->text('dimpbmn', 50)->nullable();
            $table->text('dimpbus', 50)->nullable();
            $table->text('dinacom', 50)->nullable();
            $table->text('digvcom', 50)->nullable();
            $table->text('dmedpag', 50)->nullable();
            $table->text('dmoncom', 50)->nullable();
            $table->text('dcolcom', 50)->nullable();
            $table->text('dbascom', 50)->nullable();
            $table->text('dtpconv', 50)->nullable();
            $table->text('dflgcom', 50)->nullable();
            $table->text('dtipaco', 50)->nullable();
            $table->text('danecom', 50)->nullable();
   
            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion')->nullable();
            $table->unsignedInteger('usuario_edicion')->nullable();
            $table->timestamp('fecha_creacion')->nullable();
            $table->timestamp('fecha_modificacion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contabilidad_cuentas');
    }
}
