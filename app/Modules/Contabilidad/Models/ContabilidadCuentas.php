<?php

namespace App\Modules\Contabilidad\Models;

use Illuminate\Database\Eloquent\Model;

class ContabilidadCuentas extends Model
{
    protected $fillable = [
        'dsubdia',
        'dcompro',
        'dsecue',
        'dfeccom',
        'dcuenta',
        'dcodane',
        'dcencos',
        'dcodmon',
        'ddh',
        'dimport',
        'dtipdoc',
        'dnumdoc',
        'dfecdoc',
        'dfecven',
        'darea',
        'dflag',
        'ddate',
        'dxglosa',
        'dusimpor',
        'dmnimpor',
        'dcodarc',
        'dfeccom2',
        'dfecdoc2',
        'dfecven2',
        'dcodane2',
        'dvanexo',
        'dvanexo2',
        'dtipcam',
        'dcantid',
        'drete',
        'dporre',
        'dtipdor',
        'dnumdor',
        'dfecdo2',
        'dtiptas',
        'dimptas',
        'dimpbmn',
        'dimpbus',
        'dinacom',
        'digvcom',
        'dmedpag',
        'dmoncom',
        'dcolcom',
        'dbascom',
        'dtpconv',
        'dflgcom',
        'dtipaco',
        'danecom',
     
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'contabilidad_cuentas';
    public $timestamps = false;
}
