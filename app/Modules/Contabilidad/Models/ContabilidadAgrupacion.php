<?php

namespace App\Modules\Contabilidad\Models;

use Illuminate\Database\Eloquent\Model;

class ContabilidadAgrupacion extends Model
{
    protected $fillable = [
        'pcuenta',
        'xx',
        'pdescripcion',
        'descripcion',
        'tipo',
        'indice_3',
        'subgrupo',
        'indice_4',
        'grupo_flujo',
        'grupo_flujo_proyectado',
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'contabilidad_agrupacion';
    public $timestamps = false;
}
