<?php

namespace App\Modules\Contabilidad\Models;

use Illuminate\Database\Eloquent\Model;

class ContabilidadReporte extends Model
{
    protected $fillable = [
        'adjunto',
        'comentario',
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'contabilidad_reporte';
    public $timestamps = false;
}
