<?php

namespace App\Modules\Contabilidad\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('contabilidad', 'Resources/Lang', 'app'), 'contabilidad');
        $this->loadViewsFrom(module_path('contabilidad', 'Resources/Views', 'app'), 'contabilidad');
        $this->loadMigrationsFrom(module_path('contabilidad', 'Database/Migrations', 'app'), 'contabilidad');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('contabilidad', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('contabilidad', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
