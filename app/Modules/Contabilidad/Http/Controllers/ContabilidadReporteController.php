<?php

namespace App\Modules\Contabilidad\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Contabilidad\Services\ContabilidadReporteService as MainService;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\Contabilidad\Exports\ContabilidadExport;
use Illuminate\Support\Facades\Storage;
class ContabilidadReporteController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarReporte1All(){
        $data = $this->service->cargarReporte1All();
        return $data;
    }
    public function cargarReporte1(Request $request){
        $data = $this->service->cargarReporte1($request);
        return $data;
    }
    
    public function editarReporte1(Request $request)
    {
        $data = $this->service->editarReporte1($request);
        return $data;
    }
    public function eliminar(Request $request)
    {
        $data = $this->service->eliminar($request);
        return $data;
    }

    
    public function download(Request $request)
    {
        //$x=Excel::store(new ContabilidadExport(), 'Contabilidad.xlsx','excelStoreContabilidad');
        //return Storage::download(config("app.upload_paths.contabilidad.reportes")."/Contabilidad.xlsx");
        $datos = $this->service->download($request->route('id'));
        return Storage::download(config("app.upload_paths.contabilidad.reportes")."/".$datos->id."-".$datos->adjunto.".xlsx");
    }


}
