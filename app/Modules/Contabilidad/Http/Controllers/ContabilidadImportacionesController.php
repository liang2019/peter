<?php

namespace App\Modules\Contabilidad\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Contabilidad\Services\ContabilidadImportacionesService as MainService;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\Contabilidad\Exports\ContabilidadExport;
class ContabilidadImportacionesController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function importarCuentas(Request $request){
        $data = $this->service->importarCuentas($request);
        if ($data) {
            $data = $this->service->guardarReporte($request);
            $this->guardarArchivoGenerado($data);
        }
        return $data;
    }
    public function guardarArchivoGenerado($input) 
    {
        return Excel::store(new ContabilidadExport(), ($input["id"].'-'.$input["adjunto"].'.xlsx') ,'excelStoreContabilidad');
    }

}
