<?php

namespace App\Modules\Contabilidad\Exports;


use App\Modules\Contabilidad\Services\ContabilidadImportacionesService as MainService;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ContabilidadExport implements ShouldAutoSize,WithEvents,FromView
{
    //FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithDrawings,
    private $id;
    private $countReg;
    public function __construct()
    {
        $this->service = new MainService();
     
    }/*
    public function map($invoice): array
    {
        return [
            $invoice->invoice_number,
            $invoice->user->name,
            Date::dateTimeToExcel($invoice->created_at),
        ];
    }*/
    
    public function columnFormats(): array
    {
        return [
            'E' => '0.00',
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $border =
                    array(
                        'outline' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => 'FFFF0000'],
                        ),
                    );
                    $cellRange = 'D5:D6'; // 
                    $event->sheet->getStyle($cellRange)->applyFromArray(
                        array(
                            'borders'=>$border,
                        )
                    );
                    $event->sheet->getStyle($cellRange)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('597ADF');
                    $cellRange = 'B8:E8'; // 
                    $event->sheet->getStyle($cellRange)->applyFromArray(
                        array(
                            'borders'=>$border,
                        )
                    );
                    $event->sheet->getStyle($cellRange)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('CDD0D8');
                    $cellRange = 'B17:E17'; // 
                    $event->sheet->getStyle($cellRange)->applyFromArray(
                        array(
                            'borders'=>$border,
                        )
                    );
                    $cellRange = 'B19:E19'; // 
                    $event->sheet->getStyle($cellRange)->applyFromArray(
                        array(
                            'borders'=>$border,
                        )
                    );
                    $event->sheet->getStyle($cellRange)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('9CA8C5');
                    $cellRange = 'B17:E17'; // 
                    $event->sheet->getStyle($cellRange)->applyFromArray(
                        array(
                            'borders'=>$border,
                        )
                    );
                    $cellRange = 'B158:E158'; // 
                    $event->sheet->getStyle($cellRange)->applyFromArray(
                        array(
                            'borders'=>$border,
                        )
                    );
                    $event->sheet->getStyle($cellRange)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('CDD0D8');
                    $cellRange = 'B160:E160'; // 
                    $event->sheet->getStyle($cellRange)->applyFromArray(
                        array(
                            'borders'=>$border,
                        )
                    );
                    $event->sheet->getStyle($cellRange)->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('B8CAF6');
                    

            }
        ];

     
    }
    public function view(): View
    {

        $data = $this->service->exportarContabilidad();
      

        return view('layouts.reportes.contabilidad', [
            'contabilidad' => $data,
            'fecha_importado'=>$data["fecha_creacion"]
        ]);
    }
}
