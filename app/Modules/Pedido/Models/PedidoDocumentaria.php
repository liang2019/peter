<?php

namespace App\Modules\Pedido\Models;

use Illuminate\Database\Eloquent\Model;

class PedidoDocumentaria extends Model
{
    protected $fillable = [
        'proyecto_id',
        'adjunto_id',
        'descripcion',
        'monto',
        'observacion',
        'adjunto',
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'pedido_documentaria';
    public $timestamps = false;

}
