<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'auth'],function () {
    Route::group(['prefix' => 'pedido'], function () {

        Route::get('/', ['as' => 'pedido', 'uses' => 'PedidoProyectoController@index']);
        Route::get('/proyectos/cargar', ['as' => 'pedido.cargar', 'uses' => 'PedidoProyectoController@cargarAll']);
        Route::post('/cancelar', ['as' => 'pedido.cancelar', 'uses' => 'PedidoProyectoController@cancelar']);
        Route::post('/completar', ['as' => 'pedido.completar', 'uses' => 'PedidoProyectoController@completar']);
    
        Route::get('/documentaria', ['as' => 'documentaria', 'uses' => 'PedidoAdjuntoController@index']);
        Route::get('/documentaria/{id}', ['as' => 'documentaria.id', 'uses' => 'PedidoAdjuntoController@index']);
        Route::post('/documentaria/cargar', ['as' => 'documentaria.cargar', 'uses' => 'PedidoAdjuntoController@cargarAll']);
        Route::post('/documentaria/buscar/', ['as' => 'documentaria.buscar', 'uses' => 'PedidoAdjuntoController@buscar']);
        Route::patch('/documentaria/editar/', ['as' => 'documentaria.editar', 'uses' => 'PedidoAdjuntoController@editar']);
        Route::post('/documentaria/eliminar/', ['as' => 'documentaria.eliminar', 'uses' => 'PedidoAdjuntoController@eliminar']);
        Route::post('/documentaria/guardar/', ['as' => 'documentaria.guardar', 'uses' => 'PedidoAdjuntoController@guardar']);
        Route::post('/documentaria/updatefile/', ['as' => 'documentaria.updatefile', 'uses' => 'PedidoAdjuntoController@updatefile']);
        Route::get('/documentaria/download/{id}', ['as' => 'documentaria.download', 'uses' => 'PedidoAdjuntoController@download']);
    
    });
});