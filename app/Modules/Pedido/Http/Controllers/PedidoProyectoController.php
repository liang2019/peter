<?php

namespace App\Modules\Pedido\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Pedido\Services\PedidoProyectoService as MainService;

class PedidoProyectoController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }
    
    public function cargarAll()
    {
        $data = $this->service->cargarAll();
        return $data;
    }

    public function cancelar(Request $request)
    {
        $data = $this->service->cancelar($request);
        return $data;
    }

    public function completar(Request $request)
    {
        $data = $this->service->completar($request);
        return $data;
    }

}
