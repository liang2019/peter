<?php

namespace App\Modules\Pedido\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Pedido\Http\Requests\PedidoRequest as MainRequest;
use App\Modules\Pedido\Services\PedidoDocumentariaService as MainService;
use Illuminate\Support\Facades\Storage;



class PedidoAdjuntoController extends Controller
{

    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarAll(Request $request)
    {
        $data = $this->service->cargarAll($request);
        return $data;
    }

    public function guardar(MainRequest $request)
    {
        $form = $request->validated();
        $save = $this->service->guardar($form);
        return $save;
    }

    public function buscar(Request $request)
    {
        $data = $this->service->buscar($request);
        return $data;
    }


    public function editar(MainRequest $request)
    {
        $form = $request->validated();
        $data = $this->service->editar($form);
        return $data;

    }

    public function updatefile(MainRequest $request)
    {
        $save = $this->service->updatefile($request);
        return $save;
    }

    public function eliminar(Request $request)
    {
        $data = $this->service->eliminar($request);
        return $data;
    }

    public function download(Request $request)
    {
        $datos = $this->service->download($request->route('id'));
        return Storage::download(config("app.upload_paths.pedidos.adjuntos")."/".$datos->id."-".$datos->adjunto);
    }
}
