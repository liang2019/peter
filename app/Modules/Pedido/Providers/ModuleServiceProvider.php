<?php

namespace App\Modules\Pedido\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('pedido', 'Resources/Lang', 'app'), 'pedido');
        $this->loadViewsFrom(module_path('pedido', 'Resources/Views', 'app'), 'pedido');
        $this->loadMigrationsFrom(module_path('pedido', 'Database/Migrations', 'app'), 'pedido');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('pedido', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('pedido', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
