<?php

namespace App\Modules\Pedido\Services;

use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Tracking\Models\Tracking;
use Illuminate\Support\Facades\DB;

class PedidoProyectoService
{

    public function __construct()
    {
        $this->model = new Proyecto();
        $this->model_tarcking = new Tracking();

    }

    public function cargarAll(){

        $datos = $this->model
        ->where("tracking.pedido","<>", 3)
        ->leftJoin('cliente', 'proyecto.cliente_id', '=', 'cliente.id')
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*', 'cliente.descripcion as nombre_cliente',
        DB::raw("(SELECT COUNT(pedido_documentaria.id) from pedido_documentaria  WHERE pedido_documentaria.proyecto_id=proyecto.id) as cantidad_adjuntos"),
        DB::raw('IFNULL((SELECT COUNT(*) FROM pedido_documentaria WHERE pedido_documentaria.estado = 1 AND pedido_documentaria.adjunto_id=19),0) as adjuntos'),
        'tracking.pedido as estado_tracking')
        ->orderBy('proyecto.id', 'desc')
        ->get();

        return $datos;
    }


    public function cancelar($data){

        $data["usuario_edicion"] = auth()->user()["id"];
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update(['estado' => 0]);
         //actualizar estado del tracking
        $tracking = $this->model_tarcking->where('proyecto_id', $data['id'])
        ->update([
                    'pedido' => 0,
                    'usuario_edicion' => auth()->user()->id,
                    'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                ]);
        if(!$tracking){
            return false;
        }
        return $update?1:$update;
    }

    public function completar($data){

        //completar la etapa de pedido

        $update = $this->model_tarcking->where('proyecto_id', $data['id'])
        ->update([
                    'pedido' => 2,
                    'pedido_fecha' => now()->format('Y-m-d H:i:s'),
                    'diseno' => 1,
                    'produccion' => 1,
                    'montaje' => 1,
                    'usuario_edicion' => auth()->user()->id,
                    'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                ]);

        return $update?1:$update;

   }

}
