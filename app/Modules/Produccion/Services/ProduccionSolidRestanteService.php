<?php

namespace App\Modules\Produccion\Services;


use App\Modules\Diseno\Models\Solid;
use App\Modules\Diseno\Models\SolidItems;
use App\Modules\Requerimiento\Models\Proyecto;
use Illuminate\Support\Facades\DB;
class ProduccionSolidRestanteService
{

    public function __construct()
    {
   
        $this->model_solid = new Solid();
        $this->model_solid_items = new SolidItems();
        $this->model_proyecto = new Proyecto();

    }

    public function listarSolid($data){

        $datos["items"] = $this->model_solid->where("solid.proyecto_id",$data['proyecto_id'])
        ->where('solid.estado', 1)
        ->where('solid.actualizado', 1)
        ->where('solid.verificado', 1)
        ->select('solid.*')
        ->orderBy('solid.id', 'desc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["proyecto_id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.produccion as estado_tracking')
        ->first();
        
        return $datos;
    }

    public function listarItems($data){

        $datos["items"] = $this->model_solid_items->where("solid_items.solid_id",$data['solid_id'])
        ->leftjoin("factor", "factor.id", "solid_items.factor_id")
        ->leftjoin("alarti", "alarti.id", "factor.alarti_id")
        ->where('solid_items.estado', 1)
        ->where('solid_items.actualizado', 1)
        ->select('solid_items.*',
                'alarti.descripcion as nombre_material'
        )
        ->orderBy('solid_items.id', 'desc')
        ->get();

        return $datos;
    }

    
    public function buscar($data){

        $datos =  $this->model
        ->select('*')->where('id', $data["id"])->first();
        return $datos;
    }

    public function editar($data){
        $data = $data->all();
        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model_solid_items->find($data['id'])->update($data);
        return $update?1:$update;
    }


}
