<?php

namespace App\Modules\Produccion\Services;

use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Tracking\Models\Tracking;
use Illuminate\Support\Facades\DB;
class ProduccionProyectoService
{


    public function __construct()
    {
        $this->model = new Proyecto();
        $this->model_tarcking = new Tracking();
    }

    public function cargarAll(){

        $datos = $this->model
        ->where("tracking.produccion","<>", 3)
        ->leftJoin('cliente', 'proyecto.cliente_id', '=', 'cliente.id')
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'cliente.descripcion as nombre_cliente',
        DB::raw('(SELECT usuario from usuario WHERE usuario.id = proyecto.usuario_creacion) as user'),
        'tracking.produccion as estado_tracking'
        )
        ->orderBy('proyecto.id', 'desc')
        ->get();
        return $datos;
    }


}
