<?php

namespace App\Modules\Produccion\Services;

use App\Modules\Produccion\Models\SolicitudEntregas;
use App\Modules\Produccion\Models\ProduccionSolicitud;
use App\Modules\Produccion\Models\ProduccionSolicitudItem;
use App\Modules\Requerimiento\Models\Proyecto;
use Illuminate\Support\Facades\DB;
class ProduccionSolicitudEntregasService
{

    public function __construct()
    {
        $this->model = new SolicitudEntregas();
        $this->model_solid = new ProduccionSolicitud();
        $this->model_solid_items = new ProduccionSolicitudItem();
        $this->model_proyecto = new Proyecto();
    }

    public function listarSolicitud($data){

        $datos["items"] = $this->model_solid->where("produccion_solicitud.proyecto_id",$data['proyecto_id'])
        ->where('produccion_solicitud.estado', 1)
        ->where('produccion_solicitud.revisado', 1)
        ->where('produccion_solicitud.aprobado', 1)
        ->select('produccion_solicitud.*','produccion_solicitud.id as descripcion')
        ->orderBy('produccion_solicitud.id', 'desc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["proyecto_id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.produccion as estado_tracking')
        ->first();
        
        return $datos;
    }

    public function listarItems($data){

        $datos["items"] = $this->model_solid_items

        ->leftjoin("alarti", "alarti.id", "produccion_solicitud_item.alarti_id")
        ->where("produccion_solicitud_item.produccion_solicitud_id",$data['produccion_solicitud_id'])
        ->where('produccion_solicitud_item.estado', 1)
        ->where('produccion_solicitud_item.recibido', 1)
        ->select('produccion_solicitud_item.*','alarti.descripcion as nombre')
        ->orderBy('produccion_solicitud_item.id', 'desc')
        ->get();

        return $datos;
    }

    public function cargar($data){

        $datos["items"] = $this->model->where("solicitud_entregas.produccion_solicitud_item_id",$data['produccion_solicitud_item_id'])
        ->where('solicitud_entregas.estado', 1)
        ->select('solicitud_entregas.*')
        ->orderBy('solicitud_entregas.id', 'desc')
        ->get();

        return $datos;
    }

    public function registrar($data)
    {
        $data = $data->all();
        if ($this->excesoCantidadEntregas($data,"registrar")) {
            return json_encode(array("result"=>0,"mensaje"=>"Ha excedido la cantidad máxima de las entregas"));
        }
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $save = $this->model->create($data);
        return json_encode(array("result"=>($save?1:$save)));
    }

    public function excesoCantidadEntregas($data,$opcion=""){
        $cantidad_solicitud_items = $this->model_solid_items->where("id",$data['produccion_solicitud_item_id'])
        ->select('cantidad')->first();
        $cantidad_total_entregas = $this->model->where("produccion_solicitud_item_id",$data['produccion_solicitud_item_id'])->where("estado",1)
        ->select(DB::raw('SUM(cantidad) as cantidad'))->first();
        if ($opcion=="registrar") {
            return ($cantidad_solicitud_items["cantidad"] - ($cantidad_total_entregas["cantidad"]+$data["cantidad"]))<0; 
        }else{
            return ($cantidad_solicitud_items["cantidad"] - $data["cantidad"])<0; 
        }
    }
    public function buscar($data){

        $datos =  $this->model
        ->select('*')->where('id', $data["id"])->first();
        return $datos;
    }

    public function editar($data){
        $data = $data->all();
        if ($this->excesoCantidadEntregas($data,"editar")) {
            return json_encode(array("result"=>0,"mensaje"=>"Ha excedido la cantidad máxima de las entregas"));
        }
        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update($data);
        return json_encode(array("result"=>($update?1:$update)));
    }

    public function eliminar($data){

        $update = $this->model->find($data['id'])->update([
            'estado' => 0,
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

    public function recibir($data){
        $update = $this->model->find($data['id'])->update([
            'recibido' => 1,
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

}
