<?php

namespace App\Modules\Produccion\Services;

use App\Modules\Produccion\Models\Tareo;
use App\Modules\Mantenimiento\Models\Tarea;
use App\Modules\Produccion\Models\TareoEmpleado;
use App\Modules\Requerimiento\Models\Proyecto;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
class ProduccionTareoService
{

    public function __construct()
    {
        $this->model = new Tareo();
        $this->model_tareo_empleado = new TareoEmpleado();
        $this->model_proyecto = new Proyecto();
    }

    public function cargar($data){

        $datos["items"] = $this->model->where('tareo.proyecto_id', $data["id"])
        ->where('tareo.estado', 1)
        ->where('tareo.tipo', 1)
        ->leftJoin('tarea', 'tareo.tarea_id', 'tarea.id')
        ->select('tareo.*', 'tarea.nombre as nombre_actividad',
        DB::raw("IFNULL((SELECT solid.descripcion from solid  WHERE solid.id=tareo.solid_id AND solid.estado=1 ),'Solid eliminado') as solid") )
        ->orderBy('tareo.id', 'desc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.produccion as estado_tracking')
        ->first();
        return $datos;
    }

    public function empleados($data)
    {
        $datos= $this->model_tareo_empleado->where('tareo_empleado.tareo_id', $data["tareo_id"])
        ->where('tareo_empleado.estado', 1)
        ->leftJoin('empleado', 'tareo_empleado.empleado_id', 'empleado.id')
        ->select('tareo_empleado.*', 'empleado.nombres as empleado_nombres', 'empleado.apellidos as empleado_apellidos')
        ->orderBy('empleado.nombres', 'asc')
        ->get();

        return $datos;
    }

    public function calcularCostoTrabajador($id){
        $consulta = $this->model_tareo_empleado

        ->where('tareo.tipo', 1)
        ->leftJoin('tareo', 'tareo.id', 'tareo_empleado.tareo_id')
        ->where('tareo.proyecto_id', $id)
        ->where('tareo.estado', 1)
        ->rightJoin('empleado', 'tareo_empleado.empleado_id', 'empleado.id')
        ->leftJoin('proyecto', 'tareo.proyecto_id', 'proyecto.id')
        ->leftJoin('tarea', 'tareo.tarea_id', 'tarea.id')
        ->select(   'tareo_empleado.empleado_id',
                    'proyecto.nombre as nombre_proyecto',
                    'empleado.sueldo',
                    'empleado.nombres',
                    'tareo.fecha_creacion',
                    'empleado.apellidos',
                    'tarea.nombre as nombre_actividad',
                    'tareo.horas_trabajadas' 
        )
        ->get();
        $datos = array();
        $sumatoria=0;
  
        foreach ($consulta as $key => $value) {

            $datos["item"][$key]["proyecto"]= $value["nombre_proyecto"];
            $datos["item"][$key]["nombres"]= $value["nombres"];
            $datos["item"][$key]["nombre_actividad"]= $value["nombre_actividad"];
            $datos["item"][$key]["apellidos"]= $value["apellidos"];
            $datos["item"][$key]["fecha"]= date('d-m-Y',strtotime($value["fecha_creacion"]));
            $datos["item"][$key]["horas_trabajadas"]= floor($value["horas_trabajadas"]).":".floor(($value["horas_trabajadas"]-floor($value["horas_trabajadas"]))*60);
            $sueldo = floatval(Crypt::decryptString($value["sueldo"]));
            $horas_trabajadas = floatval($value["horas_trabajadas"]);
            $costo = $sueldo * $horas_trabajadas;
            $datos["item"][$key]["costo"]= $costo;
            $sumatoria = $sumatoria + $value["horas_trabajadas"];
        }
        $datos["total_horas"] = floor($sumatoria).":".floor(($sumatoria-floor($sumatoria))*60);
        return $datos;
    }


    public function reporte($data){


        $datos["items"] = $this->calcularCostoTrabajador($data["proyecto_id"]);

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["proyecto_id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.produccion as estado_tracking')
        ->first();

        return $datos;
    }

    public function exportar($id){

        $datos = $this->calcularCostoTrabajador($id);
       // $datos=collect($datos);
        $datos["tareo"]=$datos;
        $datos["proyecto"] = $this->model_proyecto
        ->leftJoin('cliente','cliente.id','=','proyecto.cliente_id')
        ->where("proyecto.id",$id)
        ->select('proyecto.nombre','proyecto.codigo','cliente.descripcion',
        DB::raw("(DATE_FORMAT(proyecto.fecha_creacion, '%d-%m-%Y')) as fecha_creacion")
        )->first();

        return $datos;
    }

    public function cargarTareas(){

        $datos = $this->model->select('*')->where('estado',1)->get();
        return $datos;
    }

}
