<?php

namespace App\Modules\Produccion\Services;

use App\Modules\Produccion\Models\SolidEntregas;
use App\Modules\Diseno\Models\Solid;
use App\Modules\Diseno\Models\SolidItems;
use App\Modules\Requerimiento\Models\Proyecto;
use Illuminate\Support\Facades\DB;
class ProduccionSolidEntregasService
{

    public function __construct()
    {
        $this->model = new SolidEntregas();
        $this->model_solid = new Solid();
        $this->model_solid_items = new SolidItems();
        $this->model_proyecto = new Proyecto();

    }

    public function listarSolid($data){

        $datos["items"] = $this->model_solid->where("solid.proyecto_id",$data['proyecto_id'])
        ->where('solid.estado', 1)
        ->where('solid.actualizado', 1)
        ->where('solid.verificado', 1)
        ->select('solid.*')
        ->orderBy('solid.id', 'desc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["proyecto_id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.produccion as estado_tracking')
        ->first();
        
        return $datos;
    }

    public function listarItems($data){

        $datos["items"] = $this->model_solid_items->where("solid_items.solid_id",$data['solid_id'])
        ->where('solid_items.estado', 1)
        ->where('solid_items.actualizado', 1)
        ->select('solid_items.*',
        DB::raw('(SELECT alarti.descripcion FROM alarti LEFT JOIN factor ON alarti.id=factor.alarti_id WHERE solid_items.factor_id=factor.id) as nombre'),
        DB::raw('IFNULL((SELECT SUM(solid_entregas.cantidad) FROM solid_entregas WHERE 
        solid_entregas.estado=1 AND solid_items.id=solid_entregas.solid_items_id ),0) as entregados')
        
        )
        ->orderBy('solid_items.id', 'desc')
        ->get();

        return $datos;
    }

    public function cargar($data){

        $datos["items"] = $this->model->where("solid_entregas.solid_items_id",$data['solid_items_id'])
        ->where('solid_entregas.estado', 1)
        ->select('solid_entregas.*')
        ->orderBy('solid_entregas.id', 'desc')
        ->get();

        return $datos;
    }

    public function registrar($data)
    {
        $data = $data->all();
        if ($this->excesoCantidadEntregas($data,"registrar")) {
            return json_encode(array("result"=>0,"mensaje"=>"Ha excedido la cantidad máxima de las entregas"));
        }
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $save = $this->model->create($data);
        return json_encode(array("result"=>($save?1:0),"mensaje"=>"Ha excedido la cantidad máxima de las entregas"));
    }
    public function excesoCantidadEntregas($data,$opcion=""){
        $cantidad_solid_items = $this->model_solid_items->where("id",$data['solid_items_id'])
        ->select('cantidad')->first();
        $cantidad_total_entregas = $this->model->where("solid_items_id",$data['solid_items_id'])->where("estado",1)
        ->select(DB::raw('SUM(cantidad) as cantidad'))->first();

        if ($opcion=="registrar") {
            return ($cantidad_solid_items["cantidad"] - ($cantidad_total_entregas["cantidad"]+$data["cantidad"]))<0; 
        }else{
            return ($cantidad_solid_items["cantidad"] - $data["cantidad"])<0; 
        }

    }
    
    public function buscar($data){

        $datos =  $this->model
        ->select('*')->where('id', $data["id"])->first();
        return $datos;
    }

    public function editar($data){
        $data = $data->all();
        if ($this->excesoCantidadEntregas($data,"editar")) {
            return json_encode(array("result"=>0,"mensaje"=>"Ha excedido la cantidad máxima de las entregas"));
        }
        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update($data);
        return $update?1:$update;
    }

    public function eliminar($data){

        $update = $this->model->find($data['id'])->update([
            'estado' => 0,
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

    public function recibir($data){
        $update = $this->model->find($data['id'])->update([
            'recibido' => 1,
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

}
