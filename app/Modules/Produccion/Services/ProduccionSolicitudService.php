<?php

namespace App\Modules\Produccion\Services;

use App\Modules\Produccion\Models\ProduccionSolicitud;
use App\Modules\Produccion\Models\ProduccionSolicitudItem;
use App\Modules\Requerimiento\Models\Proyecto;
use Illuminate\Support\Facades\DB;
class ProduccionSolicitudService
{

    public function __construct()
    {
        $this->model = new ProduccionSolicitud();
        $this->model_solicitud_item = new ProduccionSolicitudItem();
        $this->model_proyecto = new Proyecto();
    }

    public function cargar($data){

        $datos["items"] = $this->model->where('produccion_solicitud.proyecto_id', $data["id"])
        ->where('produccion_solicitud.estado', 1)
        ->select('produccion_solicitud.*','produccion_solicitud.id as identificador_auto',
                DB::raw("(SELECT IFNULL(COUNT(produccion_solicitud_item.id),0) FROM produccion_solicitud_item WHERE produccion_solicitud_item.produccion_solicitud_id = produccion_solicitud.id
                AND produccion_solicitud_item.recibido = 0) as recibidos_faltantes")
        )
        ->orderBy('id', 'desc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.produccion as estado_tracking')
        ->first();
        return $datos;
    }

    public function guardar($data)
    {
        //si es una solicitud de consumiles el tipo = 1 y manda aprobado = 1
        //si es una solicitud de materiales el tipo = 2
        DB::beginTransaction();
        try {
            $response =false;
            //guardar la solicitud
            $data = $data->all();
            $data["solicitud"]["usuario_creacion"] = auth()->user()["id"];
            $data["solicitud"]["usuario_edicion"] = auth()->user()["id"];
                $save = $this->model->create($data["solicitud"]);
                foreach ($data["items"] as $key => $value) {

                    //se guardan los items de la solicitud
                    $this->model_solicitud_item->create(
                        array(
                            "produccion_solicitud_id" => $save->id,
                            "alarti_id" => $value["id"],
                            "cantidad" => $value["cantidad"],
                            "usuario_creacion" => auth()->user()->id,
                            "usuario_edicion" =>  auth()->user()->id
                       )

                    );

                }


                $response=true;
                DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
        return $response?1:$response;

    }

    public function buscar($data)
    {
        $datos= $this->model_solicitud_item->where('produccion_solicitud_item.produccion_solicitud_id', $data["solicitud_id"])
        ->where('produccion_solicitud_item.estado', 1)
        ->leftJoin('alarti', 'alarti.id', '=', 'produccion_solicitud_item.alarti_id')
        ->select('produccion_solicitud_item.*','alarti.codigo','alarti.descripcion','alarti.grupo')
        ->orderBy('produccion_solicitud_item.id', 'desc')
        ->get();

        return $datos;
    }
    public function eliminar($data)
    {
        //borrar items
        $delete = $this->model_solicitud_item
        ->where('produccion_solicitud_id', $data['solicitud_id'])->delete();
        //borrar solicitud
        if($delete){
            $delete = $this->model
        ->where('id', $data['solicitud_id'])->delete();
        }

        return $delete?1:$delete;
    }

    public function aprobar($data){
        $update = $this->model->find($data['id'])->update([
            'aprobado' => 1,
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

    public function revisar($data){
        $update = $this->model->find($data['id'])->update([
            'revisado' => 1,
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

    public function verificar($data){
        $update = $this->model_solicitud_item->find($data['id'])->update([
            'recibido' => $data['recibido'],
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

}
