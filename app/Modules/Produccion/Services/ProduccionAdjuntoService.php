<?php

namespace App\Modules\Produccion\Services;

use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Produccion\Models\ProduccionDocumentaria;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use App\Modules\Tracking\Models\Tracking;


class ProduccionAdjuntoService
{


    public function __construct()
    {
        $this->model = new ProduccionDocumentaria();
        $this->model_proyecto = new Proyecto();
        $this->model_tarcking = new Tracking();

    }

    public function cargarAll($data){

        $datos["adjuntos"] = $this->model->where('produccion_documentaria.proyecto_id',$data["id"])
        ->leftJoin('adjunto', 'produccion_documentaria.adjunto_id', '=', 'adjunto.id')
        ->select('produccion_documentaria.*', 'adjunto.nombre as tipo_adjunto', 'adjunto.codigo as codigo_adjunto')
        ->orderBy('produccion_documentaria.id', 'desc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select(
                    'proyecto.*',
                    'tracking.produccion as estado_tracking'
                )
        ->first();

        return $datos;
    }

    public function guardar($data)
    {
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $adjunto_file = $data["adjunto_file"];
        unset($data["adjunto_file"]);
        $save = $this->model->create($data);

        if(  isset($data["adjunto"]) && $data["adjunto"]!='null' ){
            $path = Storage::putFileAs(
                config('app.upload_paths.produccion.adjuntos'), $adjunto_file, $save->id.'-'.$data["adjunto"]
            );
            if(!$path){
                return 0;
            }
        }
        if ($data["completarProduccion"]=="true") {
            $this->completar($data);
        }
        return $save;
    }

    public function buscar($data){

        $datos =  $this->model->all()->where('id', $data["id"])->first();
        return $datos;
    }


    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update($data);

        return $update?1:$update;

    }

    public function updatefile($data){

        
        if ($data["adjunto_file"]!='null') {
            $fileSearch = $this->model->find($data['id'])->adjunto;
            $exists = Storage::disk('local')->exists(config('app.upload_paths.produccion.adjuntos').'/'.$data['id'].'-'.$fileSearch);
            if ($exists) {
                $delete = Storage::disk('local')->delete(config('app.upload_paths.produccion.adjuntos').'/'.$data['id'].'-'.$fileSearch);
            }
            $adjunto_file = $data["adjunto_file"];
            $path = Storage::disk('local')->putFileAs(
                config('app.upload_paths.produccion.adjuntos'), $adjunto_file, $data['id'].'-'.$data["adjunto"]
            );
        }
        return "true";
}

    public function eliminar($data){
        $data["usuario_edicion"] = auth()->user()["id"];
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');

        // elimina el archivo
        $archivo = $this->model->find($data['id']);

        $exists = Storage::disk('local')->exists(config('app.upload_paths.produccion.adjuntos').'/'.$data['id'].'-'.$archivo->adjunto);

        if ($exists) {
            $delete = Storage::disk('local')->delete(config('app.upload_paths.produccion.adjuntos').'/'.$data['id'].'-'.$archivo->adjunto);
        }
          // elimina el adjunto
        $update = $archivo->delete();
        return $update?1:$update;
    }

    public function download($id)
    {
        $datos =  $this->model->all()->where('id', $id)->first();
        return $datos;
    }

    public function completar($data)
    {
        //completar etapa produccion
        $update = $this->model_tarcking->where('proyecto_id', $data["proyecto_id"])
        ->update([
                    'produccion' => 2,
                    'produccion_fecha'=>now()->format('Y-m-d H:i:s'),
                    'usuario_edicion' => auth()->user()->id,
                    'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                ]);
        return $update;
    }

}
