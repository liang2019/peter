<?php

namespace App\Modules\Produccion\Services;

use App\Modules\Produccion\Models\ProduccionPesos;
use App\Modules\Presupuesto\Models\ItemInsumo;
use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Diseno\Models\SolidItems;
use App\Modules\Mantenimiento\Models\Factor;
use App\Modules\Produccion\Models\TipoPeso;
use Illuminate\Support\Facades\DB;
class ProduccionPesoService
{

    public function __construct()
    {
        $this->model = new ProduccionPesos();
        $this->model_proyecto = new Proyecto();
        $this->model_item_insumo = new ItemInsumo();
        $this->model_solid_items = new SolidItems();
        $this->model_factor = new Factor();
        $this->model_tipo_peso = new TipoPeso();
    }

    public function cargar($data){

        $datos["items"] = $this->model->where('produccion_pesos.proyecto_id', $data["id"])
        ->where('produccion_pesos.estado', 1)
        ->select('produccion_pesos.*',
        DB::raw("(SELECT tipo_peso.nombre FROM tipo_peso WHERE tipo_peso.id=produccion_pesos.tipo_peso_id) as tipo_peso")
        
        )
        ->orderBy('produccion_pesos.id', 'desc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.produccion as estado_tracking')
        ->first();
        return $datos;
    }

    public function cargarTipoPeso(){

        $datos = $this->model_tipo_peso->where("estado", 1)
        ->select('*')
        ->orderBy('id', 'asc')
        ->get();

        return $datos;
    }

    public function guardar($data)
    {
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $save = $this->model->create($data);
        return $save;
    }

    
    public function buscar($data){

        $datos =  $this->model
        ->select('*')->where('id', $data["id"])->first();
        return $datos;
    }

    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update($data);
        return $update?1:$update;
    }

    public function eliminar($data){

        $update = $this->model->find($data['id'])->update([
            'estado' => 0,
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

    public function grafico($data){
     
        //peso presupuestado
        $peso_presupuestado= $this->model_item_insumo->where("proyecto_id", $data["proyecto_id"])
        ->where("estado", 1)
        ->select(DB::raw("(IFNULL(SUM(peso_parcial),0)) as peso"))
        ->first();

        //peso programado
        $peso_programado= $this->model_solid_items
        ->where("solid_items.estado", 1)
        ->leftjoin("solid", "solid.id", "solid_items.solid_id")
        ->where("solid.proyecto_id", $data["proyecto_id"])
        //->select(DB::raw("(IFNULL(SUM(solid_items.masa),0)) as peso"))
        ->select(DB::raw("(IFNULL(SUM(solid_items.masa*solid_items.cantidad),0)) as peso")) //corrección de Jose
        ->first();
        
        $peso_trabajado= $this->model->where("proyecto_id", $data["proyecto_id"])
        ->where("estado", 1)
        ->select(DB::raw("(IFNULL(SUM(peso),0)) as peso"))
        ->first();

        $porcentaje_presupuestado = ($peso_trabajado->peso / $peso_presupuestado->peso) *100;
        $porcentaje_programado =( $peso_trabajado->peso / $peso_programado->peso) * 100;
 
        $datos["items"] = ([
            'peso_presupuestado' => number_format($peso_presupuestado->peso, 2, '.', ''),
            'peso_programado' => number_format($peso_programado->peso, 2, '.', ''),
            'peso_entregado' => number_format($peso_trabajado->peso, 2, '.', ''),
            'porcentaje_presupuestado' => number_format($porcentaje_presupuestado, 2, '.', ''),
            'porcentaje_programado' =>number_format($porcentaje_programado, 2, '.', ''),
        ]);
        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["proyecto_id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.produccion as estado_tracking')
        ->first();
        return $datos;
    }

    public function cargarMateriales(){

        //lista de materiales para el peso
        $datos = $this->model_factor
        ->leftJoin('alarti', 'factor.alarti_id', '=', 'alarti.id')
        ->select(
                    'alarti.id as id',
                    'alarti.descripcion as nombre_material',
                    'alarti.codigo as codigo_material'
                )
        ->get();

        return $datos;
    }
}
