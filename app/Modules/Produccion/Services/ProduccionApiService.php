<?php

namespace App\Modules\Produccion\Services;

use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Mantenimiento\Models\Tarea;
use App\Modules\Produccion\Models\Tareo;
use App\Modules\Produccion\Models\TareoEmpleado;
use App\Modules\Mantenimiento\Models\Empleado;
use App\Modules\Diseno\Models\Solid;
use Illuminate\Support\Facades\DB;
class ProduccionApiService
{

    public function __construct()
    {
        $this->model_proyecto = new Proyecto();
        $this->model_tarea = new Tarea();
        $this->model_solid = new Solid();
        $this->model = new Tareo();
        $this->model_tareo_empleado = new TareoEmpleado();
        $this->model_empleado = new Empleado();

    }

    public function cargarProyectos($data){

        $datos = $this->model_proyecto
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id');
        if ($data=="1") { //montaje
            $datos = $datos->where("tracking.montaje",1)->where("proyecto.montaje",$data);
        }else{//producción
            $datos = $datos->where("tracking.produccion",1);
        }
        $datos = $datos
        ->where("proyecto.estado",1)
        ->select('proyecto.*',DB::raw('UPPER(proyecto.nombre) as nombre'))
        ->orderBy('proyecto.nombre', 'asc')
        ->get();
        return response()->json($datos,200);
    }

    public function cargarTareas(){

        $datos = $this->model_tarea
        ->where("estado",1)
        ->select('*' ,DB::raw('UPPER(nombre) as nombre'))
        ->orderBy('nombre', 'asc')
        ->get();
        return response()->json($datos,200);
    }

    public function cargarSolids($data){

        $datos = $this->model_solid
        ->where("estado",1)
        ->where("verificado",1)
        ->where("proyecto_id",$data)
        ->select('id', 'proyecto_id' ,DB::raw('UPPER(descripcion) as nombre'))
        ->orderBy('descripcion', 'asc')
        ->get();
        return response()->json($datos,200);
    }
    public function cargartipoproyectos($data){

        $datos = $this->model_proyecto
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id');
        if ($data=="1") { //montaje
            $datos = $datos->where("tracking.montaje",1)->where("proyecto.montaje",$data);
        }else{//producción
            $datos = $datos->where("tracking.produccion",1)->where("proyecto.montaje",0);
        }
        $datos = $datos
        ->where("proyecto.estado",1)
        ->select('proyecto.*',DB::raw('UPPER(proyecto.descripcion) as nombre'))
        ->orderBy('proyecto.descripcion', 'asc')
        ->get();
        return response()->json($datos,200);
    }
    
    public function cargarTrabajadores(){

        $datos = $this->model_empleado
        ->where("estado",1)
        ->where("tareo",1)
        ->select('*' ,DB::raw('UPPER(nombres) as nombres'),DB::raw('UPPER(apellidos) as apellidos'))
        ->orderBy('apellidos', 'asc')
        ->get();
        return response()->json($datos,200);
    }

    public function guardarTareo($data){

        $save = $this->model->create([
            "proyecto_id" =>   $data["proyecto_id"],
            "solid_id" =>   $data["solid_id"],
            "tarea_id" =>   $data["tarea_id"],
            "hora_inicio" =>   $data["hora_inicio"],
            "hora_fin" =>   $data["hora_fin"],
            "tipo" => $data["tipo"],
            "horas_trabajadas" =>   $data["horas_trabajadas"],
            "usuario_creacion" => $data["usuario_id"],
            "usuario_edicion" => $data["usuario_id"],
            "estado"=> empty(trim($data["hora_fin"]))?2:1,
            "fecha_creacion" => now()->format('Y-m-d H:i:s'),
            "fecha_modificacion" => now()->format('Y-m-d H:i:s'),
        ]);

        foreach ($data["empleados"] as $key => $value) {
            $save_ = $this->model_tareo_empleado->create([
                "tareo_id" =>   $save->id,
                "empleado_id" =>   $value,
                "usuario_creacion" => $data["usuario_id"],
                "usuario_edicion" => $data["usuario_id"],
                "fecha_creacion" => now()->format('Y-m-d H:i:s'),
                "fecha_modificacion" => now()->format('Y-m-d H:i:s'),
            ]);
        }

        return response()->json($save,200);

    }

    public function varTareo($data){
        //SELECT * FROM `tareo` WHERE fecha_creacion BETWEEN '2019-12-03 00:00:00' AND '2019-12-03 23:59:59'
        $date = date('Y-m-d');
    }
    
    public function listarTareoSinHoraFin($data){

        $datos = $this->model
		 
		->where("usuario_creacion",$data)
        ->where("estado",2)
        ->select('tareo.id' ,'tareo.hora_inicio', 
		DB::raw("(SELECT GROUP_CONCAT(empleado.nombres  SEPARATOR ', ') FROM tareo_empleado LEFT JOIN empleado ON (tareo_empleado.empleado_id=empleado.id) WHERE tareo_empleado.tareo_id=tareo.id) as empleados"),
        DB::raw("(SELECT UPPER(nombre) FROM proyecto WHERE proyecto.id = tareo.proyecto_id AND proyecto.estado = 1) as proyecto_nombre"),
        DB::raw("(SELECT UPPER(descripcion) FROM solid WHERE solid.id = tareo.solid_id AND solid.estado = 1) as solid_nombre"),
        DB::raw("(SELECT UPPER(nombre) FROM tarea WHERE tarea.id = tareo.tarea_id AND tarea.estado = 1) as tarea_nombre")
        )
        ->orderBy('tareo.id', 'desc')
        ->get();
        return response()->json($datos,200);
    }

    public function editar($data){

        $update = $this->model->find($data['id'])->update([
            'estado' => 1,
            'hora_fin' => $data['hora_fin'],
            'horas_trabajadas' => $data['horas_trabajadas'],
            'usuario_edicion' => $data['usuario_id'],
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return response()->json([],200);
    }
}
