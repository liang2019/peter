<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'auth'],function () {
    Route::group(['prefix' => 'produccion'], function () {

        Route::get('/proyectos', ['as' => 'produccion.proyectos', 'uses' => 'ProduccionProyectoController@cargarAll']);

        Route::get('/cronograma', ['as' => 'produccion.cronograma', 'uses' => 'ProduccionCronogramaController@index']);
        Route::get('/cronograma/actividades/{id}', ['as' => 'cronograma', 'uses' => 'ProduccionCronogramaController@index']);
        Route::get('/cronograma/{id}', ['as' => 'cronograma', 'uses' => 'ProduccionCronogramaController@index']);
        Route::post('/cronograma/cargar', ['as' => 'cronograma.cargar', 'uses' => 'ProduccionCronogramaController@cargar']);
        Route::post('/cronograma/guardar', ['as' => 'cronograma.guardar', 'uses' => 'ProduccionCronogramaController@guardar']);
        Route::post('/cronograma/buscar', ['as' => 'cronograma.buscar', 'uses' => 'ProduccionCronogramaController@buscar']);
        Route::patch('/cronograma/editar', ['as' => 'cronograma.editar', 'uses' => 'ProduccionCronogramaController@editar']);
        Route::post('/cronograma/eliminar', ['as' => 'cronograma.eliminar', 'uses' => 'ProduccionCronogramaController@eliminar']);
        Route::post('/cronograma/completar', ['as' => 'cronograma.completar', 'uses' => 'ProduccionCronogramaController@completar']);

        Route::get('/pesos', ['as' => 'pesos.proyectos', 'uses' => 'ProduccionPesosController@index']);
        Route::get('/pesos/registrar/{id}', ['as' => 'pesos.registrar', 'uses' => 'ProduccionPesosController@index']);
        Route::get('/pesos/{id}', ['as' => 'pesos', 'uses' => 'ProduccionPesosController@index']);
        Route::post('/pesos/cargar', ['as' => 'pesos.cargar', 'uses' => 'ProduccionPesosController@cargar']);
        Route::post('/pesos/materiales', ['as' => 'pesos.materiales', 'uses' => 'ProduccionPesosController@cargarMateriales']);
        Route::post('/pesos/guardar', ['as' => 'pesos.guardar', 'uses' => 'ProduccionPesosController@guardar']);
        Route::post('/pesos/buscar', ['as' => 'pesos.buscar', 'uses' => 'ProduccionPesosController@buscar']);
        Route::patch('/pesos/editar', ['as' => 'pesos.editar', 'uses' => 'ProduccionPesosController@editar']);
        Route::post('/pesos/eliminar', ['as' => 'pesos.eliminar', 'uses' => 'ProduccionPesosController@eliminar']);
        Route::post('/pesos/grafico', ['as' => 'pesos.grafico', 'uses' => 'ProduccionPesosController@grafico']);
        

        Route::get('/solicitudes', ['as' => 'solicitudes.proyectos', 'uses' => 'ProduccionSolicitudesController@index']);
        Route::get('/solicitudes/registro/{id}', ['as' => 'solicitudes.registrar', 'uses' => 'ProduccionSolicitudesController@index']);
       
        Route::post('/solicitudes/cargar', ['as' => 'solicitud.cargar', 'uses' => 'ProduccionSolicitudesController@cargar']);
        Route::post('/solicitudes/guardar', ['as' => 'solicitud.guardar', 'uses' => 'ProduccionSolicitudesController@guardar']);
        Route::post('/solicitudes/buscar', ['as' => 'solicitud.buscar', 'uses' => 'ProduccionSolicitudesController@buscar']);
       
        Route::post('/solicitudes/eliminar', ['as' => 'solicitud.eliminar', 'uses' => 'ProduccionSolicitudesController@eliminar']);
        Route::post('/solicitudes/aprobar', ['as' => 'solicitud.aprobar', 'uses' => 'ProduccionSolicitudesController@aprobar']);
        Route::post('/solicitudes/revisar', ['as' => 'solicitud.revisar', 'uses' => 'ProduccionSolicitudesController@revisar']);
        Route::post('/solicitudes/verificar', ['as' => 'solicitud.verificar', 'uses' => 'ProduccionSolicitudesController@verificar']);

        Route::get('/tareo', ['as' => 'tareo', 'uses' => 'ProduccionTareoController@index']);
        Route::get('/tareo/actividades/{id}', ['as' => 'tareo.actividades', 'uses' => 'ProduccionTareoController@index']);
        Route::get('/tareo/reporte/{id}', ['as' => 'tareo.reporte', 'uses' => 'ProduccionTareoController@index']);
        Route::post('/tareo/cargar', ['as' => 'tareo.cargar', 'uses' => 'ProduccionTareoController@cargar']);
        Route::post('/tareo/empleados', ['as' => 'tareo.empleados', 'uses' => 'ProduccionTareoController@empleados']);
        Route::post('/tareo/reporte', ['as' => 'tareo.reporte', 'uses' => 'ProduccionTareoController@reporte']);
        Route::get('/tareo/exportar/{id}', ['as' => 'tareo.exportar', 'uses' => 'ProduccionTareoController@exportar']);

        Route::get('/documentaria', ['as' => 'documentaria', 'uses' => 'ProduccionAdjuntoController@index']);
        Route::get('/documentaria/{id}', ['as' => 'documentaria.id', 'uses' => 'ProduccionAdjuntoController@index']);
        Route::post('/documentaria/cargar', ['as' => 'documentaria.cargar', 'uses' => 'ProduccionAdjuntoController@cargarAll']);
        Route::post('/documentaria/buscar/', ['as' => 'documentaria.buscar', 'uses' => 'ProduccionAdjuntoController@buscar']);
        Route::patch('/documentaria/editar/', ['as' => 'documentaria.editar', 'uses' => 'ProduccionAdjuntoController@editar']);
        Route::post('/documentaria/eliminar/', ['as' => 'documentaria.eliminar', 'uses' => 'ProduccionAdjuntoController@eliminar']);
        Route::post('/documentaria/guardar/', ['as' => 'documentaria.guardar', 'uses' => 'ProduccionAdjuntoController@guardar']);
        Route::post('/documentaria/updatefile/', ['as' => 'documentaria.updatefile', 'uses' => 'ProduccionAdjuntoController@updatefile']);
        Route::get('/documentaria/download/{id}', ['as' => 'documentaria.download', 'uses' => 'ProduccionAdjuntoController@download']);

        Route::get('/entregas/solid', ['as' => 'entregas', 'uses' => 'ProduccionSolidEntregasController@index']);
        Route::post('/entregas/solid/listar', ['as' => 'entregas.listarSolid', 'uses' => 'ProduccionSolidEntregasController@listarSolid']);
        Route::post('/entregas/solid/items', ['as' => 'entregas.listarItems', 'uses' => 'ProduccionSolidEntregasController@listarItems']);
        Route::post('/entregas/solid/cargar', ['as' => 'entregas.cargar', 'uses' => 'ProduccionSolidEntregasController@cargar']);
        Route::post('/entregas/solid/registrar', ['as' => 'entregas.registrar', 'uses' => 'ProduccionSolidEntregasController@registrar']);
        Route::post('/entregas/solid/buscar', ['as' => 'entregas.buscar', 'uses' => 'ProduccionSolidEntregasController@buscar']);
        Route::patch('/entregas/solid/editar', ['as' => 'entregas.editar', 'uses' => 'ProduccionSolidEntregasController@editar']);
        Route::post('/entregas/solid/eliminar', ['as' => 'entregas.eliminar', 'uses' => 'ProduccionSolidEntregasController@eliminar']);
        Route::post('/entregas/solid/recibir', ['as' => 'entregas.recibir', 'uses' => 'ProduccionSolidEntregasController@recibir']);

        Route::get('/entregas/solicitud', ['as' => 'entregas', 'uses' => 'ProduccionSolicitudEntregasController@index']);
        Route::post('/entregas/solicitud/listar', ['as' => 'entregas.listarSolid', 'uses' => 'ProduccionSolicitudEntregasController@listarSolicitud']);
        Route::post('/entregas/solicitud/items', ['as' => 'entregas.listarItems', 'uses' => 'ProduccionSolicitudEntregasController@listarItems']);
        Route::post('/entregas/solicitud/cargar', ['as' => 'entregas.cargar', 'uses' => 'ProduccionSolicitudEntregasController@cargar']);
        Route::post('/entregas/solicitud/registrar', ['as' => 'entregas.registrar', 'uses' => 'ProduccionSolicitudEntregasController@registrar']);
        Route::post('/entregas/solicitud/buscar', ['as' => 'entregas.buscar', 'uses' => 'ProduccionSolicitudEntregasController@buscar']);
        Route::patch('/entregas/solicitud/editar', ['as' => 'entregas.editar', 'uses' => 'ProduccionSolicitudEntregasController@editar']);
        Route::post('/entregas/solicitud/eliminar', ['as' => 'entregas.eliminar', 'uses' => 'ProduccionSolicitudEntregasController@eliminar']);
        Route::post('/entregas/solicitud/recibir', ['as' => 'entregas.recibir', 'uses' => 'ProduccionSolicitudEntregasController@recibir']);

        Route::get('/restante/solid', ['as' => 'entregas', 'uses' => 'ProduccionSolidRestanteController@index']);
        Route::post('/restante/solid/listar', ['as' => 'entregas.listarSolid', 'uses' => 'ProduccionSolidRestanteController@listarSolid']);
        Route::post('/restante/solid/items', ['as' => 'entregas.listarItems', 'uses' => 'ProduccionSolidRestanteController@listarItems']);
        Route::post('/restante/solid/buscar', ['as' => 'entregas.buscar', 'uses' => 'ProduccionSolidRestanteController@buscar']);
        Route::patch('/restante/solid/editar', ['as' => 'entregas.editar', 'uses' => 'ProduccionSolidRestanteController@editar']);
      


    });

    Route::get('/pesos/tipo', ['as' => 'pesos.tipo', 'uses' => 'ProduccionPesosController@cargarTipoPeso']);
});