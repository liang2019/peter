<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'produccion'], function () {
    Route::get('/proyectos/ver/{tipo}', ['as' => 'produccion.cargarProyectos', 'uses' => 'ProduccionApiController@cargarProyectos']);
    Route::get('/solid/ver/{id}', ['as' => 'produccion.cargarSolids', 'uses' => 'ProduccionApiController@cargarSolids']);
      Route::get('/tareas/ver/', ['as' => 'produccion.cargarTareas', 'uses' => 'ProduccionApiController@cargarTareas']);
      Route::get('/trabajadores/ver/', ['as' => 'produccion.cargarTrabajadores', 'uses' => 'ProduccionApiController@cargarTrabajadores']);
      Route::post('/tareo/guardar/', ['as' => 'produccion.guardarTareo', 'uses' => 'ProduccionApiController@guardarTareo']);
      Route::post('/tareo/editar/', ['as' => 'produccion.editar', 'uses' => 'ProduccionApiController@editar']);
      Route::get('/tareo/listado/{id}', ['as' => 'produccion.listarTareoSinHoraFin', 'uses' => 'ProduccionApiController@listarTareoSinHoraFin']);
      //Route::get('/tipoproyectos/ver/{tipo}', ['as' => 'produccion.tipoproyectos', 'uses' => 'ProduccionApiController@cargartipoproyectos']);
   
});

