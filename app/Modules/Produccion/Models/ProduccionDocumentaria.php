<?php

namespace App\Modules\Produccion\Models;

use Illuminate\Database\Eloquent\Model;

class ProduccionDocumentaria extends Model
{
    protected $fillable = [
        'proyecto_id',
        'adjunto_id',
        'monto',
        'descripcion',
        'observacion',
        'adjunto',
      
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'produccion_documentaria';
    public $timestamps = false;
}
