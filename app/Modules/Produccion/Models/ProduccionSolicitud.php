<?php

namespace App\Modules\Produccion\Models;

use Illuminate\Database\Eloquent\Model;

class ProduccionSolicitud extends Model
{
    protected $fillable = [
        'proyecto_id',
        'descripcion',
        'tipo',
        'aprobado',
        'revisado',
          
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'produccion_solicitud';
    public $timestamps = false;
}
