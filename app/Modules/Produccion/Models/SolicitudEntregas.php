<?php

namespace App\Modules\Produccion\Models;

use Illuminate\Database\Eloquent\Model;

class SolicitudEntregas extends Model
{
    protected $fillable = [
        'produccion_solicitud_item_id',
        'cantidad',
        'recibido',
          
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'solicitud_entregas';
    public $timestamps = false;
}
