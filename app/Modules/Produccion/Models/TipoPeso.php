<?php

namespace App\Modules\Produccion\Models;

use Illuminate\Database\Eloquent\Model;

class TipoPeso extends Model
{
    protected $fillable = [
        'nombre',
        'codigo',
        
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'tipo_peso';
    public $timestamps = false;
}
