<?php

namespace App\Modules\Produccion\Models;

use Illuminate\Database\Eloquent\Model;

class SolidEntregas extends Model
{
    protected $fillable = [
        'solid_items_id',
        'cantidad',
        'recibido',
          
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'solid_entregas';
    public $timestamps = false;
}
