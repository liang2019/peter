<?php

namespace App\Modules\Produccion\Models;

use Illuminate\Database\Eloquent\Model;

class ProduccionSolicitudItem extends Model
{
    protected $fillable = [
        'produccion_solicitud_id',
        'alarti_id',
        'cantidad',
        'recibido',
          
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'produccion_solicitud_item';
    public $timestamps = false;
}
