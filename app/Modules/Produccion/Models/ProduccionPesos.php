<?php

namespace App\Modules\Produccion\Models;

use Illuminate\Database\Eloquent\Model;

class ProduccionPesos extends Model
{
    protected $fillable = [
        'proyecto_id',
        'tipo_peso_id',
        'peso',
        'descripcion',
        'observacion',
        'cantidad',
       
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'produccion_pesos';
    public $timestamps = false;
}
