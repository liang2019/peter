<?php

namespace App\Modules\Produccion\Models;

use Illuminate\Database\Eloquent\Model;

class TareoEmpleado extends Model
{
    protected $fillable = [
        'tareo_id',
        'empleado_id',
        
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'tareo_empleado';
    public $timestamps = false;
}
