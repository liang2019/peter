<?php

namespace App\Modules\Produccion\Models;

use Illuminate\Database\Eloquent\Model;

class ProduccionCronograma extends Model
{
    protected $fillable = [
        'proyecto_id',
        'actividad',
        'observacion',
        'fecha_inicio',
        'fecha_final',
        'completado',
        'orden',
        
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'produccion_cronograma';
    public $timestamps = false;
}
 