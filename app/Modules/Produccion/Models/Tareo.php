<?php

namespace App\Modules\Produccion\Models;

use Illuminate\Database\Eloquent\Model;

class Tareo extends Model
{
    protected $fillable = [
        'proyecto_id',
        'solid_id',
        'tarea_id',
        'tipo',
        'hora_inicio',
        'hora_fin',
        'horas_trabajadas',
          
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'tareo';
    public $timestamps = false;
}
