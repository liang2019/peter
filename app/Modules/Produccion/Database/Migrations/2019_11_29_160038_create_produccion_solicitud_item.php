<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduccionSolicitudItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produccion_solicitud_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('produccion_solicitud_id')->unsigned();
            $table->foreign('produccion_solicitud_id')->references('id')->on('produccion_solicitud')->onDelete('cascade');
            $table->bigInteger('alarti_id')->unsigned();
            $table->foreign('alarti_id')->references('id')->on('alarti')->onDelete('cascade');
            $table->integer('cantidad');
            $table->integer('recibido')->default(0);

            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion');
            $table->unsignedInteger('usuario_edicion');
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->timestamp('fecha_modificacion')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produccion_solicitud_item');
    }
}
