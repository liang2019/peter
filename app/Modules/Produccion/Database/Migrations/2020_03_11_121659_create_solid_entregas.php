<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolidEntregas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solid_entregas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('solid_items_id')->unsigned();
            $table->foreign('solid_items_id')->references('id')->on('solid_items')->onDelete('cascade');
            $table->integer('cantidad')->nullable();
            $table->unsignedInteger('recibido')->default(0);
            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion');
            $table->unsignedInteger('usuario_edicion');
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->timestamp('fecha_modificacion')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solid_entregas');
    }
}
