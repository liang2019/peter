<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudEntregas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_entregas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('produccion_solicitud_item_id')->unsigned();
            $table->foreign('produccion_solicitud_item_id')->references('id')->on('produccion_solicitud_item')->onDelete('cascade');
            $table->integer('cantidad')->nullable();
            $table->unsignedInteger('recibido')->default(0);
            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion');
            $table->unsignedInteger('usuario_edicion');
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->timestamp('fecha_modificacion')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_entregas');
    }
}
