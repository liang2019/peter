<?php

namespace App\Modules\Produccion\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Produccion\Services\ProduccionProyectoService as MainService;

class ProduccionProyectoController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarAll()
    {
        $data = $this->service->cargarAll();
        return $data;
    } 
}
