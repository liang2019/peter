<?php

namespace App\Modules\Produccion\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Produccion\Http\Requests\ProduccionPesoRequest as MainRequest;
use App\Modules\Produccion\Services\ProduccionPesoService as MainService;

class ProduccionPesosController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargar(Request $request)
    {
        $data = $this->service->cargar($request);
        return $data;
    }

    public function cargarTipoPeso()
    {
        $data = $this->service->cargarTipoPeso();
        return $data;
    }
    
    
    public function cargarMateriales(Request $request)
    {
        $data = $this->service->cargarMateriales($request);
        return $data;
    }


    public function buscar(Request $request)
    {
        $data = $this->service->buscar($request);
        return $data;
    }


    public function guardar(MainRequest $request)
    {
        $form = $request->validated();
        $save = $this->service->guardar($form);
        return $save;
    }

    public function editar(MainRequest $request)
    {
        $form = $request->validated();
        $data = $this->service->editar($form);
        return $data;
    }

    public function eliminar(Request $request)
    {
        $data = $this->service->eliminar($request);
        return $data;
    }

    public function grafico(Request $request)
    {
        $data = $this->service->grafico($request);
        return $data;
    }

}
