<?php

namespace App\Modules\Produccion\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Produccion\Services\ProduccionTareoService as MainService;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\Produccion\Exports\TareoExport;

class ProduccionTareoController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargar(Request $request)
    {
        $data = $this->service->cargar($request);
        return $data;
    }

    public function empleados(Request $request)
    {
        $data = $this->service->empleados($request);
        return $data;
    }

    public function reporte(Request $request)
    {
        $data = $this->service->reporte($request);
        return $data;
    }
    
    public function cargarTareas()
    {
        $data = $this->service->cargarTareas();
        return $data;
    }

    public function exportar($id) 
    {
      return Excel::download(new TareoExport($id), 'Costo Trabajador.xlsx');
    }
}
