<?php

namespace App\Modules\Produccion\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Produccion\Services\ProduccionApiService as MainService;

class ProduccionApiController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function cargarProyectos(Request $request)
    {
        $data = $this->service->cargarProyectos($request->route('tipo'));
        return $data;
    }
    public function cargarSolids(Request $request)
    {
        $data = $this->service->cargarSolids($request->route('id'));
        return $data;
    }
    public function cargartipoproyectos(Request $request)
    {
        $data = $this->service->cargartipoproyectos($request->route('tipo'));
        return $data;
    }
    
    public function cargarTareas()
    {
        $data = $this->service->cargarTareas();
        return $data;
    }

    public function cargarTrabajadores()
    {
        $data = $this->service->cargarTrabajadores();
        return $data;
    }

    public function guardarTareo(Request $request)
    {
        $save = $this->service->guardarTareo($request);
        return $save;
    }

    public function listarTareoSinHoraFin(Request $request)
    {
        $save = $this->service->listarTareoSinHoraFin($request->route('id'));
        return $save;
    }
    
    public function editar(Request $request)
    {
       
        $data = $this->service->editar($request);
        return $data;
    }

}
