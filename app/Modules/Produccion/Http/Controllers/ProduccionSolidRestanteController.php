<?php

namespace App\Modules\Produccion\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Produccion\Services\ProduccionSolidRestanteService as MainService;

class ProduccionSolidRestanteController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function listarSolid(Request $request)
    {
        $data = $this->service->listarSolid($request);
        return $data;
    } 

    public function listarItems(Request $request)
    {
        $data = $this->service->listarItems($request);
        return $data;
    } 


    public function buscar(Request $request)
    {
        $data = $this->service->buscar($request);
        return $data;
    }

    public function editar(Request $request)
    {
        $data = $this->service->editar($request);
        return $data;
    }

}
