<?php

namespace App\Modules\Produccion\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Produccion\Services\ProduccionSolicitudService as MainService;

class ProduccionSolicitudesController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargar(Request $request)
    {
        $data = $this->service->cargar($request);
        return $data;
    }

    public function guardar(Request $request)
    {
        $save = $this->service->guardar($request);
        return $save;
    }

    public function buscar(Request $request)
    {
        $data = $this->service->buscar($request);
        return $data;
    }

    public function eliminar(Request $request)
    {
        $data = $this->service->eliminar($request);
        return $data;
    }

    public function aprobar(Request $request)
    {
        $data = $this->service->aprobar($request);
        return $data;
    }

    public function revisar(Request $request)
    {
        $data = $this->service->revisar($request);
        return $data;
    }


    public function verificar(Request $request)
    {
        $data = $this->service->verificar($request);
        return $data;
    }
}
