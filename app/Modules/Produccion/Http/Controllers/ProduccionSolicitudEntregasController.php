<?php

namespace App\Modules\Produccion\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Produccion\Services\ProduccionSolicitudEntregasService as MainService;

class ProduccionSolicitudEntregasController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function listarSolicitud(Request $request)
    {
        $data = $this->service->listarSolicitud($request);
        return $data;
    } 

    public function listarItems(Request $request)
    {
        $data = $this->service->listarItems($request);
        return $data;
    } 

    public function cargar(Request $request)
    {
        $data = $this->service->cargar($request);
        return $data;
    }

    public function registrar(Request $request)
    {
        $data = $this->service->registrar($request);
        return $data;
    }

    public function eliminar(Request $request)
    {
        $data = $this->service->eliminar($request);
        return $data;
    }

    public function buscar(Request $request)
    {
        $data = $this->service->buscar($request);
        return $data;
    }

    public function editar(Request $request)
    {
        $data = $this->service->editar($request);
        return $data;
    }

    public function recibir(Request $request)
    {
        $data = $this->service->recibir($request);
        return $data;
    }
}
