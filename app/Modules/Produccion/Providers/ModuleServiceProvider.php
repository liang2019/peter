<?php

namespace App\Modules\Produccion\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('produccion', 'Resources/Lang', 'app'), 'produccion');
        $this->loadViewsFrom(module_path('produccion', 'Resources/Views', 'app'), 'produccion');
        $this->loadMigrationsFrom(module_path('produccion', 'Database/Migrations', 'app'), 'produccion');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('produccion', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('produccion', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
