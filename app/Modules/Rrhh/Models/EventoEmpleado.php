<?php

namespace App\Modules\Rrhh\Models;

use Illuminate\Database\Eloquent\Model;

class EventoEmpleado extends Model
{
    protected $fillable = [
        'empleado_id',
        'evento_id',
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
     ];
     protected $table = 'evento_empleado';
     public $timestamps = false;
}
