<?php

namespace App\Modules\Rrhh\Models;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $fillable = [
        'actividad_id',
        'descripcion',
        'fecha_inicio',
        'fecha_fin',
        'adjunto',
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion',
    ];
    protected $table = 'evento';
    public $timestamps = false;
}
