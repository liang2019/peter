<?php

namespace App\Modules\Rrhh\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('rrhh', 'Resources/Lang', 'app'), 'rrhh');
        $this->loadViewsFrom(module_path('rrhh', 'Resources/Views', 'app'), 'rrhh');
        $this->loadMigrationsFrom(module_path('rrhh', 'Database/Migrations', 'app'), 'rrhh');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('rrhh', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('rrhh', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
