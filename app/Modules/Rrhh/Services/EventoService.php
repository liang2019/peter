<?php

namespace App\Modules\Rrhh\Services;

use App\Modules\Rrhh\Models\Evento;
use App\Modules\Rrhh\Models\EventoEmpleado;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class EventoService
{


    public function __construct()
    {
        $this->model = new Evento();
        $this->model_evento_empleado = new EventoEmpleado();

    }

    public function cargarAll()
    {

        $datos = $this->model->where('evento.estado',1)
        ->leftJoin('actividad', 'evento.actividad_id', '=', 'actividad.id')
        ->select('evento.*', 'actividad.nombre as nombre_actividad',
        DB::raw("DATE_FORMAT(evento.fecha_inicio, '%d-%m-%Y' )as fecha_inicio"),
     
        DB::raw("IF(evento.fecha_fin IS NULL or evento.fecha_fin ='','No hay fecha',DATE_FORMAT(evento.fecha_fin, '%d-%m-%Y' )) as fecha_fin ")
        )
        ->get();
        return $datos;
    }

    public function cargarActividadesTrabajador($data)
    {
        $datos = $this->model
        ->where('evento.estado',1)
        ->where('evento_empleado.empleado_id',$data["empleado_id"])
        ->leftJoin('actividad', 'evento.actividad_id', '=', 'actividad.id')
        ->leftJoin('evento_empleado', 'evento.id', '=', 'evento_empleado.evento_id')
        ->select('evento.*', 'actividad.nombre as nombre_actividad',
        DB::raw("DATE_FORMAT(evento.fecha_inicio, '%d-%m-%Y' )as fecha_inicio"),
      
        DB::raw("IF(evento.fecha_fin IS NULL or evento.fecha_fin ='','No hay fecha',DATE_FORMAT(evento.fecha_fin, '%d-%m-%Y' )) as fecha_fin "),
        DB::raw("(SELECT CONCAT(empleado.nombres, ' ',empleado.apellidos) from empleado  WHERE evento_empleado.empleado_id=empleado.id ) as empleado") )
        ->get();
        return $datos;
    }

    public function guardar($data)
    {

        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $adjunto_file = $data["adjunto_file"];
        unset($data["adjunto_file"]);
        $save = $this->model->create($data);

        foreach ( json_decode($data["empleados"]) as $key => $value) {
            $reg = array(
                "evento_id" => $save->id,
                "empleado_id" => $value,
                "usuario_creacion" => auth()->user()["id"],
                "usuario_edicion" => auth()->user()["id"]
            );
            $empelado = $this->model_evento_empleado->create($reg);
        }


        if(  isset($data["adjunto"]) ){

            $path = Storage::putFileAs(
                config('app.upload_paths.events.attachments'), $adjunto_file, $save->id.'-'.$data["adjunto"]
            );
        }

        if(!$empelado){
            return false;
        }else{
            return $save;
        }

    }

    public function buscar($data){

        $datos =  $this->model->all()->where('id', $data["id"])->first();
        $empleados = $this->model_evento_empleado->where('evento_empleado.evento_id',$data["id"])
        ->leftJoin('empleado', 'evento_empleado.empleado_id', '=', 'empleado.id')
        ->select('empleado.*')
        ->get();
        $datos["empleados"] = $empleados;
        return $datos;
    }

    public function buscarEmpleados($data){

        $datos = $this->model_evento_empleado->where('evento_empleado.estado',1)
        ->leftJoin('empleado', 'evento_empleado.empleado_id', '=', 'empleado.id')
        ->select('empleado.*')
        ->get();
        return $datos;
    }

    public function updatefile($data){

            //unset($data["adjunto_file"]);

            
            if ($data["adjunto_file"]!='null') {
                $fileSearch = $this->model->find($data['id'])->adjunto;
                $exists = Storage::disk('local')->exists(config('app.upload_paths.events.attachments').'/'.$data['id'].'-'.$fileSearch);
                if ($exists) {
                    $delete = Storage::disk('local')->delete(config('app.upload_paths.events.attachments').'/'.$data['id'].'-'.$fileSearch);
                }
                $adjunto_file = $data["adjunto_file"];
                $path = Storage::disk('local')->putFileAs(
                    config('app.upload_paths.events.attachments'), $adjunto_file, $data['id'].'-'.$data["adjunto"]
                );
            }
            return "true";
    }
    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $datos = $this->model->find($data['id'])->update($data);
        if($datos){
            $update = $this->model_evento_empleado->where('evento_id', $data['id'])->delete();
            foreach ( $data["empleados"] as $key => $value) {
                $reg = array(
                    "evento_id" => $data['id'],
                    "empleado_id" => $value,
                    "usuario_creacion" => auth()->user()["id"],
                    "usuario_edicion" => auth()->user()["id"]
                );
                $empleado = $this->model_evento_empleado->create($reg);
            }

            if(!$empleado){
                return false;
            }else{
                return $update;
            }
        }else{
            return false;
        }

    }

    public function eliminar($data){

        $data["usuario_edicion"] = auth()->user()["id"];
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        //primero borrar a los empleados
        $update = $this->model_evento_empleado->where('evento_id', $data['id'])->delete();
        //luego elimina el archivo y el evento
        $archivo = $this->model->find($data['id']);

        $exists = Storage::disk('local')->exists(config('app.upload_paths.events.attachments').'/'.$data['id'].'-'.$archivo->adjunto);
        if ($exists) {
            $delete = Storage::disk('local')->delete(config('app.upload_paths.events.attachments').'/'.$data['id'].'-'.$archivo->adjunto);
        }
        $update = $archivo->delete();
        return $update;
    }

    public function download($id)
    {
        $datos =  $this->model->all()->where('id', $id)->first();
        return $datos;

    }

    public function exportar($data){

        $datos = $this->model
        ->where('evento.estado',1)
        ->where('evento_empleado.empleado_id',$data["empleado_id"])
        ->leftJoin('actividad', 'evento.actividad_id', '=', 'actividad.id')
        ->leftJoin('evento_empleado', 'evento.id', '=', 'evento_empleado.evento_id')
        ->select(
        
        'actividad.nombre as nombre_actividad',
        DB::raw("(SELECT CONCAT(empleado.nombres, ' ',empleado.apellidos) from empleado  WHERE evento_empleado.empleado_id=empleado.id ) as empleado") ,
        DB::raw("DATE_FORMAT(evento.fecha_inicio, '%d-%m-%Y' )as fecha_inicio"),
        DB::raw("IF(evento.fecha_fin IS NULL or evento.fecha_fin ='','No hay fecha',DATE_FORMAT(evento.fecha_fin, '%d-%m-%Y' )) as fecha_fin "),
        'evento.descripcion as descripcion')
        ->get();

        return collect($datos);
    }

}
