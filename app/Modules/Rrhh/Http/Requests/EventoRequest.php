<?php

namespace App\Modules\Rrhh\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'PATCH':
                $rules = $this->rules_editar();
                break;
            default:
                $rules = $this->rules_guardar();
                break;
        }
        return $rules;
    }

    public function rules_guardar(){
        return [
            'empleados' => 'required',
            'actividad_id' => 'required',
            'descripcion' => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'nullable',
            'adjunto' => 'nullable',
            'adjunto_file' => 'nullable',


        ];
    }

    public function rules_editar(){
        return [

            'id' => 'required',
            'empleados' => 'required',
            'actividad_id' => 'required',
            'descripcion' => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'nullable',
            'adjunto' => 'nullable',
            'adjunto_file' => 'nullable',


        ];
    }
}
