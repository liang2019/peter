<?php

namespace App\Modules\Rrhh\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Rrhh\Http\Requests\EventoRequest as MainRequest;
use App\Modules\Rrhh\Services\EventoService as MainService;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\Rrhh\Exports\RrhhEmpleadoExport;

class RrhhEventosController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }

    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function guardar(MainRequest $request)
    {
        $form = $request->validated();
        $save = $this->service->guardar($form);
        return $save;
    }
    public function updatefile(MainRequest $request)
    {
        $save = $this->service->updatefile($request);
        return $save;
    }

    public function cargarAll()
    {
        $data = $this->service->cargarAll();
        return $data;
    }

    public function cargarActividadesTrabajador(Request $request)
    {
        $data = $this->service->cargarActividadesTrabajador($request);
        return $data;
    }

    public function buscar(Request $request)
    {
        $data = $this->service->buscar($request);
        return $data;
    }

    public function buscarEmpleados(Request $request)
    {
        $data = $this->service->buscarEmpleados($request);
        return $data;
    }

    public function editar(MainRequest $request)
    {
        $form = $request->validated();
        $data = $this->service->editar($form);
        if($data){
            return 1;
        }else{
            return 0;
        }

    }
    public function eliminar(Request $request){
        $data = $this->service->eliminar($request);
        if($data){
            return 1;
        }else{
            return 0;
        }
    }

    public function download(Request $request)
    {
        $datos = $this->service->download($request->route('id'));
        return Storage::download(config("app.upload_paths.events.attachments")."/".$datos->id."-".$datos->adjunto);
    }

    public function exportar($id) 
    {
      return Excel::download(new RrhhEmpleadoExport($id), 'ActividadesEmpleado.xlsx');
    }
}
