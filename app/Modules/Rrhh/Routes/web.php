<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'auth'],function () {
    Route::group(['prefix' => 'rrhh'], function () {

        Route::get('/actividades', ['as' => 'eventos', 'uses' => 'RrhhEventosController@index']);
        Route::get('/actividades/empleado', ['as' => 'eventos', 'uses' => 'RrhhEventosController@index']);
        Route::get('/actividades/cargar', ['as' => 'eventos.cargar', 'uses' => 'RrhhEventosController@cargarAll']);
        Route::post('/actividades/listar', ['as' => 'eventos.cargar', 'uses' => 'RrhhEventosController@cargarActividadesTrabajador']);
        Route::post('/actividades/buscar/', ['as' => 'eventos.buscar', 'uses' => 'RrhhEventosController@buscar']);
        Route::post('/actividades/buscarEmpleados/', ['as' => 'eventos.buscarEmpleados', 'uses' => 'RrhhEventosController@buscarEmpleados']);
        Route::patch('/actividades/editar/', ['as' => 'eventos.editar', 'uses' => 'RrhhEventosController@editar']);
        Route::post('/actividades/eliminar/', ['as' => 'eventos.eliminar', 'uses' => 'RrhhEventosController@eliminar']);
        Route::post('/actividades/guardar/', ['as' => 'eventos.guardar', 'uses' => 'RrhhEventosController@guardar']);
        Route::post('/actividades/updatefile/', ['as' => 'eventos.updatefile', 'uses' => 'RrhhEventosController@updatefile']);
        Route::get('/actividades/download/{id}', ['as' => 'eventos.download', 'uses' => 'RrhhEventosController@download']);
        Route::get('/actividades/empleado/exportar/{id}', ['as' => 'eventos.exportar', 'uses' => 'RrhhEventosController@exportar']);

    });
});
