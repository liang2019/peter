<?php

namespace App\Modules\Configuracion\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('configuracion', 'Resources/Lang', 'app'), 'configuracion');
        $this->loadViewsFrom(module_path('configuracion', 'Resources/Views', 'app'), 'configuracion');
        $this->loadMigrationsFrom(module_path('configuracion', 'Database/Migrations', 'app'), 'configuracion');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('configuracion', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('configuracion', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
