<?php

namespace App\Modules\Configuracion\Services;

use App\Modules\Mantenimiento\Models\Proceso;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\Importaciones\Models\Alarti;
use Illuminate\Support\Facades\Storage;

class ProcesosService
{
    public function __construct()
    {
        $this->model = new Proceso();
    }
    public function importar(){
        $file_n = base_path().'/procesos/procesos.csv';
        $file = fopen($file_n, "r");
        $all_data = array();
        $i=0;
        while (($record = fgetcsv($file, 200, ",")) !==FALSE) {
            $data = explode(";",$record[0]);
            if ($i>0) {
                Proceso::firstOrCreate(
                    ["nombre"=>$data[0]],
                    [ 'nombre' => $data[0],
                    'unidad' =>$data[2],
                    'costo' =>floatval($data[3]),
                    'grupo' =>$data[4],
                    'usuario_creacion' =>auth()->user()->id,
                    'usuario_edicion' =>auth()->user()->id,
                    'fecha_creacion' =>date('Y-m-d H:i:s'),
                    'fecha_modificacion' =>date('Y-m-d H:i:s'),
                    ]
                );
            }
            $i++;
         }
        fclose($file);
        return true;
    }
}
