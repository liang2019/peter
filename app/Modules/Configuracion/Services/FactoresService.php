<?php

namespace App\Modules\Configuracion\Services;

use App\Modules\Mantenimiento\Models\Factor;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\Importaciones\Models\Alarti;
use Illuminate\Support\Facades\Storage;

class FactoresService
{


    public function __construct()
    {
        $this->model = new Factor();


    }
    public function importarExcel($data){

        $count = 0;
        dd($data);
        foreach ($data[0] as $key => $value) {
            if ($count>0) {
                dd($value);
                $alarti_id=Alarti::where('codigo',$value[0])->first();

                if ($alarti_id["codigo"]!=null) {
                    Factor::updateOrInsert(
                        ["alarti_id"=>$alarti_id["id"]],
                        [ 'e' => floatval(isset($value[1])?$value[1]:0),
                        'ld' =>floatval(isset($value[2])?$value[2]:0),
                        'caras' =>intval($value[3]),
                        'unidad' =>$value[4],
                        'pieza' =>$value[5],
                        'perimetro' =>floatval(isset($value[6])?$value[6]:0),
                        'seccion' =>floatval(isset($value[7])?$value[7]:0),
                        'unidad_pieza' =>floatval(isset($value[8])?$value[8]:0),
                        'kilogramo_unidad' =>floatval(isset($value[9])?$value[9]:0),
                        'kilogramo_pieza' =>floatval(isset($value[10])?$value[10]:0),
                        'metro_unidad' =>floatval(isset($value[11])?$value[11]:0),
                        'metro_pieza' =>floatval(isset($value[12])?$value[12]:0),

                        'usuario_creacion' =>1,
                        'usuario_edicion' =>1
                        ]
                    );
                }

            }
            $count++;
        }
        return true;
    }
    public function importar($data){

        $count = 0;
        foreach ($data[0] as $key => $value) {
            if ($count>0) {


                $alarti_id=Alarti::where('codigo',$value[0])->first();

                if ($alarti_id["codigo"]!=null) {
                    Factor::updateOrInsert(
                        ["alarti_id"=>$alarti_id["id"]],
                        [ 'e' => floatval(isset($value[1])?$value[1]:0),
                        'ld' =>floatval(isset($value[2])?$value[2]:0),
                        'caras' =>intval($value[3]),
                        'unidad' =>$value[4],
                        'pieza' =>$value[5],
                        'perimetro' =>floatval(isset($value[6])?$value[6]:0),
                        'seccion' =>floatval(isset($value[7])?$value[7]:0),
                        'unidad_pieza' =>floatval(isset($value[8])?$value[8]:0),
                        'kilogramo_unidad' =>floatval(isset($value[9])?$value[9]:0),
                        'kilogramo_pieza' =>floatval(isset($value[10])?$value[10]:0),
                        'metro_unidad' =>floatval(isset($value[11])?$value[11]:0),
                        'metro_pieza' =>floatval(isset($value[12])?$value[12]:0),

                        'usuario_creacion' =>1,
                        'usuario_edicion' =>1
                        ]
                    );
                }

            }
            $count++;
        }
        return true;
    }
}
