<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['middleware' => 'auth'],function () {
    Route::group(['prefix' => 'configuracion'], function () {

        Route::get('/', ['as' => 'factores', 'uses' => 'ConfiguracionController@index']);
        //Route::get('/factores', ['as' => 'factores', 'uses' => 'ConfiguracionFactoresController@index']);
        Route::get('factores/importar', ['as' => 'configuracion.factores', 'uses' => 'ConfiguracionController@factoresImportar']);
        Route::get('procesos/importar', ['as' => 'configuracion.procesos', 'uses' => 'ConfiguracionController@procesosImportar']);
        Route::get('importarexcel', ['as' => 'configuracion.procesos', 'uses' => 'ConfiguracionController@importarExcel']);



    });
});
