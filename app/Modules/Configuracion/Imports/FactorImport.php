<?php

namespace App\Modules\Configuracion\Imports;

use App\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Modules\Mantenimiento\Models\Factor;
use App\Modules\Importaciones\Models\Alarti;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
class FactorImport implements ToModel,WithMultipleSheets,WithCalculatedFormulas
{
    public function sheets(): array
    {
        return [
            0 => new FirstSheetImport(),1=> new SecondSheetImport(), 2=>new ThirdSheetImport()
        ];
    }
    public function model(array $row)
    {
        
    } 
}

