<?php
namespace App\Modules\Configuracion\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Modules\Configuracion\Services\FactoresService as FactorService;
use App\Modules\Configuracion\Services\ProcesosService as ProcesoService;
use App\Modules\Configuracion\Imports\FactorImport;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
class ConfiguracionController extends Controller implements WithCalculatedFormulas
{
    public function __construct()
    {
        $this->factorService = new FactorService();
        $this->procesoService = new ProcesoService();
    }

    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function factoresImportar(Request $request)
    {
       // $save = $this->factorService->importar($request);
       $valor = Excel::toArray(new FactorImport, base_path().'/factores/factores.xlsx');
       $save = $this->factorService->importar($valor);
        return $save?1:$save;
    }
    public function procesosImportar(Request $request)
    {
        $save = $this->procesoService->importar($request);
    }
    public function importarExcel(Request $request)
    {
       // $save = $this->factorService->importar($request);
       $valor = Excel::toArray(new FactorImport, storage_path().'/app/public/alarti_factor_procesos.xlsx');
       $save = $this->factorService->importarExcel($valor);
        return $save?1:$save;
    }
    
}
