<?php

namespace App\Modules\Montaje\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('montaje', 'Resources/Lang', 'app'), 'montaje');
        $this->loadViewsFrom(module_path('montaje', 'Resources/Views', 'app'), 'montaje');
        $this->loadMigrationsFrom(module_path('montaje', 'Database/Migrations', 'app'), 'montaje');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('montaje', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('montaje', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
