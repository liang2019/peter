<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupervision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supervision', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('proyecto_id')->unsigned();
            $table->foreign('proyecto_id')->references('id')->on('proyecto')->onDelete('cascade');
            $table->bigInteger('geolocalizacion_estado_id')->unsigned();
            $table->foreign('geolocalizacion_estado_id')->references('id')->on('geolocalizacion_estado')->onDelete('cascade');
            $table->string('descripcion', 500)->nullable();
            $table->bigInteger('montaje_cronograma_id')->unsigned();
            $table->foreign('montaje_cronograma_id')->references('id')->on('montaje_cronograma')->onDelete('cascade');
            $table->string('latitud', 191);
            $table->string('logitud', 191);
          
            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion');
            $table->unsignedInteger('usuario_edicion');
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->timestamp('fecha_modificacion')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supervision');
    }
}
