<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupervisionIncidencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supervision_incidencia', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('supervision_id')->unsigned();
            $table->foreign('supervision_id')->references('id')->on('supervision')->onDelete('cascade');
            $table->bigInteger('incidencia_id')->unsigned();
            $table->foreign('incidencia_id')->references('id')->on('incidencia')->onDelete('cascade');

            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion');
            $table->unsignedInteger('usuario_edicion');
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->timestamp('fecha_modificacion')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supervision_incidencia');
    }
}
