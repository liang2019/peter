<?php

namespace App\Modules\Montaje\Services;

use App\Modules\Montaje\Models\MontajeCronograma;
use App\Modules\Requerimiento\Models\Proyecto;
use Illuminate\Support\Facades\DB;
class MontajeCronogramaService
{


    public function __construct()
    {
        $this->model = new MontajeCronograma();
        $this->model_proyecto = new Proyecto();
    }

    public function cargar($data){

        $datos["items"] = $this->model->where('montaje_cronograma.proyecto_id', $data["id"])
        ->where('montaje_cronograma.estado', 1)
        ->select('montaje_cronograma.*',
        'montaje_cronograma.actividad as name',
        DB::raw('IF(montaje_cronograma.completado=1,"completado","En Curso") as preview'),
        'montaje_cronograma.observacion as details',
        'montaje_cronograma.fecha_inicio as start',
        'montaje_cronograma.fecha_final as end',DB::raw('IF(montaje_cronograma.completado=1,"green","blue") as color') )
        ->orderBy('montaje_cronograma.orden', 'asc')
        ->orderBy('montaje_cronograma.id', 'asc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.montaje as estado_tracking')
        ->first();
        return $datos;
    }

    public function guardar($data)
    {
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $ultimo =  $this->model->select('*')->where('estado', 1)->where('proyecto_id', $data["proyecto_id"])->orderBy('orden', 'desc')->first();
        $ultimo_orden = 1;
        $ultimo_orden = ($ultimo==null)?$ultimo_orden:intval($ultimo->orden) + 1;
        $data["orden"] = $ultimo_orden;
        $save = $this->model->create($data);
        return $save;
    }

    public function buscar($data){

        $datos =  $this->model
        ->select('*')->where('id', $data["id"])->first();
        return $datos;
    }

    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update($data);
        return $update?1:$update;
    }

    public function eliminar($data){

        $update = $this->model->find($data['id'])->update([
            'estado' => 0,
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

    public function completar($data){

        $update = $this->model->find($data['id'])->update([
            'completado' => 1,
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

}
