<?php

namespace App\Modules\Montaje\Services;

use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Montaje\Models\GeolocalizacionEstado;
use App\Modules\Montaje\Models\Supervision;
use App\Modules\Montaje\Models\SupervisionAdjunto;
use App\Modules\Montaje\Models\SupervisionIncidencia;
use App\Modules\Montaje\Models\Incidencia;
use App\Modules\Montaje\Models\MontajeCronograma;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
class MontajeApiService
{

    public function __construct()
    {
        $this->model = new Supervision();
        $this->model_proyecto = new Proyecto();
        $this->model_estados = new GeolocalizacionEstado();
        $this->model_incidencia = new Incidencia();
        $this->model_supervision_incidencia = new SupervisionIncidencia();
        $this->model_supervision_adjunto = new SupervisionAdjunto();
        $this->model_hitos = new MontajeCronograma();
    }

    public function cargarProyectos(){

        $datos = $this->model_proyecto
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->where("tracking.montaje",1)
        ->where("proyecto.montaje", 1)
        ->where("proyecto.estado",1)
        ->select('proyecto.*',DB::raw('UPPER(proyecto.nombre) as nombre'))
        ->orderBy('proyecto.nombre', 'asc')
        ->get();
       // $datos = strtoupper($datos);
        return response()->json($datos,200);
    }

    public function cargarEstados(){

        $datos = $this->model_estados
        ->where("estado",1)
        ->select('*' ,DB::raw('UPPER(nombre) as nombre'))
        ->orderBy('nombre', 'asc')
        ->get();

        return response()->json($datos,200);
    }

    public function cargarHitos($data){

        $datos = $this->model_hitos
        ->where("proyecto_id",$data)
        ->where("estado",1)
        ->select('*' ,DB::raw('UPPER(actividad) as actividad'))
        ->orderBy('actividad', 'asc')
        ->get();

        return response()->json($datos,200);
    }

    public function cargarIncidencias(){

        $datos = $this->model_incidencia
        ->where("estado",1)
        ->select('*' ,DB::raw('UPPER(nombre) as nombre'))
        ->orderBy('nombre', 'asc')
        ->get();
        //$datos = strtoupper($datos);
        return response()->json($datos,200);
    }

    public function supervisionGuardarAdjuntos($data){
        ini_set('memory_limit','360M');
        ini_set('max_execution_time', 1280);
        $adjuntos = null;
        try {
            Log::info("MontajeApiService.php::supervisionGuardarAdjuntos::ENTRO OK");
            $extension =strtolower(pathinfo($data["nombre_adjunto"], PATHINFO_EXTENSION));
            $adjuntos = $this->model_supervision_adjunto->create([
                "supervision_id" => $data["supervision_id"],
                "adjunto" => $data["nombre_adjunto"],
                "tipo" => ($extension=="png" || $extension=="jpeg")?1:($extension=="mp4"?2:3),
                "usuario_creacion" => $data["usuario_id"],
                "usuario_edicion" => $data["usuario_id"],
            ]);
            $adjunto_file = base64_decode($data["videobuffer"]);
            if( isset($adjunto_file) && $adjunto_file!='null' ){
                $path = Storage::put(config('app.upload_paths.supervision.adjuntos').'/'.$data["nombre_adjunto"], $adjunto_file);
                if(!$path){
                    return 0;
                }
            }
            Log::info("MontajeApiService.php::supervisionGuardarAdjuntos::SALIÓ OK");

            /*Log::info($_FILES);
            Log::info($_FILES["adjuntos"]);
            foreach ($_FILES["adjuntos"]["name"] as $key => $value) {
                $extension =strtolower(pathinfo($value, PATHINFO_EXTENSION));
                Log::info($extension);

                $adjuntos = $this->model_supervision_adjunto->create([
                    "supervision_id" => $data["supervision_id"],
                    "adjunto" => $value,
                    "tipo" => $extension=="png"?1:($extension=="mp4"?2:3),
                    "usuario_creacion" => $data["usuario_id"],
                    "usuario_edicion" => $data["usuario_id"],
                ]);

            $adjunto_file = $data["adjuntos"][$key];
            // unset($file["tmp_name"]);
                if(  isset($value) && $value!='null' ){
                    $path = Storage::put(config('app.upload_paths.supervision.adjuntos').'/'.$value, $adjunto_file);
                    /*$path = Storage::putFileAs(
                        config('app.upload_paths.supervision.adjuntos'), $adjunto_file,$data["supervision_id"].'-'.$value
                    );
                    if(!$path){
                        return 0;
                    }
                }
            }*/
            return response()->json($adjuntos,200);
        } catch (Exception $th) {
            Log::debug("ERROR",$th);
            return response()->json($th,500);
        }
        return response()->json($adjuntos,500);
    }
    public function guardarSupervision($data){
        //return response()->json(200);
        $save = $this->model->create([
            "proyecto_id" =>   $data["proyecto_id"],
            "geolocalizacion_estado_id" =>   $data["geolocalizacion_estado_id"],
            "descripcion" =>   str_replace('"','',$data["descripcion"]),
            "montaje_cronograma_id" =>   $data["montaje_cronograma_id"],
            "latitud" =>   str_replace('"','',$data["latitud"]) ,
            "logitud" =>   str_replace('"','',$data["logitud"]),
            "usuario_creacion" => $data["usuario_id"],
            "usuario_edicion" => $data["usuario_id"],
            "fecha_creacion" => now()->format('Y-m-d H:i:s'),
            "fecha_modificacion" => now()->format('Y-m-d H:i:s'),
        ]);

        if ($save){
            if (isset($data["incidencias"])) {
                foreach ($data["incidencias"] as $key => $value) {
                    $incidencias = $this->model_supervision_incidencia->create([
                        "supervision_id" =>   $save->id,
                        "incidencia_id" =>   $value,
                        "usuario_creacion" => $data["usuario_id"],
                        "usuario_edicion" => $data["usuario_id"],
                        "fecha_creacion" => now()->format('Y-m-d H:i:s'),
                        "fecha_modificacion" => now()->format('Y-m-d H:i:s'),
                    ]);
                }
            }

            /*Log::info($_FILES);
            Log::info($_FILES["adjuntos"]);*/
            try {
                if (isset($_FILES["adjuntos"])) {
                    foreach ($_FILES["adjuntos"]["name"] as $key => $value) {
                        $extension =strtolower(pathinfo($value, PATHINFO_EXTENSION));
                        Log::info($extension);

                        $adjuntos = $this->model_supervision_adjunto->create([
                            "supervision_id" => $save->id,
                            "adjunto" => $value,
                            "tipo" => ($extension=="png" || $extension=="jpeg")?1:($extension=="mp4"?2:3),
                            "usuario_creacion" => $data["usuario_id"],
                            "usuario_edicion" => $data["usuario_id"],
                            "fecha_creacion" => now()->format('Y-m-d H:i:s'),
                            "fecha_modificacion" => now()->format('Y-m-d H:i:s'),
                        ]);

                    $adjunto_file = $data["adjuntos"][$key];
                    // unset($file["tmp_name"]);
                        if(  isset($value) && $value!='null' ){
                            //$path = Storage::put(config('app.upload_paths.supervision.adjuntos').'/'.$value, $adjunto_file);
                            $path = Storage::putFileAs(
                                config('app.upload_paths.supervision.adjuntos'), $adjunto_file,$save->id.'-'.$value
                            );
                            if(!$path){
                                return 0;
                            }
                        }
                    }
                }
            } catch (Exception $th) {
                Log::info("ERROR",$th);
                return response()->json($th,500);
            }
            return response()->json(array("mensaje"=>"El montaje ha sido registrado con éxito."),200);
        }
        return response()->json($save,500);
    }

    public function verActividades($data){

        $datos = $this->model
        ->where("estado",1)
        ->where("usuario_creacion", $data["id"])
        ->where(DB::raw("date_format(fecha_creacion,'%Y-%m-%d')"), date('Y-m-d'))
        ->select('supervision.*',
        DB::raw("(SELECT nombre FROM proyecto WHERE proyecto.id=supervision.proyecto_id) as nombre_proyecto"),
        DB::raw("(SELECT nombre FROM geolocalizacion_estado WHERE geolocalizacion_estado.id=supervision.geolocalizacion_estado_id) as estado_proyecto"),
        DB::raw("(SELECT actividad FROM montaje_cronograma WHERE montaje_cronograma.id=supervision.montaje_cronograma_id) as hito"),
        DB::raw("(IFNULL((SELECT COUNT(supervision_adjunto.id) FROM supervision_adjunto WHERE  supervision_adjunto.estado=1 AND supervision_adjunto.supervision_id=supervision.id) ,0)) as cantidad_adjuntos")

        )
        ->orderBy('id', 'asc')
        ->get();
        return response()->json($datos,200);
    }


}
