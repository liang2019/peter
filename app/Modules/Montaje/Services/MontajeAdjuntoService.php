<?php

namespace App\Modules\Montaje\Services;

use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Montaje\Models\MontajeDocumentaria;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;


class MontajeAdjuntoService
{


    public function __construct()
    {
        $this->model = new MontajeDocumentaria();
        $this->model_proyecto = new Proyecto();

    }

    public function cargarAll($data){

        $datos["adjuntos"] = $this->model->where('montaje_documentaria.proyecto_id',$data["id"])
        ->leftJoin('adjunto', 'montaje_documentaria.adjunto_id', '=', 'adjunto.id')
        ->select('montaje_documentaria.*', 'adjunto.nombre as tipo_adjunto')
        ->orderBy('montaje_documentaria.id', 'desc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select(
                    'proyecto.*',
                    'tracking.montaje as estado_tracking'
                )
        ->first();

        return $datos;
    }

    public function guardar($data)
    {
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $adjunto_file = $data["adjunto_file"];
        unset($data["adjunto_file"]);
        $save = $this->model->create($data);

        if(  isset($data["adjunto"]) && $data["adjunto"]!='null' ){
            $path = Storage::putFileAs(
                config('app.upload_paths.montaje.adjuntos'), $adjunto_file, $save->id.'-'.$data["adjunto"]
            );
            if(!$path){
                return 0;
            }
        }
        if ($data["completarMontaje"]=="true") {
            $this->completar($data);
        }
        return $save;
    }

    public function buscar($data){

        $datos =  $this->model->all()->where('id', $data["id"])->first();
        return $datos;
    }


    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update($data);

        return $update?1:$update;

    }

    public function updatefile($data){

        
        if ($data["adjunto_file"]!='null') {
            $fileSearch = $this->model->find($data['id'])->adjunto;
            $exists = Storage::disk('local')->exists(config('app.upload_paths.montaje.adjuntos').'/'.$data['id'].'-'.$fileSearch);
            if ($exists) {
                $delete = Storage::disk('local')->delete(config('app.upload_paths.montaje.adjuntos').'/'.$data['id'].'-'.$fileSearch);
            }
            $adjunto_file = $data["adjunto_file"];
            $path = Storage::disk('local')->putFileAs(
                config('app.upload_paths.montaje.adjuntos'), $adjunto_file, $data['id'].'-'.$data["adjunto"]
            );
        }
        return "true";
}

    public function eliminar($data){
        $data["usuario_edicion"] = auth()->user()["id"];
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');

        // elimina el archivo
        $archivo = $this->model->find($data['id']);

        $exists = Storage::disk('local')->exists(config('app.upload_paths.montaje.adjuntos').'/'.$data['id'].'-'.$archivo->adjunto);

        if ($exists) {
            $delete = Storage::disk('local')->delete(config('app.upload_paths.montaje.adjuntos').'/'.$data['id'].'-'.$archivo->adjunto);
        }
          // elimina el adjunto
        $update = $archivo->delete();
        return $update?1:$update;
    }

    public function download($id)
    {
        $datos =  $this->model->all()->where('id', $id)->first();
        return $datos;
    }

    public function completar($data)
    {
        //completar etapa montaje
        $update = $this->model_tarcking->where('proyecto_id', $data["proyecto_id"])
        ->update([
                    'montaje' => 2,
                    'montaje_fecha'=>now()->format('Y-m-d H:i:s'),
                    'usuario_edicion' => auth()->user()->id,
                    'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                ]);

                $update = $this->model_proyecto->where('id', $data["proyecto_id"])
                ->update([
                            'estado' => 2,
                            'usuario_edicion' => auth()->user()->id,
                            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                        ]);
        return $update;
    }

}
