<?php

namespace App\Modules\Montaje\Services;

use App\Modules\Montaje\Models\Supervision;
use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Montaje\Models\SupervisionAdjunto;
use App\Modules\Montaje\Models\SupervisionIncidencia;
use Illuminate\Support\Facades\DB;

class MontajeAvanceService
{

    public function __construct()
    {
        $this->model = new Supervision();
        $this->model_proyecto = new Proyecto();
        $this->model_supervision_adjunto = new SupervisionAdjunto();
        $this->model_supervision_incidencia = new SupervisionIncidencia();
    }

    public function cargar($data){

        $datos["items"] = $this->model->where('supervision.proyecto_id', $data["proyecto_id"])
        ->leftJoin('geolocalizacion_estado', 'supervision.geolocalizacion_estado_id', '=', 'geolocalizacion_estado.id')
        ->where('supervision.estado', 1)
        ->select('supervision.*', 'geolocalizacion_estado.nombre as estado_nombre',
            DB::raw("(SELECT actividad FROM montaje_cronograma WHERE supervision.montaje_cronograma_id = montaje_cronograma.id AND supervision.estado=1) as nombre_hito "),
            DB::raw("(IFNULL((SELECT COUNT(supervision_adjunto.id) FROM supervision_adjunto WHERE supervision_adjunto.tipo=1 AND supervision_adjunto.estado=1 AND supervision_adjunto.supervision_id=supervision.id) ,0)) as cantidad_fotos"),
            DB::raw("(IFNULL((SELECT COUNT(supervision_adjunto.id) FROM supervision_adjunto WHERE supervision_adjunto.tipo=2 AND supervision_adjunto.estado=1 AND supervision_adjunto.supervision_id=supervision.id) ,0)) as cantidad_videos"),
            DB::raw("(IFNULL((SELECT COUNT(supervision_adjunto.id) FROM supervision_adjunto WHERE supervision_adjunto.tipo=3 AND supervision_adjunto.estado=1 AND supervision_adjunto.supervision_id=supervision.id) ,0)) as cantidad_audios")
        )
        ->orderBy('supervision.id', 'desc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["proyecto_id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.montaje as estado_tracking')
        ->first();
        return $datos;
    }

    public function verAdjuntos($data){
        $datos = $this->model_supervision_adjunto
        ->where('supervision_id', $data["supervision_id"])
        ->where('estado',1)
        ->select('*')
        ->orderBy('id', 'desc')
        ->get();
        return $datos;
    }

    public function verIncidencias($data){
        $datos = $this->model_supervision_incidencia
        ->leftJoin('incidencia', 'supervision_incidencia.incidencia_id', '=', 'incidencia.id')
        ->where('supervision_incidencia.supervision_id', $data["supervision_id"])
        ->where('supervision_incidencia.estado',1)
        ->select('supervision_incidencia.*', 'incidencia.nombre')
        ->orderBy('supervision_incidencia.id', 'desc')
        ->get();
        return $datos;
    }

    public function verGeolocalización($data){
        $datos =  $this->model->all()->where('id', $data["id"])->first();
        return $datos;
    }

    public function verRecorrido($data){
        $datos["items"] = $this->model->where('supervision.proyecto_id', $data["proyecto_id"])
        ->leftJoin('geolocalizacion_estado', 'supervision.geolocalizacion_estado_id', '=', 'geolocalizacion_estado.id')
        ->where('supervision.estado', 1)
        ->select('supervision.*', 'geolocalizacion_estado.nombre as estado_nombre')
        ->orderBy('supervision.id', 'desc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["proyecto_id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.montaje as estado_tracking')
        ->first();
        return $datos;
    }
    public function download($id)
    {
        $datos =  $this->model_supervision_adjunto->all()->where('id', $id)->first();
        return $datos;
    }

}
