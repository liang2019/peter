<?php

namespace App\Modules\Montaje\Services;

use App\Modules\Montaje\Models\Kardex;
use App\Modules\Requerimiento\Models\Proyecto;
use Illuminate\Support\Facades\DB;

class MontajeKardexService
{

    public function __construct()
    {
        $this->model = new Kardex();
        $this->model_proyecto = new Proyecto();
    }

    public function cargar($data){

        $datos["items"] = $this->model->where('kardex.proyecto_id', $data["proyecto_id"])
        ->where('kardex.estado', 1)
        ->leftJoin('alarti', 'kardex.alarti_id', '=', 'alarti.id')
        ->select('kardex.*', 'alarti.descripcion as nombre_material',
        DB::raw('kardex.cantidad_entrada-kardex.cantidad_salida as kardex')
        )
        ->orderBy('kardex.id', 'desc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["proyecto_id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.montaje as estado_tracking')
        ->first();
        return $datos;
    }

    public function guardar($data)
    {
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        $save = $this->model->create($data);
        return $save;
    }

    public function eliminar($data){

        $update = $this->model->find($data['id'])->update([
            'estado' => 0,
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

    public function descontar($data){

        $update = $this->model->find($data['id'])->update([
            'cantidad_salida' => $data['cantidad_salida'],
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

    public function kardex($data){

        $datos["items"] = $this->model->where('kardex.proyecto_id',$data["proyecto_id"])
        ->where('kardex.estado', 1)
        ->leftJoin('alarti', 'kardex.alarti_id', '=', 'alarti.id')
        ->select(
            'kardex.alarti_id',
            'alarti.descripcion as nombre_material',
            'alarti.codigo as codigo_material',
            DB::raw("(IFNULL(SUM(kardex.cantidad_entrada),0)) as cantidad_entrada"),
            DB::raw("(IFNULL(SUM(kardex.cantidad_salida),0)) as cantidad_salida"),
            DB::raw('(IFNULL(SUM(kardex.cantidad_entrada)-SUM(kardex.cantidad_salida),0)) as kardex')
        )
        ->groupBy('kardex.alarti_id', 'alarti.descripcion', 'alarti.codigo')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["proyecto_id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.montaje as estado_tracking')
        ->first();
        return $datos;
    }

}
