<?php

namespace App\Modules\Montaje\Services;

use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Tracking\Models\Tracking;
use Illuminate\Support\Facades\DB;
class MontajeProyectoService
{


    public function __construct()
    {
        $this->model = new Proyecto();
        $this->model_tarcking = new Tracking();
    }

    public function cargarAll(){

        $datos = $this->model
        ->where("tracking.montaje","<>", 3)
        ->where("proyecto.montaje","=", 1)
        ->leftJoin('cliente', 'proyecto.cliente_id', '=', 'cliente.id')
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'cliente.descripcion as nombre_cliente',
        'tracking.montaje as estado_tracking'
        )
        ->orderBy('proyecto.id', 'desc')
        ->get();
        return $datos;
    }

    
}
