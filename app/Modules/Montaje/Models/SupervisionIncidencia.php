<?php

namespace App\Modules\Montaje\Models;

use Illuminate\Database\Eloquent\Model;

class SupervisionIncidencia extends Model
{
    protected $fillable = [
        'supervision_id',
        'incidencia_id',
        
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'supervision_incidencia';
    public $timestamps = false;
}
