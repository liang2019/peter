<?php

namespace App\Modules\Montaje\Models;

use Illuminate\Database\Eloquent\Model;

class Kardex extends Model
{
    protected $fillable = [
        'proyecto_id',
        'alarti_id',
        'cantidad_entrada',
        'cantidad_salida',
        'completado',
      
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'kardex';
    public $timestamps = false;
}
