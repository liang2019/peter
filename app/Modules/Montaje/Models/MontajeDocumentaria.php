<?php

namespace App\Modules\Montaje\Models;

use Illuminate\Database\Eloquent\Model;

class MontajeDocumentaria extends Model
{
    protected $fillable = [
        'proyecto_id',
        'adjunto_id',
        'monto',
        'descripcion',
        'observacion',
        'adjunto',
      
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'montaje_documentaria';
    public $timestamps = false;
}
