<?php

namespace App\Modules\Montaje\Models;

use Illuminate\Database\Eloquent\Model;

class Supervision extends Model
{
    protected $fillable = [
        'proyecto_id',
        'geolocalizacion_estado_id',
        'descripcion',
        'montaje_cronograma_id',
        'latitud',
        'logitud',
      
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'supervision';
    public $timestamps = false;
}
