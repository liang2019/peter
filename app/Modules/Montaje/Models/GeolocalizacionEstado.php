<?php

namespace App\Modules\Montaje\Models;

use Illuminate\Database\Eloquent\Model;

class GeolocalizacionEstado extends Model
{
    protected $fillable = [
        'nombre',
        
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'geolocalizacion_estado';
    public $timestamps = false;
}
