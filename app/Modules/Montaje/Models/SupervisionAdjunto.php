<?php

namespace App\Modules\Montaje\Models;

use Illuminate\Database\Eloquent\Model;

class SupervisionAdjunto extends Model
{
    protected $fillable = [
        'supervision_id',
        'adjunto',
        'tipo',
        
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'supervision_adjunto';
    public $timestamps = false;
}
