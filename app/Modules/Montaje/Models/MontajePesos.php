<?php

namespace App\Modules\Montaje\Models;

use Illuminate\Database\Eloquent\Model;

class MontajePesos extends Model
{
    protected $fillable = [
        'proyecto_id',
        'tipo_peso_id',
        'peso',
        'descripcion',
        'observacion',
        'cantidad',
      
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'montaje_pesos';
    public $timestamps = false;
}
