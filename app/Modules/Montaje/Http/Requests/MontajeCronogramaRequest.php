<?php

namespace App\Modules\Montaje\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MontajeCronogramaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
           
            case 'PATCH':
                $rules = $this->rules_editar();
                break;
            default:
                $rules = $this->rules_guardar();
                break;
        }
        return $rules;
    }

    public function rules_guardar(){
        return [
      
            'proyecto_id' => 'required',
            'actividad' => 'required',
            'observacion' => 'nullable',
            'fecha_inicio' => 'required',
            'fecha_final' => 'required',

        ];
    }

    public function rules_editar(){
        return [
          
            'id' => 'required',
            'proyecto_id' => 'required',
            'actividad' => 'required',
            'observacion' => 'nullable',
            'fecha_inicio' => 'required',
            'fecha_final' => 'required',
            'orden' => 'required',

        ];
    }
}
