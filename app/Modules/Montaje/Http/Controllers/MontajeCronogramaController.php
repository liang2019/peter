<?php

namespace App\Modules\Montaje\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Montaje\Http\Requests\MontajeCronogramaRequest as MainRequest;
use App\Modules\Montaje\Services\MontajeCronogramaService as MainService;
use App\Http\Controllers\Controller;

class MontajeCronogramaController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargar(Request $request)
    {
        $data = $this->service->cargar($request);
        return $data;
    }

    public function guardar(MainRequest $request)
    {
        $form = $request->validated();
        $save = $this->service->guardar($form);
        return $save;
    }

    public function buscar(Request $request)
    {
        $data = $this->service->buscar($request);
        return $data;
    }

    public function editar(MainRequest $request)
    {
        $form = $request->validated();
        $data = $this->service->editar($form);
        return $data;
    }

    public function eliminar(Request $request)
    {
        $data = $this->service->eliminar($request);
        return $data;
    }

    public function completar(Request $request)
    {
        $data = $this->service->completar($request);
        return $data;
    }

}
