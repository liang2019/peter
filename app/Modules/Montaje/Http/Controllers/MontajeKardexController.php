<?php

namespace App\Modules\Montaje\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Montaje\Http\Requests\MontajeKardexRequest as MainRequest;
use App\Modules\Montaje\Services\MontajeKardexService as MainService;

class MontajeKardexController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargar(Request $request)
    {
        $data = $this->service->cargar($request);
        return $data;
    }

    public function guardar(MainRequest $request)
    {
        $form = $request->validated();
        $save = $this->service->guardar($form);
        return $save;
    }

    public function eliminar(Request $request)
    {
        $data = $this->service->eliminar($request);
        return $data;
    }

    public function kardex(Request $request)
    {
        $data = $this->service->kardex($request);
        return $data;
    }

    public function descontar(Request $request)
    {
        $data = $this->service->descontar($request);
        return $data;
    }
}
