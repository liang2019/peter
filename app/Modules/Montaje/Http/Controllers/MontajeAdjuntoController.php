<?php

namespace App\Modules\Montaje\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Montaje\Http\Requests\MontajeDocumentariaRequest as MainRequest;
use App\Modules\Montaje\Services\MontajeAdjuntoService as MainService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
class MontajeAdjuntoController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarAll(Request $request)
    {
        $data = $this->service->cargarAll($request);
        return $data;
    }

    public function guardar(MainRequest $request)
    {
        $form = $request->validated();
        $save = $this->service->guardar($form);
        return $save;
    }

    public function buscar(Request $request)
    {
        $data = $this->service->buscar($request);
        return $data;
    }


    public function editar(MainRequest $request)
    {
        $form = $request->validated();
        $data = $this->service->editar($form);
        return $data;

    }

    public function updatefile(MainRequest $request)
    {
        $save = $this->service->updatefile($request);
        return $save;
    }

    public function eliminar(Request $request)
    {
        $data = $this->service->eliminar($request);
        return $data;
    }

    public function download(Request $request)
    {
        $datos = $this->service->download($request->route('id'));
        return Storage::download(config("app.upload_paths.montaje.adjuntos")."/".$datos->id."-".$datos->adjunto);
    }

    public function completar(Request $request)
    {
        $data = $this->service->completar($request);
        return $data;
    }
}
