<?php

namespace App\Modules\Montaje\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Montaje\Services\MontajeAvanceService as MainService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
class MontajeAvanceController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargar(Request $request)
    {
        $data = $this->service->cargar($request);
        return $data;
    }

    public function verAdjuntos(Request $request)
    {
        $data = $this->service->verAdjuntos($request);
        return $data;
    }

    public function verIncidencias(Request $request)
    {
        $data = $this->service->verIncidencias($request);
        return $data;
    }

    public function verGeolocalización(Request $request)
    {
        $data = $this->service->verGeolocalización($request);
        return $data;
    }

    public function verRecorrido(Request $request)
    {
        $data = $this->service->verRecorrido($request);
        return $data;
    }
    public function download(Request $request)
    {
        $datos = $this->service->download($request->route('id'));
        return Storage::download(config("app.upload_paths.supervision.adjuntos")."/".$datos->supervision_id."-".$datos->adjunto);
    }
}
