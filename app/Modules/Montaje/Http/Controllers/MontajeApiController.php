<?php

namespace App\Modules\Montaje\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Montaje\Services\MontajeApiService as MainService;

class MontajeApiController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function cargarProyectos()
    {
        $data = $this->service->cargarProyectos();
        return $data;
    }

    public function cargarEstados()
    {
        $data = $this->service->cargarEstados();
        return $data;
    }

    public function cargarHitos($data)
    {
        $data = $this->service->cargarHitos($data);
        return $data;
    }

    public function cargarIncidencias()
    {
        $data = $this->service->cargarIncidencias();
        return $data;
    }

    public function guardarSupervision(Request $request)
    {
        $save = $this->service->guardarSupervision($request);
        return $save;
    }
    public function supervisionGuardarAdjuntos(Request $request)
    {
        $save = $this->service->supervisionGuardarAdjuntos($request);
        return $save;
    }
    public function verActividades(Request $request)
    {
        $save = $this->service->verActividades($request);
        return $save;
    }

}
