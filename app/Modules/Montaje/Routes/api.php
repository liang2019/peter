<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::group(['middleware' => 'auth:api'],function () {
    Route::group(['prefix' => 'montaje'], function () {
        Route::get('/proyectos/ver/', ['as' => 'montaje.cargarProyectos', 'uses' => 'MontajeApiController@cargarProyectos']);
        Route::get('/estados/ver/', ['as' => 'montaje.cargarEstados', 'uses' => 'MontajeApiController@cargarEstados']);
        Route::get('/incidencias/ver/', ['as' => 'montaje.cargarIncidencias', 'uses' => 'MontajeApiController@cargarIncidencias']);
        Route::get('/hitos/ver/{id}', ['as' => 'montaje.cargarHitos', 'uses' => 'MontajeApiController@cargarHitos']);
        Route::post('/supervision/guardar/', ['as' => 'montaje.guardarSupervision', 'uses' => 'MontajeApiController@guardarSupervision']);
        Route::post('/supervision/actividades/', ['as' => 'montaje.verActividades', 'uses' => 'MontajeApiController@verActividades']);
        Route::post('/supervision/guardarAdjuntos/', ['as' => 'montaje.guardarSupervision', 'uses' => 'MontajeApiController@supervisionGuardarAdjuntos']);
    });
//});


