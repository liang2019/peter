<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'auth'],function () {
    Route::group(['prefix' => 'montaje'], function () {

        Route::get('/proyectos', ['as' => 'montaje.proyectos', 'uses' => 'MontajeProyectoController@cargarAll']);

        Route::get('/cronograma', ['as' => 'montaje.cronograma', 'uses' => 'MontajeCronogramaController@index']);
        Route::get('/cronograma/actividades/{id}', ['as' => 'cronograma', 'uses' => 'MontajeCronogramaController@index']);
        Route::get('/cronograma/{id}', ['as' => 'cronograma', 'uses' => 'MontajeCronogramaController@index']);
        Route::post('/cronograma/cargar', ['as' => 'cronograma.cargar', 'uses' => 'MontajeCronogramaController@cargar']);
        Route::post('/cronograma/guardar', ['as' => 'cronograma.guardar', 'uses' => 'MontajeCronogramaController@guardar']);
        Route::post('/cronograma/buscar', ['as' => 'cronograma.buscar', 'uses' => 'MontajeCronogramaController@buscar']);
        Route::patch('/cronograma/editar', ['as' => 'cronograma.editar', 'uses' => 'MontajeCronogramaController@editar']);
        Route::post('/cronograma/eliminar', ['as' => 'cronograma.eliminar', 'uses' => 'MontajeCronogramaController@eliminar']);
        Route::post('/cronograma/completar', ['as' => 'cronograma.completar', 'uses' => 'MontajeCronogramaController@completar']);

        Route::get('/pesos', ['as' => 'pesos.proyectos', 'uses' => 'MontajePesosController@index']);
        Route::get('/pesos/registrar/{id}', ['as' => 'pesos.registrar', 'uses' => 'MontajePesosController@index']);
        Route::get('/pesos/{id}', ['as' => 'pesos', 'uses' => 'MontajePesosController@index']);
        Route::post('/pesos/cargar', ['as' => 'pesos.cargar', 'uses' => 'MontajePesosController@cargar']);
        Route::post('/pesos/materiales', ['as' => 'pesos.materiales', 'uses' => 'MontajePesosController@cargarMateriales']);
        Route::post('/pesos/guardar', ['as' => 'pesos.guardar', 'uses' => 'MontajePesosController@guardar']);
        Route::post('/pesos/buscar', ['as' => 'pesos.buscar', 'uses' => 'MontajePesosController@buscar']);
        Route::patch('/pesos/editar', ['as' => 'pesos.editar', 'uses' => 'MontajePesosController@editar']);
        Route::post('/pesos/eliminar', ['as' => 'pesos.eliminar', 'uses' => 'MontajePesosController@eliminar']);
        Route::post('/pesos/grafico', ['as' => 'pesos.grafico', 'uses' => 'MontajePesosController@grafico']);

        Route::get('/kardex', ['as' => 'kardex.proyectos', 'uses' => 'MontajeKardexController@index']);
        Route::get('/kardex/registro/{id}', ['as' => 'kardex', 'uses' => 'MontajeKardexController@index']);
        Route::post('/kardex/cargar', ['as' => 'kardex.cargar', 'uses' => 'MontajeKardexController@cargar']);
        Route::post('/kardex/guardar', ['as' => 'kardex.guardar', 'uses' => 'MontajeKardexController@guardar']);
        Route::post('/kardex/eliminar', ['as' => 'kardex.eliminar', 'uses' => 'MontajeKardexController@eliminar']);
        Route::post('/kardex/descontar', ['as' => 'kardex.descontar', 'uses' => 'MontajeKardexController@descontar']);
        Route::post('/kardex/reporte', ['as' => 'kardex.kardex', 'uses' => 'MontajeKardexController@kardex']);
        Route::get('/kardex/reporte/{id}', ['as' => 'kardex', 'uses' => 'MontajeKardexController@index']);

        Route::get('/avance', ['as' => 'avance.proyectos', 'uses' => 'MontajeAvanceController@index']);
        Route::post('/avance/cargar', ['as' => 'avance.cargar', 'uses' => 'MontajeAvanceController@cargar']);
        Route::get('/avance/reporte/{id}', ['as' => 'avance', 'uses' => 'MontajeAvanceController@index']);
        Route::get('/avance/mapa/{id}', ['as' => 'avance', 'uses' => 'MontajeAvanceController@index']);
        Route::post('/avance/adjuntos', ['as' => 'avance.verAdjuntos', 'uses' => 'MontajeAvanceController@verAdjuntos']);
        Route::post('/avance/incidencias', ['as' => 'avance.verIncidencias', 'uses' => 'MontajeAvanceController@verIncidencias']);
        Route::post('/avance/geolocalizacion', ['as' => 'avance.verGeolocalización', 'uses' => 'MontajeAvanceController@verGeolocalización']);
        Route::post('/avance/recorrido', ['as' => 'avance.verRecorrido', 'uses' => 'MontajeAvanceController@verRecorrido']);
        Route::get('/avance/download/{id}', ['as' => 'avance.download', 'uses' => 'MontajeAvanceController@download']);

        Route::get('/documentaria', ['as' => 'documentaria', 'uses' => 'MontajeAdjuntoController@index']);
        Route::get('/documentaria/{id}', ['as' => 'documentaria.id', 'uses' => 'MontajeAdjuntoController@index']);
        Route::post('/documentaria/cargar', ['as' => 'documentaria.cargar', 'uses' => 'MontajeAdjuntoController@cargarAll']);
        Route::post('/documentaria/buscar/', ['as' => 'documentaria.buscar', 'uses' => 'MontajeAdjuntoController@buscar']);
        Route::patch('/documentaria/editar/', ['as' => 'documentaria.editar', 'uses' => 'MontajeAdjuntoController@editar']);
        Route::post('/documentaria/eliminar/', ['as' => 'documentaria.eliminar', 'uses' => 'MontajeAdjuntoController@eliminar']);
        Route::post('/documentaria/guardar/', ['as' => 'documentaria.guardar', 'uses' => 'MontajeAdjuntoController@guardar']);
        Route::post('/documentaria/updatefile/', ['as' => 'documentaria.updatefile', 'uses' => 'MontajeAdjuntoController@updatefile']);
        Route::get('/documentaria/download/{id}', ['as' => 'documentaria.download', 'uses' => 'MontajeAdjuntoController@download']);

        Route::get('/tareo', ['as' => 'tareo', 'uses' => 'MontajeTareoController@index']);
        Route::get('/tareo/actividades/{id}', ['as' => 'tareo.actividades', 'uses' => 'MontajeTareoController@index']);
        Route::get('/tareo/reporte/{id}', ['as' => 'tareo.reporte', 'uses' => 'MontajeTareoController@index']);
        Route::post('/tareo/cargar', ['as' => 'tareo.cargar', 'uses' => 'MontajeTareoController@cargar']);
        Route::post('/tareo/empleados', ['as' => 'tareo.empleados', 'uses' => 'MontajeTareoController@empleados']);
        Route::post('/tareo/reporte', ['as' => 'tareo.reporte', 'uses' => 'MontajeTareoController@reporte']);
        Route::get('/tareo/exportar/{id}', ['as' => 'tareo.exportar', 'uses' => 'MontajeTareoController@exportar']);

    });
});