<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'auth'],function () {
    Route::group(['prefix' => 'reportes'], function () {

        Route::get('/materiales', ['as' => 'reports', 'uses' => 'ReportesMaterialesController@index']);
        
        Route::get('/materiales/cargar', ['as' => 'reportes.materiales', 'uses' => 'ReportesMaterialesController@cargarReporteMateriales']);
        Route::get('/materiales/exportar', ['as' => 'reportes.exportar', 'uses' => 'ReportesMaterialesController@exportar']);

        Route::get('/general/proyectos', ['as' => 'reports', 'uses' => 'ReporteGeneralController@index']);
        Route::get('/general/cargar/proyectos', ['as' => 'reportes.proyectos', 'uses' => 'ReporteGeneralController@cargarProyectos']);
        Route::get('/general/{id}', ['as' => 'reporte', 'uses' => 'ReporteGeneralController@index']);
        Route::post('/general/reporte', ['as' => 'reports', 'uses' => 'ReporteGeneralController@reporteGeneral']);
    });
});
