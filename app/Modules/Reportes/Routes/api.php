<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/reportes', function (Request $request) {
    // return $request->zatana();
})->middleware('auth:api');


//Route::get('/contact/store', ['as' => 'contact.store', 'uses' => 'ContactController@index']);
//Route::post('/api/reportes', ['as' => 'reportes.exportar', 'uses' => 'ReportesController@exportar']);
//Route::get('reportes/materiales/exportar', ['as' => 'reports.export', 'uses' => 'ReportesController@exportar']);
