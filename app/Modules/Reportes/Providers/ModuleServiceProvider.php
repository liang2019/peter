<?php

namespace App\Modules\Reportes\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('reportes', 'Resources/Lang', 'app'), 'reportes');
        $this->loadViewsFrom(module_path('reportes', 'Resources/Views', 'app'), 'reportes');
        $this->loadMigrationsFrom(module_path('reportes', 'Database/Migrations', 'app'), 'reportes');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('reportes', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('reportes', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
