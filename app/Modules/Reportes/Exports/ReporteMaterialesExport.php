<?php

namespace App\Modules\Reportes\Exports;


use App\Modules\Reportes\Services\ReporteMaterialService as MainService;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

use Illuminate\Support\Facades\DB;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class ReporteMaterialesExport implements   ShouldAutoSize,WithEvents,FromView
{
    private $countReg;

    public function __construct()
    {
        $this->service = new MainService();
    }

    public function collection()
    {
        ini_set('memory_limit','350M');
        ini_set('max_execution_time', 680);
        $data = $this->service->cargarReporteMateriales();
        return $data;
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $border =
                    array(
                        'outline' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => 'FFFF0000'],
                        ),
                    );
                    $cellRange = 'B3:X3'; // All headers
                    $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(13);

                $event->sheet->getStyle('B3:X3')->applyFromArray(
                    array(
                        'borders'=>$border
                    )
                );
                $event->sheet->getStyle('B3:X'.(3+$this->countReg))->applyFromArray(
                    array(
                        'borders'=>$border
                    )
                );

            }
        ];
    }
    public function view(): View
    {
        ini_set('memory_limit','350M');
        ini_set('max_execution_time', 680);
        $data = $this->service->cargarReporteMateriales();
        $this->countReg = count($data);

      /*  
        $sheet->getStyle('A1')->applyFromArray(array(
            'fill' => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FF0000')
            )
        ));*/

        return view('layouts.reportes.materiales', [
            'arr' => $data,
        ]);
    }
   /* public function headings(): array
    {
        return [    "N°",
                    "CÓDIGO", 
                    "DESCRIPCIÓN", 
                    "FAMILIA", 
                    "UNIDAD", 
                    "GRUPO", 
                    "CUENTA", 
                    "PESO", 
                    "TIPO",
                    "COD MONEDA", 
                    "COD MONEDA C", 
                    "PRECOM", 
                    "FECHA",
                    "FSTOCK",
                    "ESTADO",
                    "FECHA VENCIMIENTO",
                    "MON COS",
                    "PRE COS",
                    "FECHA COS",
                    "TIPO EXI",
                    "STOCK",
                    "PRECIO UNIT",
                    "FECHA DOC 2"
                 
                ];
    }*/
}
