<?php

namespace App\Modules\Reportes\Services;

use App\Modules\Importaciones\Models\Alarti;
use App\Modules\Importaciones\Models\Almovd;
use App\Modules\Importaciones\Models\Alstoc;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class ReporteMaterialService
{


    public function __construct()
    {
        $this->model = new Alarti();

    }

    public function cargarReporteMateriales(){

        $datos = $this->model->where('alarti.estado',1)
        ->leftJoin('alstoc', 'alarti.codigo', '=', 'alstoc.codigo')
        ->leftJoin('almovd', 'alarti.codigo', '=', 'almovd.codigo')
        ->select(
        'alarti.id',
        'alarti.codigo as acodigo',
        'alarti.descripcion as adescripcion',
        'alarti.familia as afamilia',
        'alarti.unidad as aunidad',
        'alarti.grupo as agrupo',
        'alarti.cuenta as acuenta',
        'alarti.peso as apeso',
        'alarti.tipo as atipo',
        'alarti.cod_mon as acodmon',
        'alarti.cod_monC as acodmonc',
        'alarti.pre_com as aprecom',
        'alarti.fecha as afecha',
        'alarti.f_stock as afstock',
        'alarti.estado_import as aestado',
        'alarti.fecha_vencimiento as afecven',
        'alarti.mon_cos as amoncos',
        'alarti.pre_cos as aprecos',
        'alarti.fec_cos as afeccos',
        'alarti.tip_exi as atipexi',
        DB::raw('IFNULL(alstoc.skdis,0) as alskdis'),
        'almovd.precio_unit as c6preuni',
        'almovd.fecdoc2 as c6fecdoc2'
        )
        ->get();
        return $datos;
    }



}
