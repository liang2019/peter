<?php

namespace App\Modules\Reportes\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Reportes\Services\ReporteMaterialService as MainService;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\Reportes\Exports\ReporteMaterialesExport;


class ReportesMaterialesController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }

    public function cargarReporteMateriales()
    {
        $data = $this->service->cargarReporteMateriales();
        return $data;
    }

    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function exportar() 
    {
    
        return Excel::download(new ReporteMaterialesExport, 'Materiales.xlsx');
    }
}
