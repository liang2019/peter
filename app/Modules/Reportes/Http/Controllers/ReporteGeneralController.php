<?php

namespace App\Modules\Reportes\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Reportes\Services\ReporteGeneralService as MainService;
use App\Http\Controllers\Controller;

class ReporteGeneralController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }


    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarProyectos()
    {
        $data = $this->service->cargarProyectos();
        return $data;
    }

    public function reporteGeneral(Request $request)
    {
        $data = $this->service->reporteGeneral($request);
        return $data;
    }

}
