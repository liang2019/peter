<?php

namespace App\Modules\Importaciones\Services;

use App\Modules\Importaciones\Models\Cliente;
use org\majkel\dbase\Table;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
class ClienteService
{

    public function __construct()
    {
        $this->model = new Cliente();

    }

    public function cargarAll(){

        $datos = $this->model
        ->select('*')
        ->where('estado',1)
        ->orderBy('descripcion', 'ASC')
        ->get();
        return $datos;
    }

    public function importar_clientes($request){
        $input = $request->all();
        $adjunto_file = $input["adjunto_file"];
        unset($input["adjunto_file"]);
        $nombre = auth()->user()["id"]."clientes.dbf";
    /** borramos el archivo */
    $exists = Storage::disk('local')->exists(config('app.upload_paths.importaciones.adjuntos').'/'.$nombre);
    if ($exists) {
        $delete = Storage::disk('local')->delete(config('app.upload_paths.importaciones.adjuntos').'/'.$nombre);
    }
    /** borramos el archivo */

    if(  isset($input["adjunto"]) && $input["adjunto"]!='null' ){
        $path = Storage::putFileAs(
            config('app.upload_paths.importaciones.adjuntos'), $adjunto_file, $nombre
        );
        if(!$path){
            return 0;
        }
    }
    $filename=base_path().'/storage/app/'.config('app.upload_paths.importaciones.adjuntos').'/'.$nombre;

        //$filename=base_path().'/clientes/CANE2_Cliente_provedores.dbf';
        $totalSum = 0;

        $dbf = Table::fromFile($filename);
        $i=0;
        /*AVANEXO	ACODANE	ADESANE	AREFANE	ARUC	ACODMON	AESTADO	ADATE	AHORA*/
        $valuesClient = array();
        $idsArr=array();
        foreach ($dbf as $record) {
            if ($record->AVANEXO=="C") { //cliente
                $record->ACODANE = trim($record->ACODANE);
                $record->AVANEXO = trim($record->AVANEXO);
                $record->ADESANE = trim($record->ADESANE);
                $record->AREFANE = trim($record->AREFANE);
                $record->ARUC = trim($record->ARUC);
                $record->ACODMON = trim($record->ACODMON);
                $record->AESTADO = trim($record->AESTADO);
                $valuesClient = array(

                    "codigo"=>utf8_encode($record->ACODANE),
                    "anexo"=>utf8_encode($record->AVANEXO),
                    "descripcion"=>utf8_encode($record->ADESANE),
                    "referencia"=>utf8_encode($record->AREFANE),
                    "ruc"=>$record->ARUC,
                    "codmon"=>$record->ACODMON,
                    "estadocliente"=>$record->AESTADO,
                    'usuario_creacion' => auth()->user()["id"],
                    'usuario_edicion' => auth()->user()["id"],
                    'fecha_creacion' => now()->format('Y-m-d H:i:s'),
                    'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                );
                $this->model::updateOrInsert(["codigo"=>utf8_encode($record->ACODANE)],$valuesClient);
            }
        }
        return json_encode(array("response"=>true));
    }


}
