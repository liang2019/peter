<?php

namespace App\Modules\Importaciones\Services;

use App\Modules\Importaciones\Models\Alarti;
use org\majkel\dbase\Table;

class MaterialService
{

    public function __construct()
    {
        $this->model = new Alarti();

    }

    public function cargarMaterialesOnly(){

        $datos = $this->model
        ->select('*')
        ->where('estado',1)
        ->where('grupo','MAP')
        ->orderBy('descripcion', 'ASC')
        ->get();
        return $datos;
    }

    public function cargarConsumibles(){

        $datos = $this->model
        ->select('*')
        ->where('estado',1)
        ->where('grupo','<>','MAP')
        ->orderBy('descripcion', 'ASC')
        ->get();
        return $datos;
    }




}
