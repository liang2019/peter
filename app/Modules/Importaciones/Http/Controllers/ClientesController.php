<?php

namespace App\Modules\Importaciones\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Importaciones\Services\ClienteService as MainService;

class ClientesController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));

    }
    
    public function cargarAll()
    {
        $data = $this->service->cargarAll();
        return $data;
    }

    public function importar_clientes(Request $request)
    {
        $data = $this->service->importar_clientes($request);
        return $data;
    }
}
