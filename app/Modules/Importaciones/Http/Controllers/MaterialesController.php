<?php

namespace App\Modules\Importaciones\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Importaciones\Models\Alarti;
use App\Modules\Importaciones\Models\Almovd;
use App\Modules\Importaciones\Models\Alstoc;
use App\Modules\Importaciones\Services\MaterialService as MainService;
use org\majkel\dbase\Table;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
class MaterialesController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));

    }
    public function alartiImportArray($record){
        $array = array(
            "codigo"=>utf8_encode($record->ACODIGO),
            "codigo2"=>utf8_encode($record->ACODIGO2),
            "descripcion"=>utf8_encode($record->ADESCRI),
            "familia"=>utf8_encode($record->AFAMILIA),
            "modelo"=>$record->AMODELO,
            "unidad"=>utf8_encode($record->AUNIDAD),
            "grupo"=>$record->AGRUPO,
            "cuenta"=>$record->ACUENTA,
            "f_talla"=>$record->AFTALLA,
            "f_deci"=>$record->AFDECI,
            "uni_art"=>$record->AUNIART,
            "f_serie"=>$record->AFSERIE,
            "peso"=>$record->APESO,
            "tipo"=>$record->ATIPO,
            "cod_mon"=>$record->ACODMON,
            "precio_1"=>$record->APRECIO1,
            "precio_2"=>$record->APRECIO2,
            "precio_3"=>$record->APRECIO3,
            "precio_4"=>$record->APRECIO4,
            "descto"=>$record->ADESCTO,
            "isc_por"=>$record->AISCPOR,
            "igv_por"=>$record->AIGVPOR,
            "cod_monC"=>$record->ACODMONC,
            "pre_com"=>$record->APRECOM,
            "cod_pro"=>$record->ACODPRO,
            "fecha"=>$record->AFECHA,
            "hora"=>$record->AHORA,
            "casillero"=>$record->ACASILLERO,
            "f_stock"=>$record->AFSTOCK,
            "f_pre_lib"=>$record->AFPRELIB,
            "f_resta"=>$record->AFRESTA,
            "user"=>$record->AUSER,
            "estado_import"=>$record->AESTADO,
            "f_uni_ref"=>$record->AFUNIREF,
            "uni_ref"=>$record->AUNIREF,
            "fac_ref"=>$record->AFACREF,
            "p_dis"=>$record->APDIS,
            "p_com"=>$record->APCOM,
            "fecha_vencimiento"=>$record->AFECVEN,
            "parara"=>$record->APARARA,
            "cla_art"=>$record->ACLAART,
            "mon_fob"=>$record->AMONFOB,
            "pre_fob"=>$record->APREFOB,
            "com_imp"=>$record->ACOMIMP,
            "com_tip"=>$record->ACOMTIP,
            "pro_com"=>$record->APROCOM,
            "precio_5"=>$record->APRECIO5,
            "inf_tec"=>$record->AINFTEC,
            "control"=>$record->ACONTROL,
            "catalog"=>$record->ACATALOG,
            "frotab"=>$record->AFROTAB,
            "mon_cos"=>$record->AMONCOS,"pre_cos"=>$record->APRECOS,"fec_cos"=>$record->AFECCOS,
            "margen_1"=>$record->AMARGEN1,
            "margen_2"=>$record->AMARGEN2,
            "descto_2"=>$record->ADESCTO2,
            "fecha_i"=>$record->AFECHAI,
            "fecha_f"=>$record->AFECHAF,
            "tipo_i"=>$record->ATIPOI,
            "clase"=>$record->ACLASE,
            "categoria"=>$record->ACATEGORIA,
            "pre_imp"=>$record->APREIMP,
            "ancho"=>$record->AANCHO,
            "largo"=>$record->ALARGO,
            "factor"=>$record->AFACTOR,
            "volumen"=>$record->AVOLUMEN,
            "area"=>$record->AAREA,
            "p_mino"=>$record->APMINO,
            "p_proo"=>$record->APPROO,
            "p_maxo"=>$record->APMAXO,
            "poro"=>$record->APORO,
            "p_plata"=>$record->APPLATA,
            "p_bronce"=>$record->APBRONCE,
            "p_otros"=>$record->APOTROS,
            "uni_pre"=>$record->AUNIPRE,
            "cataran"=>$record->ACATARAN,
            "marca"=>$record->AMARCA,
            "ano_fab"=>$record->AANOFAB,
            "lugori"=>$record->ALUGORI,
            "tip_exi"=>$record->ATIPEXI,
            "cuentr"=>$record->ACUENTR
        );
        return $array;
    }
    public function almovdImportArray($record){
        $array = array(
            "alma"=>utf8_encode($record->C6ALMA),
            "td"=>utf8_encode($record->C6TD),
            "numdoc"=>utf8_encode($record->C6NUMDOC),
            "item"=>utf8_encode($record->C6ITEM),
            "codigo"=>$record->C6CODIGO,
            "cantidad"=>utf8_encode($record->C6CANTID),
            "precio_unit"=>$record->C6PREUNI,
            "serie"=>$record->C6SERIE,
            "situa"=>$record->C6SITUA,
            "fec_doc"=>$record->C6FECDOC,
            "cencos"=>$record->C6CENCOS,
            "rfalma"=>$record->C6RFALMA,
            "talla"=>$record->C6TALLA,
            "estado_movd"=>$record->C6ESTADO,
            'codmov'=>$record->C6CODMOV,
            "orden"=>$record->C6ORDEN,
            "valtot"=>$record->C6VALTOT,
            "subdia"=>$record->C6SUBDIA,
            "compro"=>$record->C6COMPRO,
            "codmon"=>$record->C6CODMON,
            "tipo"=>$record->C6TIPO,
            "tipcam"=>$record->C6TIPCAM,
            "cantent"=>$record->C6CANTENT,
            "canfac"=>$record->C6CANFAC,
            "prevta"=>$record->C6PREVTA,
            "monvta"=>$record->C6MONVTA,
            "unixenv"=>$record->C6UNIXENV,
            "numeenv"=>$record->C6NUMEENV,
            "fecven"=>$record->C6FECVEN,
            "devol"=>$record->C6DEVOL,
            "soli"=>$record->C6SOLI,
            "precio"=>$record->C6PRECIO,
            "preci1"=>$record->C6PRECI1,
            "descto"=>$record->C6DESCTO,
            "igv"=>$record->C6IGV,
            "impmn"=>$record->C6IMPMN,
            "impus"=>$record->C6IMPUS,
            "descripcion"=>utf8_encode($record->C6DESCRI),
            "pordes"=>$record->C6PORDES,
            "fecdoc2"=>$record->C6FECDOC2,
            "fecent2"=>$record->C6FECENT2,
            "fecven2"=>$record->C6FECVEN2,
            "peso"=>$record->C6PESO,
            "tr"=>$record->C6TR,
            "prsigv"=>$record->C6PRSIGV,
            "tipane"=>$record->C6TIPANE,
            "codane"=>$record->C6CODANE,
            "stock"=>$record->C6STOCK,
            "itemaux"=>$record->C6ITEMAUX,
            "locali"=>$record->C6LOCALI,
            "premn"=>$record->C6PREMN,
            "preus"=>$record->C6PREUS,
            "valmn"=>$record->C6VALMN,
            "valus"=>$record->C6VALUS
        );
        return $array;
    }
    public function alstocImportArray($record){
        $array = array(
            "alma"=>utf8_encode($record->ALALMA),
            "codigo"=>utf8_encode($record->ALCODIGO),
            "skdis"=>$record->ALSKDIS,
            "skcom"=>utf8_encode($record->ALSKCOM),
            "skmin"=>$record->ALSKMIN,
            "skmax"=>utf8_encode($record->ALSKMAX),
            "mespro"=>$record->ALMESPRO,
            "preuni"=>$record->ALPREUNI,
            "fecmov"=>$record->ALFECMOV,
            "skmes"=>$record->ALSKMES,
            "skval"=>$record->ALSKVAL,
            "punrep"=>$record->ALPUNREP,
            "semrep"=>$record->ALSEMREP,
            "tiprep"=>$record->ALTIPREP,
            'ubialm'=>$record->ALUBIALM,
            "lotcom"=>$record->ALLOTCOM,
            "tipcom"=>$record->ALTIPCOM
        );
        return $array;
    }
    public function importar_materiales(Request $request){
        //$filename = '/c/xampp/htdocs/emer_files/ALARTI.DBF';
        $input = $request->all();
        if (isset($input["tipo_material"])) {
            $tipo_material=$input["tipo_material"];
            $adjunto_file = $input["adjunto_file"];
            unset($input["adjunto_file"]);
            switch ($tipo_material) {
                case 'alarti':
                    $nombre = auth()->user()["id"]."alarti.dbf";
                    break;
                case 'almovd':
                    $nombre = auth()->user()["id"]."almovd.dbf";
                    break;
                case 'alstoc':
                    $nombre = auth()->user()["id"]."alstoc.dbf";
                    break;
            }
            /** borramos el archivo */
            $exists = Storage::disk('local')->exists(config('app.upload_paths.importaciones.adjuntos').'/'.$nombre);
            if ($exists) {
                $delete = Storage::disk('local')->delete(config('app.upload_paths.importaciones.adjuntos').'/'.$nombre);
            }
            /** borramos el archivo */

            if(  isset($input["adjunto"]) && $input["adjunto"]!='null' ){
                $path = Storage::putFileAs(
                    config('app.upload_paths.importaciones.adjuntos'), $adjunto_file, $nombre
                );
                if(!$path){
                    return 0;
                }
            }
            $filename=base_path().'/storage/app/'.config('app.upload_paths.importaciones.adjuntos').'/'.$nombre;

            try {

                $totalSum = 0;
                // erro decimal fixed https://github.com/krzyc/dbase/commit/d8c4b5ecc436faa0ab25e4c4cf80bab977e22b4a
                //documentación https://packagist.org/packages/org.majkel/dbase
                $dbf = Table::fromFile($filename);
                
                
                $i=0;
                $valuesClient = array();
                $idsArr=array();
                ini_set('memory_limit','360M');
                ini_set('max_execution_time', 1280);
                $i=0;
                DB::beginTransaction();
    
                    if ($tipo_material=="alarti") {
                        foreach ($dbf as $record) {

                            $valuesDBF = $this->alartiImportArray($record);
                            Alarti::updateOrInsert(
                                ["codigo"=>utf8_encode($record->ACODIGO)],
                                $valuesDBF
                            );
                        }
                    }else if($tipo_material=="almovd"){
                        $newArr = array();
                        $fechaCodAnterior=array();
                        foreach ($dbf as $record) {
                            if ($record->C6TD=='PE' && $record->C6CODMOV=='CO') {
                                $valuesDBF[$record->C6CODIGO] = $this->almovdImportArray($record);
                                $fechaRow = $valuesDBF[$record->C6CODIGO]["fecdoc2"]->format('Y-m-d');
                                if (!isset($fechaCodAnterior[$record->C6CODIGO])) {
                                    $fechaCodAnterior[$record->C6CODIGO]= $record->C6FECDOC2->format('Y-m-d');
                                }
                                if (strtotime($fechaRow) >= strtotime($fechaCodAnterior[$record->C6CODIGO])) {
                                    $fechaCodAnterior[$record->C6CODIGO] = $fechaRow;
                                    $newArr[$record->C6CODIGO] = $this->almovdImportArray($record);
                                }
                            }
                        }
                        $alarti = Alarti::select('codigo')->get();
                        if (count($alarti)<=0) {
                            foreach ($newArr as $key => $value) {
                                Almovd::updateOrInsert(
                                    ["codigo"=>utf8_encode($value["codigo"])],
                                    $value
                                );
                            }
                        }else {
                            foreach ($alarti as $key => $value) {
                                Almovd::updateOrInsert(
                                    ["codigo"=>utf8_encode($value["codigo"])],
                                    isset($newArr[$value["codigo"]])?$newArr[$value["codigo"]]:["codigo"=>utf8_encode($value["codigo"])]
                                );
                            }
                        }
                    }else if($tipo_material=="alstoc"){
                        $alarti = Alarti::select('codigo')->get();
                        $newArr=array();
                        if (count($alarti)<=0) {
                            foreach ($dbf as $record) {
                                if (trim($record->ALALMA)!='') {
                                    $valuesDBF = $this->alstocImportArray($record);
                                    Alstoc::updateOrInsert(
                                        ["codigo"=>utf8_encode($record->ALCODIGO)],
                                        $valuesDBF
                                    );
                                }
                            }
                        }else {
                            foreach ($dbf as $record) {
                                if (trim($record->ALALMA)!='') {
                                    $newArr[$record->ALCODIGO] = $this->alstocImportArray($record);
                                }
                            }
                            foreach ($alarti as $value) {
                                Alstoc::updateOrInsert(
                                    ["codigo"=>utf8_encode($value["codigo"])],
                                    isset($newArr[$value["codigo"]])?$newArr[$value["codigo"]]:["codigo"=>utf8_encode($value["codigo"])]
                                );
                            }
                        }
    
                    }
                DB::commit();

                //code...
            } catch (Exception $th) {
             
                Storage::disk('local')->delete(config('app.upload_paths.importaciones.adjuntos').'/'.$nombre);
                DB::rollBack();
                //throw $th;
            }
            
            
        return json_encode(array("response"=>true));
        }
    }

    public function cargarMaterialesOnly()
    {
        $data = $this->service->cargarMaterialesOnly();
        return $data;
    }

    public function cargarConsumibles()
    {
        $data = $this->service->cargarConsumibles();
        return $data;
    }
}
