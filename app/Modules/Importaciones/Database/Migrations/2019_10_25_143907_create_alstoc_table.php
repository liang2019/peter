<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlstocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alstoc', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('alma',10)->nullable();
            $table->string('codigo',100)->unique()->nullable();
            $table->decimal('skdis',16,3)->nullable();
            $table->string('skcom',50)->nullable();
            $table->unsignedInteger('skmin')->default(0);
            $table->unsignedInteger('skmax')->default(0);
            $table->string('mespro',50)->nullable();
            $table->string('preuni',50)->nullable();
            $table->string('fecmov',50)->nullable();
            $table->string('skmes',50)->nullable();
            $table->string('skval',50)->nullable();
            $table->string('punrep',50)->nullable();
            $table->string('semrep',50)->nullable();
            $table->string('tiprep',50)->nullable();
            $table->string('ubialm',50)->nullable();
            $table->string('lotcom',50)->nullable();
            $table->string('tipcom',50)->nullable();

            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion')->nullable();
            $table->unsignedInteger('usuario_edicion')->nullable();
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->timestamp('fecha_modificacion')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alstoc');
    }
}
