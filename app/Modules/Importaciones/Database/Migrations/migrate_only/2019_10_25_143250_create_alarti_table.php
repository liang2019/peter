<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlartiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alarti', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo',200)->nullable();
            $table->string('codigo2',15)->nullable();
            $table->string('descripcion',200)->nullable();
            $table->string('familia',10)->nullable();
            $table->string('modelo',10)->nullable();
            $table->string('unidad',10)->nullable();
            $table->string('grupo',10)->nullable();
            $table->unsignedInteger('cuenta')->nullable();
            $table->string('f_talla',50)->nullable();
            $table->string('f_deci',10)->nullable();
            $table->string('uni_art',50)->nullable();
            $table->string('f_serie',10)->nullable();
            $table->decimal('peso',16,8)->nullable();
            $table->char('tipo',1)->nullable();
            $table->string('cod_mon',5)->nullable();
            $table->string('precio_1',50)->nullable();
            $table->string('precio_2',50)->nullable();
            $table->string('precio_3',50)->nullable();
            $table->string('precio_4',50)->nullable();
            $table->string('descto',50)->nullable();
            $table->string('isc_por',50)->nullable();
            $table->string('igv_por',15)->nullable();
            $table->string('cod_monC',5)->nullable();
            $table->decimal('pre_com',16,3)->nullable();
            $table->string('cod_pro',50)->nullable();
            $table->dateTime('fecha')->nullable();
            $table->string('hora',50)->nullable();
            $table->string('casillero',50)->nullable();
            $table->char('f_stock',1)->nullable();
            $table->string('f_pre_lib',10)->nullable();
            $table->string('f_resta',10)->nullable();
            $table->string('user',100)->nullable();
            $table->char('estado_import',1)->nullable();
            $table->string('f_uni_ref',10)->nullable();
            $table->string('uni_ref',10)->nullable();
            $table->string('fac_ref',50)->nullable();
            $table->string('p_dis',50)->nullable();
            $table->string('p_com',50)->nullable();
            $table->dateTime('fecha_vencimiento')->nullable();
            $table->string('parara',50)->nullable();
            $table->string('cla_art',50)->nullable();
            $table->string('mon_fob',50)->nullable();
            $table->string('pre_fob',50)->nullable();
            $table->string('com_imp',50)->nullable();
            $table->string('com_tip',50)->nullable();
            $table->string('pro_com',50)->nullable();
            $table->string('precio_5',50)->nullable();
            $table->string('inf_tec',50)->nullable();
            $table->string('control',50)->nullable();
            $table->string('catalog',50)->nullable();
            $table->string('frotab',10)->nullable();
            $table->string('mon_cos',10)->nullable();
            $table->decimal('pre_cos',16,2)->nullable();
            $table->dateTime('fec_cos')->nullable();
            $table->string('margen_1',50)->nullable();
            $table->string('margen_2',50)->nullable();
            $table->string('descto_2',50)->nullable();
            $table->string('fecha_i',50)->nullable();
            $table->string('fecha_f',50)->nullable();
            $table->string('tipo_i',50)->nullable();
            $table->string('clase',50)->nullable();
            $table->string('categoria',50)->nullable();
            $table->string('pre_imp',50)->nullable();
            $table->string('ancho',50)->nullable();
            $table->string('largo',50)->nullable();
            $table->string('factor',50)->nullable();
            $table->string('volumen',50)->nullable();
            $table->string('area',50)->nullable();
            $table->string('p_mino',50)->nullable();
            $table->string('p_proo',50)->nullable();
            $table->string('p_maxo',50)->nullable();
            $table->string('poro',50)->nullable();
            $table->string('p_plata',50)->nullable();
            $table->string('p_bronce',50)->nullable();
            $table->string('p_otros',50)->nullable();
            $table->string('uni_pre',50)->nullable();
            $table->string('cataran',50)->nullable();
            $table->string('marca',50)->nullable();
            $table->string('ano_fab',50)->nullable();
            $table->string('lugori',50)->nullable();
            $table->string('tip_exi',5)->nullable();
            $table->string('cuentr',50)->nullable();
            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion')->nullable();
            $table->unsignedInteger('usuario_edicion')->nullable();
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->timestamp('fecha_modificacion')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alarti');
    }
}
