<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlmovdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('almovd', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('alma',50)->nullable();
            $table->string('td',50)->nullable();
            $table->string('numdoc',50)->nullable();
            $table->string('item',50)->nullable();
            $table->string('codigo',100)->unique()->nullable();
            $table->unsignedInteger('cantidad')->default(0);
            $table->decimal('precio_unit',16,4)->default(0);
            $table->string('serie',50)->nullable();
            $table->string('situa',50)->nullable();
            $table->string('fec_doc',50)->nullable();
            $table->string('cencos',50)->nullable();
            $table->string('rfalma',20)->nullable();
            $table->string('talla',50)->nullable();
            $table->string('estado_movd',10)->nullable();
            $table->string('codmov',10)->nullable();
            $table->string('orden',10)->nullable();
            $table->string('valtot',50)->default(0);
            $table->string('subdia',50)->default(0);
            $table->string('compro',50)->nullable();
            $table->string('codmon',10)->nullable();
            $table->string('tipo',10)->nullable();
            $table->unsignedInteger('tipcam')->default(0);
            $table->string('cantent',20)->nullable();
            $table->string('canfac',50)->nullable();
            $table->string('prevta',50)->nullable();
            $table->string('monvta',50)->nullable();
            $table->string('unixenv',50)->nullable();
            $table->string('numeenv',50)->nullable();
            $table->string('fecven',50)->nullable();
            $table->string('devol',50)->nullable();
            $table->string('soli',20)->nullable();
            $table->string('precio',50)->nullable();
            $table->string('preci1',50)->nullable();
            $table->string('descto',50)->nullable();
            $table->string('igv',50)->nullable();
            $table->string('impmn',50)->nullable();
            $table->string('impus',50)->nullable();
            $table->string('descripcion',200)->nullable();
            $table->string('pordes',50)->nullable();
            $table->string('fecdoc2',50)->nullable();
            $table->string('fecent2',50)->nullable();
            $table->string('fecven2',50)->nullable();
            $table->string('peso',50)->nullable();
            $table->string('tr',50)->nullable();
            $table->string('prsigv',50)->nullable();
            $table->string('tipane',50)->nullable();
            $table->string('codane',50)->nullable();
            $table->string('stock',50)->nullable();
            $table->string('itemaux',50)->nullable();
            $table->string('locali',50)->nullable();
            $table->string('premn',50)->nullable();
            $table->string('preus',50)->nullable();
            $table->string('valmn',50)->nullable();
            $table->string('valus',50)->nullable();
            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion')->nullable();
            $table->unsignedInteger('usuario_edicion')->nullable();
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->timestamp('fecha_modificacion')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('almovd');
    }
}
