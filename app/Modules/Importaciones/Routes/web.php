<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['middleware' => 'auth'],function () {
    Route::group(['prefix' => 'importaciones'], function () {

        Route::get('/clientes', ['as' => 'reports', 'uses' => 'ClientesController@index']);
        Route::post('/clientes', ['as' => 'reports', 'uses' => 'ClientesController@importar_clientes']);
        Route::get('/materiales', ['as' => 'reports', 'uses' => 'MaterialesController@index']);
        Route::get('/materialesonly', ['as' => 'reports', 'uses' => 'MaterialesController@cargarMaterialesOnly']);
        Route::get('/consumibles', ['as' => 'reports', 'uses' => 'MaterialesController@cargarConsumibles']);
        Route::post('/materiales', ['as' => 'reports', 'uses' => 'MaterialesController@importar_materiales']);


        Route::get('/clientes/cargar', ['as' => 'clientes', 'uses' => 'ClientesController@cargarAll']);

    });
});
