<?php

namespace App\Modules\Importaciones\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('importaciones', 'Resources/Lang', 'app'), 'importaciones');
        $this->loadViewsFrom(module_path('importaciones', 'Resources/Views', 'app'), 'importaciones');
        $this->loadMigrationsFrom(module_path('importaciones', 'Database/Migrations', 'app'), 'importaciones');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('importaciones', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('importaciones', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
