<?php

namespace App\Modules\Importaciones\Models;

use Illuminate\Database\Eloquent\Model;

class Alarti extends Model
{
    protected $fillable = ['id','codigo','codigo2','descripcion','familia','modelo','unidad','grupo','cuenta','f_talla','f_deci','uni_art','f_serie','peso','tipo','cod_mon','precio_1','precio_2','precio_3','precio_4','descto','isc_por','igv_por','cod_monC','pre_com','cod_pro','fecha','hora','casillero','f_stock','f_pre_lib','f_resta','user','estado_import','f_uni_ref','uni_ref','fac_ref','p_dis','p_com','fecha_vencimiento','parara','cla_art','mon_fob','pre_fob','com_imp','com_tip','pro_com','precio_5','inf_tec','control','catalog','frotab','mon_cos','pre_cos','fec_cos','margen_1','margen_2','descto_2','fecha_i','fecha_f','tipo_i','clase','categoria','pre_imp','ancho','largo','factor','volumen','area','p_mino','p_proo','p_maxo','poro','p_plata','p_bronce','p_otros','uni_pre','cataran','marca','ano_fab','lugori','tip_exi','cuentr','estado','usuario_creacion','usuario_edicion','fecha_creacion','fecha_modificacion'];

    protected $table = 'alarti';
    public $timestamps = false;
}
