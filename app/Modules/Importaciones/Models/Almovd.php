<?php

namespace App\Modules\Importaciones\Models;

use Illuminate\Database\Eloquent\Model;

class Almovd extends Model
{
    protected $fillable = ['id','alma','td','numdoc','item','codigo','cantidad','precio_unit','serie','situa','fec_doc','cencos','rfalma','talla','estado_movd','codmov','orden','valtot','subdia','compro','codmon','tipo','tipcam','cantent','canfac','prevta','monvta','unixenv','numeenv','fecven','devol','soli','precio','preci1','descto','igv','impmn','impus','descripcion','pordes','fecdoc2','fecent2','fecven2','peso','tr','prsigv','tipane','codane','stock','itemaux','locali','premn','preus','valmn','valus','estado','usuario_creacion','usuario_edicion','fecha_creacion','fecha_modificacion'];

    protected $table = 'almovd';
    public $timestamps = false;
}
