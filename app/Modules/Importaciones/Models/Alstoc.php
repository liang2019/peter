<?php

namespace App\Modules\Importaciones\Models;

use Illuminate\Database\Eloquent\Model;

class Alstoc extends Model
{
    protected $fillable = ['id','alma','codigo','skdis','skcom','skmin','skmax','mespro','preuni','fecmov','skmes','skval','punrep','semrep','tiprep','ubialm','lotcom','tipcom','estado','usuario_creacion','usuario_edicion','fecha_creacion','fecha_modificacion'];
    protected $table = 'alstoc';
    public $timestamps = false;
}
