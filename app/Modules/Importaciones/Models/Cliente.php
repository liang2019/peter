<?php

namespace App\Modules\Importaciones\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //
    protected $fillable = ['codigo','anexo','descripcion','referencia','ruc','codmon','estadocliente','estado','usuario_creacion','usuario_edicion','fecha_creacion','fecha_modificacion'];
    protected $table = 'cliente';
    public $timestamps = false;
}
