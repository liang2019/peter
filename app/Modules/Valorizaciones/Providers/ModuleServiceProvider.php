<?php

namespace App\Modules\Valorizaciones\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('Valorizaciones', 'Resources/Lang', 'app'), 'Valorizaciones');
        $this->loadViewsFrom(module_path('Valorizaciones', 'Resources/Views', 'app'), 'Valorizaciones');
        $this->loadMigrationsFrom(module_path('Valorizaciones', 'Database/Migrations', 'app'));
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('Valorizaciones', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('Valorizaciones', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
