<?php

namespace App\Modules\Valorizaciones\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Presupuesto\Services\PresupuestoService as MainService;

class ValorizacioneController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }

    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarAll()
    {

        $data = $this->service->cargarAll();
        return $data;
    }
}
