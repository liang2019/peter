<?php

namespace App\Modules\Diseno\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('diseno', 'Resources/Lang', 'app'), 'diseno');
        $this->loadViewsFrom(module_path('diseno', 'Resources/Views', 'app'), 'diseno');
        $this->loadMigrationsFrom(module_path('diseno', 'Database/Migrations', 'app'), 'diseno');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('diseno', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('diseno', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
