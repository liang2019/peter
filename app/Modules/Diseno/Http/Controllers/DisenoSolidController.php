<?php

namespace App\Modules\Diseno\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Diseno\Services\DisenoSolidService as MainService;

class DisenoSolidController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarSolid(Request $request)
    {
        $data = $this->service->cargarSolid($request);
        return $data;
    }

    public function cargarItems(Request $request)
    {
        $data = $this->service->cargarItems($request);
        return $data;
    }

    public function actualizar(Request $request)
    {
        $data = $this->service->actualizar($request);
        return $data;
    }

    public function completar(Request $request)
    {
        $data = $this->service->completar($request);
        return $data;
    }

    public function importar(Request $request)
    {
        $data = $this->service->importar($request);
        return $data;
    }

    public function eliminar(Request $request)
    {
        $data = $this->service->eliminar($request);
        return $data;
    }
}
