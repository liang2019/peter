<?php

namespace App\Modules\Diseno\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Diseno\Services\DisenoProyectoService as MainService;

class DisenoProyectoController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }
    
    public function cargarAll()
    {
        $data = $this->service->cargarAll();
        return $data;
    }

    public function cancelar(Request $request)
    {
        $data = $this->service->cancelar($request);
        return $data;
    }
    public function planos(Request $request)
    {
        $data = $this->service->planos($request);
        return $data;
    }
    public function etapas(Request $request)
    {
        $data = $this->service->etapas($request);
        return $data;
    }
    public function completar(Request $request)
    {
        $data = $this->service->completar($request);
        return $data;
    }
}
