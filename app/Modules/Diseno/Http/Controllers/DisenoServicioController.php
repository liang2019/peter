<?php

namespace App\Modules\Diseno\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Diseno\Http\Requests\DisenoServicioRequest as MainRequest;
use App\Modules\Diseno\Services\DisenoServicioService as MainService;
use App\Http\Controllers\Controller;

class DisenoServicioController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }
    
    public function cargarAll(Request $request)
    {
        $data = $this->service->cargarAll($request);
        return $data;
    }

    public function guardar(MainRequest $request)
    {
        $form = $request->validated();
        $save = $this->service->guardar($form);
        return $save;
    }

    public function buscar(Request $request)
    {
        $data = $this->service->buscar($request);
        return $data;
    }

    public function editar(MainRequest $request)
    {
        $form = $request->validated();
        $data = $this->service->editar($form);
        return $data;
    }

    public function eliminar(Request $request)
    {
        $data = $this->service->eliminar($request);
        return $data;
    }

    public function actualizarPrecio(Request $request)
    {
        $data = $this->service->actualizarPrecio($request);
        return $data;
    }
}
