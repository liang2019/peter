<?php

namespace App\Modules\Diseno\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Diseno\Services\DisenoSolicitudService as MainService;

class DisenoSolicitudController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function index()
    {
        $menu =getMenu();
        return view('dashboard::admin.home',compact('menu'));
    }

    public function cargarListaSolicitudes(Request $request)
    {
        $data = $this->service->cargarListaSolicitudes($request);
        return $data;
    }

    public function cargar(Request $request)
    {
        $data = $this->service->cargar($request);
        return $data;
    }

    public function actualizar(Request $request)
    {
        $data = $this->service->actualizar($request);
        return $data;
    }

    public function completar(Request $request)
    {
        $data = $this->service->completar($request);
        return $data;
    }
}
