<?php

namespace App\Modules\Diseno\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DisenoServicioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
           
            case 'PATCH':
                $rules = $this->rules_editar();
                break;
            default:
                $rules = $this->rules_guardar();
                break;
        }
        return $rules;
    }

    public function rules_guardar(){
        return [
      
           
            'solid_id' => 'required',
            'descripcion' => 'required',
            'precio' => 'required',
            'cantidad' => 'required',

        ];
    }

    public function rules_editar(){
        return [
          
            'id' => 'required',
         
            'solid_id' => 'required',
            'descripcion' => 'required',
            'precio' => 'required',
            'cantidad' => 'required',

        ];
    }
}
