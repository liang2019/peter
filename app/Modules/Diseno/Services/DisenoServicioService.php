<?php

namespace App\Modules\Diseno\Services;

use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Diseno\Models\SolidServicios;

use Illuminate\Support\Facades\DB;
class DisenoServicioService
{


    public function __construct()
    {
        $this->model = new SolidServicios();
        $this->model_proyecto = new Proyecto();
    }

    public function cargarAll($data){

        $datos["items"] = $this->model->where('solid_servicios.solid_id', $data["solid_id"])
        ->where('solid_servicios.estado', 1)
        ->select('solid_servicios.*' )
        ->orderBy('solid_servicios.id', 'asc')
        ->get();

     
        return $datos;
    }

    public function guardar($data)
    {
        $data["usuario_creacion"] = auth()->user()["id"];
        $data["usuario_edicion"] = auth()->user()["id"];
        //crear servicio
        $save = $this->model->create($data);
        return $save;
    }

    public function buscar($data){

        $datos =  $this->model->all()->where('id', $data["id"])->first();
        return $datos;
    }

    public function editar($data){

        $data["usuario_edicion"] = auth()->user()->id;
        $data["fecha_modificacion"] = now()->format('Y-m-d H:i:s');
        $update = $this->model->find($data['id'])->update($data);
        return $update?1:$update;
    }

    public function eliminar($data){

        $update = $this->model->find($data['id'])->update([
            'estado' => 0,
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
        return $update?1:$update;
    }

    public function actualizarPrecio($data)
   {
        $update = $this->model->find($data['id'])->update(array(
            "precio" => $data["precio"],
            "verificado" => $data["verificado"],
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ));
        return $update?1:$update;
    }    

}
