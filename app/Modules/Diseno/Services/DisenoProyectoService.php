<?php

namespace App\Modules\Diseno\Services;

use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Diseno\Models\DisenoProyecto;
use App\Modules\Diseno\Models\SolidInsumo;
use App\Modules\Tracking\Models\Tracking;
use Illuminate\Support\Facades\DB;

class DisenoProyectoService
{

    public function __construct()
    {
        $this->model = new Proyecto();
        $this->model_tarcking = new Tracking();
        $this->model_diseno_proyecto = new DisenoProyecto();
        $this->model_solid_insumo = new SolidInsumo();

    }

    public function cargarAll(){

        $datos = $this->model
        ->where("tracking.diseno","<>", 3)
        ->leftJoin('cliente', 'proyecto.cliente_id', '=', 'cliente.id')
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select(   'proyecto.*', 'cliente.descripcion as nombre_cliente',
        DB::raw('"" as validacion'),
                    'tracking.diseno as estado_tracking',
                    DB::raw("IFNULL((SELECT planos from diseno_proyecto WHERE diseno_proyecto.proyecto_id = proyecto.id),0) as planos"),
                    DB::raw('(SELECT usuario from usuario WHERE usuario.id = proyecto.usuario_creacion) as user'),
                    DB::raw("(SELECT COUNT(diseno_documentaria.id) from diseno_documentaria  WHERE diseno_documentaria.proyecto_id=proyecto.id) as cantidad_adjuntos"),
                    DB::raw("IFNULL((SELECT etapas from diseno_proyecto WHERE diseno_proyecto.proyecto_id = proyecto.id),0) as etapas"),
                    DB::raw("IFNULL((SELECT count(diseno_documentaria.id) from diseno_documentaria LEFT JOIN adjunto ON  diseno_documentaria.adjunto_id = adjunto.id WHERE diseno_documentaria.proyecto_id = proyecto.id AND adjunto.codigo='planos'),0) as planos_adjuntos"),
                    DB::raw("IFNULL((SELECT count(solid.id) from solid  WHERE solid.verificado=0 AND solid.estado=1 AND solid.proyecto_id=proyecto.id),0) as verificado"),
                    DB::raw("IFNULL((SELECT count(solid.id) from solid  WHERE solid.actualizado=0 AND solid.estado=1 AND solid.proyecto_id=proyecto.id),0) as actualizado"),
                    DB::raw("IFNULL((SELECT count(solid.id) from solid  WHERE solid.estado=1 AND solid.proyecto_id=proyecto.id),0) as planos_entregados")
        )
        ->orderBy('proyecto.id', 'desc')
        ->get();

        return $datos;
    }


    public function cancelar($data){

        $update = $this->model->find($data['id'])->update([
            'estado' => 0,
            'usuario_edicion' => auth()->user()["id"],
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ]);
         //actualizar estado del tracking
        $tracking = $this->model_tarcking->where('proyecto_id', $data['id'])
        ->update([
                    'diseno' => 0,
                    'produccion' => 0,
                    'montaje' => 0,
                    'usuario_edicion' => auth()->user()->id,
                    'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                ]);
        if(!$tracking){
            return false;
        }
        return $update?1:$update;
    }

    public function planos($data){

        $exist = $this->model_diseno_proyecto->where("proyecto_id", $data['proyecto_id'])
        ->select('*')->first();

        if($exist){
            $update = $this->model_diseno_proyecto->where('proyecto_id', $data["proyecto_id"])->update([
                'planos' => $data['planos'],
                'usuario_edicion' => auth()->user()["id"],
                'fecha_modificacion' => now()->format('Y-m-d H:i:s')
            ]);
        }else{
            $update = $this->model_diseno_proyecto->create(array(
                "usuario_creacion" => auth()->user()["id"],
                "usuario_edicion" => auth()->user()["id"],
                "proyecto_id" => $data['proyecto_id'],
                "planos" => $data['planos']

            ));
        }

        return $update?1:$update;

    }

    public function etapas($data){

            $update = $this->model_diseno_proyecto->where('proyecto_id', $data["proyecto_id"])->update([
                'etapas' => $data['etapas'],
                'usuario_edicion' => auth()->user()["id"],
                'fecha_modificacion' => now()->format('Y-m-d H:i:s')
            ]);
        

        return $update?1:$update;

    }

    public function completar($data)
    {
        //completar etapa diseño
        $update = $this->model_tarcking->where('proyecto_id', $data["id"])
        ->update([
                    'diseno' => 2,
                    'diseno_fecha'=>now()->format('Y-m-d H:i:s'),
                    'usuario_edicion' => auth()->user()->id,
                    'fecha_modificacion' => now()->format('Y-m-d H:i:s')
                ]);
        return $update;
    }


}
