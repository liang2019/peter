<?php

namespace App\Modules\Diseno\Services;

use App\Modules\Diseno\Models\Solid;
use App\Modules\Diseno\Models\SolidItems;
use App\Modules\Diseno\Models\SolidInsumo;
use App\Modules\Requerimiento\Models\Proyecto;
use Illuminate\Support\Facades\DB;

class DisenoSolicitudService
{

    public function __construct()
    {
        $this->model = new Solid();
        $this->model_solid_items = new SolidItems();
        $this->model_proyecto = new Proyecto();
        $this->model_solid_insumo = new SolidInsumo();

    }

    public function cargarListaSolicitudes($data){

        $datos["items"] = $this->model
        ->where("solid.proyecto_id",$data['proyecto_id'])
        ->where("solid.actualizado",1)
        ->select('solid.*','solid.id as identificador_id',
        DB::raw("(SELECT COUNT(solid_insumo.id) from solid_insumo WHERE solid_insumo.verificado = 0 AND solid_insumo.estado = 1 AND solid_insumo.solid_id = solid.id) as items_faltantes"),
        DB::raw("(SELECT IFNULL(COUNT(solid_servicios.id), 0) from solid_servicios WHERE solid_servicios.verificado = 0 AND solid_servicios.estado = 1 AND solid_servicios.solid_id = solid.id) as servicios_faltantes")
        )
        ->orderBy('solid.id', 'desc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["proyecto_id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.diseno as estado_tracking' )->first();

        return $datos;
    }

    public function cargar($data){

        $consulta =  $this->model_solid_insumo
        ->where("solid_insumo.solid_id",$data['solid_id'])
        /*
        ->leftJoin('factor', 'solid_insumo.factor_id', '=', 'factor.id')
        ->leftJoin('alarti', 'factor.alarti_id', '=', 'alarti.id')
        */
        ->select(   'solid_insumo.id','solid_insumo.precio','solid_insumo.verificado','solid_insumo.solid_id','solid_insumo.factor_id',
                    DB::raw('"PZA" as unidad'),

                    DB::raw("(SELECT SUM(solid_items.cantidad) from solid_items WHERE solid_items.factor_id = solid_insumo.factor_id AND solid_items.solid_id=".$data['solid_id'].") as cantidad"),
                    DB::raw("(SELECT SUM(solid_items.masa) from solid_items WHERE solid_items.factor_id = solid_insumo.factor_id AND solid_items.solid_id=".$data['solid_id'].") as masa"),
                    DB::raw("(SELECT descripcion FROM alarti LEFT JOIN factor ON alarti.id=factor.alarti_id WHERE factor.id = solid_insumo.factor_id) as descripcion"),
                    DB::raw("(SELECT kilogramo_unidad from factor WHERE factor.id = solid_insumo.factor_id) as kilogramo_unidad"),
                    DB::raw("(SELECT metro_unidad from factor WHERE factor.id = solid_insumo.factor_id) as metro_unidad"),
                    DB::raw("(SELECT unidad_pieza from factor WHERE factor.id = solid_insumo.factor_id) as unidad_pieza")


        )

        ->get();


        foreach ($consulta as $key => $value) {

            //masa total = peso
            $consulta2 =  $this->model_solid_items
            ->where("solid_items.factor_id",$value['factor_id'])
            ->where("solid_items.solid_id",$value['solid_id'])
            ->select("solid_items.masa", "solid_items.cantidad")->get();

            $item_peso = 0;
            foreach ($consulta2 as $key2 => $value2) {
               $item_masa = ($value2["masa"]!='')?$value2["masa"]:0;
               $item_cantidad = ($value2["cantidad"]!='')?$value2["cantidad"]:0;
               $multiplica = $item_cantidad * $item_masa;
               $item_peso = $item_peso + $multiplica;
            }

           // $masa_total = $value["cantidad"] * $value["masa"];
            $masa_total = $item_peso;
            //longitud total
            $kilogramo_unidad = ($value["kilogramo_unidad"]==0)? 1 : $value["kilogramo_unidad"];
            $longitud_total = $masa_total / $kilogramo_unidad;
            //area total
            
            $area_total = $value["metro_unidad"] * $longitud_total;
            //cantidad
            $cantidad = $longitud_total / $value["unidad_pieza"];
            $cantidad = ceil($cantidad);
            $datos["items"][$key]["id"] = $value["id"];
            $datos["items"][$key]["cantidad"] = $cantidad;
            $datos["items"][$key]["unidad"] = $value["unidad"];
            $datos["items"][$key]["descripcion"] = $value["descripcion"];
            $datos["items"][$key]["peso"] = $masa_total;
            $datos["items"][$key]["area"] = $area_total;
            $datos["items"][$key]["precio"] = $value["precio"];
            $datos["items"][$key]["verificado"] = $value["verificado"];
            $datos["items"][$key]["solid_id"] = $value["solid_id"];


        }

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["proyecto_id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.diseno as estado_tracking' )->first();

        return $datos;


    }

    public function actualizar($data)
   {
        //esta ruta es para solicitud de abastecimiento
        $update = $this->model_solid_insumo->find($data['id'])->update(array(
            "precio" => $data["precio"],
            "verificado" => $data["verificado"],
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ));
        return $update?1:$update;
    }

    public function completar($data)
   {
        //esta ruta es para solicitud de abastecimiento
        $update = $this->model->find($data['id'])->update(array(

            "verificado" => 1,
            'usuario_edicion' => auth()->user()->id,
            'fecha_modificacion' => now()->format('Y-m-d H:i:s')
        ));
        return $update?1:$update;
    }

}
