<?php

namespace App\Modules\Diseno\Services;

use App\Modules\Diseno\Models\Solid;
use App\Modules\Diseno\Models\SolidItems;
use App\Modules\Diseno\Models\SolidInsumo;
use App\Modules\Requerimiento\Models\Proyecto;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Modules\Diseno\Imports\SolidImport;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class DisenoSolidService implements WithCalculatedFormulas
{

    public function __construct()
    {
        $this->model = new Solid();
        $this->model_solid_items = new SolidItems();
        $this->model_proyecto = new Proyecto();
        $this->model_solid_insumo = new SolidInsumo();

    }

    public function cargarSolid($data){

        $datos["items"] = $this->model
        ->where("solid.proyecto_id",$data['proyecto_id'])
        ->select('solid.*',
        DB::raw("(SELECT COUNT(solid_items.id) from solid_items WHERE solid_items.actualizado = 0 AND solid_items.solid_id = solid.id) as items_faltantes")
        )
        ->orderBy('solid.id', 'desc')
        ->get();

        $datos["proyecto"] = $this->model_proyecto->where("proyecto.id",$data["proyecto_id"])
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
        ->select('proyecto.*',
        'tracking.diseno as estado_tracking' )->first();

        return $datos;
    }

    public function cargarItems($data){

        $datos = $this->model_solid_items
        ->where("solid_id",$data['solid_id'])
        ->select('*')
        ->orderBy('id', 'desc')
        ->get();

        return $datos;
    }

    public function actualizar($data){

       //Se actualiza los solid_items que pertenecen al solid id
       DB::beginTransaction();
       try {
           $response =false;

               foreach ($data["items"] as $key => $value) {

                   //actualizar el porncentaje en la tabla insumo
                   $update = $this->model_solid_items
                   ->where("id",$value['id'])
                   ->update(array(
                        "factor_id" => $value['factor_id'],
                        "actualizado" => 1,
                        "usuario_edicion" => auth()->user()->id,
                        "fecha_modificacion" =>now()->format('Y-m-d H:i:s')
                   ));
               }


               $response=true;
               DB::commit();

       } catch (\Exception $e) {
           DB::rollback();
           throw $e;
       } catch (\Throwable $e) {
           DB::rollback();
           throw $e;
       }
       return $response?1:$response;
    }

    public function completar($data){

        
        // guardar el precio del material por cada solid
        $datos = $this->model_solid_items
        ->where("solid_items.solid_id",$data['solid_id'])
        ->leftJoin('solid', 'solid_items.solid_id', '=', 'solid.id')
        ->select(
            DB::raw('solid_items.factor_id'),
            DB::raw("(SELECT ins.precio from insumo ins WHERE ins.factor_id = solid_items.factor_id AND ins.proyecto_id=".$data['proyecto_id'].") as precio"),
            DB::raw("(SELECT almovd.precio_unit from factor LEFT JOIN  alarti ON factor.alarti_id= alarti.id LEFT JOIN almovd ON alarti.codigo = almovd.codigo WHERE factor.id = solid_items.factor_id ) as precio_alarti")
            )
        
        ->groupBy('solid_items.factor_id')
        ->orderBy('solid_items.id', 'desc')
        ->get();
        $delete = $this->model_solid_insumo
        ->where('solid_id', $data['solid_id'])->delete();
        foreach ($datos as $key => $value) {

         $precio = ($value["precio"]==null) ? $value["precio_alarti"] : $value["precio"];
         //borrar todo
       
            $save = $this->model_solid_insumo->create(array(
                "usuario_creacion" => auth()->user()["id"],
                "usuario_edicion" => auth()->user()["id"],
                "solid_id" => $data['solid_id'],
                "factor_id" => $value["factor_id"],
                "precio" => $precio

            ));
            if(!$save){
                return false;
            }
        }

         //completar el solid
        $update = $this->model
        ->where("id", $data["solid_id"])
        ->update(array(
            "actualizado" => 1,
            "usuario_edicion" => auth()->user()->id,
            "fecha_modificacion" =>now()->format('Y-m-d H:i:s')
        ));

        return $update?1:$update;
    }

    public function importar($data){

        //se
        DB::beginTransaction();
        try {
            $response =false;
            if ($data["adjunto_file"]!='null') {
                $adjunto_file = $data["adjunto_file"];
                $exp = explode(".",$data["adjunto"]);
                $nombre_file = str_replace(".".$exp[(count($exp)-1)],"",$data["adjunto"]);
            
                $path = Storage::putFileAs(
                    config('app.upload_paths.solid.adjuntos'), $adjunto_file, 'solid.xlsx'
                );
                if(!$path){
                    return 0;
                }
            }
            //buscar la última versión
            $sql = $this->model->where("proyecto_id",$data["proyecto_id"])
            ->select('*')
            ->orderBy('id', 'desc')
            ->first();
            $version= $sql["version"] + 1;
            //crear solid
            $save = $this->model->create(array(
                "usuario_creacion" => auth()->user()["id"],
                "usuario_edicion" => auth()->user()["id"],
                "proyecto_id" => $data["proyecto_id"],
                "descripcion" => $data["descripcion"],
                "nombre" => $nombre_file,
                "version" => $version

            ));

            $solidItems = Excel::toArray(new SolidImport, config('app.upload_paths.solid.adjuntos').'/solid.xlsx');
          
            $i=0;
            foreach ($solidItems[0] as $key => $value) {
                if ($i>0 && trim($value[0])!="") {
                    SolidItems::firstOrCreate(
                        ["nombre"=>$value[0],'solid_id'=>floatval($save->id)],
                        [ 'nombre' => $value[0],
                        'cantidad' =>$value[1],
                        'masa' =>floatval($value[2]),
                        'solid_id' => floatval($save->id),
                        'usuario_creacion' =>auth()->user()->id,
                        'usuario_edicion' =>auth()->user()->id
              
                        ]
                    );

                }

                $i++;
            }
            
         
            //borra el archivo
            $delete = Storage::disk('local')->delete(config('app.upload_paths.solid.adjuntos').'/solid.xlsx');

            $response=true;
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }


        return $delete?1:$delete;
    }

    public function eliminar($data){

    
        $delete = $this->model
        ->where('id', $data['id'])->delete();
        return $delete?1:$delete;
    }

}
