<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolidItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solid_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('solid_id')->unsigned();
            $table->foreign('solid_id')->references('id')->on('solid')->onDelete('cascade');
            $table->bigInteger('factor_id')->unsigned()->nullable();
            $table->foreign('factor_id')->references('id')->on('factor')->onDelete('cascade');
            $table->string('nombre', 191);
            $table->integer('cantidad')->nullable();
            $table->string('restante', 191)->nullable();
            $table->double('masa', 15, 4)->nullable();
            $table->unsignedInteger('actualizado')->default(0);
            $table->unsignedInteger('estado')->default(1);
            $table->unsignedInteger('usuario_creacion');
            $table->unsignedInteger('usuario_edicion');
            $table->timestamp('fecha_creacion')->useCurrent();
            $table->timestamp('fecha_modificacion')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solid_items');
    }
}
