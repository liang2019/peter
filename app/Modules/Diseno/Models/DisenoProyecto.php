<?php

namespace App\Modules\Diseno\Models;

use Illuminate\Database\Eloquent\Model;

class DisenoProyecto extends Model
{
    protected $fillable = [
        'proyecto_id',
       
        'planos',
        'etapas',
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'diseno_proyecto';
    public $timestamps = false;
}
