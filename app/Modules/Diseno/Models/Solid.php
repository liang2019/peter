<?php

namespace App\Modules\Diseno\Models;

use Illuminate\Database\Eloquent\Model;

class Solid extends Model
{
    protected $fillable = [
        'proyecto_id',
        'nombre',
        'descripcion',
        'version',
        'actualizado',
        'verificado',
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'solid';
    public $timestamps = false;
}
