<?php

namespace App\Modules\Diseno\Models;

use Illuminate\Database\Eloquent\Model;

class SolidServicios extends Model
{
    protected $fillable = [
        'solid_id',
        'descripcion',
        'precio',
        'cantidad',
        'verificado',

        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'solid_servicios';
    public $timestamps = false;
}
