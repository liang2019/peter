<?php

namespace App\Modules\Diseno\Models;

use Illuminate\Database\Eloquent\Model;

class SolidItems extends Model
{
    protected $fillable = [
        'solid_id',
        'factor_id',
        'nombre',
        'cantidad',
        'restante',
        'masa',
        'actualizado',

        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'solid_items';
    public $timestamps = false;
}
