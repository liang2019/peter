<?php

namespace App\Modules\Diseno\Models;

use Illuminate\Database\Eloquent\Model;

class DisenoDocumentaria extends Model
{
    protected $fillable = [
        'proyecto_id',
        'adjunto_id',
        'descripcion',
        'monto',
        'observacion',
        'adjunto',
        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'diseno_documentaria';
    public $timestamps = false;
}
