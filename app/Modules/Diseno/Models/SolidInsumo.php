<?php

namespace App\Modules\Diseno\Models;

use Illuminate\Database\Eloquent\Model;

class SolidInsumo extends Model
{
    protected $fillable = [
        'solid_id',
        'factor_id',
        'precio',
        'verificado',

        'estado',
        'usuario_creacion',
        'usuario_edicion',
        'fecha_creacion',
        'fecha_modificacion'
    ];
    protected $table = 'solid_insumo';
    public $timestamps = false;
}
