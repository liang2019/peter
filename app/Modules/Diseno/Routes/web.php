<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'auth'],function () {
    Route::group(['prefix' => 'diseno'], function () {


        Route::get('/proyectos', ['as' => 'diseno', 'uses' => 'DisenoProyectoController@index']);
        Route::get('/proyectos/cargar', ['as' => 'diseno.cargar', 'uses' => 'DisenoProyectoController@cargarAll']);
        Route::post('/cancelar', ['as' => 'diseno.cancelar', 'uses' => 'DisenoProyectoController@cancelar']);
        Route::post('/planos', ['as' => 'diseno.planos', 'uses' => 'DisenoProyectoController@planos']);
        Route::post('/etapas', ['as' => 'diseno.etapas', 'uses' => 'DisenoProyectoController@etapas']);
        Route::post('/completar', ['as' => 'diseno.completar', 'uses' => 'DisenoProyectoController@completar']);

        Route::get('/solid/{id}', ['as' => 'diseno.id', 'uses' => 'DisenoSolidController@index']);
        Route::post('/solid/cargar', ['as' => 'diseno.cargarSolid', 'uses' => 'DisenoSolidController@cargarSolid']);
        Route::post('/solid/items', ['as' => 'diseno.cargarItems', 'uses' => 'DisenoSolidController@cargarItems']);
        Route::post('/solid/actualizar', ['as' => 'diseno.actualizar', 'uses' => 'DisenoSolidController@actualizar']);
        Route::post('/solid/completar', ['as' => 'diseno.completar', 'uses' => 'DisenoSolidController@completar']);
        Route::post('solid/importar', ['as' => 'diseno.importar', 'uses' => 'DisenoSolidController@importar']);
        Route::post('solid/eliminar', ['as' => 'diseno.eliminar', 'uses' => 'DisenoSolidController@eliminar']);
       
        Route::get('/solicitud/{id}', ['as' => 'diseno.id', 'uses' => 'DisenoSolicitudController@index']);
        Route::post('/solicitud/lista', ['as' => 'diseno.cargarListaSolicitudes', 'uses' => 'DisenoSolicitudController@cargarListaSolicitudes']);
        Route::post('/solicitud/cargar', ['as' => 'diseno.cargar', 'uses' => 'DisenoSolicitudController@cargar']);
        Route::post('/solicitud/actualizar', ['as' => 'diseno.actualizar', 'uses' => 'DisenoSolicitudController@actualizar']);
        Route::post('/solicitud/completar', ['as' => 'diseno.completar', 'uses' => 'DisenoSolicitudController@completar']);

        Route::get('/documentaria', ['as' => 'documentaria', 'uses' => 'DisenoAdjuntoController@index']);
        Route::get('/documentaria/{id}', ['as' => 'documentaria.id', 'uses' => 'DisenoAdjuntoController@index']);
        Route::post('/documentaria/cargar', ['as' => 'documentaria.cargar', 'uses' => 'DisenoAdjuntoController@cargarAll']);
        Route::post('/documentaria/buscar/', ['as' => 'documentaria.buscar', 'uses' => 'DisenoAdjuntoController@buscar']);
        Route::patch('/documentaria/editar/', ['as' => 'documentaria.editar', 'uses' => 'DisenoAdjuntoController@editar']);
        Route::post('/documentaria/eliminar/', ['as' => 'documentaria.eliminar', 'uses' => 'DisenoAdjuntoController@eliminar']);
        Route::post('/documentaria/guardar/', ['as' => 'documentaria.guardar', 'uses' => 'DisenoAdjuntoController@guardar']);
        Route::post('/documentaria/updatefile/', ['as' => 'documentaria.updatefile', 'uses' => 'DisenoAdjuntoController@updatefile']);
        Route::get('/documentaria/download/{id}', ['as' => 'documentaria.download', 'uses' => 'DisenoAdjuntoController@download']);
       
           //servicios
           Route::get('/servicio/{id}', ['as' => 'servicio', 'uses' => 'DisenoServicioController@index']);
           Route::post('/servicio/cargar', ['as' => 'servicio.cargar', 'uses' => 'DisenoServicioController@cargarAll']);
           Route::post('/servicio/buscar/', ['as' => 'servicio.buscar', 'uses' => 'DisenoServicioController@buscar']);
           Route::patch('/servicio/editar/', ['as' => 'servicio.editar', 'uses' => 'DisenoServicioController@editar']);
           Route::post('/servicio/guardar', ['as' => 'servicio.guardar', 'uses' => 'DisenoServicioController@guardar']);
           Route::post('/servicio/eliminar/', ['as' => 'servicio.eliminar', 'uses' => 'DisenoServicioController@eliminar']);
           Route::post('/servicio/actualizarPrecio', ['as' => 'solicitud.actualizar', 'uses' => 'DisenoServicioController@actualizarPrecio']);
    });
});