<?php

namespace App\Modules\Dashboard\Services;

use App\Modules\Requerimiento\Models\Proyecto;
use App\Modules\Tracking\Models\Tracking;
use App\Modules\Presupuesto\Models\Item;
use App\Modules\Presupuesto\Models\Utilidad;
use App\Modules\Presupuesto\Models\ItemServicio;
use App\Modules\Presupuesto\Models\ItemInsumo;
use App\Modules\Diseno\Models\Solid;
use App\Modules\Diseno\Models\SolidInsumo;
use App\Modules\Produccion\Models\TareoEmpleado;
use App\Modules\Produccion\Models\SolicitudEntregas;
use App\Modules\Produccion\Models\SolidEntregas;
use App\Modules\Diseno\Models\SolidItems;
use App\Modules\Produccion\Models\ProduccionSolicitudItem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class DashboardIndicadoresService
{


    public function __construct()
    {
        $this->model_proyecto = new Proyecto();
        $this->model_tarcking = new Tracking();
        $this->model_item = new Item();
        $this->model_item_servicio = new ItemServicio();
        $this->model_item_insumo = new ItemInsumo();
        $this->model_utilidad = new Utilidad();
        $this->model_solid = new Solid();
        $this->model_solid_insumo = new SolidInsumo;
        $this->model_tareo_empleado = new TareoEmpleado();
        $this->model_solicitud_entregas = new SolicitudEntregas();
        $this->model_solid_entregas = new SolidEntregas();
        $this->model_solid_items = new SolidItems();
        $this->model_produccion_solicitud_item = new ProduccionSolicitudItem();
        
    }

    public function cargarProyectos(){

        $datos = $this->model_proyecto
       
        ->where("proyecto.estado","=",1)
        ->leftJoin('cliente', 'proyecto.cliente_id', '=', 'cliente.id')
        ->leftJoin('tracking', 'proyecto.id', '=', 'tracking.proyecto_id')
       // ->where("tracking.diseno","<>",3)
        ->select('proyecto.*',
        'cliente.descripcion as nombre_cliente',
        DB::raw("IF(proyecto.estado=1,'En Curso','Completado') as estado"),
        'tracking.diseno as estado_tracking'
        )
        ->orderBy('proyecto.id', 'desc')
        ->get();
        return $datos;
    }

    public function cargarIndicadores(){

        $proyectos = $this->model_proyecto
        ->where("proyecto.estado",1)
        ->select(
        DB::raw("COUNT(id) as vigentes"),
        DB::raw("(SELECT COUNT(proyecto.id) FROM proyecto LEFT JOIN tracking ON proyecto.id=tracking.proyecto_id WHERE tracking.cotizacion=1 ) as cotizacion"),
        DB::raw("(SELECT COUNT(proyecto.id) FROM proyecto LEFT JOIN tracking ON proyecto.id=tracking.proyecto_id WHERE tracking.presupuesto=1 ) as presupuesto")
        )
        ->first();

        $datos = array(
            "vigentes" =>  $proyectos["vigentes"],
            "cotizacion" =>  $proyectos["cotizacion"],
            "presupuesto" =>  $proyectos["presupuesto"]
        );

        return $datos;
    }

    public function cargarGrafico($data){
      

        $grafico_presupuestado = 0;
        $grafico_gastado = 0;
        if($data["proyecto_id"]!=null){
            $proyecto = $this->model_proyecto->where("proyecto.id",$data["proyecto_id"])
            ->select('proyecto.*')
            ->first();
        $materiales_servicios_presupuestado = $this->materiales_servicios_presupuestado($data["proyecto_id"]);
        $gastos_generales_presupuesto = $this->gastos_generales_presupuesto($data["proyecto_id"]);
        $materiales_servicios_diseno = $this->materiales_servicios_diseno($data["proyecto_id"]);
        //TAREO
        $tareoGeneral =  $this->tareoGeneral($data["proyecto_id"]);

        //GASTADO VS PRESUPUESTADO
        
        $suma_gastado = $materiales_servicios_diseno + $tareoGeneral["total_costo"] + $gastos_generales_presupuesto;
        $grafico_gastado = $materiales_servicios_diseno + $tareoGeneral["total_costo"] + $gastos_generales_presupuesto;
        $suma_presupuestado = $materiales_servicios_presupuestado + $gastos_generales_presupuesto;
        $grafico_presupuestado = $materiales_servicios_presupuestado + $gastos_generales_presupuesto;
        }
        $datos = array(
            "grafico_presupuestado" => $grafico_presupuestado,
            "grafico_gastado" => $grafico_gastado
        );
      
     return $datos;
    }

    public function materiales_servicios_presupuestado($id){

        $servicios = $this->model_item_servicio->where('item_servicio.proyecto_id', $id)
        ->where('item_servicio.estado', 1)
        ->select( DB::raw('SUM(item_servicio.precio) as costo'))->first();

        $materiales = $this->model_item->where("item.proyecto_id", $id)
        ->where("item.estado", 1)
        ->where("item.habilitado", 1)
        ->select(
                    DB::raw(" 
                    SUM(
                        (SELECT SUM(item_insumo.costo_parcial) from item_insumo WHERE item_insumo.item_id = item.id)
                        +
                        (SELECT SUM(item_insumo.costo_material_2) from item_insumo WHERE item_insumo.item_id = item.id)
                    ) as costo")
                    
                )
        ->first();
      
        $total = $servicios["costo"] + $materiales["costo"];


        return $total;
    }

    public function gastos_generales_presupuesto($id){

        $gatos = $this->model_utilidad
        ->leftjoin("item", "item.id", "utilidad.item_id")
        ->where("utilidad.proyecto_id", $id)
        ->where("item.proyecto_id", $id)
        ->where("item.nombre", "Gastos")
        ->select('*')
        ->first();

        $total = $gatos["precio_parcial"];
        return $total;

    }

    public function materiales_servicios_diseno($id){

        $solids = $this->model_solid
        ->where("solid.proyecto_id",$id)
        ->where("solid.actualizado",1)
        ->select('solid.id as solid_id')
        ->get();
        $monto_total = 0;
        foreach ($solids as $key => $value) {
            
            $items =  $this->model_solid_insumo
            ->where("solid_insumo.solid_id",$value["solid_id"])
            ->leftJoin('factor', 'solid_insumo.factor_id', '=', 'factor.id')
            ->leftJoin('alarti', 'factor.alarti_id', '=', 'alarti.id')
            ->select(   'solid_insumo.id',
                        'solid_insumo.precio',

                DB::raw("(solid_insumo.precio*
                (SELECT SUM(solid_items.cantidad) from solid_items WHERE solid_items.factor_id = solid_insumo.factor_id AND solid_items.solid_id=".$value['solid_id'].")
                ) as monto")       
            )
            ->get();
            foreach ($items as $key2 => $value2) {
                $monto_total = $value2["monto"] + $monto_total;
            }
        }

     
        return $monto_total;

    }

    public function tareoGeneral($id){
        $consulta = $this->model_tareo_empleado
        ->leftJoin('tareo', 'tareo.id', 'tareo_empleado.tareo_id')
        ->where('tareo.proyecto_id', $id)
        ->where('tareo.estado', 1)
        ->rightJoin('empleado', 'tareo_empleado.empleado_id', 'empleado.id')
        ->leftJoin('proyecto', 'tareo.proyecto_id', 'proyecto.id')
        ->leftJoin('tarea', 'tareo.tarea_id', 'tarea.id')
        ->select(   'tareo_empleado.empleado_id',
                    'proyecto.nombre as nombre_proyecto',
                    'empleado.sueldo',
                    'empleado.nombres',
                    'tareo.tipo',
                    'tareo.fecha_creacion',
                    'empleado.apellidos',
                    'tarea.nombre as nombre_actividad',
                    'tareo.horas_trabajadas' 
        )
        ->get();
        $datos = array();
  
        $horas_produccion=0;
        $costo_produccion = 0;
        $horas_montaje =0;
        $costo_montaje = 0;
        $horas_produccion_sub = 0;
        $horas_montaje_sub = 0;
        foreach ($consulta as $key => $value) {

            if($value["tipo"]==1){

              //  $datos["item"][$key]["horas_trabajadas"]= floor($value["horas_trabajadas"]).":".floor(($value["horas_trabajadas"]-floor($value["horas_trabajadas"]))*60);
                $sueldo_produccion = floatval(Crypt::decryptString($value["sueldo"]));
                $horas_trabajadas = floatval($value["horas_trabajadas"]);
                $costo_produccion_sub = $sueldo_produccion * $horas_trabajadas;
                $horas_produccion_sub = $horas_produccion_sub + $value["horas_trabajadas"];
                $costo_produccion = $costo_produccion + $costo_produccion_sub;
            }else{
             //   $datos["item"][$key]["horas_trabajadas"]= floor($value["horas_trabajadas"]).":".floor(($value["horas_trabajadas"]-floor($value["horas_trabajadas"]))*60);
                $sueldo_montaje= floatval(Crypt::decryptString($value["sueldo"]));
                $horas_trabajadas = floatval($value["horas_trabajadas"]);
                $costo_montaje_sub = $sueldo_montaje * $horas_trabajadas;
                $horas_montaje_sub = $horas_montaje_sub + $value["horas_trabajadas"];
                $costo_montaje = $costo_montaje + $costo_montaje_sub;
            }

           
        }
        $horas_produccion = floor($horas_produccion_sub).":".floor(($horas_produccion_sub-floor($horas_produccion_sub))*60);
        $horas_montaje = floor($horas_montaje_sub).":".floor(($horas_montaje_sub-floor($horas_montaje_sub))*60);
        $total_horas = $horas_produccion_sub + $horas_montaje_sub;
        $total_horas = floor($total_horas).":".floor(($total_horas-floor($total_horas))*60);
        $total_costo = $costo_montaje + $costo_produccion; 
        $costo_produccion = number_format($costo_produccion,2);
        $costo_montaje = number_format($costo_montaje,2);

        $empleados= $this->model_tareo_empleado
        ->leftJoin('tareo', 'tareo.id', 'tareo_empleado.tareo_id')
        ->where('tareo.proyecto_id', $id)
        ->where('tareo.estado', 1)
        ->select(
            DB::raw("(SELECT COUNT(DISTINCT(tareo_empleado.empleado_id)) FROM tareo_empleado LEFT JOIN tareo ON tareo_empleado.tareo_id=tareo.id WHERE tareo.tipo=1 AND tareo.proyecto_id=".$id.") as trabajadores_produccion"),
            DB::raw("(SELECT COUNT(DISTINCT(tareo_empleado.empleado_id)) FROM tareo_empleado LEFT JOIN tareo ON tareo_empleado.tareo_id=tareo.id WHERE tareo.tipo=2 AND tareo.proyecto_id=".$id.") as trabajadores_montaje"),
            DB::raw("(SELECT COUNT(DISTINCT(tareo_empleado.empleado_id)) FROM tareo_empleado LEFT JOIN tareo ON tareo_empleado.tareo_id=tareo.id WHERE  tareo.proyecto_id=".$id.") as trabajadores_total")
        )->first();

        $costo_parcial = $this->costoParcial($id);
        $diferencia = $total_costo - $costo_parcial;
        $diferencia = number_format($diferencia,2);
        $costo_parcial = number_format($costo_parcial,2);
        $items = array(
            "costo_parcial" => $costo_parcial,
            "horas_produccion" => $horas_produccion,
            "costo_produccion" => $costo_produccion,
            "trabajadores_produccion" =>  $empleados["trabajadores_produccion"],
            "horas_montaje" => $horas_montaje,
            "costo_montaje" => $costo_montaje,
            "trabajadores_montaje" =>  $empleados["trabajadores_montaje"],
            "total_costo" => $total_costo,
            "total_horas" => $total_horas,
            "trabajadores_total" => $empleados["trabajadores_total"],
            "diferencia" => $diferencia
        );
      
        return $items;
    }

    public function costoParcial($id){
        $costo_parcial = $this->model_item_insumo
        ->where("proyecto_id", $id)
        ->where("estado", 1)
        ->select(
            DB::raw("SUM(costo_parcial) as costo_parcial")
        )->first();
        $total = $costo_parcial["costo_parcial"];
      return  $total;
    }

    public function procentaje_compras($id){
        $solids = $this->model_solid_entregas
        ->leftjoin("solid_items", "solid_items.id", "solid_entregas.solid_items_id")
        ->leftjoin("solid", "solid.id", "solid_items.solid_id")
        ->where("solid_entregas.estado", 1)
        ->where("solid.estado", 1)
        ->where("solid.proyecto_id", $id)
        ->select("solid_entregas.*")
        ->get();
        $solid_items = $this->model_solid_items
        ->leftjoin("solid", "solid.id", "solid_items.solid_id")
        ->where("solid_items.estado", 1)
        ->where("solid.estado", 1)
        ->where("solid.proyecto_id", $id)
        ->select(
            DB::raw("IFNULL(SUM(solid_items.cantidad),0) as cantidad_items_solid")
        )
        ->first();
        $cantidad_solids_completados = 0;
        foreach ($solids as $key => $value) {
           if($value["cantidad"]==$value["recibido"]){
               $cantidad_solids_completados=$cantidad_solids_completados + $value["cantidad"];
           }
        }


        $produccion_solicitud_items = $this->model_produccion_solicitud_item
        ->leftjoin("produccion_solicitud", "produccion_solicitud.id", "produccion_solicitud_item.produccion_solicitud_id")
        ->where("produccion_solicitud.estado", 1)
        ->where("produccion_solicitud_item.estado", 1)
        ->where("produccion_solicitud.proyecto_id", $id)
        ->select(
            DB::raw("IFNULL(SUM(produccion_solicitud_item.cantidad),0) as cantidad_items_solicitud")
        )
        ->first();

       
        $solicitudes = $this->model_solicitud_entregas
        ->leftjoin("produccion_solicitud_item", "produccion_solicitud_item.id", "solicitud_entregas.produccion_solicitud_item_id")
        ->leftjoin("produccion_solicitud", "produccion_solicitud.id", "produccion_solicitud_item.produccion_solicitud_id")
        ->where("produccion_solicitud.estado", 1)
        ->where("produccion_solicitud_item.estado", 1)
        ->where("solicitud_entregas.estado", 1)
        ->where("produccion_solicitud.proyecto_id", $id)
        ->select("solicitud_entregas.*")
        ->get();

        $cantidad_solicitudes_completados = 0;
        foreach ($solicitudes as $key => $value) {
           if($value["cantidad"]==$value["recibido"]){
               $cantidad_solicitudes_completados = $cantidad_solicitudes_completados + $value["cantidad"];
           }
        }
        $cantidad_items = $produccion_solicitud_items["cantidad_items_solicitud"] + $solid_items["cantidad_items_solid"];
        $cantidad_completados = $cantidad_solids_completados + $cantidad_solicitudes_completados;
        $items = array(
            "items_total" => $cantidad_items,
            "items_completados" => $cantidad_completados
    
        );
      
        return $items;

    }

}
