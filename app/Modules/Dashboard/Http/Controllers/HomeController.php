<?php

namespace App\Modules\Dashboard\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Dashboard\Services\DashboardIndicadoresService as MainService;
use App\Http\Controllers\Controller;
use App\Modules\Dashboard\Models\Menu;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    public function __construct()
    {
        $this->service = new MainService();
    }
    public function modulos()
    {
        /*$obj = array();
        $obj["codigo_proyecto"]='codigo_proyecto';
        $obj["nombre_proyecto"]='nombre_proyecto';
        $obj["cliente"]='cliente';
        $obj["mensaje"]='mensaje';
        $obj["usuario_responsable"]='usuario_responsable';

        correoTracking($obj);*/
        $modules = Menu::where('estado',1)->orderBy('order','asc')->get();
        $tree = $this->buildTree($modules);
        $treeHtml = $this->buildTreeHtml($tree);
        return $treeHtml;
        //return $tree;
    }
    public function buildTree($elements, $parentId = 0)
    {
        $branch = array();
        foreach ($elements as $element) {
            if ($element['padre_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }
    public function buildTreeHtml($elements, $opt="")
    {
        $branch = array();
        $li = '';

        foreach ($elements as $element) {
            $li = $li . (isset($element['children'])

            ? ('
            <li class="treeview">
                <a href="#">
                <i class="material-icons">'.$element["icon"].'</i>
                    <span style="position: absolute; margin: 5px;">

                    '.$element['nombre'].'</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    '. $this->buildTreeHtml($element['children'], 'childs').'
                </ul>
           '.' </li>')
            :
                            ('<li >
                            <a href="'.(trim(config('app.env'))=='local'?'/':'/').$element["url"].'">
                                <i class="material-icons">'.$element["icon"].'</i>'.($element["url"]=="tracking"?('<span class="v-alert__border v-alert__border--left cyan v-alert__border--has-color"></span>'):'').' <span  style="position: absolute; margin: 5px;">'.$element['nombre'].'</span>
                            </a>
                        </li> ') );
        }
        return $li;
    }

    public function index()
    {
        $menu = $this->modulos();
        return view('dashboard::admin.home',compact('menu'));

    }

    public function cargarProyectos()
    {
        $data = $this->service->cargarProyectos();
        return $data;
    }

    public function cargarIndicadores()
    {
        $data = $this->service->cargarIndicadores();
        return $data;
    }


    public function cargarGrafico(Request $request)
    {
        $data = $this->service->cargarGrafico($request);
        return $data;
    }
}
