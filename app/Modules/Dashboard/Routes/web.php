<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
//Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth'],function () {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/', ['as' => 'reports', 'uses' => 'HomeController@index']);
        Route::get('/proyectos', ['as' => 'reports', 'uses' => 'HomeController@cargarProyectos']);
        Route::get('/indicadores', ['as' => 'reports', 'uses' => 'HomeController@cargarIndicadores']);
      
        Route::post('/grafico', ['as' => 'indicadores.cargar', 'uses' => 'HomeController@cargarGrafico']);
    });
});

