<html>
<table>
    <tbody>
        <tr>
            <td><img src="{{public_path('img/Logo_Emer_HD-R2.jpg')}}" width="150" alt=""></td>
        </tr>
        <tr>
            <td></td>
            <td style="font-size:15px"><b>Señores: </b></td>
            <td>{{ $proyecto["descripcion"] }}</td>
        </tr>
        <tr>
            <td></td>
            <td style="font-size:15px"><b>Código: </b></td>
            <td>{{ $proyecto["codigo"] }}</td>
        </tr>
        <tr>
            <td></td>
            <td style="font-size:15px"><b>Proyecto: </b></td>
            <td>{{ $proyecto["nombre"] }}</td>
        </tr>
        <tr>
            <td></td>
            <td style="font-size:15px"><b>Fecha Inicio: </b></td>
            <td>{{ $proyecto["fecha_creacion"] }}</td>
        </tr>
    </tbody>
</table>

<table >
    <thead style="font-size:125px">
    <tr>
        <th></th>
        <td><b>NOMBRE</b></td>
        <th><b>UNIDAD</b></th>
        <th><b>CANTIDAD</b></th>
        <th><b>PRECIO UNITARIO</b></th>
        <th><b>PRECIO PARCIAL</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($cotizacion as $cotiza)
        <tr>
            <td></td>
            <td>{{ $cotiza["nombre"] }}</td>
            <td>{{ $cotiza["unidad"] }}</td>
            <td>{{ $cotiza["cantidad"] }}</td>
            <td>{{ round($cotiza["unitario"],2) }}</td>
            <td>{{ round($cotiza["parcial"],2) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

</html>
