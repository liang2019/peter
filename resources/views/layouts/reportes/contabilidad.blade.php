<html>
<table>
    <tbody>
        <tr>
            <td></td>
            <td colspan="2"><img src="{{public_path('img/Logo_Emer_HD-R2.jpg')}}" width="150" alt=""></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2" style="font-size:15px"><b>FLUJO DE CAJA EN MONEDA EXTRANJERA </b></td>
        </tr>
        <tr>
            <td></td>
            <td style="font-size:15px"><b>Fecha Importado: </b></td>
            <td>{{ $fecha_importado }}</td>
        </tr>

    </tbody>
</table>

<table>
    <tr>
        <th colspan="3"></th>
        <th style="font-size:13 px;text-align:center;" colspan="2"><b>Flujo</b></th>
    </tr>
    <tr>
        <th colspan="3"></th>
        <th colspan="2">&nbsp;</th>
    </tr>
    <tr>
        <th></th>
        <td style="background-color:red;font-size:12 px"><b>Saldo Inicial $</b></td>
        <th></th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["saldo_inicial"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th style="font-size:12 px"><b>INGRESOS</b></th>
        <th></th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["total_ingresos"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">CUENTAS POR COBRAR COMERCIALES</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ctas_cobrar_comerciales"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">CUENTAS POR COBRAR RELACIONADAS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ctas_cobrar_relacionadas"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">PRESTAMOS POR COBRAR EMPLEADOS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ctas_cobrar_empleados"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">PRESTAMOS POR COBRAR ACCIONISTAS Y DIRECTORES</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ctas_cobrar_accionistas"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">PRESTAMOS POR COBRAR GERENTES</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ctas_cobrar_gerentes"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">CUENTAS POR COBRAR A TERCEROS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ctas_cobrar_terceros"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">CUENTAS POR COBRAR DIVERSAS RELACIONADAS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ctas_cobrar_divrelacionadas"]}}</b></th>
    </tr>
    <tr>
        <td colspan="5"></td>
    </tr>
    <tr>
        <th></th>
        <th colspan="2" style="font-size:12 px"><b>TOTAL</b></th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["total_ingresos"]}}</b></th>
    </tr>
</table>
<table>

    <tr>
        <th></th>
        <th style="font-size:12 px"><b>EGRESOS</b></th>
        <th></th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["total_egresos"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th style="font-size:12 px;text-align:right;"><b>TRIBUTOS POR PAGAR</b></th>
        <th></th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["tributos_pagar"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">IGV - CTA. PROPIA</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["igv_cta_propia"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">IGV - PENDIENTE DE PAGO</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["igv_pend_pago"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">IGV - CTA. PROPIA POR APLICAR ME</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["igv_cta_propia_me"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">IGV - SERV. PREST. X NO DOMICILIADOS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["igv_serv_nodomiciliario"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">IGV - REG. PERCEP. VENTAS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["igv_reg_percep_ventas"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">IGV - REG. PERCEP. COMPRAS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["igv_reg_percep_compras"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">IGV - REG. PERCEP. POR APLICAR ME</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["igv_reg_percep_aplicar_me"]}}</b></th>
    </tr>

    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">IGV - REG. PERCEP. POR APLICAR MN</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["igv_reg_percep_aplicar_mn"]}}</b></th>
    </tr>

    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">IGV - REG. PERCEP. VTAS POR COBRAR MN</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["igv_reg_percep_vtas_cobrar_mn"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">IGV - REG. PERCEP. VTAS POR COBRAR ME</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["igv_reg_percep_vtas_cobrar_me"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">IGV - REG. DE RETENCIONES</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["igv_reg_retenciones"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">DERECHOS ARANCELARIOS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["derechos_arancelarios"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">RENTA DE 3RA CATEG.</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["renta_3era_categ"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">RENTA DE 4TA CATEG.MN</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["renta_4ta_categ_mn"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">RENTA DE 4TA CATEG.ME</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["renta_4ta_categ_me"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">RENTA DE 5TA CATEGORIA</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["renta_5ta_categ"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">RENTA DE NO DOMICILIADOS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["renta_no_domiciliados"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">OTRAS RETENCIONES</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["otras_retenciones"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">ITF - IMPTO.O A LAS TRANSAC. FINANCIERAS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["itf_impto_financieras"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">ITAN - IMPTO. TEMPORAL A LOS ACTIVOS NETOS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["itan_impto_activos_netos"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">OTROS IMPUESTOS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["otros_impuestos"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">OSCE-PROVEED. BIENES Y SERVICIOS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["osce_bienes_servicios"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">ESSALUD</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["essalud"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">SCTR - SEG. COMPLEMENT. TRAB. RIESGO</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["complement_trab_riesgo"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">ESSALUD VIDA</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["essalud_vida"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">ESSALUD - VACACIONES DEVENGADAS NO PAGADAS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["essalud_vacaciones_devengadas_no_pagadas"]}}</b>
        </th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">ONP</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["onp"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">CONTRIBUCION AL SENATI</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["contrib_senati"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">CONASEV</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["conasev"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">IMPTO. AL PATRIMONIO VEHICULAR</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["impto_patrimonio_vehicular"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">IMPTO. DE ALCABALA</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["impto_alcabala"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">IMPTO. PREDIAL</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["impto_predial"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">LICENCIA DE APERTURA DE ESTABLECIMIENTOS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["licencia_apertura_establec"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">SERVICIOS PUBLICOS O ARBITRIOS</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["servicios_publicos_arbitrios"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">FRACCIONAMIENTO - SUNAT</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fracc_sunat"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">FRACCIONAMIENTO - ESSALUD</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fracc_essalud"]}}</b></th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th style="font-size:12 px">FRACCIONAMIENTO - ONP</th>
        <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fracc_onp"]}}</b></th>
    </tr>
   
</table>
<table width="745">
    <tbody>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">
                <p><strong>AFP</strong></p>
            </td>
            <td width="376">&nbsp;</td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["afp"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>AFP PROFUTURO</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["afp_profuturo"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>AFP HORIZONTE</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["afp_horizonte"]}}</b></th>

        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>AFP INTEGRA</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["afp_integra"]}}</b></th>

        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>AFP PRIMA</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["afp_prima"]}}</b></th>

        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>AFP HABITAT</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["afp_habitat"]}}</b></th>

        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">&nbsp;</td>
           
            <td width="207">&nbsp;</td>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td colspan="2" width="447">
                <p><strong>REMUNERACIONES Y PARTICIPACIONES POR PAGAR</strong></p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["remune_partici_pagar"]}}</b></th>

        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>SUELDOS</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["sueldos"]}}</b></th>

        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>SALARIOS</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["salarios"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>GRATIFICACIONES EMPLEADOS</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["gratif_empleados"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>GRATIFICACIONES OBREROS</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["gratif_obreros"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>VACACIONES EMPLEADOS</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["vacaciones_empleados"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>VACACIONES OBREROS</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["vacaciones_obreros"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>PARTICIP. EMPLEADOS POR PAGAR</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["particip_empleados_pagar"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>PARTICIP. OBREROS POR PAGAR</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["particip_obreros_pagar"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>C.T.S. EMPLEADOS MN</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["cts_empleados_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>C.T.S. EMPLEADOS ME</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["cts_empleados_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>ADELANTO C.T.S. EMPLEADOS</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["cts_empleados_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>ADELANTO C.T.S. OBREROS</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["adelanto_cts_obreros"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>OTRAS REMUNERACIONES Y PARTICIPACIONES POR PAGAR</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["otras_remuneraciones_particip_pagar"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">&nbsp;</td>
            
            <td width="207">&nbsp;</td>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td colspan="2" width="447">
                <p><strong>CUENTAS POR PAGAR COMERCIALES</strong></p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ctas_pagar_comerciales"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACTURAS MN NO EMITIDAS POR PAGAR</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["facturas_mn_noemitidas_pagar"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACTURAS ME NO EMITIDAS POR PAGAR</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["facturas_me_noemitidas_pagar"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACT. EMIT. POR PAGAR M.N. TERCEROS</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fact_emit_pagar_mn_terceros"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACT. EMIT. POR PAGAR M.E. TERCEROS</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fact_emit_pagar_me_terceros"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>DETRACCIONES POR PAGAR MN</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["detracc_pagar_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACT. CON DETRACC X PAGAR M.N.</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fact_detracc_pagar_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACT. CON DETRACC X PAGAR M.E.</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fact_detracc_pagar_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>ANTICIPOS A PROVEEDORES M.N.</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["anticip_provee_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>ANTICIPOS A PROVEEDORES M.E.</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["anticip_provee_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>LETRAS POR PAGAR M.N. TERCEROS</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["letras_pagar_mn_terceros"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>LETRAS POR PAGAR M.E. TERCEROS</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["letras_pagar_me_terceros"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>HONORARIOS POR PAGAR M.N.</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["honorarios_pagar_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>HONORARIOS POR PAGAR M.E.</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["honorarios_pagar_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">&nbsp;</td>
           
            <td width="207">&nbsp;</td>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td colspan="2" width="447">
                <p><strong>CUENTAS POR PAGAR RELACIONADAS</strong></p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ctas_pagar_relacionadas"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACTURAS EMITIDAS POR PAGAR M.N. REL. MATRIZ</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fact_emit_pagar_mn_matriz"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACTURAS EMITIDAS POR PAGAR M.E. REL. MATRIZ</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fact_emit_pagar_me_matriz"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACTURAS EMITIDAS POR PAGAR M.N.RELAC.SUBSIDIARIAS</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fact_emit_pagar_mn_subsid"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACTURAS EMITIDAS POR PAGAR M.E.RELAC.SUBSIDIARIAS</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fact_emit_pagar_me_subsid"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACTURAS EMITIDAS POR PAGAR M.N.RELAC.ASOCIADAS</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fact_emit_pagar_mn_asoc"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACTURAS EMITIDAS POR PAGAR M.E.RELAC.ASOCIADAS</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fact_emit_pagar_me_asoc"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACTURAS EMITIDAS POR PAGAR M.N.RELAC.SUCURSALES</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fact_emit_pagar_mn_sucur"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACTURAS EMITIDAS POR PAGAR M.E.RELAC.SUCURSALES</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["fact_emit_pagar_me_sucur"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>ANTICIPOS OTORGADOS M.N. MATRIZ</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["antic_otorgados_mn_matriz"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>ANTICIPOS OTORGADOS M.E. MATRIZ</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["antic_otorgados_me_matriz"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>ANTICIPOS OTORGADOS M.N. SUBSIDIARIAS</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["antic_otorgados_mn_subsid"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>ANTICIPOS OTORGADOS M.E. SUBSIDIARIAS</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["antic_otorgados_me_subsid"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>ANTICIPOS OTORGADOS M.N. ASOCIADAS</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["antic_otorgados_mn_asoc"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>ANTICIPOS OTORGADOS M.E. ASOCIADAS</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["antic_otorgados_me_asoc"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>ANTICIPOS OTORGADOS M.N. SUCURSALES</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["antic_otorgados_mn_sucur"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>ANTICIPOS OTORGADOS M.E. SUCURSALES</p>
            </td>
        
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["antic_otorgados_me_sucur"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>ANTICIPOS OTORGADOS M.N. OTROS</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["antic_otorgados_mn_otros"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>ANTICIPOS OTORGADOS M.E. OTROS</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["antic_otorgados_me_otros"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>LETRAS POR PAGAR M.N. RELACIONADAS MATRIZ</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["letras_pagar_mn_relac_matriz"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>LETRAS POR PAGAR M.E. RELACIONADAS MATRIZ</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["letras_pagar_me_relac_matriz"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">&nbsp;</td>
            
            <td width="207">&nbsp;</td>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td colspan="2" width="447">
                <p><strong>PRESTAMOS POR PAGAR ACCIONISTAS DIRECTORES GTES</strong></p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ptmos_pagar_accion_direct_gtes"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>PTMOS. ACCIONIS. CORTO PLAZO M.N.</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ptmos_accion_corto_plazo_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>PTMOS. ACCIONIS. CORTO PLAZO M.E.</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ptmos_accion_corto_plazo_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>PTMOS. ACCIONIS. LARGO PLAZO M.N.</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ptmos_accion_largo_plazo_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>PTMOS. ACCIONIS. LARGO PLAZO M.E.</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ptmos_accion_largo_plazo_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>DIETAS AL DIRECTORIO</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["dietas_directorio"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">&nbsp;</td>
            
            <td width="207">&nbsp;</td>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td colspan="2" width="447">
                <p><strong>PRESTAMOS Y OBLIGACIONES FINANCIERAS</strong></p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ptmos_oblig_financ"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>PTMOS. INST. FINANCIERAS M.N.</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ptmos_inst_financ_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>PTMOS. INST. FINANCIERAS M.E.</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ptmos_inst_financ_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>LEASING M.N.</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["leasing_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>LEASING M.E.</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["leasing_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>LEASEBACK M.N.</p>
            </td>
         
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["leaseback_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>LEASEBACK M.E.</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["leaseback_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>PAGARES M.N.</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["pagares_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>PAGARES M.E.</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["pagares_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>CONFIRMING M.N.</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["confirming_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>CONFIRMING M.E.</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["confirming_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACTORING M.N.</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["factoring_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>FACTORING M.E.</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["factoring_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>LETRAS EN DESCUENTO M.N.</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["letras_dsc_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>LETRAS EN DESCUENTO M.E.</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["letras_dsc_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>TARJETA DE CREDITO M.E.</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["tarjeta_credito_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">&nbsp;</td>
           
            <td width="207">&nbsp;</td>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td colspan="2" width="447">
                <p><strong>CUENTAS POR PAGAR DIVERSAS</strong></p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ctas_pagar_diversas"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>RECLAMACIONES DE TERCEROS M.N.</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["reclam_terceros_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>RECLAMACIONES DE TERCEROS M.E.</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["reclam_terceros_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>DEPOSITOS RECIBIDOS EN GARANTIA</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["depos_recib_garantia"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>CTAS POR PAGAR DIV. M.N.</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ctas_pagar_div_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>CTAS POR PAGAR DIV. M.E.</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ctas_pagar_div_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">&nbsp;</td>
            
            <td width="207">&nbsp;</td>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td colspan="2" width="447">
                <p><strong>CUENTAS POR PAGAR DIVERSAS REALCIONADAS</strong></p>
            </td>
         
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ctas_pagar_diversas_rela"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>PRESTAMOS RECIBIDOS M.N.</p>
            </td>
           
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ptmos_recib_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>PRESTAMOS RECIBIDOS M.E.</p>
            </td>
         
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ptmos_recib_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>PTMO.ACCIONISTAS MN</p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ptmos_accion_mn"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">
                <p>PTMO.ACCIONISTAS ME</p>
            </td>
            
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["ptmos_accion_me"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">&nbsp;</td>
          
            <td width="207">&nbsp;</td>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td colspan="2" width="447">
                <p><strong>PROVICIONES POR PAGAR</strong></p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["prov_pagar"]}}</b></th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">&nbsp;</td>
           
            <td width="207">&nbsp;</td>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">&nbsp;</td>
           
            <td width="207">&nbsp;</td>
        </tr>
        <tr>
        <th></th>

        <th style="font-size:12 px"><b>Total Gastos</b></th>
    
           
            <td width="376">
                <p><strong>&nbsp;</strong></p>
            </td>
           
            <th style="text-align:center;" colspan="2">
                
            <b>{{$contabilidad["total_egresos"]}}</b>
            </th>
        </tr>
        <tr>
            <td width="55">&nbsp;</td>
            <td width="71">&nbsp;</td>
            <td width="376">&nbsp;</td>
           
            <td width="207">&nbsp;</td>
        </tr>
        <tr>
        <th></th>

            <th style="font-size:12 px"><b>SALDO FINAL $</b></th>
           
            <td width="376">
                <p><strong>&nbsp;</strong></p>
            </td>
          
            <th style="text-align:center;" colspan="2"><b>{{$contabilidad["saldo_final"]}}</b></th>
        </tr>
    </tbody>
</table>

</html>