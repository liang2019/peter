<html>
<table>
    <tbody>
        <tr>
            <td><img src="{{public_path('img/Logo_Emer_HD-R2.jpg')}}" width="150" alt=""></td>
        </tr>
       
    </tbody>
</table>

<table >
    <thead style="font-size:125px">
    <tr>
        <th></th>
        <td><b>N°</b></td>
        <th><b>CÓDIGO</b></th>
        <th><b>DESCRIPCIÓN</b></th>
        <th><b>FAMILIA</b></th>
        <th><b>UNIDAD</b></th>
        <th><b>GRUPO</b></th>
        <th><b>CUENTA</b></th>
        <th><b>PESO</b></th>
        <th><b>TIPO</b></th>
        <th><b>COD MONEDA</b></th>
        <th><b>COD MONEDA C</b></th>
        <th><b>PRECOM</b></th>
        <th><b>FECHA</b></th>
        <th><b>FSTOCK</b></th>
        <th><b>ESTADO</b></th>
        <th><b>FECHA VENCIMIENTO</b></th>
        <th><b>MON COS</b></th>
        <th><b>PRE COS</b></th>
        <th><b>FECHA COS</b></th>
        <th><b>TIPO EXI</b></th>
        <th><b>STOCK</b></th>
        <th><b>PRECIO UNIT</b></th>
        <th><b>FECHA DOC 2</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($arr as $value)
        <tr>
            <td></td>
            <td>{{ $value["id"] }}</td>
            <td>{{ $value["acodigo"] }}</td>
            <td>{{ $value["adescripcion"] }}</td>
            <td>{{ $value["afamilia"] }}</td>
            <td>{{ $value["aunidad"] }}</td>
            <td>{{ $value["agrupo"] }}</td>
            <td>{{ $value["acuenta"] }}</td>
            <td>{{ $value["apeso"] }}</td>
            <td>{{ $value["atipo"] }}</td>
            <td>{{ $value["acodmon"] }}</td>
            <td>{{ $value["acodmonc"] }}</td>
            <td>{{ $value["aprecom"] }}</td>
            <td>{{ $value["afecha"] }}</td>
            <td>{{ $value["afstock"] }}</td>
            <td>{{ $value["aestado"] }}</td>
            <td>{{ $value["afecven"] }}</td>
            <td>{{ $value["amoncos"] }}</td>
            <td>{{ $value["aprecos"] }}</td>
            <td>{{ $value["afeccos"] }}</td>
            <td>{{ $value["atipexi"] }}</td>
            <td>{{ $value["alskdis"] }}</td>
            <td>{{ $value["c6preuni"] }}</td>
            <td>{{ $value["c6fecdoc2"] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

</html>
