<html>
<table>
    <tbody>
        <tr>
        <td><img src="{{public_path('img/Logo_Emer_HD-R2.jpg')}}" width="150" alt=""></td>
        </tr>
       
    </tbody>
</table>

<table >
    <thead style="font-size:125px">
    <tr>
        <th></th>
        <td><b>CÓDIGO</b></td>
        <th><b>NOMBRE</b></th>
        <th><b>CLIENTE</b></th>
        <th><b>FECHA INICIO</b></th>
        <th><b>MONTAJE</b></th>
        <th><b>REQUERIMIENTO</b></th>
        <th><b>REQUERIMIENTO FECHA</b></th>
        <th><b>PRESUPUESTO</b></th>
        <th><b>PRESUPUESTO FECHA</b></th>
        <th><b>COTIZACIÓN</b></th>
        <th><b>COTIZACIÓN FECHA</b></th>
        <th><b>PEDIDO</b></th>
        <th><b>PEDIDO FECHA</b></th>
        <th><b>DISEÑO</b></th>
        <th><b>DISEÑO FECHA</b></th>
        <th><b>PRODUCCIÓN</b></th>
        <th><b>PRODUCCIÓN FECHA</b></th>
        <th><b>MONTAJE</b></th>
        <th><b>MONTAJE FECHA</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($tracking as $track)
    {{$estilo_req = $track['requerimiento']=='Cancelado'?'background-color:#ff5252;':($track['requerimiento']=='Completado'?'background-color:#4caf50;':($track['requerimiento']==''?'':'background-color:#2196f3;')).'color:#ffffff;'}};
    {{$estilo_pres = $track['presupuesto']=='Cancelado'?'background-color:#ff5252;':($track['presupuesto']=='Completado'?'background-color:#4caf50;':($track['presupuesto']==''?'':'background-color:#2196f3;')).'color:#ffffff;'}};
    {{$estilo_cot = $track['cotizacion']=='Cancelado'?'background-color:#ff5252;':($track['cotizacion']=='Completado'?'background-color:#4caf50;':($track['cotizacion']==''?'':'background-color:#2196f3;')).'color:#ffffff;'}};
    {{$estilo_ped = $track['pedido']=='Cancelado'?'background-color:#ff5252;':($track['pedido']=='Completado'?'background-color:#4caf50;':($track['pedido']==''?'':'background-color:#2196f3;')).'color:#ffffff;'}};
    {{$estilo_dis = $track['diseno']=='Cancelado'?'background-color:#ff5252;':($track['diseno']=='Completado'?'background-color:#4caf50;':($track['diseno']==''?'':'background-color:#2196f3;')).'color:#ffffff;'}};
    {{$estilo_prod = $track['produccion']=='Cancelado'?'background-color:#ff5252;':($track['produccion']=='Completado'?'background-color:#4caf50;':($track['produccion']==''?'':'background-color:#2196f3;')).'color:#ffffff;'}};
    {{$estilo_mont = $track['montaje']=='Cancelado'?'background-color:#ff5252;':($track['montaje']=='Completado'?'background-color:#4caf50;':($track['montaje']==''?'':'background-color:#2196f3;')).'color:#ffffff;'}};
        <tr>
            <td></td>
            <td>{{ $track["codigo"] }}</td>
            <td>{{ $track["nombre"] }}</td>
            <td>{{ $track["nombre_cliente"] }}</td>
            <td>{{ $track["fecha_inicio"] }}</td>
            <td style="{{$track['indicador_montaje']==1?'background-color:#2196f3;color:#ffffff;':''}}">{{ $track["indicador_montaje"]==1?"Si":"No" }}</td>
            <td style="{{$estilo_req}}">{{ $track["requerimiento"] }}</td>
            <td>{{$track["requerimiento_fecha"]}}</td>
            <td style="{{$estilo_pres}}">{{ $track["presupuesto"] }}</td>
            <td>{{$track["presupuesto_fecha"]}}</td>
            <td style="{{$estilo_cot}}" >{{ $track["cotizacion"] }}</td>
            <td>{{$track["cotizacion_fecha"]}}</td>
            <td style="{{$estilo_ped}}">{{ $track["pedido"] }}</td>
            <td>{{$track["pedido_fecha"]}}</td>
            <td style="{{$estilo_dis}}">{{ $track["diseno"] }} </td>
            <td>{{$track["diseno_fecha"]}}</td>
            <td style="{{$estilo_prod}}">{{ $track["produccion"] }} </td>
            <td>{{$track["produccion_fecha"]}}</td>
            <td style="{{$estilo_mont}}">{{ $track["montaje"] }} </td>
            <td>{{$track["montaje_fecha"]}}</td>

        </tr>
    @endforeach
    </tbody>
</table>

</html>
