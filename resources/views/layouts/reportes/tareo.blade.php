<html>
<table>
    <tbody>
        <tr>
            <td><img src="{{public_path('img/Logo_Emer_HD-R2.jpg')}}" width="150" alt=""></td>
        </tr>
        <tr>
            <td></td>
            <td style="font-size:15px"><b>Señores: </b></td>
            <td>{{ $proyecto["descripcion"] }}</td>
        </tr>
        <tr>
            <td></td>
            <td style="font-size:15px"><b>Código: </b></td>
            <td>{{ $proyecto["codigo"] }}</td>
        </tr>
        <tr>
            <td></td>
            <td style="font-size:15px"><b>Proyecto: </b></td>
            <td>{{ $proyecto["nombre"] }}</td>
        </tr>
        <tr>
            <td></td>
            <td style="font-size:15px"><b>Fecha Inicio: </b></td>
            <td>{{ $proyecto["fecha_creacion"] }}</td>
        </tr>
    </tbody>
</table>

<table >
    <thead style="font-size:125px">
    <tr>
        <th></th>
        <th><b>FECHA</b></th>
        <th><b>NOMBRES EMPLEADO</b></th>
        <th><b>APELLIDOS EMPLEADO</b></th>
        <th><b>HORAS TRABAJADAS</b></th>
        <th><b>COSTO DEL TRABAJADOR</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($tareo as $tar)
        <tr>
            <td></td>
            <td>{{ $tar["fecha"] }}</td>
            <td>{{ $tar["nombres"] }}</td>
            <td>{{ $tar["apellidos"] }}</td>
            <td>{{ $tar["horas_trabajadas"] }}</td>
            <td>{{ round($tar["costo"],2) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

</html>
