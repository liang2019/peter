
  <html xmlns='http://www.w3.org/1999/xhtml' xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'>
    <head>
      <meta http-equiv='X-UA-Compatible' content='IE=edge'>
      <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
      <meta name='viewport' content='width=device-width, initial-scale=1'>
      <style type='text/css'>
        #outlook a { padding:0; }
        .ReadMsgBody { width:100%; }
        .ExternalClass { width:100%; }
        .ExternalClass * { line-height:100%; }
        body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
        table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
        img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
        p { display:block;margin:13px 0; }
      </style>
      <style type='text/css'>
        @media only screen and (max-width:480px) {
          @-ms-viewport { width:320px; }
          @viewport { width:320px; }
        }
      </style>
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <style type='text/css'>
          @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
        </style>

      <style type='text/css'>
      @media only screen and (min-width:480px) {
        .mj-column-per-100 { width:100% !important; max-width: 100%; }
      }
      </style>
      <style type='text/css'>
        @media only screen and (max-width:480px) {
          table.full-width-mobile { width: 100% !important; }
          td.full-width-mobile { width: auto !important; }
        }

            </style>
    </head>
    <body style='background-color:#cccccc;'>
      <div style='display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;'>
        Formulario Informativo 
      </div>

      <div style='background-color:#cccccc;'>
      <div class='header' style='background:#0156a2;background-color:#0156a2;Margin:0px auto;max-width:600px;'>
        <table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='background:#0156a2;background-color:#0156a2;width:100%;'>
          <tbody>
            <tr>
              <td style='direction:ltr;font-size:0px;padding:18px 0;text-align:center;vertical-align:top;'>
                <div class='mj-column-per-100 outlook-group-fix' style='font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;'>

                <table border='0' cellpadding='0' cellspacing='0' role='presentation' style='vertical-align:top;' width='100%'>

                      <tr>
                        <td align='center' style='font-size:0px;padding:0;word-break:break-word;'>

                <table border='0' cellpadding='0' cellspacing='0' role='presentation' style='border-collapse:collapse;border-spacing:0px;'>
                  <tbody>
                    <tr>
                      <td style='width:202px;'>

                      <img src="{{public_path('img/Logo_Emer_HD-R2.jpg')}}" width="150" alt="">


                      </td>
            </tr>
          </tbody>
        </table>

            </td>
          </tr>
        </table>
        </div>
            </td>
          </tr>
        </tbody>
      </table>
      </div>
      <div class='body' style='background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;'>

        <table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='background:#ffffff;background-color:#ffffff;width:100%;'>
          <tbody>
            <tr>
              <td style='direction:ltr;font-size:0px;padding:35px 0;text-align:center;vertical-align:top;'>
                <div class='mj-column-per-100 outlook-group-fix' style='font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;'>

                <table border='0' cellpadding='0' cellspacing='0' role='presentation' width='100%'>
                  <tbody>
                    <tr>
                      <td style='vertical-align:top;padding:0;'>

                <table border='0' cellpadding='0' cellspacing='0' role='presentation' style width='100%'>

                      <tr>
                        <td align='left' style='font-size:0px;padding:0 20px;word-break:break-word;'>

                <table cellpadding='0' cellspacing='0' width='100%' border='0' style='cellspacing:0;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;table-layout:auto;width:100%;'>

                              <tr>
                                  <td class='category' colspan='2' style='background-color: #004570; color: #fff; font-weight: bold; padding: 6px 10px; font-size: 16px;' bgcolor='#004570'>EMER S.A.C Información de Tracking</td>
                              </tr>
                              <tr>
                                  <td class='td td_odd td_bold' style='padding: 6px 10px; font-size: 14px; color: #000; font-weight: bold; background-color: #fff;' bgcolor='#fff'>Código:</td>
                                  <td class='td td_odd' style='padding: 6px 10px; font-size: 14px; color: #000; background-color: #fff;' bgcolor='#fff'>
<p> {{ $obj->codigo_proyecto }}</p>
                              </tr>
                            
                              <tr>
                                  <td class='td td_even td_bold' style='padding: 6px 10px; font-size: 14px; color: #000; font-weight: bold; background-color: #e7f0f5;' bgcolor='#e7f0f5'>Proyecto:</td>
                                  <td class='td td_even' style='padding: 6px 10px; font-size: 14px; color: #000; background-color: #e7f0f5;' bgcolor='#e7f0f5'>
<p> {{ $obj->nombre_proyecto }}</p></td>
                              </tr><tr>
                                  <td class='td td_odd td_bold' style='padding: 6px 10px; font-size: 14px; color: #000; font-weight: bold; background-color: #fff;' bgcolor='#fff'>Cliente:</td>
                                  <td class='td td_odd' style='padding: 6px 10px; font-size: 14px; color: #000; background-color: #fff;' bgcolor='#fff'>
<p>{{ $obj->cliente }}</p></td>
                              </tr>
                             <tr>
                                  <td class='td td_odd td_bold' style='padding: 6px 10px; font-size: 14px; color: #000; font-weight: bold; background-color: #fff;' bgcolor='#fff'>Mensaje:</td>
                                  <td class='td td_odd' style='padding: 6px 10px; font-size: 14px; color: #000; background-color: #fff;' bgcolor='#fff'>
<p>{{ $obj->mensaje }}</p></td>
                              </tr>
                              <tr>
                                  <td class='td td_odd td_bold' style='padding: 6px 10px; font-size: 14px; color: #000; font-weight: bold; background-color: #fff;' bgcolor='#fff'>Usuario responsable:</td>
                                  <td class='td td_odd' style='padding: 6px 10px; font-size: 14px; color: #000; background-color: #fff;' bgcolor='#fff'>
<p>{{ $obj->usuario_responsable }}</p></td>
                              </tr>
                </table>
            </td>
          </tr>
          </table>
          </td>
        </tr>
      </tbody>

      </table>
    </div>
    </body>
  </html>