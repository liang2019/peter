const routes = [
    { path: '/dashboard', name:'Dashboard', component: require('./components/WelcomeComponent.vue').default },
    { path: '/importaciones/clientes', name:'Importación clientes', component: require('./importaciones/ClienteComponent.vue').default },
    { path: '/importaciones/materiales', name:'Importación materiales', component: require('./importaciones/MaterialesComponent.vue').default },

    //REPORTES
    { path: '/reportes/materiales', name:'Reporte materiales', component: require('./reportes/ReporteMaterialesComponent.vue').default },
    { path: '/reportes/general/proyectos', name:'Reporte General', component: require('./reportes/ReporteGeneralProyectosComponent.vue').default },
    { path: '/reportes/general/:id', name:'Reporte General', component: require('./reportes/ReporteGeneralComponent.vue').default },

    { path: '/mantenimiento/trabajadores', name:'Mantenimiento Trabajadores', component: require('./mantenimiento/MantenimientoEmpleadosComponent.vue').default },
    { path: '/mantenimiento/factores', name:'Mantenimiento Factores', component: require('./mantenimiento/MantenimientoFactoresComponent.vue').default },
    { path: '/mantenimiento/actividades', name:'Mantenimiento Actividades',component: require('./mantenimiento/MantenimientoActividadesComponent.vue').default },
    { path: '/mantenimiento/unidades', name:'Mantenimiento Unidades',component: require('./mantenimiento/MantenimientoUnidadesComponent.vue').default },
    { path: '/mantenimiento/procesos', name:'Mantenimiento Procesos',component: require('./mantenimiento/MantenimientoProcesosComponent.vue').default },
    { path: '/mantenimiento/tareas', name:'Mantenimiento Tareas',component: require('./mantenimiento/MantenimientoTareasComponent.vue').default },
    { path: '/mantenimiento/adjuntos', name:'Mantenimiento Tipo de Adjuntos',component: require('./mantenimiento/MantenimientoAdjuntosComponent.vue').default },
    { path: '/mantenimiento/usuarios', name:'Mantenimiento Usuarios', component: require('./mantenimiento/MantenimientoUsuariosComponent.vue').default },
    { path: '/mantenimiento/perfiles', name:'Mantenimiento Perfiles',component: require('./mantenimiento/MantenimientoPerfilesComponent.vue').default },
    { path: '/mantenimiento/perfil', name:'Perfil Usuario',component: require('./mantenimiento/MantenimientoPerfilComponent.vue').default },
    { path: '/mantenimiento/correo', name:'Correos',component: require('./mantenimiento/MantenimientoCorreosComponent.vue').default },

    { path: '/rrhh/actividades', name:'RRHH Actividades', component: require('./rrhh/RrhhEventosComponent.vue').default },
    { path: '/rrhh/actividades/empleado', name:'RRHH Actividad - Trabajador', component: require('./rrhh/RrhhEventosEmpleadosComponent.vue').default },

    { path: '/configuracion', name:'Importación Interno', component: require('./configuracion/ConfiguracionComponent.vue').default },

    { path: '/requerimiento/proyectos', name:'Requerimiento > Lista de Proyectos', component: require('./requerimiento/RequerimientoProyectosComponent.vue').default },
    { path: '/requerimiento/documentaria', name:'Requerimiento > Documentaria', component: require('./requerimiento/RequerimientoDocumentariaComponent.vue').default },
    { path: '/requerimiento/documentaria/:id', name:'Requerimiento > Adjuntos', component: require('./requerimiento/RequerimientoAdjuntosComponent.vue').default },

    { path: '/tracking', name:'Tracking', component: require('./tracking/TrackingComponent.vue').default },

    { path: '/presupuesto/proyectos', name:'Presupuesto > Lista de Proyectos', component: require('./presupuesto/PresupuestoRegistraProyectosComponent.vue').default },
    { path: '/presupuesto/items/:id', name:'Presupuesto > Lista de Items', component: require('./presupuesto/PresupuestoRegistraItemsComponent.vue').default },
    { path: '/presupuesto/insumos/:id',props:{etapa:'presupuesto'}, name:'Presupuesto > Lista de Materiales', component: require('./presupuesto/PresupuestoRegistraInsumosComponent.vue').default },
    { path: '/presupuesto/materiales', name:'Presupuesto > Lista de Materiales', component: require('./presupuesto/PresupuestoMaterialesProyectosComponent.vue').default },
    { path: '/presupuesto/materiales/:id', name:'Presupuesto > Lista de Materiales', component: require('./presupuesto/PresupuestoMaterialesListaComponent.vue').default },
    { path: '/presupuesto/solicitud', name:'Presupuesto > Actualización de Precios', component: require('./presupuesto/PresupuestoSolicitudProyectosComponent.vue').default },
    { path: '/presupuesto/solicitud/:id', name:'Presupuesto > Actualización de Precios', component: require('./presupuesto/PresupuestoSolicitudAbastecimientoComponent.vue').default },
    { path: '/presupuesto', name:'Presupuesto', component: require('./presupuesto/PresupuestoProyectosComponent.vue').default },
    { path: '/presupuesto/consolidado/:id', name:'Presupuesto', component: require('./presupuesto/PresupuestoConsolidado.vue').default },
    { path: '/presupuesto/documentaria/:id', name:'Presupuesto > Documentaria', component: require('./presupuesto/PresupuestoAdjuntosComponent.vue').default },
    { path: '/presupuesto/documentaria', name:'Presupuesto > Documentaria', component: require('./presupuesto/PresupuestoDocumentariaComponent.vue').default },
    { path: '/presupuesto/utilidad', name:'Presupuesto > Gastos Generales', component: require('./presupuesto/PresupuestoUtilidadProyectosComponent.vue').default },
    { path: '/presupuesto/utilidad/:id', name:'Presupuesto > Gastos Generales', component: require('./presupuesto/PresupuestoUtilidadComponent.vue').default },

    //COTIZACIÓN
    { path: '/cotizacion/proyectos', name:'Cotización > Lista de Proyectos', component: require('./cotizacion/CotizacionProyectosComponent.vue').default },
    { path: '/cotizacion/proyectos/:id', name:'Cotización ', component: require('./cotizacion/CotizacionConsolidadoComponent.vue').default },
    { path: '/cotizacion/documentaria', name:'Cotización > Documentaria', component: require('./cotizacion/CotizacionDocumentarioComponent.vue').default },
    { path: '/cotizacion/documentaria/:id', name:'Cotización > Documentaria', component: require('./cotizacion/CotizacionAdjuntosComponent.vue').default },

    //PEDIDO
    { path: '/pedido', name:'Pedido > Lista de Proyectos', component: require('./pedido/PedidoProyectosComponent.vue').default },
    { path: '/pedido/documentaria/:id', name:'Pedido > Documentaria', component: require('./pedido/PeiddoAdjuntosComponent.vue').default },

      //DISEÑO
  { path: '/diseno/proyectos', name:'Diseño > Lista de Proyectos', component: require('./diseno/DisenoProyectosComponent.vue').default },
  { path: '/diseno/solid/:id', name:'Diseño > Lista de Solid ', component: require('./diseno/DisenoSolidComponent.vue').default },
  { path: '/diseno/solicitud/:id', name:'Diseño > Solicitud de Abastecimiento ', component: require('./diseno/DisenoSolicitudComponent.vue').default },
  { path: '/diseno/documentaria', name:'Cotización > Documentaria', component: require('./diseno/DisenoDocumentarioComponent.vue').default },
  { path: '/diseno/documentaria/:id', name:'Cotización > Documentaria', component: require('./diseno/DisenoAdjuntosComponent.vue').default },

    //produccion

      //cronograma
    { path: '/produccion/cronograma', name:'Cronograma > Lista de Proyectos', component: require('./produccion/ProduccionCronogramaProyectosComponent.vue').default },
    { path: '/produccion/cronograma/actividades/:id', name:'Cronograma > Lista de Actividades', component: require('./produccion/ProduccionCronogramaActividadesComponent.vue').default },
    { path: '/produccion/cronograma/:id',  props:{etapa:'produccion'},name:'Producción > Cronograma', component: require('./produccion/ProduccionCronogramaCalendarioComponent.vue').default },
    //pesos
    { path: '/produccion/pesos', name:'Pesos > Lista de Proyectos', component: require('./produccion/ProduccionPesosProyectosComponent.vue').default },
    { path: '/produccion/pesos/registrar/:id', props:{etapa:'produccion'},name:'Pesos > Lista de Registros', component: require('./produccion/ProduccionPesosRegistrarComponent.vue').default },
    { path: '/produccion/pesos/:id', name:'Pesos > Gráfico', component: require('./produccion/ProduccionPesosGraficoComponent.vue').default },

    //solicitudes
    { path: '/produccion/solicitudes', name:'Solicitudes > Lista de Proyectos', component: require('./produccion/ProduccionSolicitudesProyectosComponent.vue').default },
    { path: '/produccion/solicitudes/registro/:id', name:'Solicitudes > Lista de Registros', component: require('./produccion/ProduccionSolicitudesRegistrarComponent.vue').default },

    //tareo
    { path: '/produccion/tareo', name:'Tareo > Lista de Proyectos', component: require('./produccion/ProduccioTareoProyectosComponent.vue').default },
    { path: '/produccion/tareo/actividades/:id', name:'Tareo > Lista de Activiades', component: require('./produccion/ProduccionTareoActividadesComponent.vue').default },
    { path: '/produccion/tareo/reporte/:id', name:'Tareo > Reporte', component: require('./produccion/ProduccionTareoReporteComponent.vue').default },
    //documentaria
    { path: '/produccion/documentaria', name:'Producción > Documentaria', component: require('./produccion/ProduccionDocumentariaComponent.vue').default },
    { path: '/produccion/documentaria/:id', name:'Producción > Documentaria', component: require('./produccion/ProduccionAdjuntosComponent.vue').default },
    //entregas solids
    { path: '/produccion/entregas/solid', name:'Producción > Entregas', component: require('./produccion/ProduccionSolidEntregasComponent.vue').default },
    //entregas solicitud
    { path: '/produccion/entregas/solicitud', name:'Producción > Solicitud Entregas', component: require('./produccion/ProduccionSolicitudEntregasComponent.vue').default },
    //restante solids
    { path: '/produccion/restante/solid', name:'Producción > Restante', component: require('./produccion/ProduccionSolidRestanteComponent.vue').default },

    //Montaje
      //cronograma
      { path: '/montaje/cronograma', name:'Cronograma > Lista de Proyectos', component: require('./montaje/MontajeCronogramaProyectosComponent.vue').default },
      { path: '/montaje/cronograma/actividades/:id', name:'Cronograma > Lista de Actividades', component: require('./montaje/MontajeCronogramaActividadesComponent.vue').default },
      { path: '/montaje/cronograma/:id', props:{etapa:'montaje'}, name:'Producción > Cronograma', component: require('./montaje/MontajeCronogramaCalendarioComponent.vue').default },
    //pesos
    { path: '/montaje/pesos', name:'Pesos > Lista de Proyectos', component: require('./montaje/MontajePesosProyectosComponent.vue').default },
    { path: '/montaje/pesos/registrar/:id', props:{etapa:'montaje'}, name:'Pesos > Lista de Registros', component: require('./montaje/MontajePesosRegistrarComponent.vue').default },
    { path: '/montaje/pesos/:id', name:'Pesos > Gráfico', component: require('./montaje/MontajePesosGraficoComponent.vue').default },
     //tareo
    { path: '/montaje/tareo', props:{etapa:'montaje'}, name:'Tareo > Lista de Proyectos', component: require('./montaje/MontajeTareoProyectosComponent.vue').default },
    { path: '/montaje/tareo/actividades/:id',props:{etapa:'montaje'}, name:'Tareo > Lista de Activiades', component: require('./montaje/MontajeTareoActividadesComponent.vue').default },
    { path: '/montaje/tareo/reporte/:id', props:{etapa:'montaje'}, name:'Tareo > Reporte', component: require('./montaje/MontajeTareoReporteComponent.vue').default },

      //documentaria
      { path: '/montaje/documentaria', name:'Montaje > Documentaria', component: require('./montaje/MontajeDocumentariaComponent.vue').default },
      { path: '/montaje/documentaria/:id', name:'Montaje > Documentaria', component: require('./montaje/MontajeAdjuntosComponent.vue').default },
      //kardex
      { path: '/montaje/kardex', name:'Kardex > Lista de Proyectos', component: require('./montaje/MontajeKardexProyectosComponent.vue').default },
      { path: '/montaje/kardex/registro/:id', name:'Kardex > Registro de Entrada', component: require('./montaje/MontajeKardexEntradaComponent.vue').default },

      { path: '/montaje/kardex/reporte/:id', name:'Kardex > Gráfico', component: require('./montaje/MontajeKardexGraficoComponent.vue').default },

      //avance
      { path: '/montaje/avance', name:'Avance > Lista de Proyectos', component: require('./montaje/MontajeAvanceProyectosComponent.vue').default },
      { path: '/montaje/avance/reporte/:id', name:'Avance > Reporte', component: require('./montaje/MontajeAvanceReporteComponent.vue').default },
      { path: '/montaje/avance/mapa/:id', name:'Avance > Reporte', component: require('./montaje/MontajeAvanceMapaComponent.vue').default },


      //documentos
     { path: '/documentos', name:'Documentos', component: require('./documentos/DocumentosComponent.vue').default },
     { path: '/cronograma',props:{etapa:'cronograma'}, name:'Cronograma Proyectos', component: require('./cronograma/CronogramaProyectosCalendarioComponent.vue').default },
      //contabilidad
      { path: '/contabilidad/importar', name:'Contabilidad > Importar', component: require('./contabilidad/ContabilidadImportarComponent.vue').default },
      //servicio
    { path: '/presupuesto/servicio/:id', name:'Presupuesto > Verficiado Servicio', component: require('./presupuesto/PresupuestoSolicitudServicioComponent.vue').default },

    //valorizaciones
    { path: '/valorizacione/proyectos', name:'Valorizacione > Lista de Proyectos', component: require('./valorizaciones/ValorizacioneProyectosComponent.vue').default },

  ];
  export default routes;
